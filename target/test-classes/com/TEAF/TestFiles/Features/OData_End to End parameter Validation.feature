@odata_End_to_End
Feature: Customer and sales order data validations

Description: This Feature file covers validations of sales order details and customter information details between the api response and Elastic search results.

@odata_End_to_End_TC1
Scenario Outline: Validate customer data for Order header between DB and api respsonse with input params as DEL_DOC_NUM, CUST code and Search Tag
	
	Given API EndPoint 'odata_orderheader_uat'
	And Query params with Key "$top" and value '<topValue>'
	And Query params with Key '$filter' and value "<filter>"
	And Authorization BasicAuth '<username>' '<password>'
	When Method 'GET_WITHURLENCODINGDISABLED'
	Then Statuscode '200'
	And Print response
	
	Given ES Search with 'odata_custinfo_uat'
	And Select and Verify the index 'so' exists
	Then Get Result as String by passing Query '<query>'
	And Print ES Query Response
	
	When Retrive data exists in all the fields from API XML response and store in variable for order header
	And Retrive list of data in all the fields from BQ JSON response and store in variable for order header
	Then Verify API response values and DB values are equal for order header
	
	Then Verify MERGED_CUST_CD and ULTIMATE_CUST_CD field values from cross ref index
	
	Examples:
	|topValue|filter|username|password|query|
	|10|DEL_DOC_NUM+eq+'AX-S032987804'|mulesoftuat|MfrmMuleSoftUat|{"query":{"bool":{"must":[{"match":{"DEL_DOC_NUM.keyword":"AX-S032987804"}}],"must_not":[],"should":[]}},"from":0,"size":100,"sort":[],"aggs":{}}|
	|10|CUST_CD+eq+'AX-C001028045'|mulesoftuat|MfrmMuleSoftUat|{"query":{"bool":{"must":[{"match":{"CUST_CD.keyword":"AX-C001028045"}}],"must_not":[],"should":[]}},"from":0,"size":100,"sort":[],"aggs":{}}|
#	|10|SEARCH_TAG+eq+'AX-S032654676+AX-C003918814+Ricky+Turner+20005+Belinda+Lane++512-484-5161++5124845161++rturner@directpropaneservices.com+'|mulesoftuat|MfrmMuleSoftUat|{"query":{"bool":{"must":[{"match":{"SEARCH_TAG.keyword":"AX-S032654676 AX-C003918814 Ricky Turner 20005 Belinda Lane  512-484-5161  5124845161  rturner@directpropaneservices.com "}}],"must_not":[],"should":[]}},"from":0,"size":100,"sort":[],"aggs":{}}|
	
@odata_End_to_End_TC1.1
Scenario Outline: Validate customer data for Order header between DB and api respsonse with input params as ULTIMATE_CUST_CD
	
	Given API EndPoint 'odata_orderheader_uat'
	And Query params with Key "$top" and value '<topValue>'
	And Query params with Key '$filter' and value "<filter>"
	And Authorization BasicAuth '<username>' '<password>'
	When Method 'GET_WITHURLENCODINGDISABLED'
	Then Statuscode '200'
	And Print response
	
	Given ES Search with 'odata_custinfo_uat'
	And Select and Verify the index 'customer_cross_ref' exists
	Then Get Result as String by passing Query '<query>'
	And Print ES Query Response
	When Retrieve list of data '$.hits.hits[*]._source.customerCode' from JSON response and store in variable 'OH_CrossRef_CC'
	And Retrieve list of unique data '$.hits.hits[*]._source.ultimateMasterCustomerCode' from JSON response and store in variable 'OH_CrossRef_UMCC'
	Then Add 'OH_CrossRef_CC' and 'OH_CrossRef_UMCC' into a single list variable 'OH_CC_And_UMCC'
	And Update data in the Request 'OData/OData_OH_UMCC' for key '$.query.bool.must[*].match[*]' with Array data '$$OH_CC_And_UMCC'
	
	Examples:
	|topValue|filter|username|password|query|
#	|10|ULTIMATE_CUST_CD+eq+'AX-C008859869'|mulesoftuat|MfrmMuleSoftUat|{"query":{"bool":{"must":[{"match":{"ultimateMasterCustomerCode.keyword": "AX-C008859869"}}],"must_not":[],"should":[]}},"from":0,"size":100,"sort":[],"aggs":{}}|
	|10|ULTIMATE_CUST_CD+eq+'AX-C016589026'|mulesoftuat|MfrmMuleSoftUat|{"query":{"bool":{"must":[{"match":{"ultimateMasterCustomerCode.keyword": "AX-C016589026"}}],"must_not":[],"should":[]}},"from":0,"size":100,"sort":[],"aggs":{}}|
	

@odata_End_to_End_TC2
Scenario Outline: Validate customer data for Order lines between DB and api respsonse
	Given API EndPoint 'odata_orderlines_uat'
	And Query params with Key "$top" and value '<topValue>'
	And Query params with Key '$filter' and value "<filter>"
	And Authorization BasicAuth '<username>' '<password>'
	When Method 'GET_WITHURLENCODINGDISABLED'
	Then Statuscode '200'
	And Print response
	
	Given ES Search with 'odata_custinfo_uat'
	And Select and Verify the index 'so_ln' exists
	Then Get Result as String by passing Query '<query>'
	And Print ES Query Response
	
	When Retrive data exists in all the fields from API XML response and store in variable for order lines
	And Retrive list of data in all the fields from BQ JSON response and store in variable for order lines
	Then Verify API response values and DB values are equal for order lines
	
	Examples:
	|topValue|filter|username|password|query|
	|10|DEL_DOC_NUM+eq+'AX-S032654676'|mulesoftuat|MfrmMuleSoftUat|{"query":{"bool":{"must":[{"match":{"DEL_DOC_NUM.keyword":"AX-S032654676"}}],"must_not":[],"should":[]}},"from":0,"size":100,"sort":[],"aggs":{}}|
	
@odata_End_to_End_TC3
Scenario Outline: Validate the count of orders displayed in the response should not exceed with the value defined for $top query param
	
	Given API EndPoint 'odata_orderlines_uat'
	And Query params with Key "$top" and value "<topValue>"
	And Query params with Key '$filter' and value "<filter>"
	And Authorization BasicAuth '<username>' '<password>'
	When Method 'GET_WITHURLENCODINGDISABLED'
	Then Statuscode '200'
	And Print response
	When Retrieve data 'feed.entry.content.properties.ITM_CD' from XML response and store in variable 'ITM_CD'
	Then Verify records displayed '$$ITM_CD' should not be more than '<topValue>'
	
	Examples:
	|topValue|filter|username|password|
	|5|DEL_DOC_NUM+eq+'AX-WEB00053911'|mulesoftuat|MfrmMuleSoftUat|
	
@odata_End_to_End_TC4
Scenario Outline: Validate the customer data is not shown if searched with invalid CUST_CD OR DEL_DOC_NUM
	
	Given ES Search with 'odata_custinfo_uat'
	And Select and Verify the index 'so' exists
	Then Get Result as String by passing Query '<query>'
	And Print ES Query Response
	And Verify '$.hits.total' should be zero
	
	Given API EndPoint 'odata_orderheader_uat'
	And Query params with Key "$top" and value "<topValue>"
	And Query params with Key '$filter' and value "<filter>"
	And Authorization BasicAuth '<username>' '<password>'
	When Method 'GET_WITHURLENCODINGDISABLED'
	Then Statuscode '200'
	
	When Retrieve data 'feed.entry.content.properties.CUST_CD' from XML response and store in variable 'API_CUST_CD'
	Then Verify '$$API_CUST_CD' should be zero
	
	Examples:
	|topValue|filter|username|password|query|
	|10|CUST_CD+eq+'AX-C01104833133'|mulesoftuat|MfrmMuleSoftUat|{"query":{"bool":{"must":[{"match":{"CUST_CD.keyword":"AX-C01104833133"}}],"must_not":[],"should":[]}},"from":0,"size":10,"sort":[],"aggs":{}}|
	|10|DEL_DOC_NUM+eq+'AX-WEB0005391111'|mulesoftuat|MfrmMuleSoftUat|{"query":{"bool":{"must":[{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0005391111"}}],"must_not":[],"should":[]}},"from":0,"size":10,"sort":[],"aggs":{}}|
	|10|CUST_CD232+eq+'AX-C01104833133'|mulesoftuat|MfrmMuleSoftUat|{"query":{"bool":{"must":[{"match":{"CUST_CD345.keyword":"AX-C01104833133"}}],"must_not":[],"should":[]}},"from":0,"size":10,"sort":[],"aggs":{}}|
	|10|DEL_DOC_NUM111+eq+'AX-WEB0005391111'|mulesoftuat|MfrmMuleSoftUat|{"query":{"bool":{"must":[{"match":{"DEL_DOC_NUM111.keyword":"AX-WEB0005391111"}}],"must_not":[],"should":[]}},"from":0,"size":10,"sort":[],"aggs":{}}|
	|0|CUST_CD+eq+'AX-C01104833133'|mulesoftuat|MfrmMuleSoftUat|{"query":{"bool":{"must":[{"match":{"CUST_CD.keyword":"AX-C01104833133"}}],"must_not":[],"should":[]}},"from":0,"size":10,"sort":[],"aggs":{}}|
	|0|DEL_DOC_NUM+eq+'AX-WEB0005391111'|mulesoftuat|MfrmMuleSoftUat|{"query":{"bool":{"must":[{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0005391111"}}],"must_not":[],"should":[]}},"from":0,"size":10,"sort":[],"aggs":{}}|
