@D365
Feature: Validation of MFRM DBC signup and Lead contact creation in Sales Force

Description:


Scenario Outline: Lead contact creation through DBC
Given EndPoint 'https://qa-api-gw2.sapi.ms.sandbox.mfrm.com/qa/v2/api/d365EmailFooter/signUp/dbc' 
And Content type 'application/json'
And Header key 'client_id' value '228bdfbdcf1e40d08b7799d395ed52c1'
And Header key 'client_secret' value '96d65d7C239E4Df7841f527e4804dA33'
And Contruct request payload for DBC with params '<unique_id>' '<Email>' '<Guestlastname>' '<LeadSource>' '<AssociateName>' '<StoreID>' '<PreferredContact>' '<Lastname>' '<Guestemail>' '<Firstname>' '<AssociateEmail>' '<DetailsOffer>' '<ShoppingFor>' '<Guestphone>' '<AssociateADID>' '<Guestfirstname>' '<PostalCode>'
When Method 'POST'
And Print response
Then Statuscode '201' 
And I wait for '10' seconds
Given EndPoint 'http://mule-worker-internal-qa-sys-sf.us-w2.cloudhub.io:8091/salesforce/dynamicquery' 
And Content type 'application/json'
And Header key 'client_id' value 'test'
And Header key 'client_secret' value 'test'
And Contruct request payload for salesforce dynamic query DBC
When Method 'POST'
Then Statuscode '200' 
And Print response
And Verify the data is inserted correctly in Salesforce response

Examples:
|unique_id|Email|Guestlastname|LeadSource|AssociateName|StoreID|PreferredContact|Lastname|Guestemail|Firstname|AssociateEmail|DetailsOffer|ShoppingFor|Guestphone|AssociateADID|Guestfirstname|PostalCode|
|63bd23a5-1ea5-4549-97db-09e8ecd47d37|testEmail|Doe|dbc|Jane Doe|001110|email|Doe|testGuestEmail|John|testAssociateEmail|testdetails|Guest|1234567890|123456|Johnny|60008|
|63bd23a5-1ea5-4549-97db-09e8ecd47d38|testEmail|Doe|dbc|Jane Doe|001110|phone|Doe|testGuestEmail|John|testAssociateEmail|testdetails|Guest|1234567890|123456|Johnny|60008|
