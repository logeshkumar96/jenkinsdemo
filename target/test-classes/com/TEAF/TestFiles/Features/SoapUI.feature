@SOAP
Feature: SOAP API Automation Testing POC 

Scenario: OrderServicesInboundCmdImplService Endpoint- Success validation for OB
Given EndPoint 'https://integration.officenational.com.au/OrderIntegration/OrderServicesInboundCmdImplService'
And Content type 'text/xml'
And Request body 'SOAP.xml'
And Method 'Post' 
And Statuscode '200'
And Match XMLPath as '//OrderNumberByNC' contains '11744512'
And Match XMLPath as '//PlacedDate' contains '20200320'
And Match XMLPath as '//PlacedTime' contains '150602'
And Match XMLPath as '//OrgName' contains 'Office National Manstat'

Scenario: OrderServicesInboundCmdImplService Endpoint- Negative validation for OB
Given EndPoint 'https://integration.officenational.com.au/OrderIntegration/OrderServicesInboundCmdImplService'
And Content type 'text/xml'
And Request body 'OrderService_Negative.xml'
And Method 'Post' 
#And Statuscode '200'
And Match XMLPath as '//Success' contains 'false'
And Match XMLPath as '//Message' contains 'Please provide a valid order number'
