@SDP
Feature: SDP End to End flow 

Description: This Feature file covers validations of System API

@SDPScenario3
Scenario Outline: Validating System API with SDP data
	Given EndPoint 'http://mule-worker-internal-sys-es-dev-v1.us-w2.cloudhub.io:8091/elasticsearch/sdp/' with random postbackRefID
	And Header key 'Content-Type' value 'application/json'
	And Update data in the Request 'SDPSystemAPIdata1' for key 'PostBackRefId' with data '$$RandomPostBackrefernceID' 
	And Request body 'SDPSystemAPIdata1'
	When Method 'Post' 
	And Print response
	And Statuscode '200'
	And Match JSONPath "$.result" contains "created"
When Jest Search 'https://vpc-ticketdetail-mfrm-2nsf3ogk2vjyylzbauxlqvrgta.us-west-2.es.amazonaws.com/'
And Select and Verify the index '<Table>' exists
Then Get Result as String by passing Query '<QueryBody>'
And Print ES Query Response

And Verify query result contains Key '<AccountNumberField>' & Value '<AccountNumberDB>'
And Verify query result contains Key '<PermanentExpirationDateField>' & Value '<PermanentExpirationDateDB>'
And Verify query result contains Key '<CVVField>' & Value '<CVVDB>'

And Verify query result contains Key '<ApplicationDecisionField>' & Value '<ApplicationDecision>'
And Verify query result contains Key '<FirstNameField>' & Value '<FirstName>'
And Verify query result contains Key '<MiddleInitialField>' & Value '<MiddleInitial>'
And Verify query result contains Key '<LastNameField>' & Value '<LastName>'
And Verify query result contains Key '<HomeAddressField>' & Value '<HomeAddress>'
And Verify query result contains Key '<HomeAddress2/Apt/SuiteField>' & Value '<HomeAddress2/Apt/Suite>'
And Verify query result contains Key '<CityField>' & Value '<City>'
And Verify query result contains Key '<StateField>' & Value '<State>'
And Verify query result contains Key '<ZipCodeField>' & Value '<ZipCode>'
And Verify query result contains Key '<EmailaddressField>' & Value '<Emailaddress>'
And Verify query result contains Key '<PostbackReferenceIDField>' & Value '$$RandomPostBackrefernceID'
And Verify query result contains Key '<PrimaryPhoneNumberField>' & Value '<PrimaryPhoneNumber>'
And Verify query result contains Key '<SaleAmount/RequestedSaleAmountField>' & Value '<SaleAmount/RequestedSaleAmount>'
And Verify query result contains Key '<SiteCodeField>' & Value '<SiteCode>'
And Verify query result contains Key '<TimestampField>' & Value '<TimestampCST>'
Examples:
|QueryBody                                                                   |Table                   |AccountNumberDB|PermanentExpirationDateDB|CVVDB |ApplicationDecision|AccountNumber   |PermanentExpirationDate|CVV|ApplicationKey|FirstName|MiddleInitial|LastName|HomeAddress    |HomeAddress2/Apt/Suite|City     |State|ZipCode|Emailaddress        |PrimaryPhoneNumber|SaleAmount/RequestedSaleAmount|SiteCode  |Timestamp     |ApplicationDecisionField                |FirstNameField                |MiddleInitialField                |LastNameField                |HomeAddressField                |HomeAddress2/Apt/SuiteField      |CityField                |StateField                |ZipCodeField            |EmailaddressField         |PostbackReferenceIDField          |PrimaryPhoneNumberField         |SaleAmount/RequestedSaleAmountField|SiteCodeField                |TimestampField                |TimestampCST       |AccountNumberField                |PermanentExpirationDateField                     |CVVField                | 
|{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["44556677"]}}]}}}  |synchronyapplication    |null           |null                     |null  |A                  |1234567898765456|2210                   |123|1456789       |Kim      |M            |Jeferry |1 Main Street  |Apt. B                |Stamford |CT   |06831  |joe.approval@syf.com|5555555555        |44.54                         |QWERT67809|20200325153000|hits.hits[0]._source.ApplicationDecision|hits.hits[0]._source.FirstName|hits.hits[0]._source.MiddleInitial|hits.hits[0]._source.LastName|hits.hits[0]._source.HomeAddress|hits.hits[0]._source.HomeAddress2|hits.hits[0]._source.City|hits.hits[0]._source.State|hits.hits[0]._source.Zip|hits.hits[0]._source.Email|hits.hits[0]._source.PostBackRefId|hits.hits[0]._source.PhoneNumber|hits.hits[0]._source.SalesAmount   |hits.hits[0]._source.SiteCode|hits.hits[0]._source.TimeStamp|2020/03/25 15:40:01|hits.hits[0]._source.AccountNumber|hits.hits[0]._source.PermanentExpirationDateField|hits.hits[0]._source.CVV|


