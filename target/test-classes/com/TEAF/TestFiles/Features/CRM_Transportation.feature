@CRM_Transportation
Feature: Sales Force input Payload validation for CRM Transportation

Description: This Feature file covers validations of the input payload sent to Sales Force against the mule job log file

Scenario Outline: Validations of the input payload sent to Sales Force against the mule job log file
	
	Given ES Search with 'Elastic_Search_In_Dev'
	And Select and Verify the index 'so_ln' exists
	Then Get Result as String by passing Query '<so_ln_query>'
#	And Print ES Query Response
	Then Save order numbers, item code, MFIPOSCOSTAMT and quantity from ES query response
#	When ES Search with 'Elastic_Search_In_Dev'
	Then Select and Verify the index 'so' exists
	And Get required data by passing order numbers fetched form previous index
	Then Select and Verify the index 'store' exists
	And Get required data by passing store numbers fetched form previous index
	Then Select and Verify the index 'employee' exists
	And Get required data by passing sales person names fetched form previous index
	And Create API request body with customer code from so index to fetch contactID from SF
	
	Examples:
	|so_ln_query|
#	|{"query":{"bool":{"must":[{"terms":{"ITM_CD.keyword":["AX-105592","AX-105593","AX-105594"]}},{"range":{"ES_MODIFIEDDATETIME":{"gt":"2021/01/10 00:00:00"}}}]}},"_source":{"includes":["DEL_DOC_NUM"]},"size":5000}|
	|{"query":{"bool":{"must":[{"terms":{"ITM_CD.keyword":["AX-105592","AX-105593","AX-105594"]}},{"range":{"ES_MODIFIEDDATETIME":{"gte":"2021/01/20 12:01:00","lte": "2021/01/20 12:05:00"}}}]}},"_source":{"includes":["DEL_DOC_NUM","ES_MODIFIEDDATETIME"]},"size":5000}|
#	|{"query":{"bool":{"must":[{"range":{"ES_MODIFIEDDATETIME":{"gt":"2021/01/01 06:03:00"}}},{"match":{"ITM_CD.keyword":"AX-105593"}}]}},"_source":{"include":["DEL_DOC_NUM","ITM_CD","MFIPOSCOSTAMT","QTY"]},"size":1000}|
#	|{"query":{"bool":{"must":[{"range":{"ES_MODIFIEDDATETIME":{"gt":"2021/01/01 06:03:00"}}},{"match":{"ITM_CD.keyword":"AX-105594"}}]}},"_source":{"include":["DEL_DOC_NUM","ITM_CD","MFIPOSCOSTAMT","QTY"]},"size":1000}|
	
@Ignore
Scenario: Validations of the input payload sent to Sales Force against the mule job log file
	
	Given ES Search with 'Elastic_Search_In_Dev'
	And Select and Verify the index 'so_ln' exists
	Then Get Result as String by passing multiple Queries and fetch required data
	|{"query":{"bool":{"must":[{"range":{"ES_MODIFIEDDATETIME":{"gt":"2021/01/01 06:03:00"}}},{"match":{"ITM_CD.keyword":"AX-105592"}}]}},"_source":{"include":["DEL_DOC_NUM","ITM_CD","MFIPOSCOSTAMT","QTY"]},"size":1000}|
	|{"query":{"bool":{"must":[{"range":{"ES_MODIFIEDDATETIME":{"gt":"2021/01/01 06:03:00"}}},{"match":{"ITM_CD.keyword":"AX-105593"}}]}},"_source":{"include":["DEL_DOC_NUM","ITM_CD","MFIPOSCOSTAMT","QTY"]},"size":1000}|
	|{"query":{"bool":{"must":[{"range":{"ES_MODIFIEDDATETIME":{"gt":"2021/01/01 06:03:00"}}},{"match":{"ITM_CD.keyword":"AX-105594"}}]}},"_source":{"include":["DEL_DOC_NUM","ITM_CD","MFIPOSCOSTAMT","QTY"]},"size":1000}|
#	Then save order numbers, item code, MFIPOSCOSTAMT and quantity from ES query response
	
	