@TealiumProcessAPI&DB_CSV_Validations
Feature: Tealium End to End flow which includes Process and System API's and CSV file creations and CSV cross check validations

Description: This Tealium Feature file covers Process API's step by step and DB query responses and creating individual CSV files
for API and DB and cross check validations between API and DB CSV responses. 

Scenario: Verify data collection from Customer CrossRef index from API and DB responses
	Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/tealium/crossref' 
    And Content type 'application/json' 
    And Request body 'Tealium/CrossRef'
    When Method 'Post' 
    And Print response 
    And Statuscode '200' 
    And Retrieve data '$..customerCode' from response and store in variable 'TLMMasterCodes'
    And ES Search 'https://vpc-qa-ticketdetail-mfrm-65ktt22x2o4nram4vlhrb6kqr4.us-west-2.es.amazonaws.com/'
	And Select and Verify the index 'customer_cross_ref' exists
	Then Get Result as String by passing Query '{"query":{"bool":{"must":[{"match":{"ultimateMasterCustomerCode.keyword":""}},{"range":{"ES_MODIFIEDDATETIME":{"gte": "2020/03/28 22:20:41","lte": "2020/03/28 22:20:41"}}}],"must_not":[ ],"should":[ ]}},"from":0,"size":1000,"sort":[ ],"aggs":{ }}'
	And Print ES Query Response
	When Retrieve Record '$.hits.hits[*]._source.customerCode' from Query Result and store in variable 'TLMUltimateMasterCodes'
	
Scenario: Verify data collection from Customer CrossRef index for master codes from API and DB responses
    Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/customer/relatedContact' 
    And Content type 'application/json' 
    And Update data in the Request 'Tealium/CustomerCode' for key '$.customerCodes' with Array data '$$TLMMasterCodes'
    And Request body 'Tealium/CustomerCode'
    When Method 'Post' 
    And Print response 
    And Statuscode '200'
	And Retrieve data '$.[*].[*]' from response and store in variable 'TLMCustomerCodes'
	And add master codes and customer codes to pass for further queries
	And ES Search 'https://vpc-qa-ticketdetail-mfrm-65ktt22x2o4nram4vlhrb6kqr4.us-west-2.es.amazonaws.com/'
	And Select and Verify the index 'customer_cross_ref' exists
	And Update data in the Request 'TLMCustCrossRef' for key '$.query.bool.must[0].terms["ultimateMasterCustomerCode.keyword"]' with Array data '$$TLMUltimateMasterCodes'
	Then Get Result as String by passing Query '@TLMCustCrossRef'
	And Print ES Query Response
	When Retrieve Record '$.aggregations.group_by_cust_cd.buckets[*].key' from Query Result and store in variable 'TLMDBMasterCodes'
	When Retrieve Record '$.aggregations.group_by_cust_cd.buckets[*].view.hits.hits[*]._source.customerCode' from Query Result and store in variable 'TLMDBCustomerCodes'
	And add master codes and customer codes to pass for further queries for DB

Scenario: Verify data collection from Cloudingo SF backup index from API and DB responses
	Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/customer/salesforceContactId' 
    And Content type 'application/json' 
    And Update data in the Request 'Tealium/CloudingoSFBackup' for key '$.customerCodes' with Array data '$$Allmasterandcustcodes'
    And Request body 'Tealium/CloudingoSFBackup'
    When Method 'Post' 
	And Print response 
    And Statuscode '200'
    And Retrieve data '$.[*]..customerCode' from response and store in variable 'TLMResponseCustomerCodes'
    And create API response arraylist of master codes, customer codes, contactId, masterContactId and type object
    And ES Search 'https://vpc-qa-ticketdetail-mfrm-65ktt22x2o4nram4vlhrb6kqr4.us-west-2.es.amazonaws.com/'
	And Select and Verify the index 'cloudingo_sf_backup' exists
	And Update data in the Request 'TLMCloudingoSFBackup' for key '$.query.bool.must[0].terms["customerCode.keyword"]' with Array data '$$AllDBmasterandcustcodes'
	Then Get Result as String by passing Query '@TLMCloudingoSFBackup'
	And Print ES Query Response
	When Retrieve Record '$.aggregations.group_by_cust_cd.buckets[*].key' from Query Result and store in variable 'TLMDBResponseCustomerCodes'
	And create arraylist of master codes, customer codes, contactId, masterContactId and type object
    
Scenario: Verify data collection from Cust index from API and DB responses and create CSV files and cross check validations for CSV file data's
    Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/customer' 
    And Content type 'application/json'
    And Update data in the Request 'Tealium/CustIndex' for key '$.customerCodes' with Array data '$$Allmasterandcustcodes'
    And Request body 'Tealium/CustIndex'
    When Method 'Post' 
	And Print response 
    And Statuscode '200'
    And create API response arraylist of ZIP_CD, EMAIL_ADDR, HOME_PHONE and BUS_PHONE object
    And create CSV file 'C:\\Users\\User\\Downloads\\PremRC-matressfirm_testautomation-668ce572de29\\src\\test\\java\\com\\Resources\\TealiumCSVAPIfile.csv' using all the datas that are collected
	And ES Search 'https://vpc-qa-ticketdetail-mfrm-65ktt22x2o4nram4vlhrb6kqr4.us-west-2.es.amazonaws.com/'
	And Select and Verify the index 'cust' exists
	And Update data in the Request 'TLMCustIndex' for key '$.query.bool.must[0].terms["CUST_CD.keyword"]' with Array data '$$AllDBmasterandcustcodes'
	Then Get Result as String by passing Query '@TLMCustIndex'
	And Print ES Query Response
	And create arraylist of ZIP_CD, EMAIL_ADDR, HOME_PHONE and BUS_PHONE object
	And create CSV file 'C:\\Users\\User\\Downloads\\PremRC-matressfirm_testautomation-668ce572de29\\src\\test\\java\\com\\Resources\\TealiumCSVDBfile.csv' using all the datas that are collected from DB queries
	And Validate API generated CSV file data with DB generated CSV file data