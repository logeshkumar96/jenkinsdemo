@SDP
Feature: SDP End to End flow 

Description: This Feature file covers validations of SDP Process API 


@SDPScenario2
Scenario Outline: Validating Process API with SDP data
	Given EndPoint 'http://mule-worker-internal-proc-customer-api-dev-v1.us-w2.cloudhub.io:8091/api/svoc/credit-decision'
	And Set the Timestamp to 'UTC' 'yyyyMMddHHmmss' -10
	When Create multiple set of SDP data's with parameterization '<ApplicationDecision>''<AccountNumber>''<PermanentExpirationDate>''<CVV>''<ApplicationKey>''<FirstName>''<MiddleInitial>''<LastName>''<HomeAddress>''<HomeAddress2/Apt/Suite>''<City>''<State>''<ZipCode>''<Emailaddress>''<PostbackReferenceID>''<PrimaryPhoneNumber>''<SaleAmount/RequestedSaleAmount>''<SiteCode>''<Timestamp>'
	When Query params with Key 'SDP' and value '$$SDPValue'
	When Method 'Post' 
	And Print response
	And Statuscode '200'
	And Verify Value 'Request Received' present in field 'message'
	Given ES Search 'https://vpc-ticketdetail-mfrm-2nsf3ogk2vjyylzbauxlqvrgta.us-west-2.es.amazonaws.com/'
And Select and Verify the index '<Table>' exists
Then Get Result as String by passing Query '<QueryBody>'
And Print ES Query Response
And Verify query result contains Key '<AccountNumberField>' & Value '<AccountNumberDB>'
And Verify query result contains Key '<PermanentExpirationDateField>' & Value '<PermanentExpirationDateDB>'
And Verify query result contains Key '<CVVField>' & Value '<CVVDB>'

And Verify query result contains Key '<ApplicationDecisionField>' & Value '<ApplicationDecision>'
And Verify query result contains Key '<FirstNameField>' & Value '<FirstName>'
And Verify query result contains Key '<MiddleInitialField>' & Value '<MiddleInitial>'
And Verify query result contains Key '<LastNameField>' & Value '<LastName>'
And Verify query result contains Key '<HomeAddressField>' & Value '<HomeAddress>'
And Verify query result contains Key '<HomeAddress2/Apt/SuiteField>' & Value '<HomeAddress2/Apt/Suite>'
And Verify query result contains Key '<CityField>' & Value '<City>'
And Verify query result contains Key '<StateField>' & Value '<State>'
And Verify query result contains Key '<ZipCodeField>' & Value '<ZipCode>'
And Verify query result contains Key '<EmailaddressField>' & Value '<Emailaddress>'
And Verify query result contains Key '<PostbackReferenceIDField>' & Value '<PostbackReferenceID>'
And Verify query result contains Key '<PrimaryPhoneNumberField>' & Value '<PrimaryPhoneNumber>'
And Verify query result contains Key '<SaleAmount/RequestedSaleAmountField>' & Value '<SaleAmount/RequestedSaleAmount>'
And Verify query result contains Key '<SiteCodeField>' & Value '<SiteCode>'
And Verify query result contains Key '<TimestampField>' & Value '<TimestampCST>'

Examples:
|QueryBody                                                                  |Table                   |AccountNumberDB|PermanentExpirationDateDB|CVVDB |ApplicationDecision|AccountNumber   |PermanentExpirationDate|CVV|ApplicationKey|FirstName|MiddleInitial|LastName|HomeAddress    |HomeAddress2/Apt/Suite|City     |State|ZipCode|Emailaddress |PostbackReferenceID|PrimaryPhoneNumber|SaleAmount/RequestedSaleAmount|SiteCode  |Timestamp     |ApplicationDecisionField                |FirstNameField                |MiddleInitialField                |LastNameField                |HomeAddressField                |HomeAddress2/Apt/SuiteField      |CityField                |StateField                |ZipCodeField            |EmailaddressField         |PostbackReferenceIDField          |PrimaryPhoneNumberField         |SaleAmount/RequestedSaleAmountField|SiteCodeField                |TimestampField                |TimestampCST       |AccountNumberField                |PermanentExpirationDateField                     |CVVField                | 
|{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678441"]}}]}}}|synchronyapplication    |null           |null                     |null  |A                  |1234567898765456|2210                   |123|987678441     |Andrew   |A            |Jerry   |234 Main Street|APT 3A                |Wayne    |NJ   |11245  |aj@sys.com   |12345A BCDE        |7894635267        |3234.32                       |BCDE 1234 |20200326184500|hits.hits[0]._source.ApplicationDecision|hits.hits[0]._source.FirstName|hits.hits[0]._source.MiddleInitial|hits.hits[0]._source.LastName|hits.hits[0]._source.HomeAddress|hits.hits[0]._source.HomeAddress2|hits.hits[0]._source.City|hits.hits[0]._source.State|hits.hits[0]._source.Zip|hits.hits[0]._source.Email|hits.hits[0]._source.PostBackRefId|hits.hits[0]._source.PhoneNumber|hits.hits[0]._source.SalesAmount   |hits.hits[0]._source.SiteCode|hits.hits[0]._source.TimeStamp|2020/03/25 10:30:00|hits.hits[0]._source.AccountNumber|hits.hits[0]._source.PermanentExpirationDateField|hits.hits[0]._source.CVV|
|{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["458756871"]}}]}}}|synchronyapplication	 |null           |null                     |null  |R                  |0985469806546564|2003                   |000|458756871     |Drew     |S            |Furry   |Main Street    |SDTFO98478UI          |Gotim    |AP   |10004  |kysur@sys.com|ERTYU987574        |1276565645        |4496.49                       |WERT98794 |20200324000000|hits.hits[0]._source.ApplicationDecision|hits.hits[0]._source.FirstName|hits.hits[0]._source.MiddleInitial|hits.hits[0]._source.LastName|hits.hits[0]._source.HomeAddress|hits.hits[0]._source.HomeAddress2|hits.hits[0]._source.City|hits.hits[0]._source.State|hits.hits[0]._source.Zip|hits.hits[0]._source.Email|hits.hits[0]._source.PostBackRefId|hits.hits[0]._source.PhoneNumber|hits.hits[0]._source.SalesAmount   |hits.hits[0]._source.SiteCode|hits.hits[0]._source.TimeStamp|2020/03/25 10:30:00|hits.hits[0]._source.AccountNumber|hits.hits[0]._source.PermanentExpirationDateField|hits.hits[0]._source.CVV|
|{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["000088881"]}}]}}}|synchronyapplication	 |null           |null                     |null  |D                  |0000044449999999|1910                   |987|000088881     |E        |K            |Korry   |454 SecondCross|DPT 4598GK            |Nasur    |KA   |11226  |rajuk@sys.com|1212UIHJ 2D        |5656767877        |1001.01                       |HJUIY9TY78|20200324000000|hits.hits[0]._source.ApplicationDecision|hits.hits[0]._source.FirstName|hits.hits[0]._source.MiddleInitial|hits.hits[0]._source.LastName|hits.hits[0]._source.HomeAddress|hits.hits[0]._source.HomeAddress2|hits.hits[0]._source.City|hits.hits[0]._source.State|hits.hits[0]._source.Zip|hits.hits[0]._source.Email|hits.hits[0]._source.PostBackRefId|hits.hits[0]._source.PhoneNumber|hits.hits[0]._source.SalesAmount   |hits.hits[0]._source.SiteCode|hits.hits[0]._source.TimeStamp|2020/03/25 10:30:00|hits.hits[0]._source.AccountNumber|hits.hits[0]._source.PermanentExpirationDateField|hits.hits[0]._source.CVV|
