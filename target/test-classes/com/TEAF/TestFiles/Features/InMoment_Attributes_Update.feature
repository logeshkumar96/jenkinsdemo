@InMoment_Attributes_Update
Feature: Validation for the data load from InMoment SFTP file to SQL stage and master tables

Description:
1. Download day by day file from InMoment SFTP
2. Load the data present in the downloaded file into SQL InMomentSurveyStg table
3. Merge InMomentSurveyStg table data with InMomentSurveyData table

Scenario: Validation for the data load from InMoment SFTP file to SQL stage and master tables

	#	Dynamically generate date as XD-1: Sysdate-1, XD+1: Sysdate+1, XDC: Current Sysdate
#	Given I generate date 'XD-1' in format 'M-dd-yy' and store as 'InMomentAttUpdateDate'
#	Then Connect to SFTP with host 'ftp.inmoment.com', port '22', user name 'MattressFirm' and password 'M@ttre55F1rm43921' then find 'InMoment Attributes Update' for the date '$$InMomentAttUpdateDate' from directory '/RawData' and download to 'F:\\Manoj Thota\\InMoment attributes update\\SFTP_files\\'
#	Then Connect to MySQL DB with host 'jdbc:sqlserver://qdbsqltab01.mfrm.com:1433', user name 'Mule-QA', password 'test@123' and run the query 'SELECT * FROM [ThirdPartyProj].[dbo].[INM_InMomentSurveyStg]' and download query results to 'F:\\Manoj Thota\\InMoment attributes update\\SQL_files\\SQLInMomentSurveyStg'
#	Then assert SFTP CSV file '$$SFTPDownloadedfilePath' record count with SQL result CSV file '$$SQLResultFilePath' record count
#	And assert end to end data between source SFTP CSV file '$$SFTPDownloadedfilePath' and target SQL stage table result CSV file '$$SQLResultFilePath' for InMoment Attributes Updates
	And assert end to end data for '4' records between source as SQL stage table result CSV file '$$SQLResultFilePath' and target as SQL master table for InMoment Attributes Updates
#	And assert end to end data for all the records between source as SQL stage table result CSV file '$$SQLResultFilePath' and target as SQL master table for InMoment Attributes Updates
	