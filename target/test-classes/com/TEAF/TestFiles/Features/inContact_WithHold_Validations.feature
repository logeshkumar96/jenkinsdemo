@MF_inContact_WithHold_Report
Feature: InContact With Hold - Source InContact Endpoint results and Target is BQ and GCP

1.Call inContact endpoint and download CSV files for WithHold report.
2.Perform below validations for WithHold report by comparing downloaded source file with target MF BQ table and GCS CSV file.
2.1 Duplicates in source CSV file.
2.2 Duplicates in target BQ table and GCS Bucket CSV file.
2.3 Count validation between source and target.
2.4 End to End data validation between source and target.

Description: In this Feature file we will be covering inContact endpoint, GC Bucket and BigQuery validations
Scenario: (WithHold) Connecting to BigQuery and fetch query results and download CSV file using inContact endpoint and then downloading GCP Bucket CSV file and validate the record count and content between BigQuery result and GCS CSV file and source CSV file
	Given Connect to BigQuery with project ID 'qa-mfrm-data'
	#	Dynamically generate date as XD-1: Sysdate-1, XD+1: Sysdate+1, XDC: Current Sysdate
 	Then I generate date 'XD-1' in format 'yyyy-MM-dd' and store as 'startDate'
#	And I generate date 'XD-2' in format 'yyyy-MM-dd' and store as 'DateUsedForFetchingBQresults'
 	Then fetch BigQuery results for Start date '$$startDate' using query "select * from `qa-mfrm-data.mfrm_customer_and_social.incontact_comp_with_hold_call`" for InContact
 	And fetch 'ContactId' records and count from BigQuery results
 	Then I generate date 'XD-1' in format 'MM-dd-yyyy' and store as 'FromDate'
 	Then I generate date 'XDC' in format 'MM-dd-yyyy' and store as 'ToDate'
 	Then capture CSV file for 'WithHold' flow between '$$FromDate' and '$$ToDate' using inContact API 'https://home-c16.incontact.com/ReportService/DataDownloadHandler.ashx?CDST=ElurGcWPf8xdyZtPUsN3LrcadtHb7aNgp%2f%2bipeEVk4lCVgtKu4L9FtLYNSnvEjWOEVIOyqQPxWaCzEWM9OG9cfT%2bPqAcpeAbhUDD6teZ8drMFlRD7ryNISwWK2Cuu6XRB%2bwY7dlJR%2fiPpAlpCjdn3jUrlIgfFFSgKDpBU41rz4%2bX6KvZL6zIB2wXe59oxyleNbW5gQHkdhnPaJaYNqomTLM3kth%2fsgRmV3oskq2xZRnFSyhRQphrAx3m&Format=CSV&IncludeHeaders=True&AppendDate=False' and download the CSV file to the path 'F:\Manoj Thota\InContact\WithHold\WithHold_API_CSV_files'
 	And get the count of records available in inContact csv file '$$inContactAPIResultCSVFilePath'
 	Then assert if duplicate records exist in source inContact API result csv file '$$inContactAPIResultCSVFilePath'
 	And assert if duplicate records exist in target BigQuery results
	And assert all column values for count of '50' rows between BigQuery and CSV file '$$inContactAPIResultCSVFilePath' for WithHold
	#	Below step is to validate all the rows
#	And assert all column values for all rows between BigQuery and CSV file '$$inContactAPIResultCSVFilePath' for WithHold
#	And assert BigQuery record count with CSV file '$$inContactAPIResultCSVFilePath' record count
	#	Initiating GCP Bucket CSV file download and validations
 	Then Connect to GCS Bucket with project ID 'qa-mfrm-data'
 	Then I generate date 'XD-1' in format 'yyyy-MM-dd' and store as 'CSVDate'
 	Then for 'WithHold', open bucket 'qa-mfrm-mulesoft-data-bucket' and Download CSV file for the date '$$CSVDate' to the local path 'F:\\Manoj Thota\\InContact\\WithHold\\WithHold_Bucket_CSV_files'
#	Then assert end to end data between source CSV file '$$inContactAPIResultCSVFilePath' and target CSV file '$$GCSDownloadedCSVfilePath' for WithHold
 	And assert if duplicate records exist in target GCP Bucket result CSV file '$$GCSDownloadedCSVfilePath'
 	And assert inContact CSV file '$$inContactAPIResultCSVFilePath' record count with GCP Bucket CSV file '$$GCSDownloadedCSVfilePath' record count

