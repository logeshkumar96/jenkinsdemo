@odata_Shippo_Tracking_Status
Feature: Validation of API to fetch Shippo tracking status details from ES

Description: This Feature file covers validations on fetching order status details from ES through an API

@odata_ShippoTrackingStatus
Scenario Outline: Validation of an API that Connect to Prod ES and fetch order status from Shippo_Tracking_Status index
	
	Given API EndPoint 'odata_Shippo_Status'
	And Query params with Key "$top" and value '<topValue>'
	And Query params with Key "$filter" and value '<filter>'
	And Authorization BasicAuth '<username>' '<password>'
	When Method 'GET_WITHURLENCODINGDISABLED'
	Then Statuscode '200'
	Then Print response
	Then Retrieve data 'feed.entry.content.properties.address_from_city' from XML response and store in string variable 'API_address_from_city'
	And Retrieve data 'feed.entry.content.properties.address_from_country' from XML response and store in string variable 'API_address_from_country'
	And Retrieve data 'feed.entry.content.properties.address_from_state' from XML response and store in string variable 'API_address_from_state'
	And Retrieve data 'feed.entry.content.properties.address_from_zip' from XML response and store in string variable 'API_address_from_zip'
	And Retrieve data 'feed.entry.content.properties.address_to_city' from XML response and store in string variable 'API_address_to_city'
	And Retrieve data 'feed.entry.content.properties.address_to_country' from XML response and store in string variable 'API_address_to_country'
	And Retrieve data 'feed.entry.content.properties.address_to_state' from XML response and store in string variable 'API_address_to_state'
	And Retrieve data 'feed.entry.content.properties.address_to_zip' from XML response and store in string variable 'API_address_to_zip'
	And Retrieve data 'feed.entry.content.properties.carrier' from XML response and store in string variable 'API_carrier'
	And Retrieve data 'feed.entry.content.properties.createdBy' from XML response and store in string variable 'API_createdBy'
	And Retrieve data 'feed.entry.content.properties.createdDateTime' from XML response and store in string variable 'API_createdDateTime'
	And Retrieve data 'feed.entry.content.properties.ES_MODIFIEDDATETIME' from XML response and store in string variable 'API_ES_MODIFIEDDATETIME'
	And Retrieve data 'feed.entry.content.properties.eta' from XML response and store in string variable 'API_eta'
	And Retrieve data 'feed.entry.content.properties.location_city' from XML response and store in string variable 'API_location_city'
	And Retrieve data 'feed.entry.content.properties.location_country' from XML response and store in string variable 'API_location_country'
	And Retrieve data 'feed.entry.content.properties.location_state' from XML response and store in string variable 'API_location_state'
	And Retrieve data 'feed.entry.content.properties.location_zip' from XML response and store in string variable 'API_location_zip'
	And Retrieve data 'feed.entry.content.properties.modifiedBy' from XML response and store in string variable 'API_modifiedBy'
	And Retrieve data 'feed.entry.content.properties.modifiedDateTime' from XML response and store in string variable 'API_modifiedDateTime'
	And Retrieve data 'feed.entry.content.properties.object_created_date' from XML response and store in string variable 'API_object_created_date'
	And Retrieve data 'feed.entry.content.properties.object_id' from XML response and store in string variable 'API_object_id'
	And Retrieve data 'feed.entry.content.properties.object_updated_date' from XML response and store in string variable 'API_object_updated_date'
	And Retrieve data 'feed.entry.content.properties.order_number' from XML response and store in string variable 'API_order_number'
	And Retrieve data 'feed.entry.content.properties.original_eta' from XML response and store in string variable 'API_original_eta'
	And Retrieve data 'feed.entry.content.properties.service_level_name' from XML response and store in string variable 'API_service_level_name'
	And Retrieve data 'feed.entry.content.properties.service_level_token' from XML response and store in string variable 'API_service_level_token'
	And Retrieve data 'feed.entry.content.properties.status' from XML response and store in string variable 'API_status'
	And Retrieve data 'feed.entry.content.properties.status_date' from XML response and store in string variable 'API_status_date'
	And Retrieve data 'feed.entry.content.properties.status_details' from XML response and store in string variable 'API_status_details'
	And Retrieve data 'feed.entry.content.properties.substatus_action_required' from XML response and store in string variable 'API_substatus_action_required'
	And Retrieve data 'feed.entry.content.properties.substatus_code' from XML response and store in string variable 'API_substatus_code'
	And Retrieve data 'feed.entry.content.properties.substatus_text' from XML response and store in string variable 'API_substatus_text'
	And Retrieve data 'feed.entry.content.properties.tracking_number' from XML response and store in string variable 'API_tracking_number'
	Given ES Search with 'Prod_ES'
	And Select and Verify the index 'orderdelivery' exists
	Then Get Result as String by passing Query '<orderdelivery_query>'
	Then Print ES Query Response
	Then Retrieve Record '$.hits.hits[0]._source.TRACKING_NUM' from Query Result and store in variable 'Odata_Shippo_tracking_Number'
	And Retrieve Record '$.hits.hits[0]._source.DEL_DOC_NUM' from Query Result and store in variable 'Odata_Shippo_DEL_DOC_NUM'
	Given ES Search with 'Prod_ES'
	And Select and Verify the index 'shippo_tracking_status' exists
	Then Get Result as String by passing Query with tracking number fetched from orderdelivery index
	Then Print ES Query Response
	Then Retrieve Record '$.hits.hits[0]._source.address_from_city' from Query Result and store in variable 'ES_address_from_city'
	And Retrieve Record '$.hits.hits[0]._source.address_from_country' from Query Result and store in variable 'ES_address_from_country'
	And Retrieve Record '$.hits.hits[0]._source.address_from_state' from Query Result and store in variable 'ES_address_from_state'
	And Retrieve Record '$.hits.hits[0]._source.address_from_zip' from Query Result and store in variable 'ES_address_from_zip'
	And Retrieve Record '$.hits.hits[0]._source.address_to_city' from Query Result and store in variable 'ES_address_to_city'
	And Retrieve Record '$.hits.hits[0]._source.address_to_country' from Query Result and store in variable 'ES_address_to_country'
	And Retrieve Record '$.hits.hits[0]._source.address_to_state' from Query Result and store in variable 'ES_address_to_state'
	And Retrieve Record '$.hits.hits[0]._source.address_to_zip' from Query Result and store in variable 'ES_address_to_zip'
	And Retrieve Record '$.hits.hits[0]._source.carrier' from Query Result and store in variable 'ES_carrier'
	And Retrieve Record '$.hits.hits[0]._source.createdBy' from Query Result and store in variable 'ES_createdBy'
	And Retrieve Record '$.hits.hits[0]._source.createdDateTime' from Query Result and store in variable 'ES_createdDateTime'
	And Retrieve Record '$.hits.hits[0]._source.ES_MODIFIEDDATETIME' from Query Result and store in variable 'ES_ES_MODIFIEDDATETIME'
	And Retrieve Record '$.hits.hits[0]._source.eta' from Query Result and store in variable 'ES_eta'
	And Retrieve Record '$.hits.hits[0]._source.location_city' from Query Result and store in variable 'ES_location_city'
	And Retrieve Record '$.hits.hits[0]._source.location_country' from Query Result and store in variable 'ES_location_country'
	And Retrieve Record '$.hits.hits[0]._source.location_state' from Query Result and store in variable 'ES_location_state'
	And Retrieve Record '$.hits.hits[0]._source.location_zip' from Query Result and store in variable 'ES_location_zip'
	And Retrieve Record '$.hits.hits[0]._source.modifiedBy' from Query Result and store in variable 'ES_modifiedBy'
	And Retrieve Record '$.hits.hits[0]._source.modifiedDateTime' from Query Result and store in variable 'ES_modifiedDateTime'
	And Retrieve Record '$.hits.hits[0]._source.object_created_date' from Query Result and store in variable 'ES_object_created_date'
	And Retrieve Record '$.hits.hits[0]._source.object_id' from Query Result and store in variable 'ES_object_id'
	And Retrieve Record '$.hits.hits[0]._source.object_updated_date' from Query Result and store in variable 'ES_object_updated_date'
	And Retrieve Record '$.hits.hits[0]._source.original_eta' from Query Result and store in variable 'ES_original_eta'
	And Retrieve Record '$.hits.hits[0]._source.service_level_name' from Query Result and store in variable 'ES_service_level_name'
	And Retrieve Record '$.hits.hits[0]._source.service_level_token' from Query Result and store in variable 'ES_service_level_token'
	And Retrieve Record '$.hits.hits[0]._source.status' from Query Result and store in variable 'ES_status'
	And Retrieve Record '$.hits.hits[0]._source.status_date' from Query Result and store in variable 'ES_status_date'
	And Retrieve Record '$.hits.hits[0]._source.status_details' from Query Result and store in variable 'ES_status_details'
	And Retrieve Record '$.hits.hits[0]._source.substatus_action_required' from Query Result and store in variable 'ES_substatus_action_required'
	And Retrieve Record '$.hits.hits[0]._source.substatus_code' from Query Result and store in variable 'ES_substatus_code'
	And Retrieve Record '$.hits.hits[0]._source.substatus_text' from Query Result and store in variable 'ES_substatus_text'
	And Retrieve Record '$.hits.hits[0]._source.tracking_number' from Query Result and store in variable 'ES_tracking_number'
	Then assert the value present in '$$API_address_from_city' is equal to '$$ES_address_from_city'
	And assert the value present in '$$API_address_from_country' is equal to '$$ES_address_from_country'
	And assert the value present in '$$API_address_from_state' is equal to '$$ES_address_from_state'
	And assert the value present in '$$API_address_from_zip' is equal to '$$ES_address_from_zip'
	And assert the value present in '$$API_address_to_city' is equal to '$$ES_address_to_city'
	And assert the value present in '$$API_address_to_country' is equal to '$$ES_address_to_country'
	And assert the value present in '$$API_address_to_state' is equal to '$$ES_address_to_state'
	And assert the value present in '$$API_address_to_zip' is equal to '$$ES_address_to_zip'
	And assert the value present in '$$API_carrier' is equal to '$$ES_carrier'
	And assert the value present in '$$API_createdBy' is equal to '$$ES_createdBy'
	And assert the value present in '$$API_location_city' is equal to '$$ES_location_city'
	And assert the value present in '$$API_location_country' is equal to '$$ES_location_country'
	And assert the value present in '$$API_location_state' is equal to '$$ES_location_state'
	And assert the value present in '$$API_location_zip' is equal to '$$ES_location_zip'
	And assert the value present in '$$API_modifiedBy' is equal to '$$ES_modifiedBy'
	And assert the value present in '$$API_object_id' is equal to '$$ES_object_id'
	And assert the value present in '$$API_order_number' is equal to '$$Odata_Shippo_DEL_DOC_NUM'
	And assert the value present in '$$API_service_level_token' is equal to '$$ES_service_level_token'
	And assert the value present in '$$API_status' is equal to '$$ES_status'
	And assert the value present in '$$API_status_details' is equal to '$$ES_status_details'
	And assert the value present in '$$API_substatus_action_required' is equal to '$$ES_substatus_action_required'
	And assert the value present in '$$API_substatus_code' is equal to '$$ES_substatus_code'
	And assert the value present in '$$API_substatus_text' is equal to '$$ES_substatus_text'
	And assert the value present in '$$API_tracking_number' is equal to '$$ES_tracking_number'
	And assert the fields that contains date and time by doing transformation
	|createdDateTime	|
	|ES_MODIFIEDDATETIME|
	|eta				|
	|modifiedDateTime	|
	|object_created_date|
	|object_updated_date|
	|original_eta		|
	|service_level_name	|
	|status_date		|
	
	Examples:
	|TC_No|Moto|topValue|filter|username|password|orderdelivery_query|
	|TC_03|Positive case|1001|DEL_DOC_NUM+eq+AX-WEB0003823045|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0003823045"}}}|
	|TC_07|Carrier-FedEx|1001|DEL_DOC_NUM+eq+AX-S034687641|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034687641"}}}|
	|TC_08|Carrier-UPS|1001|DEL_DOC_NUM+eq+AX-WEB0003245734|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0003245734"}}}|
	|TC_09|Carrier-USPS|1001|DEL_DOC_NUM+eq+AX-WEB0003389313|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0003389313"}}}|
	|TC_10|Status-TRANSIT|1001|DEL_DOC_NUM+eq+AX-WEB0003245734|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0003245734"}}}|
	|TC_11|Status-PRE_TRANSIT|1001|DEL_DOC_NUM+eq+AX-WEB0003269438|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0003269438"}}}|
	|TC_12|Status-DELIVERED|1001|DEL_DOC_NUM+eq+AX-S034687641|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034687641"}}}|
	|TC_13|Status-FAILURE|1001|DEL_DOC_NUM+eq+AX-S034205958|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034205958"}}}|
	|TC_14|Status-Returned|1001|DEL_DOC_NUM+eq+AX-WEB0003832067|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0003832067"}}}|
	|TC_15|substatus_action_required-true|1001|DEL_DOC_NUM+eq+AX-S034205958|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034205958"}}}|
	|TC_16|substatus_action_required-false|1001|DEL_DOC_NUM+eq+AX-WEB0003245734|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0003245734"}}}|
	|TC_17|SubStatusCode-return_to_sender|1001|DEL_DOC_NUM+eq+AX-WEB0003832067|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0003832067"}}}|
	|TC_18|SubStatusCode-information_received|1001|DEL_DOC_NUM+eq+AX-S034814709|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034814709"}}}|
	|TC_19|SubStatusCode-delivered|1001|DEL_DOC_NUM+eq+AX-S034286650|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034286650"}}}|
	|TC_20|SubStatusCode-package_departed|1001|DEL_DOC_NUM+eq+AX-S034843837|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034843837"}}}|
	|TC_21|SubStatusCode-package_arrived|1001|DEL_DOC_NUM+eq+AX-S034900883|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034900883"}}}|
	|TC_22|SubStatusCode-delayed|1001|DEL_DOC_NUM+eq+AX-S034873635|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034873635"}}}|
	|TC_23|SubStatusCode-other|1001|DEL_DOC_NUM+eq+AX-S034812435|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034812435"}}}|
	|TC_24|SubStatusCode-out_for_delivery|1001|DEL_DOC_NUM+eq+AX-WEB0003833922|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0003833922"}}}|
	|TC_25|SubStatusCode-package_accepted|1001|DEL_DOC_NUM+eq+AX-S034922746|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034922746"}}}|
	|TC_26|SubStatusCode-delivery_scheduled|1001|DEL_DOC_NUM+eq+AX-S034676655|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034676655"}}}|
	|TC_27|SubStatusCode-package_undeliverable|1001|DEL_DOC_NUM+eq+AX-WEB0003312221|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0003312221"}}}|
	|TC_28|SubStatusCode-package_lost|1001|DEL_DOC_NUM+eq+AX-S034205958|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034205958"}}}|
	|TC_29|SubStatusCode-package_damaged|1001|DEL_DOC_NUM+eq+AX-WEB0003577612|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0003577612"}}}|
	|TC_30|SubStatusCode-package_unclaimed|1001|DEL_DOC_NUM+eq+AX-WEB0003806162|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0003806162"}}}|
	|TC_31|SubStatusCode-delivery_rescheduled|1001|DEL_DOC_NUM+eq+AX-S034733205|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034733205"}}}|
	|TC_32|SubStatusCode-pickup_available|1001|DEL_DOC_NUM+eq+AX-S033055502|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S033055502"}}}|
	|TC_33|SubStatusCode-address_issue|1001|DEL_DOC_NUM+eq+AX-S034809940|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034809940"}}}|
	|TC_34|SubStatusCode-package_processing|1001|DEL_DOC_NUM+eq+AX-S034837953|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S034837953"}}}|
	|TC_35|SubStatusCode-location_inaccessible|1001|DEL_DOC_NUM+eq+AX-S033399357|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-S033399357"}}}|
	|TC_36|SubStatusCode-package_held|1001|DEL_DOC_NUM+eq+AX-WEB0003531199|aa2c036c99cd4936aa145efce965876a|c4f4Ab7875B54e45B238978B679D32e5|{"query":{"match":{"DEL_DOC_NUM.keyword":"AX-WEB0003531199"}}}|
	
