@Order_Delivery_End_to_End
Feature: Validation of direct delivery orders by comparing order delivery API response with ES index

Description:
1. Trigger Order delivery API with direct delivery order number
2. Retrieve order details from the API response
3. Connect to ES index and fetch order details
4. Assert that API is fetching exact data that matches with ES index data

@Order_Delivery_Red_Carpet
Scenario: Validation of Red Carpet orders by comparing order delivery API response with ES index and Dispatch track API response
	
	Given ES Search with 'Elastic_Search_In_UAT'
	And Select and Verify the index 'so_ln' exists
	#	Dynamically generate date as XD-1: Sysdate-1, XD+1: Sysdate+1, XDC: Current Sysdate
	Then I generate date 'XDC' in format 'yyyy/MM/dd' and store as 'OD_SO_LN_filter'
	Then Run ES query to fetch order number by filtering with Dlv date '$$OD_SO_LN_filter' and Dlv mode as 'Red Carpet' and SalesStatus as '1'
	Then Trigger Dispatch track API with order number to check response is not null to procced for further testing
	And Select and Verify the index 'so' exists
	Then fetch zipcode, HPhone, and lastname for the filtered order number
	Then prepare Order delivery API request body, SO index query, so_ln_Direct_Del_query, so_ln_Red_Carpet_query, and DispatchTrackAPI
	
	Given EndPoint 'https://qa-api-gw2.sapi.ms.sandbox.mfrm.com/qa/v1/api/orderDeliveryTracking/deliveryInfo' 
	And Content type 'application/json'
	And Header key 'client_id' value '7e97c775eab64dfd99d52106d3a31466'
	And Header key 'client_secret' value 'AEC0Fdc207e344e4873020a8212cD2a1'
	And Request body '$$Order_delivery_API_request_body'
#	And Request body '@{"order_number":"S033615788","phonenumber":"9174530801"}'
	When Method 'POST'
#	And Print response
	Then Statuscode '200'
	And Retrieve data 'orderDetails.orderDate' from response and store in variable 'orderDetails.orderDate'
	And Retrieve data 'orderDetails.orderNumber' from response and store in variable 'orderDetails.orderNumber'
	And Retrieve data 'orderDetails.orderTotal' from response and store in variable 'orderDetails.orderTotal'
	And Retrieve data 'orderDetails.firstName' from response and store in variable 'orderDetails.firstName'
	And Retrieve data 'orderDetails.lastName' from response and store in variable 'orderDetails.lastName'
	And Retrieve data 'orderDetails.phone' from response and store in variable 'orderDetails.phone'
	And Retrieve data 'orderDetails.tax' from response and store in variable 'orderDetails.tax'
	#Below step will store all the key and values in a HashMap and can be retrieved using array index. Ex: "$$shipments[0].items[0].name"
	And Retrieve data 'shipments[*]' from response with array and store in respective key variables
	And Retrieve data 'shipments[0].trackingInfo.currentStop' from response and store in variable 'shipments[0].trackingInfo.currentStop'
	And Retrieve data 'shipments[0].trackingInfo.finalStop' from response and store in variable 'shipments[0].trackingInfo.finalStop'
	And Retrieve data 'shipments[0].trackingInfo.status' from response and store in variable 'shipments[0].trackingInfo.status'
	And Retrieve data 'shipments[0].trackingInfo.scheduledTimeWindow' from response and store in variable 'shipments[0].trackingInfo.scheduledTimeWindow'
	And Retrieve data 'shipments[0].trackingInfo.expectedTime' from response and store in variable 'shipments[0].trackingInfo.expectedTime'
	And Retrieve data 'shipments[0].trackingInfo.actualDeliveryTime' from response and store in variable 'shipments[0].trackingInfo.actualDeliveryTime'
	And Retrieve data 'shipments[0].trackingInfo.lastStopServed' from response and store in variable 'shipments[0].trackingInfo.lastStopServed'
	And Retrieve data 'shipments[0].trackingInfo.latitude' from response and store in variable 'shipments[0].trackingInfo.latitude'
	And Retrieve data 'shipments[0].trackingInfo.longitude' from response and store in variable 'shipments[0].trackingInfo.longitude'
	And Retrieve data 'shipments[0].trackingInfo.driverDetails[0].description' from response and store in variable 'shipments[0].trackingInfo.driverDetails[0].description'
	And Retrieve data 'shipments[0].trackingInfo.driverDetails[0].name' from response and store in variable 'shipments[0].trackingInfo.driverDetails[0].name'
	And Retrieve data 'shipments[0].trackingInfo.truckDetails.lon' from response and store in variable 'shipments[0].trackingInfo.truckDetails.lon'
	And Retrieve data 'shipments[0].trackingInfo.truckDetails.lat' from response and store in variable 'shipments[0].trackingInfo.truckDetails.lat'
	
	Then Get Result as String by passing Query '$$SO_index_query'
	And Print ES Query Response
	Then Retrieve data '$.hits.hits[*]._source' from 'so' index query result with array and store in respective key variables for 'Red Carpet'
	
	And Select and Verify the index 'so_ln' exists
	Then Get Result as String by passing Query '$$so_ln_query_Red_Carpet'
	Then Retrieve Record '$.hits.total' from Query Result and store in variable 'ES_RedCarpet_total'
	Then Retrieve data '$.hits.hits[*]._source' from 'so_ln' index query result with array and store in respective key variables for 'Red Carpet'
	Then Retrieve all item details from so_ln red carpet Query Result and store in respective variables
	
	Given EndPoint '$$DispatchTrackAPI' 
	And Content type 'application/json'
	And Query params with Key 'format' and value 'json'
	When Method 'GET'
	And Print response
	Then Statuscode '200'
	And Retrieve data '[0].current_stop' from response and store in variable 'DT_current_stop'
	And Retrieve data '[0].stop' from response and store in variable 'DT_final_stop'
	And Retrieve data '[0].status' from response and store in variable 'DT_status'
	And Retrieve data '[0].scheduled_time_window' from response and store in variable 'DT_scheduled_time_window'
	And Retrieve data '[0].expected_time' from response and store in variable 'DT_expected_time'
	And Retrieve data '[0].actual_delivery_time' from response and store in variable 'DT_actual_delivery_time'
	And Retrieve data '[0].last_stop_served' from response and store in variable 'DT_last_stop_served'
	And Retrieve data '[0].lat' from response and store in variable 'DT_lat'
	And Retrieve data '[0].lng' from response and store in variable 'DT_lng'
	And Retrieve data '[0].driver_details[0].description' from response and store in variable 'DT_driver_details_description'
	And Retrieve data '[0].driver_details[0].name' from response and store in variable 'DT_driver_details_name'
	And Retrieve data '[0].service_unit.lon' from response and store in variable 'DT_truckDetails_lon'
	And Retrieve data '[0].service_unit.lat' from response and store in variable 'DT_truckDetails_lat'
	
	Then assert the value present in '$$orderDetails.orderDate' is equal to '$$ES_so_RedCarpet[0].SO_WR_DT'
	And assert the value present in '$$orderDetails.orderNumber' is equal to '$$ES_so_RedCarpet[0].DEL_DOC_NUM_STRIPPED'
#	And assert the value that '$$orderDetails.orderTotal' contains '$$Red_Carpet_shipmentTotal'
	And assert the value present in '$$orderDetails.orderTotal' is equal to '$$Red_Carpet_shipmentTotal'
	And assert the value present in '$$orderDetails.firstName' is equal to '$$ES_so_RedCarpet[0].SHIP_TO_F_NAME'
	And assert the value present in '$$orderDetails.lastName' is equal to '$$ES_so_RedCarpet[0].SHIP_TO_L_NAME'
	And assert the value present in '$$orderDetails.phone' is equal to '$$ES_so_RedCarpet[0].SHIP_TO_H_PHONE'
	And assert the value that '$$orderDetails.tax' contains '$$Red_Carpet_tax'
	
	Then assert the value present in '$$shipments[0].totalItems' is equal to '$$ES_RedCarpet_total'
	And assert the value present in '$$shipments[0].deliveryDate' is equal to '$$ES_so_ln_RedCarpet[0].CONFIRMEDDLV'
	And assert the value present in '$$shipments[0].address' is equal to '$$ES_so_ln_RedCarpet[0].SHIP_TO_STREET'
	And assert the value present in '$$shipments[0].city' is equal to '$$ES_so_ln_RedCarpet[0].SHIP_TO_CITY'
	And assert the value present in '$$shipments[0].state' is equal to '$$ES_so_ln_RedCarpet[0].SHIP_TO_STATE'
	And assert the value present in '$$shipments[0].zip' is equal to '$$ES_so_ln_RedCarpet[0].SHIP_TO_ZIP_CD'
	And assert the value present in '$$shipments[0].tax' is equal to '$$Red_Carpet_tax'
	And assert the value present in '$$shipments[0].deliveryMode' is equal to '$$ES_so_ln_RedCarpet[0].DLVMODE'
	And assert the value present in '$$shipments[0].shipmentTotal' is equal to '$$Red_Carpet_shipmentTotal'
	And assert all the Red Carpet items along with params based on dynamic items count
	
	Then assert the value present in '$$DT_current_stop' is equal to '$$shipments[0].trackingInfo.currentStop'
	And assert the value present in '$$DT_final_stop' is equal to '$$shipments[0].trackingInfo.finalStop'
	And assert the value present in '$$DT_status' is equal to '$$shipments[0].trackingInfo.status'
	And assert the value present in '$$DT_scheduled_time_window' is equal to '$$shipments[0].trackingInfo.scheduledTimeWindow'
	And assert the value present in '$$DT_expected_time' is equal to '$$shipments[0].trackingInfo.expectedTime'
	And assert the value present in '$$DT_actual_delivery_time' is equal to '$$shipments[0].trackingInfo.actualDeliveryTime'
	And assert the value present in '$$DT_last_stop_served' is equal to '$$shipments[0].trackingInfo.lastStopServed'
	And assert the value present in '$$DT_lat' is equal to '$$shipments[0].trackingInfo.latitude'
	And assert the value present in '$$DT_lng' is equal to '$$shipments[0].trackingInfo.longitude'
#	And assert the value present in '$$DT_driver_details_description' is equal to '$$shipments[0].trackingInfo.driverDetails[0].description'
	And assert the value that '$$DT_driver_details_description' contains '$$shipments[0].trackingInfo.driverDetails[0].description'
	And assert the value present in '$$DT_driver_details_name' is equal to '$$shipments[0].trackingInfo.driverDetails[0].name'
	And assert the value present in '$$DT_truckDetails_lon' is equal to '$$shipments[0].trackingInfo.truckDetails.lon'
	And assert the value present in '$$DT_truckDetails_lat' is equal to '$$shipments[0].trackingInfo.truckDetails.lat'
	
@Order_Delivery_Direct_Del
Scenario: Validation of Direct delivery orders by comparing order delivery API response with ES index and Dispatch track API response
	
	Given ES Search with 'Elastic_Search_In_UAT'
	And Select and Verify the index 'so_ln' exists
	#	Dynamically generate date as XD-1: Sysdate-1, XD+1: Sysdate+1, XDC: Current Sysdate
	Then I generate date 'XDC' in format 'yyyy/MM/dd' and store as 'OD_SO_LN_filter'
	Then Run ES query to fetch order number by filtering with Dlv date '$$OD_SO_LN_filter' and Dlv mode as 'Direct Del' and SalesStatus as '1'
	Then Check order delivery index with order number to check response is not null to procced for further testing
#	And Select and Verify the index 'so' exists
#	Then fetch zipcode, HPhone, and lastname for the filtered order number
#	Then prepare Order delivery API request body, SO index query, so_ln_Direct_Del_query, so_ln_Red_Carpet_query, and DispatchTrackAPI
	
#	Given EndPoint 'https://qa-api-gw2.sapi.ms.sandbox.mfrm.com/qa/v1/api/orderDeliveryTracking/deliveryInfo' 
#	And Content type 'application/json'
#	And Header key 'client_id' value '7e97c775eab64dfd99d52106d3a31466'
#	And Header key 'client_secret' value 'AEC0Fdc207e344e4873020a8212cD2a1'
#	And Request body '$$Order_delivery_API_request_body'
#	When Method 'POST'
##	And Print response
#	Then Statuscode '200'
#	And Retrieve data 'orderDetails.orderDate' from response and store in variable 'orderDetails.orderDate'
#	And Retrieve data 'orderDetails.orderNumber' from response and store in variable 'orderDetails.orderNumber'
#	And Retrieve data 'orderDetails.orderTotal' from response and store in variable 'orderDetails.orderTotal'
#	And Retrieve data 'orderDetails.firstName' from response and store in variable 'orderDetails.firstName'
#	And Retrieve data 'orderDetails.lastName' from response and store in variable 'orderDetails.lastName'
#	And Retrieve data 'orderDetails.phone' from response and store in variable 'orderDetails.phone'
#	And Retrieve data 'orderDetails.tax' from response and store in variable 'orderDetails.tax'
#	#Below step will store all the key and values in a HashMap and can be retrieved using array index. Ex: "$$shipments[0].items[0].name"
##	And Retrieve data 'shipments[0].items[*]' from response with array and store in respective key variables
#	And Retrieve data 'shipments[*]' from response with array and store in respective key variables
#	And Retrieve data 'shipments[0].trackingInfo.trackingNumber' from response and store in variable 'shipments[0].trackingInfo.trackingNumber'
#	And Retrieve data 'shipments[0].trackingInfo.eta' from response and store in variable 'shipments[0].trackingInfo.eta'
#	And Retrieve data 'shipments[0].trackingInfo.status' from response and store in variable 'shipments[0].trackingInfo.status'
#	And Retrieve data 'shipments[0].trackingInfo.carrier' from response and store in variable 'shipments[0].trackingInfo.carrier'
#	And Retrieve data 'shipments[0].trackingInfo.deliveryHistory[*]' from response with array and store in respective key variables
#	And Retrieve data 'shipments[1].trackingInfo.currentStop' from response and store in variable 'shipments[1].trackingInfo.currentStop'
#	And Retrieve data 'shipments[1].trackingInfo.finalStop' from response and store in variable 'shipments[1].trackingInfo.finalStop'
#	And Retrieve data 'shipments[1].trackingInfo.status' from response and store in variable 'shipments[1].trackingInfo.status'
#	And Retrieve data 'shipments[1].trackingInfo.scheduledTimeWindow' from response and store in variable 'shipments[1].trackingInfo.scheduledTimeWindow'
#	And Retrieve data 'shipments[1].trackingInfo.expectedTime' from response and store in variable 'shipments[1].trackingInfo.expectedTime'
#	And Retrieve data 'shipments[1].trackingInfo.actualDeliveryTime' from response and store in variable 'shipments[1].trackingInfo.actualDeliveryTime'
#	And Retrieve data 'shipments[1].trackingInfo.lastStopServed' from response and store in variable 'shipments[1].trackingInfo.lastStopServed'
#	And Retrieve data 'shipments[1].trackingInfo.latitude' from response and store in variable 'shipments[1].trackingInfo.latitude'
#	And Retrieve data 'shipments[1].trackingInfo.longitude' from response and store in variable 'shipments[1].trackingInfo.longitude'
#	And Retrieve data 'shipments[1].trackingInfo.driverDetails[0].description' from response and store in variable 'shipments[1].trackingInfo.driverDetails[0].description'
#	And Retrieve data 'shipments[1].trackingInfo.driverDetails[0].name' from response and store in variable 'shipments[1].trackingInfo.driverDetails[0].name'
#	And Retrieve data 'shipments[1].trackingInfo.truckDetails.lon' from response and store in variable 'shipments[1].trackingInfo.truckDetails.lon'
#	And Retrieve data 'shipments[1].trackingInfo.truckDetails.lat' from response and store in variable 'shipments[1].trackingInfo.truckDetails.lat'
#	
##	Given ES Search with 'Elastic_Search_In_UAT'
##	And Select and Verify the index 'so' exists
#	Then Get Result as String by passing Query '$$SO_index_query'
#	And Print ES Query Response
#	Then Retrieve Record '$.hits.hits[0]._source.SO_WR_DT' from Query Result and store in variable 'ES_SO_WR_DT'
#	Then Retrieve Record '$.hits.hits[0]._source.DEL_DOC_NUM_STRIPPED' from Query Result and store in variable 'ES_DEL_DOC_NUM_STRIPPED'
#	Then Retrieve Record '$.hits.hits[0]._source.TOTAL_SALE' from Query Result and store in variable 'ES_TOTAL_SALE'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_F_NAME' from Query Result and store in variable 'ES_SHIP_TO_F_NAME'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_L_NAME' from Query Result and store in variable 'ES_SHIP_TO_L_NAME'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_H_PHONE' from Query Result and store in variable 'ES_SHIP_TO_H_PHONE'
#	Then Retrieve Record '$.hits.hits[0]._source.TAX_CHG' from Query Result and store in variable 'ES_TAX_CHG'
#	And Select and Verify the index 'so_ln' exists
#	Then Get Result as String by passing Query '$$so_ln_Direct_Del_query'
#	Then Retrieve Record '$.hits.total' from Query Result and store in variable 'ES_Direct_Del_total'
#	Then Retrieve Record '$.hits.hits[0]._source.CONFIRMEDDLV' from Query Result and store in variable 'ES_Direct_Del_CONFIRMEDDLV'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_STREET' from Query Result and store in variable 'ES_Direct_Del_SHIP_TO_STREET'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_CITY' from Query Result and store in variable 'ES_Direct_Del_SHIP_TO_CITY'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_STATE' from Query Result and store in variable 'ES_Direct_Del_SHIP_TO_STATE'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_ZIP_CD' from Query Result and store in variable 'ES_Direct_Del_SHIP_TO_ZIP_CD'
#	Then Retrieve Record '$.hits.hits[0]._source.DLVMODE' from Query Result and store in variable 'ES_Direct_Del_DLVMODE'
#	Then Retrieve all item details from so_ln direct delivery Query Result and store in respective variables
#	And Select and Verify the index 'orderdelivery' exists
#	Then Retrieve Tracking number from order delivery index by using order number
#	And Select and Verify the index 'shippo_tracking_status' exists
#	Then Run shippo tracking status query using '$$ES_TRACKING_NUM'
#	Then Retrieve Record '$.hits.hits[0]._source.eta' from Query Result and store in variable 'ES_eta'
#	Then Retrieve Record '$.hits.hits[0]._source.status' from Query Result and store in variable 'ES_status'
#	Then Retrieve Record '$.hits.hits[0]._source.carrier' from Query Result and store in variable 'ES_carrier'
#	And Select and Verify the index 'shippo_tracking_history' exists
#	Then Run shippo tracking history query using '$$ES_TRACKING_NUM'
#	Then Retrieve data '$.hits.hits[*]._source' from Query Result with array and store in respective key variables
#	And Select and Verify the index 'so_ln' exists
#	Then Get Result as String by passing Query '$$so_ln_query_Red_Carpet'
#	Then Retrieve Record '$.hits.total' from Query Result and store in variable 'ES_RedCarpet_total'
#	Then Retrieve Record '$.hits.hits[0]._source.CONFIRMEDDLV' from Query Result and store in variable 'ES_RedCarpet_CONFIRMEDDLV'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_STREET' from Query Result and store in variable 'ES_RedCarpet_SHIP_TO_STREET'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_CITY' from Query Result and store in variable 'ES_RedCarpet_SHIP_TO_CITY'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_STATE' from Query Result and store in variable 'ES_RedCarpet_SHIP_TO_STATE'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_ZIP_CD' from Query Result and store in variable 'ES_RedCarpet_SHIP_TO_ZIP_CD'
#	Then Retrieve Record '$.hits.hits[0]._source.DLVMODE' from Query Result and store in variable 'ES_RedCarpet_DLVMODE'
#	Then Retrieve all item details from so_ln red carpet Query Result and store in respective variables
#	
#	Given EndPoint '$$DispatchTrackAPI' 
#	And Content type 'application/json'
#	And Query params with Key 'format' and value 'json'
#	When Method 'GET'
#	And Print response
#	Then Statuscode '200'
#	And Retrieve data '[0].current_stop' from response and store in variable 'DT_current_stop'
#	And Retrieve data '[0].stop' from response and store in variable 'DT_final_stop'
#	And Retrieve data '[0].status' from response and store in variable 'DT_status'
#	And Retrieve data '[0].scheduled_time_window' from response and store in variable 'DT_scheduled_time_window'
#	And Retrieve data '[0].expected_time' from response and store in variable 'DT_expected_time'
#	And Retrieve data '[0].actual_delivery_time' from response and store in variable 'DT_actual_delivery_time'
#	And Retrieve data '[0].last_stop_served' from response and store in variable 'DT_last_stop_served'
#	And Retrieve data '[0].lat' from response and store in variable 'DT_lat'
#	And Retrieve data '[0].lng' from response and store in variable 'DT_lng'
#	And Retrieve data '[0].driver_details[0].description' from response and store in variable 'DT_driver_details_description'
#	And Retrieve data '[0].driver_details[0].name' from response and store in variable 'DT_driver_details_name'
#	And Retrieve data '[0].service_unit.lon' from response and store in variable 'DT_truckDetails_lon'
#	And Retrieve data '[0].service_unit.lat' from response and store in variable 'DT_truckDetails_lat'

@Order_Delivery_Both_RedCarpet_DirectDel
Scenario: Validation of Red Carpet orders by comparing order delivery API response with ES index and Dispatch track API response
	
	Given ES Search with 'Elastic_Search_In_UAT'
	And Select and Verify the index 'so_ln' exists
	#	Dynamically generate date as XD-1: Sysdate-1, XD+1: Sysdate+1, XDC: Current Sysdate
	Then I generate date 'XD+1' in format 'yyyy/MM/dd' and store as 'OD_SO_LN_filter'
	Then Run ES query to fetch order number by filtering with Dlv date '$$OD_SO_LN_filter' and Dlv mode as 'Red Carpet' and SalesStatus as '1'
#	Then Trigger Dispatch track API with order number to check response is not null to procced for further testing
#	And Select and Verify the index 'so' exists
#	Then fetch zipcode, HPhone, and lastname for the filtered order number
#	Then prepare Order delivery API request body, SO index query, so_ln_Direct_Del_query, so_ln_Red_Carpet_query, and DispatchTrackAPI
#	
#	Given EndPoint 'https://qa-api-gw2.sapi.ms.sandbox.mfrm.com/qa/v1/api/orderDeliveryTracking/deliveryInfo' 
#	And Content type 'application/json'
#	And Header key 'client_id' value '7e97c775eab64dfd99d52106d3a31466'
#	And Header key 'client_secret' value 'AEC0Fdc207e344e4873020a8212cD2a1'
#	And Request body '$$Order_delivery_API_request_body'
#	When Method 'POST'
##	And Print response
#	Then Statuscode '200'
#	And Retrieve data 'orderDetails.orderDate' from response and store in variable 'orderDetails.orderDate'
#	And Retrieve data 'orderDetails.orderNumber' from response and store in variable 'orderDetails.orderNumber'
#	And Retrieve data 'orderDetails.orderTotal' from response and store in variable 'orderDetails.orderTotal'
#	And Retrieve data 'orderDetails.firstName' from response and store in variable 'orderDetails.firstName'
#	And Retrieve data 'orderDetails.lastName' from response and store in variable 'orderDetails.lastName'
#	And Retrieve data 'orderDetails.phone' from response and store in variable 'orderDetails.phone'
#	And Retrieve data 'orderDetails.tax' from response and store in variable 'orderDetails.tax'
#	#Below step will store all the key and values in a HashMap and can be retrieved using array index. Ex: "$$shipments[0].items[0].name"
##	And Retrieve data 'shipments[0].items[*]' from response with array and store in respective key variables
#	And Retrieve data 'shipments[*]' from response with array and store in respective key variables
#	And Retrieve data 'shipments[0].trackingInfo.trackingNumber' from response and store in variable 'shipments[0].trackingInfo.trackingNumber'
#	And Retrieve data 'shipments[0].trackingInfo.eta' from response and store in variable 'shipments[0].trackingInfo.eta'
#	And Retrieve data 'shipments[0].trackingInfo.status' from response and store in variable 'shipments[0].trackingInfo.status'
#	And Retrieve data 'shipments[0].trackingInfo.carrier' from response and store in variable 'shipments[0].trackingInfo.carrier'
#	And Retrieve data 'shipments[0].trackingInfo.deliveryHistory[*]' from response with array and store in respective key variables
#	And Retrieve data 'shipments[1].trackingInfo.currentStop' from response and store in variable 'shipments[1].trackingInfo.currentStop'
#	And Retrieve data 'shipments[1].trackingInfo.finalStop' from response and store in variable 'shipments[1].trackingInfo.finalStop'
#	And Retrieve data 'shipments[1].trackingInfo.status' from response and store in variable 'shipments[1].trackingInfo.status'
#	And Retrieve data 'shipments[1].trackingInfo.scheduledTimeWindow' from response and store in variable 'shipments[1].trackingInfo.scheduledTimeWindow'
#	And Retrieve data 'shipments[1].trackingInfo.expectedTime' from response and store in variable 'shipments[1].trackingInfo.expectedTime'
#	And Retrieve data 'shipments[1].trackingInfo.actualDeliveryTime' from response and store in variable 'shipments[1].trackingInfo.actualDeliveryTime'
#	And Retrieve data 'shipments[1].trackingInfo.lastStopServed' from response and store in variable 'shipments[1].trackingInfo.lastStopServed'
#	And Retrieve data 'shipments[1].trackingInfo.latitude' from response and store in variable 'shipments[1].trackingInfo.latitude'
#	And Retrieve data 'shipments[1].trackingInfo.longitude' from response and store in variable 'shipments[1].trackingInfo.longitude'
#	And Retrieve data 'shipments[1].trackingInfo.driverDetails[0].description' from response and store in variable 'shipments[1].trackingInfo.driverDetails[0].description'
#	And Retrieve data 'shipments[1].trackingInfo.driverDetails[0].name' from response and store in variable 'shipments[1].trackingInfo.driverDetails[0].name'
#	And Retrieve data 'shipments[1].trackingInfo.truckDetails.lon' from response and store in variable 'shipments[1].trackingInfo.truckDetails.lon'
#	And Retrieve data 'shipments[1].trackingInfo.truckDetails.lat' from response and store in variable 'shipments[1].trackingInfo.truckDetails.lat'
#	
##	Given ES Search with 'Elastic_Search_In_UAT'
##	And Select and Verify the index 'so' exists
#	Then Get Result as String by passing Query '$$SO_index_query'
#	And Print ES Query Response
#	Then Retrieve Record '$.hits.hits[0]._source.SO_WR_DT' from Query Result and store in variable 'ES_SO_WR_DT'
#	Then Retrieve Record '$.hits.hits[0]._source.DEL_DOC_NUM_STRIPPED' from Query Result and store in variable 'ES_DEL_DOC_NUM_STRIPPED'
#	Then Retrieve Record '$.hits.hits[0]._source.TOTAL_SALE' from Query Result and store in variable 'ES_TOTAL_SALE'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_F_NAME' from Query Result and store in variable 'ES_SHIP_TO_F_NAME'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_L_NAME' from Query Result and store in variable 'ES_SHIP_TO_L_NAME'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_H_PHONE' from Query Result and store in variable 'ES_SHIP_TO_H_PHONE'
#	Then Retrieve Record '$.hits.hits[0]._source.TAX_CHG' from Query Result and store in variable 'ES_TAX_CHG'
#	And Select and Verify the index 'so_ln' exists
#	Then Get Result as String by passing Query '$$so_ln_Direct_Del_query'
#	Then Retrieve Record '$.hits.total' from Query Result and store in variable 'ES_Direct_Del_total'
#	Then Retrieve Record '$.hits.hits[0]._source.CONFIRMEDDLV' from Query Result and store in variable 'ES_Direct_Del_CONFIRMEDDLV'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_STREET' from Query Result and store in variable 'ES_Direct_Del_SHIP_TO_STREET'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_CITY' from Query Result and store in variable 'ES_Direct_Del_SHIP_TO_CITY'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_STATE' from Query Result and store in variable 'ES_Direct_Del_SHIP_TO_STATE'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_ZIP_CD' from Query Result and store in variable 'ES_Direct_Del_SHIP_TO_ZIP_CD'
#	Then Retrieve Record '$.hits.hits[0]._source.DLVMODE' from Query Result and store in variable 'ES_Direct_Del_DLVMODE'
#	Then Retrieve all item details from so_ln direct delivery Query Result and store in respective variables
#	And Select and Verify the index 'orderdelivery' exists
#	Then Retrieve Tracking number from order delivery index by using order number
#	And Select and Verify the index 'shippo_tracking_status' exists
#	Then Run shippo tracking status query using '$$ES_TRACKING_NUM'
#	Then Retrieve Record '$.hits.hits[0]._source.eta' from Query Result and store in variable 'ES_eta'
#	Then Retrieve Record '$.hits.hits[0]._source.status' from Query Result and store in variable 'ES_status'
#	Then Retrieve Record '$.hits.hits[0]._source.carrier' from Query Result and store in variable 'ES_carrier'
#	And Select and Verify the index 'shippo_tracking_history' exists
#	Then Run shippo tracking history query using '$$ES_TRACKING_NUM'
#	Then Retrieve data '$.hits.hits[*]._source' from Query Result with array and store in respective key variables
#	And Select and Verify the index 'so_ln' exists
#	Then Get Result as String by passing Query '$$so_ln_query_Red_Carpet'
#	Then Retrieve Record '$.hits.total' from Query Result and store in variable 'ES_RedCarpet_total'
#	Then Retrieve Record '$.hits.hits[0]._source.CONFIRMEDDLV' from Query Result and store in variable 'ES_RedCarpet_CONFIRMEDDLV'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_STREET' from Query Result and store in variable 'ES_RedCarpet_SHIP_TO_STREET'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_CITY' from Query Result and store in variable 'ES_RedCarpet_SHIP_TO_CITY'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_STATE' from Query Result and store in variable 'ES_RedCarpet_SHIP_TO_STATE'
#	Then Retrieve Record '$.hits.hits[0]._source.SHIP_TO_ZIP_CD' from Query Result and store in variable 'ES_RedCarpet_SHIP_TO_ZIP_CD'
#	Then Retrieve Record '$.hits.hits[0]._source.DLVMODE' from Query Result and store in variable 'ES_RedCarpet_DLVMODE'
#	Then Retrieve all item details from so_ln red carpet Query Result and store in respective variables
#	
#	Given EndPoint '$$DispatchTrackAPI' 
#	And Content type 'application/json'
#	And Query params with Key 'format' and value 'json'
#	When Method 'GET'
#	And Print response
#	Then Statuscode '200'
#	And Retrieve data '[0].current_stop' from response and store in variable 'DT_current_stop'
#	And Retrieve data '[0].stop' from response and store in variable 'DT_final_stop'
#	And Retrieve data '[0].status' from response and store in variable 'DT_status'
#	And Retrieve data '[0].scheduled_time_window' from response and store in variable 'DT_scheduled_time_window'
#	And Retrieve data '[0].expected_time' from response and store in variable 'DT_expected_time'
#	And Retrieve data '[0].actual_delivery_time' from response and store in variable 'DT_actual_delivery_time'
#	And Retrieve data '[0].last_stop_served' from response and store in variable 'DT_last_stop_served'
#	And Retrieve data '[0].lat' from response and store in variable 'DT_lat'
#	And Retrieve data '[0].lng' from response and store in variable 'DT_lng'
#	And Retrieve data '[0].driver_details[0].description' from response and store in variable 'DT_driver_details_description'
#	And Retrieve data '[0].driver_details[0].name' from response and store in variable 'DT_driver_details_name'
#	And Retrieve data '[0].service_unit.lon' from response and store in variable 'DT_truckDetails_lon'
#	And Retrieve data '[0].service_unit.lat' from response and store in variable 'DT_truckDetails_lat'

