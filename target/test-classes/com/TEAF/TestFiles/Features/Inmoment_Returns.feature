@MF_Inmoment_Returns
Feature: InMoment Returns - Source SFTP and Target is BQ and GCP

1.Connect to SFTP and download Inmoment Returns CSV file
2.Connect to GCP bucket and download CSV file
3.Validations between SFTP CSV file and GCP bucket CSV file
4.Validations between SFTP CSV file records and BQ inmoment_return_survey table records

Description: In this Feature file we will be covering Inmoment Returns SFTP file download and cross check validation with BQ table and GCP CSV file
Scenario: (InMoment Returns) Connect to SFTP and download Inmoment Returns CSV file and connect to GCP bucket and download csv file and then connect to BQ and fetch query results and validate records between SFTP, BQ and GCP
	#	Dynamically generate date as XD-1: Sysdate-1, XD+1: Sysdate+1, XDC: Current Sysdate
	Given I generate date 'XD-9' in format 'M-d-yy' and store as 'InmomentSFTPReportDate'
	Then Connect to SFTP with host 'ftp.inmoment.com', port '22', user name 'MattressFirm' and password 'M@ttre55F1rm43921' then find 'Inmoment Returns Report' for the date '$$InmomentSFTPReportDate' from directory '/RawData' and download to 'F:\\Manoj Thota\\InMoment\\New_files\\SFTP_files\\'
	Then Connect to GCS Bucket with project ID 'qa-mfrm-data'
	Then I generate date 'XD-9' in format 'M-d-yy' and store as 'InmomentGCPReportDate'
	Then for 'Inmoment Returns Report', open bucket 'qa-mfrm-mulesoft-data-bucket' and Download CSV file for the date '$$InmomentGCPReportDate' to the local path 'F:\\Manoj Thota\\InMoment\\New_files\\GCP_files'
	Then assert SFTP CSV file '$$SFTPDownloadedfilePath' record count with GCP Bucket CSV file '$$GCSDownloadedCSVfilePath' record count
	And assert end to end data between source CSV file '$$SFTPDownloadedfilePath' and target CSV file '$$GCSDownloadedCSVfilePath' for Inmoment Returns
	Then Connect to BigQuery with project ID 'qa-mfrm-data'
#	And assert all column values for count of '4' rows between BigQuery and SFTP CSV file '$$SFTPDownloadedfilePath' for Inmoment Returns
	And assert all column values for all rows between BigQuery and SFTP CSV file '$$SFTPDownloadedfilePath' for Inmoment Returns
	