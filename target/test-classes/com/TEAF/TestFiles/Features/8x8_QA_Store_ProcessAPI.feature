@8x8QA
Feature: 8x8 flow Store call using Process API in QA environment

Description: This feature File covers Process API for End to End Scenario's between the Store Call which covers all possible combinations & response Validations of StoreId & StoreName

@StoreCall1
Scenario Outline: Verify the Store Id , Store Name in response body of Process API Call  when Caller and Reciever is store
	Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/phoneNumber' 
	When Query params with Key 'caller' and value '<callerValue>' 
	When Query params with Key 'receiver' and value '<receiverValue>' 
	When Query params with Key 'testing' and value 'true' 
	When Method 'Get' 
	And Print response 
	And Statuscode '<StatusCode>' 
	And Verify JSONPath "$.STORE.STORE_ID" contains the Prefix "AX-"
	And Verify Value '<StoreId>' present in field 'STORE.STORE_ID'
	And Verify Value '<StoreName>' present in field 'STORE.STORE_NAME'
	And Verify Value '<Recipient>' present in field 'RECIPIENT'
	
Examples:
|callerValue|receiverValue|StatusCode|StoreId  |StoreName |Recipient|
|12818567088|12064641513  |200       |AX-001019|Copperwood|508044   |
|19795788846|12064641513  |200       |AX-001027|El Campo  |508044   |
|12815997908|12064641513  |200       |AX-001023|Fry Road  |508044   |
|12819989085|12064641513  |200       |AX-001025|Pasadena  |508044   |


@InvalidData1
Scenario Outline: Validation for Store Call with multiple Combination of Caller and Reciever Data's using Process API
	Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/phoneNumber' 
	When Query params with Key 'caller' and value '<callerValue>' 
	When Query params with Key 'receiver' and value '<receiverValue>' 
	When Query params with Key 'testing' and value 'true' 
	When Method 'Get' 
	And Print response 
	And Statuscode '<StatusCode>'
	And Verify response matches expected text '<Message>'

Examples:
|callerValue |receiverValue|StatusCode|Message               |
|            |12064641513  |200       |"No Data for Caller"  |
|12818567088 |             |200       |"No Data for Receiver"|
|111222333   |12064641513  |200       |"No Data for Caller"  |
|12818567088 |11112222     |200       |"No Data for Receiver"|
|14173316    |11112222     |200       |"No Data for Caller"  |
|401010      |401019       |200       |"No Data for Caller"  |
	


Scenario: Validation for Store Call by without sending the caller input parameter using Process API
	Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/phoneNumber' 
	#When Query params with Key 'caller' and value '12818567088'
	When Query params with Key 'receiver' and value '12064641513' 
	When Query params with Key 'testing' and value 'true' 
	When Method 'Get' 
	And Print response 
	And Statuscode '400' 
	And Verify Value 'Bad request' present in field 'message'
#	And Match JSONPath "message" contains "Bad request" 
	

Scenario: Validation for Store Call by without sending  the receiver input parameter using Process API
	Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/phoneNumber' 
	Given Query Param name 'caller' value '12818567088' 
	#Given Query Param name 'receiver' value '12064641513'
	Given Query Param name 'testing' value 'true' 
	When Method 'Get' 
	And Print response 
	And Statuscode '400' 
	And Verify Value 'Bad request' present in field 'message'
#	And Match JSONPath "message" contains "Bad request" 
	
	
Scenario Outline: Validation for Store Call by Sending the Customer "caller" no using Process API	
Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/phoneNumber' 
	When Query params with Key 'caller' and value '<callerValue>' 
	When Query params with Key 'receiver' and value '<receiverValue>' 
	When Query params with Key 'testing' and value 'true' 
	When Method 'Get' 
	And Print response 
	And Retrieve data '$.CUST.OPEN_ORDERS_COUNT' from response and store in variable 'OpenOrderCount'
	And Retrieve data '$.CUST.CLOSED_ORDERS_COUNT' from response and store in variable 'ClosedOrderCount'
	And Statuscode '<StatusCode>' 
	And get the Key value of '<OrderTotal1>' & '<OrderTotal2>' and verify with the total '$.CUST.TOTAL_SALE' 
	And Verify key '$.CLOSED_ORDERS.ORDER_NUMBER' count present in the response orders- '$$ClosedOrderCount' 
	And Verify key '$.OPEN_ORDERS[*].ORDER_NUMBER' count present in the response orders- '$$OpenOrderCount' 
And Key '$.CLOSED_ORDERS.ORDER_NUMBER' List should not contain more than '5' set of records
	And Key '$.OPEN_ORDERS[*].ORDER_NUMBER' List should not contain more than '5' set of records
	And Verify Value '<CustomerName>' present in field 'CUST.CUST_NAME' 
	And Verify Value '<CustomerTotalSale>' present in field 'CUST.TOTAL_SALE' 
	And Verify Value '<CustomerPhoneNumber>' present in field 'CUST.PHONE_NUMBER' 
	And Verify Value '<OpenOrderTotal>' present in field 'CUST.OPEN_ORDERS_TOTAL' 
	And Verify Value '<OpenOrderCount>' present in field as String 'CUST.OPEN_ORDERS_COUNT' 
	And Verify Value '<ClosedOrderCount>' present in field as String 'CUST.CLOSED_ORDERS_COUNT'
	And Verify Value '<ClosedOrderTotal>' present in field 'CUST.CLOSED_ORDERS_TOTAL' 
	And Verify Value '<OpenOrderNumber>' present in field '<OpenOrderNumberField>' 
	And Verify Value '<OpenOrderSalesPersonName1>' present in field '<OpenOrderSalesPersonName1Field>' 
	And Verify Value '<OpenOrderSalesPersonName2>' present in field '<OpenOrderSalesPersonName2Field>' 
	And Verify Value '<OpenOrderStoreCD>' present in field '<OpenOrderStoreCDField>' 
	And Verify Value '<OpenOrderCreatedDate>' present in field '<OpenOrderCreatedDateField>' 
	And Verify Value '<OpenOrderDeliveryDate>' present in field '<OpenOrderDeliveryDateField>' 
	
	
	And Verify Value '<OpenOrderNumber2>' present in field '<OpenOrderNumberField2>' 
	And Verify Value '<OpenOrderSalesPersonName1.2>' present in field '<OpenOrderSalesPersonName1Field2>' 
	And Verify Value '<OpenOrderSalesPersonName2.2>' present in field '<OpenOrderSalesPersonName2Field2>' 
	And Verify Value '<OpenOrderStoreCD2>' present in field '<OpenOrderStoreCDField2>' 
	And Verify Value '<OpenOrderCreatedDate2>' present in field '<OpenOrderCreatedDateField2>' 
	And Verify Value '<OpenOrderDeliveryDate2>' present in field '<OpenOrderDeliveryDateField2>' 
	
	
	And Verify Value '<OpenOrderNumber3>' present in field '<OpenOrderNumberField3>' 
	And Verify Value '<OpenOrderSalesPersonName1.3>' present in field '<OpenOrderSalesPersonName1Field3>' 
	And Verify Value '<OpenOrderSalesPersonName2.3>' present in field '<OpenOrderSalesPersonName2Field3>' 
	And Verify Value '<OpenOrderStoreCD3>' present in field '<OpenOrderStoreCDField3>' 
	And Verify Value '<OpenOrderCreatedDate3>' present in field '<OpenOrderCreatedDateField3>' 
	And Verify Value '<OpenOrderDeliveryDate3>' present in field '<OpenOrderDeliveryDateField3>' 
	
	
	And Verify Value '<OpenOrderNumber4>' present in field '<OpenOrderNumberField4>' 
	And Verify Value '<OpenOrderSalesPersonName1.4>' present in field '<OpenOrderSalesPersonName1Field4>' 
	And Verify Value '<OpenOrderSalesPersonName2.4>' present in field '<OpenOrderSalesPersonName2Field4>' 
	And Verify Value '<OpenOrderStoreCD4>' present in field '<OpenOrderStoreCDField4>' 
	And Verify Value '<OpenOrderCreatedDate4>' present in field '<OpenOrderCreatedDateField4>' 
	And Verify Value '<OpenOrderDeliveryDate4>' present in field '<OpenOrderDeliveryDateField4>' 
	
	And Verify Value '<ClosedOrderNumber1>' present in field '<ClosedOrderNumberField1>' 
	And Verify Value '<ClosedOrderNumber2>' present in field '<ClosedOrderNumberField2>' 
	And Verify Value '<ClosedOrderNumber3>' present in field '<ClosedOrderNumberField3>' 

Examples:	
|callerValue|receiverValue|StatusCode|CustomerName            |CustomerTotalSale|CustomerPhoneNumber|OpenOrderTotal|OpenOrderCount|ClosedOrderCount|ClosedOrderTotal|OpenOrderNumber|OpenOrderNumberField       |OpenOrderSalesPersonName1|OpenOrderSalesPersonName1Field   |OpenOrderSalesPersonName2|OpenOrderSalesPersonName2Field   |OpenOrderStoreCD|OpenOrderStoreCDField     |OpenOrderCreatedDate|OpenOrderCreatedDateField  |OpenOrderDeliveryDate|OpenOrderDeliveryDateField  |OpenOrderNumber2|OpenOrderNumberField2       |OpenOrderSalesPersonName1.2|OpenOrderSalesPersonName1Field2   |OpenOrderSalesPersonName2.2  |OpenOrderSalesPersonName2Field2   |OpenOrderStoreCD2|OpenOrderStoreCDField2     |OpenOrderCreatedDate2|OpenOrderCreatedDateField2  |OpenOrderDeliveryDate2|OpenOrderDeliveryDateField2  |OpenOrderNumber3|OpenOrderNumberField3       |OpenOrderSalesPersonName1.3|OpenOrderSalesPersonName1Field3   |OpenOrderSalesPersonName2.3|OpenOrderSalesPersonName2Field3   |OpenOrderStoreCD3|OpenOrderStoreCDField3     |OpenOrderCreatedDate3|OpenOrderCreatedDateField3  |OpenOrderDeliveryDate3|OpenOrderDeliveryDateField3  |OpenOrderNumber4  |OpenOrderNumberField4       |OpenOrderSalesPersonName1.4|OpenOrderSalesPersonName1Field4   |OpenOrderSalesPersonName2.4|OpenOrderSalesPersonName2Field4   |OpenOrderStoreCD4|OpenOrderStoreCDField4     |OpenOrderCreatedDate4|OpenOrderCreatedDateField4  |OpenOrderDeliveryDate4|OpenOrderDeliveryDateField4  |ClosedOrderNumber1|ClosedOrderNumber2|ClosedOrderNumber3|ClosedOrderNumberField1      |ClosedOrderNumberField2      |ClosedOrderNumberField3      |OrderTotal1               |OrderTotal2               |
|12254585758|12064641513  |200       |JOHN DOE                |560.38           |+1 (225) 458-5758  |560.38        |1             |0               |0               |AX-WEB00091404 |OPEN_ORDERS[0].ORDER_NUMBER|                         |OPEN_ORDERS[0].SALES_PERSON_NAME1|                         |OPEN_ORDERS[0].SALES_PERSON_NAME2|290007          |OPEN_ORDERS[0].SO_STORE_CD|03/23/2020          |OPEN_ORDERS[0].CREATED_DATE|12/31/2049           |OPEN_ORDERS[0].DELIVERY_DATE|                |                            |                           |                                  |                             |                                  |                 |                           |                     |                            |                      |                             |                |                            |                           |                                  |                           |                                  |                 |                           |                     |                            |                      |                             |                  |                            |                           |                                  |                           |                                  |                 |                           |                     |                            |                      |                             |                  |                  |                  |                             |                             |                             |$.CUST.OPEN_ORDERS_TOTAL  |$.CUST.CLOSED_ORDERS_TOTAL|
