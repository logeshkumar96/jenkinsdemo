@MF_ContactMergeE2E
Feature: ContactMerge End to End Flow 

Description: In this Feature We will be covering Email system API , SQL Server system API , Elastic Search API , Timestamp , Fetch Service run

Scenario: Contact Merge email Configuration that allows to setup Application Name ,Flow Name ,jobRunEmailDistroFlag ,Email Subject , Email message body content
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-dev-v1.us-w2.cloudhub.io:8091/database/emailconfig' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ContactMerge/ContactMergeEmailConfig' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Retrieve data '$..FROM_ADDRESS' from object index '0' in response and store in variable 'FROM_ADDRESS'
	And Verify Value 'CustomerPortal' present in field '[0].INTEGRATION_NAME'
	And Verify Value 'StoreUpsert' present in field '[0].APPLICATION_NAME'
	

Scenario: MF Contact Merge Flow email distribution list when the job runs successfully
	Given EndPoint 'http://mule-worker-internal-sys-email-dev-v1.us-w2.cloudhub.io:8091/mail/send-email-notification' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'ContactMerge/ContactMergeEmailNotification' for key 'FROM_ADDRESS' with data '$$FROM_ADDRESS' 
	And Request body 'ContactMerge/ContactMergeEmailNotification' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Email sent successfully to logeshkumar.r@royalcyber.com' present in field 'message'
	And I wait for '30' seconds
	And I verify the email is received and read the body content
	
Scenario:  MF ContactMerge Reset Timestamp is to resert the Last Rundate and Last Processed key
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-dev-v1.us-w2.cloudhub.io:8091/database/timestamp' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ContactMerge/ContactMergeTimestamp' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Record successfully updated in Service_Run Table.' present in field 'message'
#	And Match JSONPath "message" contains "Record successfully updated in Service_Run Table." 
	

Scenario:  MF ContactMerge Insert Log Details allows us to insert the logs into the database
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-dev-v1.us-w2.cloudhub.io:8091/database/insert-log-details' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ContactMerge/ContactMergeLogDetails' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Record inserted in LOG_DETAILS table.' present in field 'message'
#	And Match JSONPath "message" contains "Record inserted in LOG_DETAILS table." 


Scenario:  MF ContactMerge Fetch Service Run allows us to fetch the LastProcessedkey , Timestamp, LastRunDate ,lastExecDateTime ,lastExecTimeStamp from database
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-dev-v1.us-w2.cloudhub.io:8091/database/fetch-service-run' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ContactMerge/ContactMergeFetchServiceRun' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Retrieve data '$.serviceRunRecord[0].lastRunDateTime' from response and store in variable 'LastRunDateTime'
	And Print Data 'serviceRunRecord[0].lastRunDateTime'
	And Retrieve data '$.serviceRunRecord[0].timestamp' from response and store in variable 'TimeStamp'
	And Print Data 'serviceRunRecord[0].timestamp'
	And Retrieve data '$.serviceRunRecord[0].lastProcessedKey' from response and store in variable 'LastProcessedKey'
    And Print Data 'serviceRunRecord[0].lastProcessedKey'
    
	
Scenario: MF ContactMerge Elastic Search Index to fetch all the required datas and it will be upserted to the Salesforce
	Given EndPoint 'http://mule-worker-internal-sys-es-dev-v1.us-w2.cloudhub.io:8091/elasticsearch/mergedcontact'
    And Content type 'application/json'
    And Header key 'Content-Type' value 'application/json'
#    And Update data in the Request 'ContactMergeElasticSearchFetchDatas' for key '$.search_after[0].ultimateCustomerCode' with data '$$LastProcessedKey'
#    And Update data in the Request 'ContactMergeElasticSearchFetchDatas' for key '$.search_after[0].time' with data '$$TimeStamp'
#    And Update data in the Request 'ContactMergeElasticSearchFetchDatas' for key '$.size' with data '2'
    And Request body 'ContactMerge/ContactMergeElasticSearchFetchDatas'
    When Method 'Post'
    And Print response
    And Statuscode '200'
    And Key '$.masterCustCode[*].customerCode' count present in the response - '2'
    And Get the key '$..customerCode' from response and store it as array
    And Get the datas from elastic search Index and Upsert it in the Salesforce
    

Scenario:  MF ContactMerge Timestamp is to the Last Rundate and Last Processed key
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-dev-v1.us-w2.cloudhub.io:8091/database/timestamp' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'ContactMerge/ContactMergeTimestamp' for key '$.info[0].last_processedkey' with data '$$LastProcessedCustomerCode'
    And Update data in the Request 'ContactMerge/ContactMergeTimestamp' for key '$.info[0].timestamp' with data '$$latesttimestamp'
	And Request body 'ContactMerge/ContactMergeTimestamp' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200'
	And Verify Value 'Record successfully updated in Service_Run Table.' present in field 'message'


Scenario: Contact Merge email Configuration that allows to setup Application Name ,Flow Name ,jobRunEmailDistroFlag ,Email Subject , Email message body content
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-dev-v1.us-w2.cloudhub.io:8091/database/emailconfig' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ContactMerge/ContactMergeEmailConfig' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Retrieve data '$..FROM_ADDRESS' from object index '0' in response and store in variable 'FROM_ADDRESS'
	And Verify Value 'CustomerPortal' present in field '[0].INTEGRATION_NAME'
	And Verify Value 'StoreUpsert' present in field '[0].APPLICATION_NAME'
	

Scenario: MF Contact Merge Flow email distribution list when the job runs successfully
	Given EndPoint 'http://mule-worker-internal-sys-email-dev-v1.us-w2.cloudhub.io:8091/mail/send-email-notification' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'ContactMerge/ContactMergeCompletedEmailNotification' for key 'FROM_ADDRESS' with data '$$FROM_ADDRESS' 
	And Request body 'ContactMerge/ContactMergeCompletedEmailNotification' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Email sent successfully to logeshkumar.r@royalcyber.com' present in field 'message'
	And I wait for '30' seconds
And I verify the Proccess completed email is received and read the body content