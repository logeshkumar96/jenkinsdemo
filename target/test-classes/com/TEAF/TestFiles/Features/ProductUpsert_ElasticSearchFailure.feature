@MF_Product_Failure
Feature: Elastic Search Index Failure Flow
 
Description: In this Feature we will be covering the Failure Flow ,If we have no datas a from Elastic Search Index then we wont we able to proceed with the sdfcupsert
	
@ProductFailureElasticSearch
Scenario: MF Success ProductUpsert email Configuration that allows to setup Application Name ,Flow Name ,jobRunEmailDistroFlag ,Email Subject , Email message body content 
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-uat-v1.us-w2.cloudhub.io:8091/database/emailconfig' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ProductUpsert/ProductEmailConfig' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Retrieve data '$.[0]FROM_ADDRESS' from response and store in variable 'FROM_ADDRESS'
	And Verify Value 'CustomerPortal' present in field '[0].INTEGRATION_NAME'
	And Verify Value 'ProductUpsert' present in field '[0].APPLICATION_NAME'
#	And Match JSONPath "$.[0]INTEGRATION_NAME" contains "CustomerPortal" 
#	And Match JSONPath "$.[0]APPLICATION_NAME" contains "StoreUpsert" 
	
	
Scenario: MF Product Upsert Flow email distribution list when the job runs successfully
	Given EndPoint 'http://mule-worker-internal-sys-email-uat-v1.us-w2.cloudhub.io:8091/mail/send-email-notification' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'ProductUpsert/ProductEmailNotification' for key 'FROM_ADDRESS' with data '$$FROM_ADDRESS' 
	And Request body 'ProductUpsert/ProductEmailNotification' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Email sent successfully to logeshkumar.r@royalcyber.com' present in field 'message'
	And I wait for '25' seconds 
	And I verify the email is received and read the body content
	
	Scenario: MF Product Upsert Reset Timestamp allows us to resert the Last Rundate and Last Processed key
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-uat-v1.us-w2.cloudhub.io:8091/database/timestamp' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ProductUpsert/ProductTimeStamp1' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Record successfully updated in Service_Run Table.' present in field 'message'
	
	#Scenario: MF ProductUpsert Insert Log Details allows us to insert the logs into the database
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-uat-v1.us-w2.cloudhub.io:8091/database/insert-log-details' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ProductUpsert/ProductInsertLogDetails' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Record inserted in LOG_DETAILS table.' present in field 'message'
	
	
	#Scenario: MF  ProductUpsert Fetch Service Run allows us to fetch the LastProcessedkey , Timestamp, LastRunDate ,lastExecDateTime ,lastExecTimeStamp from database
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-uat-v1.us-w2.cloudhub.io:8091/database/fetch-service-run' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ProductUpsert/ProductFetchRun' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Retrieve data '$.serviceRunRecord[0].lastRunDateTime' from response and store in variable 'ProductLastRunDateTime'
	And Print Data 'serviceRunRecord[0].lastRunDateTime'
	And Retrieve data '$.serviceRunRecord[0].timestamp' from response and store in variable 'ProductTimeStamp'
	And Print Data 'serviceRunRecord[0].timestamp'
	And Retrieve data '$.serviceRunRecord[0].lastProcessedKey' from response and store in variable 'ProductLastProcessedKey'
	And Print Data 'serviceRunRecord[0].lastProcessedKey' 
	
	#Scenario: MF ProductUpsert Elastic Search Index allows us to Fetch the datas that are need to be upserted in the Salesfore .
	Given EndPoint 'http://mule-worker-internal-sys-es-uat-v1.us-w2.cloudhub.io:8091/elasticsearch/fetch-all-elasticsearch-index' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ProductUpsert/ProductFailureElasticSearch' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	Then Verify the response body is null
	#If there is no Data response will be NULL


Scenario: MF Product Failure EmailConfiguration and Email will be sent to JOB_FAILURE_EMAIL_DISTRO mailID 
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-uat-v1.us-w2.cloudhub.io:8091/database/emailconfig' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ProductUpsert/ProductEmailConfig' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Retrieve data '$.[0]FROM_ADDRESS' from response and store in variable 'FROM_ADDRESS'
	And Verify Value 'CustomerPortal' present in field '[0].INTEGRATION_NAME'
	And Verify Value 'ProductUpsert' present in field '[0].APPLICATION_NAME' 
	
	
	#Scenario: MF Product Failure Email Notification
	Given EndPoint 'http://mule-worker-internal-sys-email-uat-v1.us-w2.cloudhub.io:8091/mail/send-email-notification' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'ProductUpsert/ProductFailureEmailNotification' for key 'FROM_ADDRESS' with data '$$FROM_ADDRESS' 
	And Request body 'ProductUpsert/ProductFailureEmailNotification' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Email sent successfully to logeshkumar.r@royalcyber.com' present in field 'message'
	And I wait for '25' seconds
	And I verify the Failure email is received and read the body content 
	