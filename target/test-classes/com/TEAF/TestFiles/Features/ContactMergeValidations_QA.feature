@MF_ContactMergeValidations 
Feature: ContactMerge Validation 

Description: This Feature file is all about Contact merge validations
	
	
Scenario Outline: Validating the response of system ES Crossref API with Customer codes are based on the inputs data set with master codes 
	Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/crossref' 
	When Query params with Key 'flowType' and value '<FlowType>' 
	When Query params with Key 'childId' and value '<ChildId>' 
	When Method 'Get' 
	And Print response 
	And Statuscode '<StatusCode>' 
	And Verify Value '<Response>' present in field '<ResponseField1>' 
	
	Examples: 
		|FlowType   |ChildId      |StatusCode|Response     |ResponseField1                         |
		|CUST_UPSERT|AX-C003756378|200       |AX-C003756378|hits.hits[0]._source.masterCustomerCode|
		|			|AX-C003756378|200       |AX-C003756378|hits.hits[0]._source.customerCode      |
		
		
Scenario Outline: Validating the response of crossref API with invalid Customer codes 
	Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/crossref' 
	When Query params with Key 'flowType' and value '<FlowType>' 
	When Query params with Key 'childId' and value '<ChildId>' 
	When Method 'Get' 
	And Print response 
	And Statuscode '<StatusCode>' 
	And Key '$.hits.hits' count present in the response - '0' 
	
	Examples: 
		|FlowType   |ChildId      |StatusCode|
		|CUST_UPSERT|C009654806	  |200       |
		|CUST_UPSERT|AX-X1124DS2R8|200       |
		
		
Scenario Outline: Validating the response of crossref API with different sets of valid Customer codes 
	Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/crossref' 
	When Query params with Key 'flowType' and value '<FlowType>' 
	When Query params with Key 'childId' and value '<ChildId>' 
	When Method 'Get' 
	And Print response 
	And Statuscode '<StatusCode>' 
	And Verify Value '<CustomerCode1>' present in field '<Field1>' 
	And Verify Value '<CustomerCode2>' present in field '<Field2>' 
	And Verify Value '<CustomerCode3>' present in field '<Field3>' 
	And Verify Value '<CustomerCode4>' present in field '<Field4>' 
	And Verify Value '<CustomerCode5>' present in field '<Field5>' 
	
	
	
	Examples: 
		|FlowType   |ChildId      |StatusCode|CustomerCode1|Field1                           |CustomerCode2|Field2                           |CustomerCode3|Field3                           |CustomerCode4|Field4                           |CustomerCode5|Field5                           |
		|CUST_UPSERT|AX-C004883678|200       |AX-C004927159|hits.hits[0]._source.customerCode|AX-C004885214|hits.hits[1]._source.customerCode|AX-C004888376|hits.hits[2]._source.customerCode|AX-C004883233|hits.hits[3]._source.customerCode|AX-C004883007|hits.hits[4]._source.customerCode|	
		|CUST_UPSERT|AX-C002739969|200       |AX-C002738203|hits.hits[0]._source.customerCode|AX-C002739514|hits.hits[1]._source.customerCode|AX-C002739733|hits.hits[2]._source.customerCode|AX-C002739934|hits.hits[3]._source.customerCode|AX-C002738181|hits.hits[4]._source.customerCode|
		|CUST_UPSERT|AX-C007198622|200       |AX-C007197489|hits.hits[0]._source.customerCode|AX-C007197239|hits.hits[1]._source.customerCode|AX-C007197156|hits.hits[2]._source.customerCode|AX-C007197447|hits.hits[3]._source.customerCode|AX-C007197176|hits.hits[4]._source.customerCode|
		|CUST_UPSERT|AX-C004344353|200       |AX-C004344270|hits.hits[0]._source.customerCode|AX-C004344047|hits.hits[1]._source.customerCode|AX-C004343971|hits.hits[2]._source.customerCode|AX-C004344324|hits.hits[3]._source.customerCode|             |                                 |
		
		
		
Scenario Outline: Verify the Response of customerSalesOrder API with valid data present in the Database
	Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/customerSalesOrder/custCodes' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body '<RequestBody>' 
	When Method 'Post' 
	And Print response 
	And Statuscode '<StatusCode>' 
	#	 And Verify Value '<CustomerCode>' present in field '<CustomerCodeField>'
	And Verify Value '<OrderTotalAmount>' present in field '<OrderTotalAmountField>' 
	And Verify Value '<OrderCount>' present in field '<OrderCountField>' 
	And Verify Value '<LastOrderDate>' present in field '<LastOrderDateField>' 
	And Verify Value '<AX_Last_Update_date>' present in field '<AX_Last_Update_dateField>' 
	
	And Verify Value '<OrderTotalAmount2>' present in field '<OrderTotalAmountField2>' 
	And Verify Value '<OrderCount2>' present in field '<OrderCountField2>' 
	And Verify Value '<LastOrderDate2>' present in field '<LastOrderDateField2>' 
	And Verify Value '<AX_Last_Update_date2>' present in field '<AX_Last_Update_dateField2>' 
	
	Examples: 
		|RequestBody      |StatusCode|CustomerCode |CustomerCodeField |OrderTotalAmount|OrderTotalAmountField              |OrderCount|OrderCountField              |LastOrderDate      |LastOrderDateField              |AX_Last_Update_date|AX_Last_Update_dateField              |CustomerCode2 |CustomerCodeField2 |OrderTotalAmount2|OrderTotalAmountField2              |OrderCount2|OrderCountField2                   |LastOrderDate2      |LastOrderDateField2              |AX_Last_Update_date2|AX_Last_Update_dateField2            |
		|MergecontactData1|201       |AX-C004142486|[0].AX-C004142486|5836.33         |[0].AX-C004142486.OrderTotalAmount|2          |[0].AX-C004142486.OrderCount |2016/08/08 11:57:59|[0].AX-C004142486.LastOrderDate |2016/08/16 20:47:51|[0].AX-C004142486.AX_Last_Update_date |AX-C005097645 |[1].AX-C005097645  |373.48           |[1].AX-C005097645.OrderTotalAmount  |1          |[1].AX-C005097645.OrderCount  |2016/12/15 12:14:01 |[1].AX-C005097645.LastOrderDate  |2016/12/23 18:30:09 |[1].AX-C005097645.AX_Last_Update_date|
		
		
Scenario: Validating the Customer codes without passing 'CUST_UPSERT' parameter 
	Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/crossref' 
	#	When Query params with Key 'flowType' and value 'CUST_UPSERT' 
	When Query params with Key 'childId' and value 'AX-C003756378' 
	When Method 'Get' 
	And Print response 
	And Statuscode '200' 
	And Key '$.hits.hits' count present in the response - '0' 
	
	
	
Scenario: Validate the Response of mergedcontact when we pass the query size as zero 
	Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/mergedcontact' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ContactMergeEmptyElasticSearch' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Key '$.masterCustCode[*]' count present in the response - '0' 
	
Scenario: Validate the Response of mergedcontact when we pass the future time 
	Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/mergedcontact' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ContactMergeFutureDate' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Key '$.masterCustCode[*]' count present in the response - '0' 
	
#Scenario: Validate whether the merged customer informationis upserted successfully in database when customer_code value is not updated in request body
#    Given EndPoint 'http://mule-worker-internal-sys-salesforce-uat-v1.us-w2.cloudhub.io:8091/salesforce/sdfcustomerupsert'
#    And Header key 'Content-Type' value 'application/json'
#    And Request body 'ContactMergeSDFCwithoutCustID'
#    When Method 'Post'
#    And Print response
#    And Statuscode '200'
#    And Key '$..number_records_processed' count present in the response - '0'
	
	
