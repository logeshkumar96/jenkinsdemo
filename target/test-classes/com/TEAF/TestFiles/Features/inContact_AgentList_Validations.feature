@MF_inContact_AgentList_Report
Feature: InContact Agent List - Source InContact Endpoint results and Target is BQ and GCP

1.Call inContact endpoint and download CSV files for AgentList report.
2.Perform below validations for AgentList by comparing downloaded source file with target MF BQ table and GCS CSV file.
2.1 Duplicates in source CSV file.
2.2 Duplicates in target BQ table and GCS Bucket CSV file.
2.3 Count validation between source and target.
2.4 End to End data validation between source and target.

Description: In this Feature file we will be covering inContact endpoint, GC Bucket and BigQuery validations
Scenario: (AgentList) Connecting to BigQuery and fetch query results and download CSV file using inContact endpoint and then downloading GCP Bucket CSV file and validate the record count and content between Source CSV file and BigQuery result and GCS CSV file
	#	Dynamically generate date as XD-1: Sysdate-1, XD+1: Sysdate+1, XDC: Current Sysdate
	Given I generate date 'XD-1' in format 'MM-dd-yyyy' and store as 'AgentlistFromDate'
	And I generate date 'XDC' in format 'MM-dd-yyyy' and store as 'AgentlistToDate'
	Then capture CSV file for 'Agentlist' flow between '$$AgentlistFromDate' and '$$AgentlistToDate' using inContact API 'https://home-c16.incontact.com/ReportService/DataDownloadHandler.ashx?CDST=ElurGcWPf8xdyZtPUsN3LrcadtHb7aNgp%2f%2bipeEVk4ko4urF%2bakpohlhkzbntx%2b3zNUtQolxJR6BacUyHNmZLU6%2bRny7XKmL9D83JIcYqTz%2f40Fc8NuNU3BtYc0FCSB2Wj7zNfvU%2f5J2N5z7jIB2cc1Wz1d3%2bSKq6%2fDlNQfT1yfHBhGHr2R%2f93PDcJRJEjp5RVFk1qacxVyS00jQbsLuc6H1B126HMaPrVAgdHAM5gNmnmk4hdlpIwk0tA%3d%3d&Format=CSV&IncludeHeaders=True&AppendDate=False' and download the CSV file to the path 'F:\Manoj Thota\InContact\AgentList\Agentlist_API_CSV_files'
	And get the count of records available in inContact csv file '$$inContactAPIResultCSVFilePath'
	Then assert if duplicate records exist in source inContact API result csv file '$$inContactAPIResultCSVFilePath'
	Then Connect to GCS Bucket with project ID 'dev-mfrm-data'
	Then I generate date 'XD-1' in format 'yyyy-MM-dd' and store as 'AgentlistCSVDate'
	Then for 'Agentlist', open bucket 'dev-mfrm-mulesoft-data-bucket' and Download CSV file for the date '$$AgentlistCSVDate' to the local path 'F:\\Manoj Thota\\InContact\\AgentList\\Agentlist_Bucket_CSV_files'
	And assert if duplicate records exist in target GCP Bucket result CSV file '$$GCSDownloadedCSVfilePath'
#	And assert inContact CSV file '$$inContactAPIResultCSVFilePath' record count with GCP Bucket CSV file '$$GCSDownloadedCSVfilePath' record count
	Then assert end to end data between source CSV file '$$inContactAPIResultCSVFilePath' and target CSV file '$$GCSDownloadedCSVfilePath' for Agentlist
	Then Connect to BigQuery with project ID 'dev-mfrm-data'
	And assert all column values for count of '4' rows between BigQuery and CSV file '$$inContactAPIResultCSVFilePath' for Agentlist
	#	Used to validate all the rows
	And assert all column values for all rows between BigQuery and CSV file '$$inContactAPIResultCSVFilePath' for Agentlist
