@MF_ProductUpsertQA
Feature: ProductUpsert End to End flow in QA environment

Description: In this Feature We will be covering Email system API , SQL Server system API , Elastic Search API , Timestamp , Fetch Service run


Scenario: MF Success ProductUpsert email Configuration that allows to setup Application Name ,Flow Name ,jobRunEmailDistroFlag ,Email Subject , Email message body content 
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/emailconfig' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ProductUpsert/ProductEmailConfig' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Retrieve data '$.[0]FROM_ADDRESS' from response and store in variable 'FROM_ADDRESS'
	And Verify Value 'CustomerPortal' present in field '[0].INTEGRATION_NAME'
	And Verify Value 'ProductUpsert' present in field '[0].APPLICATION_NAME'
	
	
Scenario: MF Product Upsert Flow email distribution list when the job runs successfully
	Given EndPoint 'http://mule-worker-internal-sys-email-qa.us-w2.cloudhub.io:8091/mail/send-email-notification' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'ProductUpsert/ProductEmailNotification' for key 'FROM_ADDRESS' with data '$$FROM_ADDRESS' 
	And Request body 'ProductUpsert/ProductEmailNotification' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Email sent successfully to logeshkumar.r@royalcyber.com' present in field 'message'
	And I wait for '25' seconds 
	And I verify the email is received and read the body content	

@productUpsertqa
Scenario:  MF Product Upsert Reset Timestamp allows us to resert the Last Rundate and Last Processed key
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/timestamp' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ProductUpsert/ProductTimestamp' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Record successfully updated in Service_Run Table.' present in field 'message'

@productUpsertqa	
Scenario: MF ProductUpsert Insert Log Details allows us  to insert the recent logs into the database
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/insert-log-details' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ProductUpsert/ProductInsertLogDetails' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Record inserted in LOG_DETAILS table.' present in field 'message'
	
@productUpsertqa
Scenario: MF  ProductUpsert Fetch Service Run allows us  to fetch the LastProcessedkey , Timestamp, LastRunDate ,lastExecDateTime ,lastExecTimeStamp from database
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/fetch-service-run' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ProductUpsert/ProductFetchRun' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Retrieve data '$.serviceRunRecord[0].lastRunDateTime' from response and store in variable 'ProductLastRunDateTime'
	And Print Data 'serviceRunRecord[0].lastRunDateTime'
	And Retrieve data '$.serviceRunRecord[0].timestamp' from response and store in variable 'ProductTimeStamp'
	And Print Data 'serviceRunRecord[0].timestamp'
	And Retrieve data '$.serviceRunRecord[0].lastProcessedKey' from response and store in variable 'ProductLastProcessedKey'
	And Print Data 'serviceRunRecord[0].lastProcessedKey' 
	
@productUpsertqa	
Scenario: MF ProductUpsert Elastic Search Index allows us to Fetch the datas that are need to be upserted in the Salesfore 
	Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/fetch-all-elasticsearch-index' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'ProductUpsert/ProductElasticSearch' for key 'lastRunDate' with data '$$ProductLastRunDateTime' 
	And Update data in the Request 'ProductUpsert/ProductElasticSearch' for key 'timest' with data '$$ProductTimeStamp' 
	And Update data in the Request 'ProductUpsert/ProductElasticSearch' for key 'lastProcessedKey' with data '$$ProductLastProcessedKey' 
	And Update data in the Request 'ProductUpsert/ProductElasticSearch' for key 'esDeltaQuerySize' with data '2'
	And Request body 'ProductUpsert/ProductElasticSearch' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200'
	And Retrieve data '$.[1]ES_MODIFIEDDATETIME' from response and store in variable 'ModifiedTime'
	And Retrieve data '$.[1]ITM_CD' from response and store in variable 'ModifiedID' 
	And Key '$.[*]ITM_CD' count present in the response - '2'
	And Get the response for Elastic Search Index and store the Product keys

@productUpsertqa	
Scenario: MF SFCUpsert is to insert the data in the Salesforce with the help of "Elastic Search Index"
	Given EndPoint 'http://mule-worker-internal-sys-salesforce-qa-v1.us-w2.cloudhub.io:8091/salesforce/sdfcustomerupsert' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update the Product upsert data to SFDC Upsert
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Match JSONPath "$..number_records_processed" contains "2" 
	And Match JSONPath "$..number_records_failed" contains "0" 
	
@productUpsertqa
Scenario: MF ProductUpsert Timestamp allows us to Update the timestamp and Last Processed key in the database 
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/timestamp' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'ProductUpsert/ProductTimestamp' for key 'info[0].timestamp' with data '$$ModifiedTime' 
	And Update data in the Request 'ProductUpsert/ProductTimestamp' for key 'info[0].last_processedkey' with data '$$ModifiedID' 
	And Request body 'ProductUpsert/ProductTimestamp' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Record successfully updated in Service_Run Table.' present in field 'message'


Scenario: MF Success ProductUpsert email Configuration that allows to setup Application Name ,Flow Name ,jobRunEmailDistroFlag ,Email Subject , Email message body content 
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/emailconfig' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ProductUpsert/ProductEmailConfig' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Retrieve data '$.[0]FROM_ADDRESS' from response and store in variable 'FROM_ADDRESS'
	And Verify Value 'CustomerPortal' present in field '[0].INTEGRATION_NAME'
	And Verify Value 'ProductUpsert' present in field '[0].APPLICATION_NAME'

Scenario: MF Product Upsert Flow email distribution list when the job runs successfully
	Given EndPoint 'http://mule-worker-internal-sys-email-qa.us-w2.cloudhub.io:8091/mail/send-email-notification' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'ProductUpsert/ProductCompletedEmailNotification' for key 'FROM_ADDRESS' with data '$$FROM_ADDRESS' 
	And Request body 'ProductUpsert/ProductCompletedEmailNotification' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Email sent successfully to logeshkumar.r@royalcyber.com' present in field 'message'
	And I wait for '30' seconds 
	And I verify the Proccess completed email is received and read the body content	
