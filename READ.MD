RC TEAF Setup and Implementation

Steps to be followed for setup and implementing RC TEAF are as follows:
Step 1: Clone the DBI Project from Bit Bucket by using (git clone https://kanchanadevi72@bitbucket.org/dbi_general/ecom-automation.git)

Step 2: Import the project into eclipse (File >>Import>>Browse the project from Local folder >> finish )

Step 3: Click src/test/java >> com.TEAF.TestFiles.Feature.DavidsBridal >> Select which feature file you need to run (Personalize Product, BorderFree, GuestUser, New User Register)

Step 4: For example: If you need to run Personalize Product Feature file, you should copy the tag name at the top of the feature file and paste it over TestRunner_1 (com.TEAF.TestFiles.Runner >> TestRunner_1) in the tags section.
Tag Name format : @<tagname>

Step 5: For viewing Locator file, ( com.TEAF.TestFiles.PageObjects.DavidsBridal>> HomePage_DB .xlsx >> Right click >> select system Explorer >> View xlsx)

Step 6: For running the script, (com.TEAF.TestFiles.Runner >> TestRunner_1>> put tagname of the feature file under tags >> right click >> select Run as >> Junit)

Step 7: Configure Properties: (com.TEAF.TestFiles.Config >> Config.properties>> test.appUrl = <url> )

Step 8: For checking the Cucumber Report, (com.TestResults>>right click>> System Explorer >> custom reports >> select and click latest cucumber report >> click cucumber html reports >> click Overview-features)

Step 9: For getting Email report, (go to configure properties >> test.generateEmail = true)  and (TestRunner_1 >> Scroll down >> go to @AfterClass and give the mail id).


Test Execution on RC TEAF with Multiple Browser- Platform combination:

In config.properties file:
#Default
test.appUrl=https://dbqa.davidsbridal.com/  -> Change the application under test 

test.browserName=chrome      		-> Change it to chrome to execute it on local      	           
#test.browserName=browserstack-desktop-browser-local -> to execute on browserstack
#test.browserName=firefox                            -> to execute on firefox
test.platformName=desktop                            -> platform to execute
test.appType=webapp						-> windowsapp � for .exe app

test.disableCucumberReport=false       	     -> true to disable the cucumber report
test.highlighElements=false		     -> true to disable Highlight in the page	
test.headless=false				     -> true to run on headless mode
test.generateEmail=true                     -> false to disable report email trigger

#PageObjectMode can be either "csv" or "class"
test.pageObjectMode=xlsx                    -> change the locator file type
test.implicitlyWait=70                      -> Implicit wait maximum time out
test.pageLoadTimeout=70			     -> Page load time out


#bstack
test.bstack.userName=rvelu@dbi.com
test.bstack.automateKey=Velu2019

#bStack config 
#bStack desktop

Change the Type of Browser, Version, Platform , resolution based on below suggestions

#BrowserName - BrowserVersion - Platform - PlatfromVersion - Resolution
# Chrome - 80.0 - Windows - 10 - 1280x800 
# Firefox - 73.0 - Windows - 8 - 1024x768
#  Edge - 80.0 - Windows - 10 - 1280x1024
# IE - 11.0 - Windows - 7 - 1024x768
# Safari - 11.1 - OS X - High Sierra - 1024x768
# Opera - 12.15 - OS X - High Sierra - 1024x768

test.bstack.browser=Chrome
test.bstack.browserVersion=75.0
test.bstack.os=Windows
test.bstack.osVersion=7
test.bstack.resolution=1920x1080
test.bstack.testName=Bstack-MaverikExecution

To configure automation report email trigger at the end of the execution
#email smtp
email.smtp.server=smtp.office365.com
email.smtp.portNumber=587
email.smtp.socketPortNumber=465

email.user.emailId=QA_Automation@dbi.com
email.user.emailAddress=QA_Automation@dbi.com
email.user.password=R0t@N1mR3T

To run on Parallel Mode:

Create a Multiple Test Runner file (Based on Parallel Thread count) under �com.TEAF.TestFiles.Runner� package

And Execute using Maven Goals -> mvn clean test -P RunFeature

