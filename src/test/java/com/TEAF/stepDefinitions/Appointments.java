package com.TEAF.stepDefinitions;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.TEAF.Hooks.Hooks;
import com.TEAF.customUtilities.ECom_Exception;
import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Appointments {

	public static WebDriver driver = StepBase.getDriver();
	static Utilities util = new Utilities();
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Appointments.class.getName());

	@When("^I verify the email with subject '(.*)' is received and the body content '(.*)'$")
	public static void i_verify_the_email_body_content(String sub, String body) throws Throwable {
		String methodName= "i_verify_the_email_body_content '"+sub+"' '"+body+"'";

		
		try {
			String getemailRead = com.TEAF.framework.ReadingEmail.getemailRead("outlook.office365.com", "pop3",
					"premkumar.g@royalcyber.com", "042504Prem", sub, body);
//        java.io.File htmlFile = new java.io.File(
//                System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\EmailBody.html");
//        FileOutputStream fio = new FileOutputStream(htmlFile);
//        fio.write(getemailRead.getBytes());
//        fio.close();

			Document doc = Jsoup.parse(getemailRead);
			System.out.println(doc);
			Elements p_tags = doc.select("strong");
			String attr = null;
			for (Element p : p_tags) {
				// System.out.println("Strong tag is "+p.text());
				if (p.text().contains("EDT")) {
					attr = p.text();
				}
			}
			System.out.println(attr);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	@When("^I '(.*)' sms opt option for CA$")
	public static void i_sms_opt_option_for_CA(String option) throws ECom_Exception, Exception {

		String methodName= "i_sms_opt_option_for_CA '"+option+"'";

			try {
				if (option.equalsIgnoreCase("yes")) {
					driver.findElement(GetPageObjectRead.OR_GetElement("SMSCheckbox")).click();
					log.info("Sms opt checked in");
				} else if (option.equalsIgnoreCase("no")) {
//			driver.findElement(GetPageObjectRead.OR_GetElement("SMSCheckbox")).click();
//			;
					log.info("Sms opt unchecked");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				new ECom_Exception(methodName, e);

			}
		

	}

	@When("^I Should see Appointment Type '(.*)' and Time in Minutes '(.*)'$")
	public static void i_should_see_appointment_type_and_time_in_minutes(String type, String min) throws ECom_Exception, Exception {
		
		String methodName= "i_should_see_appointment_type_and_time_in_minutes '"+type+"' '"+min+"'";

		try {
			WebDriverWait wb = new WebDriverWait(driver, 50);
			WebElement typeandTime = driver
					.findElement(By.xpath("//h3[@class='apptType' and text()='" + type + "']//..//p[2]"));
			wb.until(ExpectedConditions.visibilityOf(typeandTime));
			System.out.println(typeandTime);
			String typeTimeText = typeandTime.getText();
			log.info(typeTimeText);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@When("^I Select the Appointment Type '(.*)' for UK$")
	public static void i_select_appointment_type_uk(String appointment) throws ECom_Exception, Exception {
		
			WebDriverWait wb = new WebDriverWait(driver, 50);
			String option = null;
			if (appointment.contains("-")) {
				String[] split = appointment.split("-");
				appointment = split[0];
				option = split[1];
			}
			WebElement type = driver.findElement(By.xpath("//div[@class='apptType' and text()='" + appointment + "']"));
			wb.until(ExpectedConditions.visibilityOf(type));
			multipleTrytoFindElement(type);

			if (appointment.equals("Bridal") || appointment.equals("Wedding Dress Selection")) {
				if (option.equals("Yes")) {
					driver.findElement(By.xpath("//input[@id='newAppt']")).click();
				} else if (option.equals("No")) {
					driver.findElement(By.xpath("//input[@id='oldAppt']")).click();

				}
			}

			if (appointment.equals("Bridal Party") || appointment.equals("Bridesmaid Dress Selection")) {
				WebElement selectDD = driver.findElement(By.id("bridalPartyAttendeesOption"));
				Select sc = new Select(selectDD);
				sc.selectByValue(option);
			}

	}

	@When("^I Select the Appointment Type '(.*)' for CA$")
	public static void i_select_appointment_type_ca(String appointment) throws ECom_Exception, Exception {
		
			WebDriverWait wb = new WebDriverWait(driver, 50);
			String option = null;
			if (appointment.contains("-")) {
				String[] split = appointment.split("-");
				appointment = split[0];
				option = split[1];
			}
			WebElement type = driver.findElement(By.xpath("//h3[@class='apptType' and text()='" + appointment + "']"));
			wb.until(ExpectedConditions.visibilityOf(type));
			multipleTrytoFindElement(type);

			if (appointment.equals("Wedding Dress Selection")) {
				if (option.equals("Yes")) {
					driver.findElement(By.xpath("//input[@id='newAppt']")).click();
				} else if (option.equals("No")) {
					driver.findElement(By.xpath("//input[@id='oldAppt']")).click();

				}
			}

			if (appointment.equals("Bridesmaid Dress Selection")) {
				WebElement selectDD = driver.findElement(By.id("bridalPartyAttendeesOption"));
				Select sc = new Select(selectDD);
				sc.selectByValue(option);
			}

	}

	@When("^I Select Make an Appointment Month '(.*)' - Date '(.*)'$")
	public static void i_select_make_an_appointment_month_date_time(String month, String date) throws ECom_Exception, Exception {
		
		String methodName= "i_select_make_an_appointment_month_date_time '"+month+"' '"+date+"'";

		try {
			WebDriverWait wb = new WebDriverWait(driver, 40);

//        driver.findElement(By.xpath("(//span[@class='storeAddress1'])[1]")).click();
			WebElement monthElement = driver.findElement(By.id("MonthYear"));
			wb.until(ExpectedConditions.visibilityOf(monthElement));

			Select monthDD = new Select(monthElement);
			String selectedMonth = null;
			monthDD.selectByIndex(Integer.parseInt(month) - 1);
			monthElement = driver.findElement(By.id("MonthYear"));
			monthDD = new Select(monthElement);

			selectedMonth = monthDD.getFirstSelectedOption().getText();

			HashMapContainer.add("$$Month", selectedMonth);
			// monthDD.selectByVisibleText(month + " 2020");
			log.info("Selected Month is " + selectedMonth);

			List<WebElement> calenderCon = driver.findElements(By.xpath("//div[@id='calendarContainer']/div"));
			int enableCount = 0;
			for (int i = 0; i < calenderCon.size(); i++) {
				String attribute = calenderCon.get(i).getAttribute("class");
				if (attribute.contains("enabledDay")) {
					enableCount++;
				}
				if (enableCount == Integer.parseInt(date)) {
					WebElement dateElement = driver
							.findElement(By.xpath("//div[@id='calendarContainer']/div[" + Integer.parseInt(date) + "]"));
					calenderCon.get(i).click();
					String st = calenderCon.get(i).getAttribute("id");
					log.info("Selected Date is " + st);
					HashMapContainer.add("$$Date", st);
					break;
				}
			}

			WebElement timeElement = driver.findElement(By.xpath("(//div[@class='appointmentTime'])[1]"));
			String time = timeElement.getText();
			HashMapContainer.add("$$AppointMentTime", time);
//        WebElement dateElement = driver.findElement(By.xpath("//div[@id='" + date + "']"));
			// String attribute = dateElement.getAttribute("class");
			// if (attribute.equals("enabledDay")) {
			// dateElement.click();
			// log.info("Selected Date is " + date);
//
			// } else {
			// log.error("Selected Date is not enabled");
			// }
//
//        WebElement appointMentResult = driver.findElement(By.id("appointmentResults"));
//        wb.until(ExpectedConditions.visibilityOf(appointMentResult));
//
//        boolean displayed = appointMentResult.isDisplayed();
//        WebElement timeElement = driver.findElement(By.xpath("(//div[@class='appointmentTime'])[1]"));
//        timeElement.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	public static boolean ignoringStaleElementException(WebElement element) throws ECom_Exception, Exception {
		String methodName= "ignoringStaleElementException '"+element+"'";

			boolean result = false;
			int attempts = 0;
			while (attempts < 3) {
				try {
					new WebDriverWait(driver, 20).ignoring(StaleElementReferenceException.class)
							.until(ExpectedConditions.visibilityOf(element));
					element.isDisplayed();
					result = true;
					break;
				} catch (StaleElementReferenceException e) {
					attempts++;
					log.error("Attempt Count " + attempts + " for StaleElement exception");
					new ECom_Exception(methodName, e);

				}
			}
			return result;
		} 

	@When("^I Select Make an Appointment Month '(.*)' - Date '(.*)' for UK and CA$")
	public static void i_select_make_an_appointment_month_date_time_for_UK_and_CA(String month, String date)
			throws ECom_Exception, Exception {
		
			WebDriverWait wb = new WebDriverWait(driver, 40);

//		driver.findElement(By.xpath("(//span[@class='storeAddress1'])[1]")).click();
			WebElement monthElement = driver.findElement(By.id("MonthYear"));
			wb.until(ExpectedConditions.visibilityOf(monthElement));

			Select monthDD = new Select(monthElement);
			String selectedMonth = null;
			if (month.startsWith("1")) {
				monthDD.selectByIndex(0);
			} else if (month.startsWith("2")) {
				monthDD.selectByIndex(1);
			} else {
				monthDD.selectByIndex(0);
			}
			monthElement = driver.findElement(By.id("MonthYear"));
			monthDD = new Select(monthElement);

			selectedMonth = monthDD.getFirstSelectedOption().getText();

			HashMapContainer.add("$$Month", selectedMonth);
			// monthDD.selectByVisibleText(month + " 2020");
			log.info("Selected Month is " + selectedMonth);

			List<WebElement> calenderCon = driver.findElements(By.xpath("//div[@id='calendarContainer']/div"));
			int enableCount = 0;
			for (int i = 0; i < calenderCon.size(); i++) {

				WebElement ts = driver.findElement(By.xpath("//div[@id='calendarContainer']/div[" + (i + 1) + "]"));

				ignoringStaleElementException(ts);
				String attribute = ts.getAttribute("class");
				if (attribute.contains("enabledDay")) {
					enableCount++;
				}
				Thread.sleep(2000);
				if (enableCount == Integer.parseInt(date)) {
					WebElement dateElement = driver
							.findElement(By.xpath("//div[@id='calendarContainer']/div[" + (i + 1) + "]"));
					wb.until(ExpectedConditions.visibilityOf(dateElement));
					String st = dateElement.getAttribute("id");
					Actions ac = new Actions(driver);
					ac.moveToElement(dateElement).click(dateElement).build().perform();
					// javaScriptClick(calenderCon.get(i));
					log.info("Selected Date is " + st);
					HashMapContainer.add("$$Date", st);
					break;
				}
			}

			WebElement timeElement = driver.findElement(By.xpath("(//div[@class=\"span6 timeBtn\"])"));
			wb.until(ExpectedConditions.visibilityOf(timeElement));

			String time = timeElement.getText();
			HashMapContainer.add("$$AppointMentTime", time);

	}

	public static void javaScriptClick(WebElement webElement) throws ECom_Exception, Exception {
		
		String methodName= "javaScriptClick '"+webElement+"'";

		try {
			WebDriverWait wb = new WebDriverWait(driver, 20);
			wb.until(ExpectedConditions.visibilityOf(webElement));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", webElement);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
		}

	}

	public static void multipleTrytoFindElement(WebElement element) throws ECom_Exception, Exception {
		
		String methodName= "multipleTrytoFindElement '"+element+"'";

		try {
			boolean flag = false;
			for (int i = 0; i < 3; i++) {
				if (flag) {
					break;
				}
				try {
					JavascriptExecutor executor = (JavascriptExecutor) driver;
					executor.executeScript("arguments[0].click();", element);
					flag = true;
					log.info("Clicked on Try ->" + i);
				} catch (Exception e) {
					flag = false;
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
		}
	}

	@When("^I Select the Appointment Type '(.*)'$")
	public static void i_select_appointment_type(String appointment) throws ECom_Exception, Exception {
		
		String methodName= "i_select_appointment_type '"+appointment+"'";

		try {
			WebDriverWait wb = new WebDriverWait(driver, 50);
			WebElement type = driver.findElement(By.xpath("//h3[@class='apptType' and text()='" + appointment + "']"));
			wb.until(ExpectedConditions.visibilityOf(type));
			WebElement typeandTime = driver
					.findElement(By.xpath("//h3[@class='apptType' and text()='" + appointment + "']//..//p[2]"));
			String mins = typeandTime.getText().substring(0, 2);
			HashMapContainer.add("$$AppMins", mins);
			multipleTrytoFindElement(type);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
		}

	}

	@Then("^I select calendar eventdate '(.*)' in checkout$")
	public static void i_select_calendar_eventdate_in_checkout(String date)
			throws ECom_Exception, Exception {
		String methodName= "i_select_calendar_eventdate_in_checkout '"+date+"'";


		try {
			WebElement monthDD = driver.findElement(By.xpath("//th[@class='dijitReset']/span"));
			monthDD.click();

			LocalDate lt = LocalDate.now();
			String localDate = lt.toString();
			String[] splitDate = localDate.split("-");

			SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(splitDate[1] + "-" + splitDate[2]));
			c.add(Calendar.DATE, Integer.parseInt(date));
			String dt = sdf.format(c.getTime());
			String[] splitAfter = dt.split("-");
			String[] monthNames = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
					"October", "November", "December" };
			Thread.sleep(2000);
			driver.findElement(By.xpath("(//div[@class='dijitCalendarMonthLabel' and text()='"
					+ monthNames[Integer.parseInt(splitAfter[0]) - 1] + "'])[2]")).click();
			Thread.sleep(2000);

			String dateAfter = splitAfter[1];
			if (dateAfter.startsWith("0")) {
				dateAfter = dateAfter.substring(1);
			}
			driver.findElement(By.xpath("//td[contains(@class, 'CurrentMonth')]//span[text()='" + dateAfter + "']"))
					.click();
		}   catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
		}

	}

	@Then("^I switch to parent Windows$")
	public static void I_switch_to_window() throws ECom_Exception, Exception {
		
		String methodName= "I_switch_to_window";

		try {

			Set<String> windowHandles = driver.getWindowHandles();
			List<String> windowList = new ArrayList<String>(windowHandles);
			String wid = windowList.get(1);
			HashMapContainer.add("$$CID", windowList.get(0));
			driver.switchTo().window(wid);
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);			

		}

	}

	@When("^I close the driver$")
	public static void i_close_the_driver() throws ECom_Exception, Exception {
		
		String methodName= "i_close_the_driver";

		try {
			driver.close();
			driver.switchTo().window(HashMapContainer.get("$$CID"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
		}
	}

	@When("^I store Email,FirstName,LastName,PhoneNumber,Timeslot,Eventrole,EventDate,SMSOpt for Appointment '(.*)' in the spreadsheet for CA$")
	public static void i_store_the_details_in_the_spreadsheet(String AppointmentType) throws ECom_Exception, Exception {


			String firstname = driver
					.findElement(By.xpath("//div[@class=\"ctnr-field-input\"]//input[@name=\"firstName\"]"))
					.getAttribute("value");
			String lastname = driver
					.findElement(By.xpath("//div[@class=\"ctnr-field-input\"]//input[@name=\"lastName\"]"))
					.getAttribute("value");
			String emailid = driver
					.findElement(By.xpath("//div[@class=\"ctnr-field-input\"]//input[@autocomplete=\"email\"]"))
					.getAttribute("value");
			String mobilenum = driver
					.findElement(By.xpath("//div[@class=\"ctnr-field-input\"]//input[@name=\"phone1\"]"))
					.getAttribute("value");
			String eventdate = driver.findElement(By.xpath("//input[@id=\"eventDate\"]")).getAttribute("value");
			boolean selected = driver
					.findElement(By.xpath("//div[@class=\"ctnr-field-input\"]//input[@class=\"cvform sms-checkbox\"]"))
					.isSelected();
			String eventrole = null;
			if (!(AppointmentType.contains("PROM & HOMECOMING") || AppointmentType.contains("SPECIAL OCCASION"))) {
				eventrole = driver.findElement(By.xpath("//select[@id=\"roleName\"]")).getAttribute("value");
			}

			java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheetAt = wb.getSheet("Appointments");
			for (int j = 1; j <= sheetAt.getPhysicalNumberOfRows(); j++) {
				if (sheetAt.getRow(j) == null) {
					continue;
				}

				String email = sheetAt.getRow(j).getCell(0).getStringCellValue();
				System.out.println("email id " + email);
				System.out.println("email id stored " + HashMapContainer.get("$$EmailIdAppointment"));
				// System.out.println(HashMapContainer.get("$$EmailIdAppointment"));
				if (email.toLowerCase().trim()
						.equals(HashMapContainer.get("$$EmailIdAppointment").toLowerCase().trim())) {
					sheetAt.getRow(j).createCell(5).setCellValue(firstname);
					sheetAt.getRow(j).createCell(6).setCellValue(lastname);
					sheetAt.getRow(j).createCell(4).setCellValue(HashMapContainer.get("$$AppointMentTime"));
					sheetAt.getRow(j).createCell(7).setCellValue(mobilenum);
					sheetAt.getRow(j).createCell(8).setCellValue(eventdate);
					if (!(AppointmentType.contains("PROM & HOMECOMING")
							|| AppointmentType.contains("SPECIAL OCCASION"))) {
						sheetAt.getRow(j).createCell(9).setCellValue(eventrole);
					}

					sheetAt.getRow(j).createCell(10).setCellValue(selected);

				}
			}

			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();
	}

	@When("^I store Email,FirstName,LastName,PhoneNumber,Timeslot,Eventrole,EventDate,SMSOpt for Appointment '(.*)' in the spreadsheet for UK$")
	public static void i_store_the_details_in_the_spreadsheet_UK(String AppointmentType) throws ECom_Exception, Exception {
		

			String firstname = driver
					.findElement(By.xpath("//div[@class=\"ctnr-field-input\"]//input[@name=\"firstName\"]"))
					.getAttribute("value");
			String lastname = driver
					.findElement(By.xpath("//div[@class=\"ctnr-field-input\"]//input[@name=\"lastName\"]"))
					.getAttribute("value");
			String emailid = driver
					.findElement(By.xpath("//div[@class=\"ctnr-field-input\"]//input[@autocomplete=\"email\"]"))
					.getAttribute("value");
			String mobilenum = driver
					.findElement(By.xpath("//div[@class=\"ctnr-field-input\"]//input[@name=\"phone1\"]"))
					.getAttribute("value");
			String eventdate = driver.findElement(By.xpath("//input[@id=\"eventDate\"]")).getAttribute("value");
			String eventrole = null;
			if (!(AppointmentType.contains("Prom & Homecoming") || AppointmentType.contains("Special Occasion"))) {
				eventrole = driver.findElement(By.xpath("//select[@id=\"roleName\"]")).getAttribute("value");
			}
			java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));

			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheetAt = wb.getSheet("Appointments");
			for (int j = 1; j <= sheetAt.getPhysicalNumberOfRows(); j++) {
				if (sheetAt.getRow(j) == null) {
					continue;
				}

				String email = sheetAt.getRow(j).getCell(0).getStringCellValue();
				System.out.println("email id " + email);
				System.out.println("email id stored " + HashMapContainer.get("$$EmailIdAppointment"));
				// System.out.println(HashMapContainer.get("$$EmailIdAppointment"));
				if (email.toLowerCase().trim()
						.equals(HashMapContainer.get("$$EmailIdAppointment").toLowerCase().trim())) {
					sheetAt.getRow(j).createCell(5).setCellValue(firstname);
					sheetAt.getRow(j).createCell(6).setCellValue(lastname);
					sheetAt.getRow(j).createCell(4).setCellValue(HashMapContainer.get("$$AppointMentTime"));
					sheetAt.getRow(j).createCell(7).setCellValue(mobilenum);
					sheetAt.getRow(j).createCell(8).setCellValue(eventdate);
					if (!(AppointmentType.contains("Prom & Homecoming")
							|| AppointmentType.contains("Special Occasion"))) {
						sheetAt.getRow(j).createCell(9).setCellValue(eventrole);
					}

				}
			}

			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();
	}

	@When("^I update the appointment details '(.*)' in the excel sheet column '(.*)'$")
	public static void i_update_the_appointment_details_in_the_excel_sheet_for_product(String appdetail, String column)
			throws Exception {
		
		String methodName= "i_update_the_appointment_details_in_the_excel_sheet_for_product '"+appdetail+"' '"+column+"'";

		try {
			String appDetailValue = null;
			if (!appdetail.contains("@")) {
				appDetailValue = driver.findElement(GetPageObjectRead.OR_GetElement(appdetail)).getText();
			}
			if (appdetail.contains("FirstName") || appdetail.contains("LastName") || appdetail.contains("PhoneNumber")) {
				appDetailValue = driver.findElement(GetPageObjectRead.OR_GetElement(appdetail)).getAttribute("value");
			}

			if (appdetail.contains("@")) {
				HashMapContainer.add("$$EmailIDApp", appdetail);
				appDetailValue = appdetail;
			}
			java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));

			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheetAt = wb.getSheet("Appointments");
			int physicalNumberOfCells = sheetAt.getRow(0).getPhysicalNumberOfCells();
			for (int i = 0; i < physicalNumberOfCells; i++) {
				String appValue = sheetAt.getRow(0).getCell(i).getStringCellValue();
				if (appValue.equals(column)) {
					for (int j = 0; j < sheetAt.getPhysicalNumberOfRows(); j++) {
						if (sheetAt.getRow(j) == null) {
							continue;
						}

						String email = sheetAt.getRow(j).getCell(0).getStringCellValue();
						// System.out.println(HashMapContainer.get("$$EmailIdAppointment"));
						if (email.toLowerCase().equals(HashMapContainer.get("$$EmailIdAppointment"))) {
							sheetAt.getRow(j).createCell(i).setCellValue(appDetailValue);
							Hooks.scenario.write(appDetailValue);

						}
					}
				}
			}
			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			

		}
	}

	@Then("^I generate random email '(.*)' for appointments and enter in the feild '(.*)'$")
	public static void i_generate_random_email_id_appointments(String value, String Locator) throws ECom_Exception, Exception {
		
		String methodName= "i_generate_random_email_id_appointments '"+value+"' '"+Locator+"'";

		try {
			WebElement emailTextBx = driver.findElement(GetPageObjectRead.OR_GetElement(Locator));
			emailTextBx.clear();
			emailTextBx.click();
			Random randomGenerator = new Random();
			int randomInt = randomGenerator.nextInt(100000000);
			String emailId = value + randomInt + "@gmail.com";
			emailTextBx.sendKeys(emailId);
			HashMapContainer.add("$$EmailIdAppointment", emailId.toLowerCase());
			java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheetAt = wb.getSheet("Appointments");
			int prows = sheetAt.getPhysicalNumberOfRows();
			sheetAt.createRow(prows).createCell(0).setCellValue(emailId);
			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
		}
	}

	@When("^I should see selected Appointment date and time are present on confirmation page$")
	public static void i_should_see_selected_appointment_date_and_time_are_present_on_confirmation_page() throws ECom_Exception, Exception {
		
		String methodName= "i_should_see_selected_appointment_date_and_time_are_present_on_confirmation_page";

		try {
			WebDriverWait wb = new WebDriverWait(driver, 40);
			WebElement date = driver.findElement(GetPageObjectRead.OR_GetElement("Confirmation_AppointmentDate"));
			WebElement time = driver.findElement(GetPageObjectRead.OR_GetElement("Confirmation_AppointmentTime"));
			String confirmationdate = date.getText();
			String confirmationtime = time.getText();
			String[] confDatesplit = confirmationdate.split(" ");

			String appointmentTime = HashMapContainer.get("$$AppointMentTime");

			String selectedmonth = HashMapContainer.get("$$Month");
			String[] selectedMonthSplit = selectedmonth.split(" ");
			String selecteddate = HashMapContainer.get("$$Date");
			if (selecteddate.startsWith("0")) {
				selecteddate = selecteddate.substring(1);
			}
			System.out.println("Date 1" + confDatesplit[0] + selectedMonthSplit[0]);
			System.out.println("Date 2" + confDatesplit[2] + selectedMonthSplit[1]);

			System.out.println("Date 3" + selecteddate + confDatesplit[1]);

			if (confDatesplit[0].equals(selectedMonthSplit[0]) && confDatesplit[2].equals(selectedMonthSplit[1])
					&& selecteddate.equals(confDatesplit[1])) {
				log.info("Selected Appointment Date and Confirmed Appointment Date are Same");
			} else {
				log.info("Selected Appointment Date and Confirmed Appointment Date are not Same");
			}

			System.out.println("Time " + appointmentTime + confirmationtime);
			if (appointmentTime.equals(confirmationtime)) {
				log.info("Selected Appointment Time and Confirmed Appointment Time are Same");
			} else {
				log.info("Selected Appointment Time and Confirmed Appointment Time are not Same");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
		}

	}

	@When("^I should see selected Appointment date and time are present on confirmation page for UK and CA$")
	public static void i_should_see_selected_appointment_date_and_time_are_present_on_confirmation_page_uk_ca() throws ECom_Exception, Exception {
		
			WebDriverWait wb = new WebDriverWait(driver, 40);
			WebElement date = driver.findElement(GetPageObjectRead.OR_GetElement("AppointmentDate_ConfirmationPage"));
			String confirmationdate = date.getText();
			System.out.println(confirmationdate);
			String[] confDatesplit = confirmationdate.split(" ", 4);
			String appointmentTime = HashMapContainer.get("$$AppointMentTime");

			String selectedmonth = HashMapContainer.get("$$Month");
			String[] selectedMonthSplit = selectedmonth.split(" ");
			String selecteddate = HashMapContainer.get("$$Date");
			if (selecteddate.startsWith("0")) {
				selecteddate = selecteddate.substring(1);
			}
			System.out.println("Date 1" + confDatesplit[0] + selectedMonthSplit[0]);
			System.out.println("Date 2" + confDatesplit[2] + selectedMonthSplit[1]);

			System.out.println("Date 3" + selecteddate + confDatesplit[1].replace(",", ""));

			if (confDatesplit[0].equals(selectedMonthSplit[0]) && confDatesplit[2].equals(selectedMonthSplit[1])
					&& selecteddate.equals(confDatesplit[1].replace(",", ""))) {
				log.info("Selected Appointment Date and Confirmed Appointment Date are Same");
			} else {
				log.info("Selected Appointment Date and Confirmed Appointment Date are not Same");
			}

			System.out.println("Time " + appointmentTime + confDatesplit[3]);
			if (appointmentTime.equals(confDatesplit[3])) {
				log.info("Selected Appointment Time and Confirmed Appointment Time are Same");
			} else {
				log.info("Selected Appointment Time and Confirmed Appointment Time are not Same");
			}

	}

	@When("^I create new Appointment sheet in Test Data$")
	public static void createAppointmentSheet() throws ECom_Exception, Exception {
		
		String methodName= "createAppointmentSheet";

		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\TestDatas.xlsx");

			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			int sheet = wb.getSheetIndex("Appointments");
			wb.removeSheetAt(sheet);
			wb.createSheet("Appointments");

			wb.getSheet("Appointments").createRow(0).createCell(0).setCellValue("Email");
			wb.getSheet("Appointments").getRow(0).createCell(1).setCellValue("AppointmentType");
			wb.getSheet("Appointments").getRow(0).createCell(2).setCellValue("StoreDetails");
			wb.getSheet("Appointments").getRow(0).createCell(3).setCellValue("Date");
			wb.getSheet("Appointments").getRow(0).createCell(4).setCellValue("TimeSlot");
			wb.getSheet("Appointments").getRow(0).createCell(8).setCellValue("ConfirmationNumber");

			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();
		}  catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
		}
	}

	@When("^I '(.*)' sms opt option$")
	public static void i_sms_opt_option(String option) throws ECom_Exception, Exception {
		
		String methodName= "i_sms_opt_option '"+option+"'";

		try {
			if (option.equalsIgnoreCase("yes")) {
				log.info("Sms opt checked in");
			} else if (option.equalsIgnoreCase("no")) {
				driver.findElement(GetPageObjectRead.OR_GetElement("SMSOptCheckbox")).click();
				log.info("Sms opt unchecked");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
		}

	}

	@When("^I get the confirmation number from Edit Appointment Href and update in the excel sheet$")
	public static void i_get_confirmation_number_from_edit_appointment_href_update_in_excel() throws Throwable {
		
		String methodName= "i_get_confirmation_number_from_edit_appointment_href_update_in_excel";

		try {
			WebElement editAppt = driver.findElement(By.xpath("//a[contains(text(),'Edit Appointment')]"));
			String href = editAppt.getAttribute("href");
			String[] sp1 = href.split("appointmentId=");
			String[] sp2 = sp1[1].split("&");
			String confNumber = sp2[0];
			HashMapContainer.add("$$ConfirmationAppNum", confNumber);
			System.out.println("Generated Email Id " + HashMapContainer.get("$$EmailIdAppointment"));

			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\TestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheetAt = wb.getSheet("Appointments");
			int physicalNumberOfCells = sheetAt.getRow(0).getPhysicalNumberOfCells();
			for (int i = 0; i < physicalNumberOfCells; i++) {
				String appValue = sheetAt.getRow(0).getCell(i).getStringCellValue();
				if (appValue.equals("ConfirmationNumber")) {
					for (int j = 0; j < sheetAt.getPhysicalNumberOfRows(); j++) {
						if (sheetAt.getRow(j) == null) {
							continue;
						}

						String email = sheetAt.getRow(j).getCell(0).getStringCellValue();
						if (email.toLowerCase().equals(HashMapContainer.get("$$EmailIdAppointment"))) {
							sheetAt.getRow(j).createCell(i).setCellValue(confNumber);
						}
					}
				}
			}
			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
		}

	}

	@When("^I should see value in the ZipCity Input feild$")
	public static void i_should_see_value_in_zip_city_input_feild() throws ECom_Exception, Exception {
		
		String methodName= "i_should_see_value_in_zip_city_input_feild";

		try {
			WebElement zipCity = driver.findElement(By.id("zipCityInput"));
			String city = zipCity.getAttribute("value");
			Assert.assertTrue(city.length() > 2);
		} catch (InvalidElementStateException e) {

			WebElement zipCity = driver.findElement(By.id("zipCityInput"));
			String city = zipCity.getAttribute("value");
			Assert.assertTrue(city.length() > 2);
		}catch(Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
			
		}
	}

	@When("^I should see empty value in the ZipCity Input feild$")
	public static void i_should_see_emptyvalue_in_zip_city_input_feild() throws ECom_Exception, Exception {
		
		String methodName= "i_should_see_emptyvalue_in_zip_city_input_feild";

		try {
			WebElement zipCity = driver.findElement(By.id("zipCityInput"));
			String city = zipCity.getAttribute("value");
			Assert.assertTrue(city.length() == 0 || city == null || city.equals("null"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
		}
	}

	@When("^I should see '(.*)' Input feild is required$")
	public static void i_should_see_input_feild_is_required(String element) throws ECom_Exception, Exception {
		
		String methodName= "i_should_see_input_feild_is_required '"+element+"'";

		try {
			WebElement inputFeild = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String attribute = inputFeild.getAttribute("required");
			log.info(attribute);
			Assert.assertTrue(Boolean.getBoolean(attribute));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
		}
	}

	@When("^I should see all the avaliable time slots are within the AppointMent type Time Span$")
	public static void i_should_see_all_the_avaliable_timeslot_are_with_appointment_type() throws ECom_Exception, Exception {
		
		String methodName= "i_should_see_all_the_avaliable_timeslot_are_with_appointment_type";

		try {
			List<WebElement> appTypes = driver.findElements(By.xpath("//div[@id='appointmentResults']//div"));
			String apps = HashMapContainer.get("$$AppMins");

			for (int i = 0; i < appTypes.size(); i++) {
				String timeGap = appTypes.get(i).getText();
				String[] split = timeGap.split(" ");

				String startTime = split[0].trim();
				String endTime = split[2].trim();

				String[] st = startTime.split(":");

				int stHour = Integer.parseInt(st[0]);
				int stmin = Integer.parseInt(st[1]);

				String[] et = endTime.split(":");
				int etHour = Integer.parseInt(et[0]);
				int etmin = Integer.parseInt(et[1]);
				int time = Integer.parseInt(HashMapContainer.get("$$AppMins"));
				if (time == 90) {
					if (etmin == 00) {
						etmin = 30;
						etHour = etHour - 2;
					} else {
						etmin = 00;
						etHour = etHour - 1;
					}
				} else if (time == 60) {
					etHour = etHour - 1;
				} else if (time == 30) {
					if (etmin == 00) {
						etmin = 30;
						etHour = etHour - 1;
					} else {
						etmin = 00;
					}
				}
				if (stHour == 12) {
					stHour = 00;
				}
				if (etHour == stHour && etmin == stmin) {
					log.info("Pass");
				} else {
					log.info("Fail " + timeGap);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
		}

	}

	@When("^I Select the Event Date '(.*)' days from appointment date$")
	public static void i_select_the_event_date_days_from_appointment_date(String date)
			throws ECom_Exception, Exception {
		
		String methodName= "i_select_the_event_date_days_from_appointment_date '"+date+"'";

		try {
			WebDriverWait wb = new WebDriverWait(driver, 40);

			WebElement eventDate = driver.findElement(By.className("ui-datepicker-month"));
			Select monthDD = new Select(eventDate);
			String Selectedmonth = HashMapContainer.get("$$Month");
			monthDD.selectByVisibleText(Selectedmonth.substring(0, 3));

			String[] splitMonth = Selectedmonth.split(" ");
			String[] monthNames = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
					"October", "November", "December" };
			int monthIndex = 0;
			for (int i = 0; i < monthNames.length; i++) {
				if (monthNames[i].equals(splitMonth[0].trim())) {
					monthIndex = i;
				}
			}
			eventDate = driver.findElement(By.className("ui-datepicker-month"));
			monthDD = new Select(eventDate);
			List<WebElement> options = monthDD.getOptions();
			int index = 0;
			for (int i = 0; i < options.size(); i++) {
				if (options.get(i).getText().equals(Selectedmonth.substring(0, 3))) {
					index = i;
				}
			}
			String Selecteddate = HashMapContainer.get("$$Date");
			if (Selecteddate.startsWith("0")) {
				Selecteddate = Selecteddate.substring(1);
			}

			List<WebElement> noOfDays = driver
					.findElements(By.xpath("//table[@class='ui-datepicker-calendar']//tbody//td/*[text()]"));
			System.out.println((Integer.parseInt(Selecteddate) + Integer.parseInt(date)));
			int eventDateUp = (Integer.parseInt(Selecteddate) + Integer.parseInt(date));

			String dt = (monthIndex + 1) + "-" + Selecteddate;
			log.info(dt);
			String time = HashMapContainer.get("$$AppointMentTime");
			// String[] startTime = time.split("-");
			String storeStartDate = "2020-" + (monthIndex + 1) + "-" + HashMapContainer.get("$$Date");
			HashMapContainer.add("$$StoredStartDate", storeStartDate);
			log.info("before");
			SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(dt));
			c.add(Calendar.DATE, Integer.parseInt(date));
			dt = sdf.format(c.getTime());
			log.info(dt);
			log.info("after");
			String[] split = dt.split("-");
			int month = Integer.parseInt(split[0]);

			String mo = monthNames[month - 1];
			monthDD.selectByVisibleText(mo.substring(0, 3));
			eventDateUp = Integer.parseInt(split[1]);
			String storedEventDate = "2020-" + Integer.parseInt(split[0]) + "-" + Integer.parseInt(split[1]);

			HashMapContainer.add("$$StoredEventDate", storedEventDate);

			WebElement datePick = driver.findElement(By.xpath("//a[text()='" + eventDateUp + "']"));
			wb.until(ExpectedConditions.visibilityOf(datePick));
			datePick.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);			
		} 

	}

	@When("^I Select the Event Date '(.*)' after the appointment date for UK and CA$")
	public static void i_select_the_event_date_month_uk_ca(String date)
			throws ECom_Exception, Exception {
		
		WebDriverWait wb = new WebDriverWait(driver, 40);

		String Selecteddate = HashMapContainer.get("$$Date");
		String Selectedmonth = HashMapContainer.get("$$Month");
		String[] splitMonth = Selectedmonth.split(" ");
		String[] monthNames = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };
		int monthIndex = 0;
		for (int i = 0; i < monthNames.length; i++) {
			if (monthNames[i].equals(splitMonth[0].trim())) {
				monthIndex = i;
			}
		}
		System.out.println((Integer.parseInt(Selecteddate) + Integer.parseInt(date)));
		int eventDateUp = (Integer.parseInt(Selecteddate) + Integer.parseInt(date));
		String dt = (monthIndex + 1) + "-" + Selecteddate;
		log.info(dt);
		log.info("before");
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(dt));
		c.add(Calendar.DATE, Integer.parseInt(date));
		dt = sdf.format(c.getTime());
		log.info(dt);
		log.info("after");
		String[] split = dt.split("-");
		int eventMonth = Integer.parseInt(split[0]);
		String mo = monthNames[eventMonth];
		eventDateUp = Integer.parseInt(split[1]);

		WebElement eventMonthDD = driver.findElement(By.id("eventDate_popup_mddb_label"));
		wb.until(ExpectedConditions.visibilityOf(eventMonthDD));

		eventMonthDD.click();

		WebElement month = driver
				.findElement(By.xpath("//div[@id='eventDate_popup_mddb_mdd']//div[text()='" + mo + "' and @month]"));
		wb.until(ExpectedConditions.visibilityOf(month));
		month.click();
		WebElement eventDate = driver
				.findElement(By.xpath("(//span[@class='dijitCalendarDateLabel' and text()='" + eventDateUp + "'])[1]"));
		wb.until(ExpectedConditions.visibilityOf(eventDate));

		eventDate.click();

		
	}

}
