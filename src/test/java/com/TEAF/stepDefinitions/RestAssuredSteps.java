package com.TEAF.stepDefinitions;

import static com.jayway.jsonpath.Criteria.where;
import static com.jayway.jsonpath.Filter.filter;
import static com.jayway.jsonpath.JsonPath.parse;
import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import com.google.api.gax.paging.Page;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.TEAF.Hooks.Hooks;
import com.TEAF.framework.API_UserDefined;
import com.TEAF.framework.D365;
import com.TEAF.framework.ElasticSearchJestUtility;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.RestAssuredUtility;
import com.TEAF.framework.Utilities;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.storage.Storage.Objects.Get;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.ReadChannel;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.FieldValue;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobId;
import com.google.cloud.bigquery.JobInfo;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableResult;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.ServiceAccount;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.Storage.BucketGetOption;
import com.google.cloud.storage.StorageOptions;
import com.jayway.jsonpath.Configuration;
import com.opencsv.CSVWriter;
//import com.shippo.Shippo;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.CucumberException;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class RestAssuredSteps {

	static RestAssuredUtility Rest = new RestAssuredUtility();
	static API_UserDefined ap = new API_UserDefined();
	static Logger log = Logger.getLogger(RestAssuredSteps.class.getName());
	
	@Given("^URL '(.*)'$")
	public static void URL(String url) {
		HashMapContainer.add("$$URL", url);
	}
	
	@Given("EndPoint {string}")
	public static void endPoint(String point) {
		String URL = HashMapContainer.get("$$URL");
		if (URL != null) {
			ap.setPath(URL + point);
		} else if (point.startsWith("$$")) {
			point = HashMapContainer.get(point);
			ap.setPath(point);
		} else {
			ap.setPath(point);
		}
	}

	public static Response getResponse() {
		return ap.getRes();
	}

	@When("Method {string}")
	public static void Method(String Method_Name) throws Exception {
		log.info("===========================" + Method_Name + "=========================");
		String Path = ap.getPath();
		log.info(Path);
		RequestSpecification spec = ap.getReq().log().all();
		Response GetResponse = null;
		if (Method_Name.equalsIgnoreCase("get")) {
			GetResponse = Rest.Get(Path, spec);
		} else if (Method_Name.equalsIgnoreCase("post")) {
			GetResponse = Rest.Post(Path, spec);

		} else if (Method_Name.equalsIgnoreCase("put")) {
			GetResponse = Rest.Put(Path, spec);

		} else if (Method_Name.equalsIgnoreCase("delete")) {
			GetResponse = Rest.Delete(Path, spec);
		} else if (Method_Name.contains("URLENCODINGDISABLED")) {
			GetResponse = Rest.GetWithURLEncodingFalse(Path, spec);
		} else {
			throw new Exception("Not a valid method");
		}
		ap.setRes(GetResponse);
		ap.setReq(given().when());

		Hooks.scenario.write("Response : \n" + GetResponse.getBody().asString());
	}

	@Then("^Statuscode '(.*)'$")
	public void statuscode(int Expected) {
		int Actual = ap.getRes().andReturn().statusCode();
		assertEquals("Status code Validation", Expected, Actual);
	}

	@Then("Match JSONPath {string} contains {string}")
	public void match_JSONPath_contains(String path, String Expected) {
		if (Expected.startsWith("$$")) {
			Expected = HashMapContainer.get(Expected);
		}

		String Actual = ap.getRes().getBody().asString();
		Object document = Configuration.defaultConfiguration().jsonProvider().parse(Actual);

		try {
			List<Object> actual;

			actual = com.jayway.jsonpath.JsonPath.read(document, path);
			if (actual.size() == 1) {
				String singledata = String.valueOf(actual.get(0));
				assertEquals(" Json Path Value Check ", Expected, singledata);

			}
			
			boolean contains = actual.contains(Expected);
			assertTrue(contains);

		} catch (java.lang.ClassCastException e) {
			String actual;

			actual = String.valueOf(com.jayway.jsonpath.JsonPath.read(document, path));
			assertEquals(" Json Path Value Check ", Expected, actual);

		}

	}

	@Then("Verify Value {string} present in field {string}")
	public void match_response_contains_the_data_expected(String Expected, String path) throws Exception {
//    String bdy = RestAssuredSteps.getResponse().getBody().asString();
		if (Expected.startsWith("$$")) {
			Expected = HashMapContainer.get(Expected);
		}
		System.out.println(Expected);
		if (Expected.length() > 0) {
			String keyValue = RestAssuredSteps.getResponse().getBody().jsonPath().get(path).toString();
			log.info("-------------------------------------------------------------------" + keyValue);
			Assert.assertEquals("Value did not match", Expected.toString(), keyValue.toString());
		}
	}
	
	@Then("Verify Value {string} present in field as String {string}")
	public void Verify_Value_present_in_field_as_String(String Expected, String path) throws Exception {
//	String bdy = RestAssuredSteps.getResponse().getBody().asString();

		String keyValue = RestAssuredSteps.getResponse().getBody().jsonPath().get(path).toString();
		System.out.println("-------------------------------------------------------------------" + keyValue);
		Assert.assertEquals("Value did not match", Expected, keyValue);
	}
	
	
	
	@Then("Match the response contains the key {string}")
	public void match_response_contains_the_data_expected(String Expected) throws Exception {
		String bdy = RestAssuredSteps.getResponse().getBody().asString();

		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(bdy);
		System.out.println(json.toString());
		if (json.containsKey(Expected)) {
			System.out.println("Key is present");
		} else {
			throw new Exception("Key " + Expected + " not Found in response body: " + bdy);
		}
	}
	
	public static Integer retuen_response_body_length() {
		String bdy = RestAssuredSteps.getResponse().getBody().asString();
		int bodyLength = bdy.length();
		return bodyLength;
	}

	@Then("^Match JSONPath with filter '(.*)' : '(.*)' equals '(.*)' contains '(.*)'$")
	public void match_JSONPath_filter_contains(String key, String value, String path, String Expected) {
		String Actual = ap.getRes().getBody().asString();

		com.jayway.jsonpath.Filter cheapFictionFilter = filter(where(key).is(value));

		try {
			List<String> actual;
			actual = parse(Actual).read(path, cheapFictionFilter);
			log.info(actual);
			assertEquals(" Json Path Value Check ", Expected, actual.toString());

		} catch (java.lang.ClassCastException e) {
			String actual;

			actual = parse(Actual).read(path, cheapFictionFilter);
			assertEquals(" Json Path Value Check ", Expected, actual);

		}

	}

	@Then("Match header {string} contains {string}")
	public void match_header_contains(String string, String Expected) {

		String Actual = ap.getRes().getHeader(string);
		if (Objects.equals(Actual, Expected))
			assertEquals("Header Matching", Expected, Actual);

	}

	@Then("^Print responsetime$")
	public void print_responsetime() {
		long temp = ap.getRes().getTime();
		log.info("Response Time" + temp + "ms");
	}

	@Then("Print response")
	public void print_response() {
		log.info("Response:" + ap.getRes().getBody().prettyPrint());
	}

	@Then("^Path '(.*)' is present in Response$")
	public void path_present_in_response(String path) {
		ResponseBody body = ap.getRes().getBody();
		JsonPath jsonPath = body.jsonPath();
		String obj;
		try {
			obj = jsonPath.get(path);
		} catch (Exception e) {
			obj = null;
		}
		Assert.assertNotNull(obj);

	}

	@Then("Print response to file {string}")
	public void print_response_to_file(String path) throws ClassNotFoundException, IOException, ParseException {
		String Response = ap.getRes().getBody().prettyPrint();
		File f = new File(path);
		boolean Exists = f.exists();
		if (Exists) {
			log.info("Printing response to File");
			this.write(f, Response);
		} else {
			f.createNewFile();
			log.info("New file created");
			this.write(f, Response);
		}

	}

	private void write(File f, String Response) {
		try {
			FileOutputStream f1 = new FileOutputStream(f);
			ObjectOutputStream o = new ObjectOutputStream(f1);
			o.writeObject(Response);
			o.close();
			f1.close();
		} catch (FileNotFoundException e) {
			log.error("File not found");
		} catch (IOException e) {
			log.error("Error initializing stream");
		}
	}

	@And("Match XMLPath {string} contains {string}")
	public void XMLPath(String xmlpath, String Expected) {

		XmlPath xml = new XmlPath(ap.getRes().asString());
		String Actual = xml.getString(xmlpath);
		assertEquals("Xml Path String check", Actual, Expected);

	}

	@And("Query params with Key {string} and value {string}")
	public static void updatequeryParams(String key, String value) {
		if (value.equals("SQL_API_Keys")) {
			List<String> values = HashMapContainer.getList("$$"+value);
			value = values.get(0);
		}else if (value.startsWith("$$")) {
			value = HashMapContainer.get(value);
		}else if (value.equals("TDIDs")) {
			List<String> values = HashMapContainer.getList("$$"+value);
			Object firstValue = values.get(0);
			if (firstValue instanceof Integer) {
				value = firstValue.toString();
			}
		}else if (value.equals("startDateAislelabsEOD")) {
			value = HashMapContainer.get("$$"+value)+"T05:00Z";
		}else if (value.equals("endDateAislelabsEOD")) {
			value = HashMapContainer.get("$$"+value)+"T04:59Z";
		}
		RequestSpecification res = ap.getReq().queryParam(key, value);
		ap.setReq(res);
	}

	@Given("Header key {string} value {string}")
	public void header_key_value(String string, String string2) throws Exception {
		if (string2.startsWith("@")) {
			String f = System.getProperty("user.dir") + "\\src\\test\\java\\com\\TEAF\\json\\" + string2.substring(1);
			string2 = FileUtils.readFileToString(new File(f), StandardCharsets.UTF_8);

		}
		if (string2.startsWith("$$")) {
			string2 = HashMapContainer.get(string2);
		}
		RequestSpecification spec = ap.getReq().header(string, string2.trim());
		ap.setReq(spec);
	}
	
	@Given("Header key {string}")
	public void header_key_value_property(String Key) throws Exception {
//	 Utilities util=new Utilities();
	 String value =Utilities.getProperties(Key);
	 String[] keyname = Key.split("_");
	 if(keyname[2]!=null) {
		RequestSpecification spec = ap.getReq().header(keyname[1]+"_"+keyname[2], value.trim());
		ap.setReq(spec);}
	 else {
		 RequestSpecification spec = ap.getReq().header(keyname[1], value.trim());
			ap.setReq(spec);
	 }
	}

	@Given("^Adding Path Param Name '(.*)' and value '(.*)'$")
	public void adding_path_param(String path, String value) {
		if (value.startsWith("$$")) {
			value = HashMapContainer.get(value);
		}
		RequestSpecification requestSpecification = ap.getReq().pathParam(path, value);
		ap.setReq(requestSpecification);
	}

	@Given("Content type {string}")
	public static void content_type(String string) {
		RequestSpecification spec = ap.getReq().contentType(string);
		ap.setReq(spec);
	}

	@Given("Request body {string}")
	public void request_body(String string) throws IOException {
		RequestSpecification spec;
		if (string.startsWith("@")) {
			spec = ap.getReq().body(string.substring(1));
			Hooks.scenario.write(string.substring(1));
		} else if(string.startsWith("$$")){
			spec = ap.getReq().body(HashMapContainer.get(string));
			Hooks.scenario.write(string.substring(1));
		} else {
			File f = new File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\TEAF\\json\\" + string + ".json");
			spec = ap.getReq().body(f);
			byte[] readAllBytes = Files.readAllBytes(Paths
					.get(System.getProperty("user.dir") + "\\src\\test\\java\\com\\TEAF\\json\\" + string + ".json"));
			String s = new String(readAllBytes);
			Hooks.scenario.write(s);

		}

		ap.setReq(spec);

	}

	@Given("Request body using JSON object {string}")
	public static void request_body_json_object(String string) throws IOException {
		RequestSpecification spec = ap.getReq().body(string);
		ap.setReq(spec);
		Hooks.scenario.write(string);
	}

	@Given("Authorization Bearer {string}")
	public void authBearerToken(String string) throws IOException {
		if (string.startsWith("$$")) {
			string = HashMapContainer.get(string);
			log.info(string);
		}
		if (string.startsWith("@")) {
			String f = System.getProperty("user.dir") + "\\src\\test\\java\\com\\TEAF\\json\\" + string.substring(1);
			string = FileUtils.readFileToString(new File(f), StandardCharsets.UTF_8);

		}
		RequestSpecification header = ap.getReq().header("Authorization", "Bearer " + string);
		ap.setReq(header);
	}

	@When("^Retrieve data '(.*)' from response and store in variable '(.*)'$")
	public void retrivedata_store(String key, String store) throws Exception {
		Response res = ap.getRes();
		RestAssuredUtility.retriveJSONforPOSTRequest(res, key, store);
	}
	
	@Then ("^Retrieve data '(.*)' from response and store in list variable '(.*)'$")
	public void retrivedata_store_list_variable(String key, String store) throws Exception {
		Response res = ap.getRes();
		RestAssuredUtility.retriveValuesAndStoreListValues(res, key, store);
	}

	@When("^Retrieve data '(.*)' from object index '(.*)' in response and store in variable '(.*)'$")
	public void retrivedata_store_withobject_index(String key, String index, String store) throws Exception {
		Response res = ap.getRes();
		RestAssuredUtility.retriveJSONforPOSTRequestwithObjectIndex(res, key, store, index);
	}

	@When("^Update data in the Request '(.*)' for key '(.*)' with data '(.*)'$")
	public static void update_String_data_request(String f, String key, String store) throws Exception {
		File ft = new File(System.getProperty("user.dir") + "\\src/test/java/com/TEAF/json/" + f + ".json");
		RestAssuredUtility.updateJSONforPOSTRequest(ft, key, store);
	}

	@When("Update data in the Request {string} for key {string} with Array data {string}")
	public static void update_array_data_request(String f, String key, String store) throws Exception {
		File ft = new File(System.getProperty("user.dir") + "\\src/test/java/com/TEAF/json/" + f + ".json");
		RestAssuredUtility.updateJSONforPOSTRequest_arrayData(ft, key, store);
	}

	public static void main(String[] args) throws Exception {
//		update_array_data_request("Test", "$.query.bool.must[0].terms[\"ultimateMasterCustomerCode.keyword\"]",
//				"[\"AX-C004937320\",\"AX-C006899579\"]");
//		HashMap<String, String> hm = new HashMap<String, String>();
//		hm.put("carrier", "FedEx");
//		hm.put("tracking_number", "152911228169");
//		JSONObject jb = new JSONObject();
//		jb.toString("carrier", "FedEx");
//		jb.toString("tracking_number", "152911228169");
//		RestAssured as = new RestAssured();
		
//		Shippo.setApiKey("<API_TOKEN>");
		
		String body = "{\"carrier\": \"FedEx\",\"tracking_number\": \"152911228169\"}";
		Response res = given().relaxedHTTPSValidation().contentType("application/json").header("Authirization","ShippoToken shippo_live_990c64b86a7b8ed4050d36692f9331dcce5e7e24")
		.body(body).post("https://api.goshippo.com:443/tracks/");
		System.out.println("res "+ res.prettyPrint());
		System.out.println(res.getStatusCode());
		
		
	}

	@When("^Key '(.*)' count present in the response - '(.*)'$")
	public void key_count_present_in_the_response(String key, String count) {
		List<String> read = com.jayway.jsonpath.JsonPath.read(ap.getRes().getBody().asString(), key);
		Assert.assertEquals(Integer.parseInt(count), read.size());
	}

	@Then("^Response Key '(.*)' length matches '(.*)'$")
	public void response_key_length(String key, int Expected) {
		Object length = com.jayway.jsonpath.JsonPath.read(ap.getRes().getBody().asString(), key + ".length()");
		log.info("Length " + length);
		Assert.assertEquals(Expected, length);
	}
	
	@And("^Basic authorization with '(.*)' and '(.*)'$")
	public void basic_auth_header(String username1, String password1) {
		RequestSpecification spec = ap.getReq().auth().preemptive().basic(username1, password1);
		ap.setReq(spec);
	}
	
	@Given("API EndPoint {string}")
	public static void apiEndPoint(String point) throws IOException {
		String url_prop = Utilities.getURL(point);
		String URL = HashMapContainer.get("$$URL");
		if (URL != null) {
			ap.setPath(URL + url_prop);
		} else {
			ap.setPath(url_prop);
		}
	}
	
	@Given("Authorization BasicAuth {string} {string}")
	public void authBasicAuth(String username, String password) throws IOException {
		RequestSpecification header = ap.getReq().given().auth().preemptive().basic(username, password).accept("*/*");
		ap.setReq(header);
	}
	
	@When("^Retrieve data '(.*)' from XML response and store in variable '(.*)'$")
	public void retriveXMLData(String key, String store) throws Exception {
		Response res = ap.getRes();
		RestAssuredUtility.retriveDataFromXMLResponse(res, key, store);
	}
	
	@When("^Retrieve data '(.*)' from XML response and store in string variable '(.*)'$")
	public void retriveXMLDataInString(String key, String store) throws Exception {
		Response res = ap.getRes();
		RestAssuredUtility.retriveDataFromXMLResponseInString(res, key, store);
	}
	
	@When("^Retrieve list of data '(.*)' from JSON response and store in variable '(.*)'$")
	public void retriveJSONDataList(String key, String store) throws Exception {
		RestAssuredUtility.retriveDataFromJSONResponse(key, store);
	}
	
	@When("^Retrieve list of unique data '(.*)' from JSON response and store in variable '(.*)'$")
	public void retriveUniqueJSONDataList(String key, String store) throws Exception {
		RestAssuredUtility.retriveUniqueDataFromJSONResponse(key, store);
	}
	
	@Then("^Add '(.*)' and '(.*)' into a single list variable '(.*)'$")
	public void AddTwoHashmpaListValuesIntoSingleString(String hashmaplist1, String hashmaplist2, String hashmaplist3) {
		List<String> list1 = HashMapContainer.getList("$$"+hashmaplist1);
		List<String> list2 = HashMapContainer.getHashSetList("$$"+hashmaplist2);
		HashSet<String> hashset1 = new HashSet<String>();
		hashset1.addAll(list1);
		hashset1.addAll(list2);
		List<String> list3 = new ArrayList<String>();
		list3.addAll(hashset1);
		HashMapContainer.addList("$$"+hashmaplist3, list3);
//		System.out.println("list3"+list3);
	}
	
	@Then("Verify {string} should be zero")
	public void verifyTotalCountIsZERO(String key) {
		if (key.contains("$$")) {
			List<String> sourceList = HashMapContainer.getList(key);
			Assert.assertTrue(sourceList.isEmpty());

		} else {
			int value = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), key);
			Assert.assertTrue(value == 0);
		}

	}
	
	@Then("Retrieve data '(.*)' from response with array and store in respective key variables")
	public void Retrieve_data__from_response_with_array_and_store_in_respective_key_variables(String key) {
		Response res = ap.getRes();
		RestAssuredUtility.retriveArrayDataFromJSONResponse(res, key);
		RestAssuredUtility.createHashMapForChildArrays(res);
	}
	
	D365 d365 = new D365();
	
	@Given("Contruct request payload for DBC with params {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void contruct_request_payload_for_DBC_with_params(String unique_id ,String Email, String Guestlastname ,String LeadSource, String AssociateName ,String StoreID ,String PreferredContact, String Lastname ,String Guestemail, String Firstname, String AssociateEmail, String DetailsOffer, String ShoppingFor ,String Guestphone, String AssociateADID ,String Guestfirstname, String PostalCode) throws ParseException, org.json.simple.parser.ParseException {
		// Write code here that turns the phrase above into concrete actions
		RequestSpecification spec;
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(100000000);
		Email="email"+randomInt+"@testing.com";
		randomInt = randomGenerator.nextInt(100000000);
		Guestemail="guestemail"+randomInt+"@testing.com";
		randomInt = randomGenerator.nextInt(100000000);
		AssociateEmail="associateemail"+randomInt+"@testing.com";
		String request="{\r\n" + 
				"  \"unique_id\": \""+unique_id+"\",\r\n" + 
				"  \"Email\": \""+Email+"\",\r\n" + 
				"  \"Guestlastname\": \""+Guestlastname+"\",\r\n" + 
				"  \"LeadSource\": \""+LeadSource+"\",\r\n" + 
				"  \"AssociateName\": \""+AssociateName+"\",\r\n" + 
				"  \"StoreID\": \""+StoreID+"\",\r\n" + 
				"  \"PreferredContact\": \""+PreferredContact+"\",\r\n" + 
				"  \"Lastname\": \""+Lastname+"\",\r\n" + 
				"  \"LeadDate\": \"2020-09-29T19:03:16.000Z\",\r\n" + 
				"  \"Guestemail\": \""+Guestemail+"\",\r\n" + 
				"  \"Firstname\": \""+Firstname+"\",\r\n" + 
				"  \"AssociateEmail\": \""+AssociateEmail+"\",\r\n" + 
				"  \"DetailsOffer\": \""+DetailsOffer+"\",\r\n" + 
				"  \"ShoppingFor\": \""+ShoppingFor+"\",\r\n" + 
				"  \"Guestphone\": \""+Guestphone+"\",\r\n" + 
				"  \"AssociateADID\": \""+AssociateADID+"\",\r\n" + 
				"  \"Guestfirstname\": \""+Guestfirstname+"\",\r\n" + 
				"  \"PostalCode\": \""+PostalCode+"\",\r\n" + 
				"}";
		d365.setEmail(Email);
		d365.setFirstname(Firstname);
		d365.setLastname(Lastname);
		d365.setAssociateName(AssociateName);
		d365.setAssociateEmail(AssociateEmail);
		d365.setAssociateADID(AssociateADID);
		d365.setDetailsOffer(DetailsOffer);
		d365.setGuestemail(Guestemail);
		d365.setGuestfirstname(Guestfirstname);
		d365.setGuestlastname(Guestlastname);
		d365.setGuestphone(Guestphone);
		d365.setLeadSource(LeadSource);
		d365.setPostalCode(PostalCode);
		d365.setShoppingFor(ShoppingFor);
		d365.setPreferredContact(PreferredContact);
		d365.setStoreID(StoreID);
		d365.setLeadDate("2020-09-29T19:03:16.000Z");
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(request);
		spec = ap.getReq().body(json);
		
		Hooks.scenario.write(json.toString());

	ap.setReq(spec);
	}
	@Given("Contruct request payload for salesforce dynamic query DBC")
	public void Contruct_request_payload_for_salesforce_dynamic_query_DBC() throws ParseException, org.json.simple.parser.ParseException {
		RequestSpecification spec;
		String request ="{\r\n" + 
				"    \"ObjectTableName\": \"Contact\",\r\n" + 
				"    \"WhereClause\": \"email = '"+d365.getEmail()+"' order by LastModifiedDate desc \",\r\n" + 
				"    \"LimitCount\": \"2500\",\r\n" + 
				"    \"Columns\": [\r\n" + 
				"        \"FirstName\",\r\n" + 
				"        \"LastName\",\r\n" + 
				"        \"Email\",\r\n" + 
				"        \"MFRM_Lead_Date__c\",\r\n" + 
				"        \"OtherPostalCode\",\r\n" + 
				"        \"MFRM_Lead_Source__c\",\r\n" + 
				"        \"RecordTypeId\",\r\n" + 
				"        \"DWC_Eligible_for_Email__c\",\r\n" + 
				"        \"DWC_Guest_Email__c\",\r\n" + 
				"        \"DWC_Guest_First_Name__c\",\r\n" + 
				"        \"DWC_Guest_Last_Name__c\",\r\n" + 
				"        \"DWC_Guest_Phone__c\",\r\n" + 
				"        \"DWC_Preferred_Method_Email__c\",\r\n" + 
				"        \"DWC_Preferred_Method_Phone__c\",\r\n" + 
				"        \"DWC_RSA_Eligible_for_Email__c\",\r\n" + 
				"        \"DWC_Shopping_For__c\",\r\n" + 
				"        \"DWC_Store_ID__c\",\r\n" + 
				"        \"DWC_Timestamp__c\",\r\n" + 
				"        \"DWC_Associate_ADID__c\",\r\n" + 
				"        \"DWC_Associate_Email__c\",\r\n" + 
				"        \"DWC_Associate_Name__c\",\r\n" + 
				"        \"DWC_Details_Offer__c\"\r\n" + 
				"    ]\r\n" + 
				"}";
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(request);
		spec = ap.getReq().body(json);
		
		Hooks.scenario.write(json.toString());


	ap.setReq(spec);
	}

	@Then("Verify the data is inserted correctly in Salesforce response")
	public void verifyDataAgainstDBSAndSalesforce() {
		List<String> Email = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..Email");
		List<String> DWC_Preferred_Method_Email__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..DWC_Preferred_Method_Email__c");
		List<String> DWC_Details_Offer__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..DWC_Details_Offer__c");
		List<String> FirstName = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..FirstName");
		List<String> OtherPostalCode = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..OtherPostalCode");
		List<String> DWC_Associate_Name__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..DWC_Associate_Name__c");
		List<String> DWC_RSA_Eligible_for_Email__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..DWC_RSA_Eligible_for_Email__c");
		List<String> DWC_Preferred_Method_Phone__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..DWC_Preferred_Method_Phone__c");
		List<String> type = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..type");
		List<String> MFRM_Lead_Source__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..MFRM_Lead_Source__c");
		List<String> DWC_Guest_Phone__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..DWC_Guest_Phone__c");
		List<String> DWC_Guest_First_Name__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..DWC_Guest_First_Name__c");
		List<String> DWC_Store_ID__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..DWC_Store_ID__c");
		List<String> MFRM_Lead_Date__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..MFRM_Lead_Date__c");
//		List<String> DWC_Timestamp__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
//				.read("$..DWC_Timestamp__c");
		List<String> DWC_Associate_Email__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..DWC_Associate_Email__c");
		List<String> DWC_Eligible_for_Email__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..DWC_Eligible_for_Email__c");
		List<String> DWC_Shopping_For__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..DWC_Shopping_For__c");
		List<String> RecordTypeId = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..RecordTypeId");
		List<String> LastName = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..LastName");
		List<String> DWC_Associate_ADID__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..DWC_Associate_ADID__c");
		List<String> DWC_Guest_Last_Name__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..DWC_Guest_Last_Name__c");
		List<String> DWC_Guest_Email__c = com.jayway.jsonpath.JsonPath.parse(ap.getRes().getBody().prettyPrint())
				.read("$..DWC_Guest_Email__c");
	
		Assert.assertEquals(d365.getEmail(), Email.get(0));
		if (d365.getPreferredContact() == "email") {
			Assert.assertEquals(DWC_Preferred_Method_Email__c, "true");
		} else if (d365.getPreferredContact() == "phone") {
			Assert.assertEquals(DWC_Preferred_Method_Email__c, "false");
			Assert.assertEquals(DWC_Preferred_Method_Phone__c, "true");
			
		}
		Assert.assertEquals(d365.getDetailsOffer(), DWC_Details_Offer__c.get(0));
		Assert.assertEquals(d365.getFirstname(), FirstName.get(0));
		Assert.assertEquals(d365.getPostalCode(), OtherPostalCode.get(0));
		Assert.assertEquals(d365.getAssociateName(), DWC_Associate_Name__c.get(0));
		Assert.assertEquals(DWC_RSA_Eligible_for_Email__c.get(0),"true");
		Assert.assertEquals(type.get(0),"Contact");
		Assert.assertEquals(d365.getGuestemail(), DWC_Guest_Email__c.get(0));
		Assert.assertEquals(d365.getLeadSource(), MFRM_Lead_Source__c.get(0));
		Assert.assertEquals(d365.getGuestphone(), DWC_Guest_Phone__c.get(0));
		Assert.assertEquals(d365.getStoreID(), DWC_Store_ID__c.get(0));
		Assert.assertEquals(d365.getLeadDate(), MFRM_Lead_Date__c.get(0));
		Assert.assertEquals(d365.getAssociateEmail(), DWC_Associate_Email__c.get(0));
		Assert.assertEquals(DWC_Eligible_for_Email__c.get(0), "true");
		Assert.assertEquals(d365.getLastname(), LastName.get(0));
		Assert.assertEquals(d365.getAssociateADID(), DWC_Associate_ADID__c.get(0));
		Assert.assertEquals(d365.getGuestlastname(), DWC_Guest_Last_Name__c.get(0));
		Assert.assertEquals(d365.getGuestfirstname(), DWC_Guest_First_Name__c.get(0));
		Assert.assertEquals(d365.getGuestfirstname(), DWC_Guest_First_Name__c.get(0));
		Assert.assertEquals(d365.getShoppingFor(), DWC_Shopping_For__c.get(0));
		Assert.assertEquals("012G0000001QVPXIA4", RecordTypeId.get(0));
		
	}

}
