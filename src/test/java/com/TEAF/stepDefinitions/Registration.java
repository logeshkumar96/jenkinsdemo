package com.TEAF.stepDefinitions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;
import com.TEAF.customUtilities.ECom_Exception;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.CucumberException;

public class Registration {

	public static WebDriver driver = StepBase.getDriver();
	static Utilities util = new Utilities();
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Registration.class.getName());

	@When("^I store Firstname,Lastname,PhoneNo,Password,citY,zipcode,EventDate for new registered user$")
	public static void i_store_registration_details() throws ECom_Exception, Exception {
		
		String methodName= "i_store_registration_details";

		try {
			String firstname = driver
					.findElement(
							By.xpath("//input[@id=\"CV_DBIConsolidatedRegForm_NameEntryForm_FormInput_firstName_1\"]"))
					.getAttribute("value");
			String lastname = driver
					.findElement(
							By.xpath("//input[@id=\"CV_DBIConsolidatedRegForm_NameEntryForm_FormInput_lastName_1\"]"))
					.getAttribute("value");
			String PhoneNo = driver
					.findElement(
							By.xpath("//input[@id=\"CV_DBIConsolidatedRegForm_FormInput_phoneNum_In_Register_1\"]"))
					.getAttribute("value");
			String PwdField = driver
					.findElement(By
							.xpath("//input[@id=\"CV_DBIConsolidatedRegForm_FormInput_logonPassword_In_Register_1\"]"))
					.getAttribute("value");
			String City = driver
					.findElement(By
							.xpath("//input[@id=\"WC_UserRegistrationUpdateForm_AddressEntryForm_FormInput_city_1\"]"))
					.getAttribute("value");
			String zip = driver
					.findElement(By.xpath(
							"//input[@id=\"WC_UserRegistrationUpdateForm_AddressEntryForm_FormInput_zipCode_1\"]"))
					.getAttribute("value");
			String State = driver
					.findElement(By.xpath(
							"//select[@id=\"WC_UserRegistrationUpdateForm_AddressEntryForm_FormInput_state_1\"]"))
					.getAttribute("value");
			WebElement EventroleW = driver.findElement(By.xpath("//select[@id=\"WC_UserRegistrationUpdateExt_div_3\"]"));
			Select eventRoleS = new Select(EventroleW);
			String Eventrole = eventRoleS.getFirstSelectedOption().getText();
					
			String email = driver
					.findElement(
							By.xpath("//input[@id=\"CV_DBIConsolidatedRegForm_FormInput_logonId_In_Register_1_1\"]"))
					.getAttribute("value");

			java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));

			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheetAt = wb.getSheet("Registration");
			for (int j = 1; j <= sheetAt.getPhysicalNumberOfRows(); j++) {
				if (sheetAt.getRow(j) == null) {
					continue;
				}

				String emailId = sheetAt.getRow(j).getCell(0).getStringCellValue();
				System.out.println("email id " + email);
				System.out.println("email id stored " + HashMapContainer.get("$$RegisteredEmailId"));
				// System.out.println(HashMapContainer.get("$$EmailIdAppointment"));
				if (emailId.toLowerCase().trim().toLowerCase()
						.equals(HashMapContainer.get("$$RegisteredEmailId").toLowerCase())) {
					System.out.println("Email id matched "+ firstname+ lastname);
					sheetAt.getRow(j).createCell(1).setCellValue(firstname);
					sheetAt.getRow(j).createCell(2).setCellValue(lastname);
					sheetAt.getRow(j).createCell(3).setCellValue(PwdField);
					sheetAt.getRow(j).createCell(4).setCellValue(PhoneNo);
					sheetAt.getRow(j).createCell(5).setCellValue(City);
					sheetAt.getRow(j).createCell(6).setCellValue(zip);
					sheetAt.getRow(j).createCell(7).setCellValue(State);
					sheetAt.getRow(j).createCell(8).setCellValue(Eventrole);

				}
			}

			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@When("^I store Firstname,Lastname,PhoneNo,Password,citY,zipcode for favorite or RealWedding registered user$")
	public static void i_store_Favorite_registration_details() throws ECom_Exception, Exception {
		
		String methodName= "i_store_Favorite_registration_details";

		try {
			String firstname = driver.findElement(By.xpath("//input[@autocomplete=\"given-name\"]"))
					.getAttribute("value");
			String lastname = driver.findElement(By.xpath("//input[@autocomplete=\"family-name\"]"))
					.getAttribute("value");
			String PhoneNo = driver.findElement(By.xpath("//input[@autocomplete=\"tel\"]")).getAttribute("value");
			String PwdField = driver
					.findElement(
							By.xpath("//div[@id=\"ctnr-address-section-orig-col\"]//input[@name=\"logonPassword\"]"))
					.getAttribute("value");
			String City = driver.findElement(By.xpath("//input[@autocomplete=\"address-level2\"]"))
					.getAttribute("value");
			String zip = driver.findElement(By.xpath("//input[@autocomplete=\"postal-code\"]")).getAttribute("value");
			String State = driver.findElement(By.xpath("//div[@id=\"stateDiv\"]//select")).getAttribute("value");
			String email = driver
					.findElement(By.xpath("//div[@class=\"ctnr-field-input\"]//input[@autocomplete=\"email\"]"))
					.getAttribute("value");

			java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));

			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheetAt = wb.getSheet("Registration");
			for (int j = 1; j <= sheetAt.getPhysicalNumberOfRows(); j++) {
				if (sheetAt.getRow(j) == null) {
					continue;
				}

				String emailId = sheetAt.getRow(j).getCell(0).getStringCellValue();
				System.out.println("email id " + email);
				System.out.println("email id stored " + HashMapContainer.get("$$RegisteredEmailId"));
				// System.out.println(HashMapContainer.get("$$EmailIdAppointment"));
				if (emailId.toLowerCase().trim().toLowerCase()
						.equals(HashMapContainer.get("$$RegisteredEmailId").toLowerCase())) {
					System.out.println("Email id matched "+ firstname+ lastname);
					sheetAt.getRow(j).createCell(1).setCellValue(firstname);
					sheetAt.getRow(j).createCell(2).setCellValue(lastname);
					sheetAt.getRow(j).createCell(3).setCellValue(PwdField);
					sheetAt.getRow(j).createCell(4).setCellValue(PhoneNo);
					sheetAt.getRow(j).createCell(5).setCellValue(City);
					sheetAt.getRow(j).createCell(6).setCellValue(zip);
					sheetAt.getRow(j).createCell(7).setCellValue(State);

				}
			}

			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@When("^I store Firstname,Lastname,PhoneNo,citY,zipcode,Eventrole for '(.*)' registered user$")
	public static void i_store_UK_CA_registration_details(String site) throws ECom_Exception, Exception {
		
		String methodName= "i_store_UK_CA_registration_details";

		try {
			String firstname = driver.findElement(By.name("firstName")).getAttribute("value");
			String lastname = driver.findElement(By.name("lastName")).getAttribute("value");
			String PhoneNo = driver.findElement(By.xpath("//input[@name=\"phone1\"]")).getAttribute("value");
			String City = driver.findElement(By.xpath("//input[@name=\"city\"]")).getAttribute("value");
			String zip = driver.findElement(By.xpath("//input[@name=\"zipCode\"]")).getAttribute("value");
			String state = null;
			if(site.equals("UK")) {
				 state = driver.findElement(By.xpath("//input[@name=\"state\"]")).getAttribute("value");
			}else {
			state = driver.findElement(By.xpath("//div[@id=\"stateDiv\"]//select")).getAttribute("value");
			}
			String email = driver.findElement(By.xpath("//input[@id=\"email1\"]")).getAttribute("value");
			String Eventrole = driver.findElement(By.xpath("//select[@id=\"roleName\"]")).getAttribute("value");

			java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));

			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheetAt = wb.getSheet("Registration");
			for (int j = 1; j <= sheetAt.getPhysicalNumberOfRows(); j++) {
				if (sheetAt.getRow(j) == null) {
					continue;
				}

				String emailId = sheetAt.getRow(j).getCell(0).getStringCellValue();
				System.out.println("email id " + email);
				System.out.println("email id stored " + HashMapContainer.get("$$RegisteredEmailId"));
				// System.out.println(HashMapContainer.get("$$EmailIdAppointment"));
				if (emailId.toLowerCase().trim().toLowerCase()
						.equals(HashMapContainer.get("$$RegisteredEmailId").toLowerCase())) {
					System.out.println("Email id matched "+ firstname+ lastname);
					sheetAt.getRow(j).createCell(1).setCellValue(firstname);
					sheetAt.getRow(j).createCell(2).setCellValue(lastname);
					sheetAt.getRow(j).createCell(4).setCellValue(PhoneNo);
					sheetAt.getRow(j).createCell(5).setCellValue(City);
					sheetAt.getRow(j).createCell(6).setCellValue(zip);
					sheetAt.getRow(j).createCell(7).setCellValue(state);
					sheetAt.getRow(j).createCell(8).setCellValue(Eventrole);

				}
			}

			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}
	
	@Then("^I generate random email id '(.*)'$")
	public static void i_generate_random_email_id(String Locator) throws Throwable {
		
		String methodName= "i_generate_random_email_id";

		try {
			Actions ac = new Actions(driver);
			WebElement emailTextBx = driver.findElement(GetPageObjectRead.OR_GetElement(Locator));
			emailTextBx.clear();
			ac.click(emailTextBx);
//		JavascriptExecutor executor = (JavascriptExecutor) driver;
//		executor.executeScript("arguments[0].click();", emailTextBx);
			Thread.sleep(1000);
//		emailTextBx.click();
			Random randomGenerator = new Random();
			int randomInt = randomGenerator.nextInt(100000000);
			String Emailid = "QA_Automation" + randomInt + "@gmail.com";
			emailTextBx.sendKeys(Emailid);
			System.out.println("Email id for new user " + "QA_Automation" + randomInt + "@gmail.com");
			if (!Locator.equals("FianceemailSignUp")) {
				HashMapContainer.add("$$RegisteredEmailId", "QA_Automation" + randomInt + "@gmail.com");
				java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));

				FileInputStream fin = new FileInputStream(f);
				Workbook wb = new XSSFWorkbook(fin);
				Sheet sheetAt = wb.getSheet("Registration");
				int prows = sheetAt.getPhysicalNumberOfRows();
				sheetAt.createRow(prows).createCell(0).setCellValue(Emailid);
				FileOutputStream fout = new FileOutputStream(f);
				wb.write(fout);
				wb.close();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}
}
