package com.TEAF.stepDefinitions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.TEAF.Hooks.Hooks;
import com.TEAF.customUtilities.ECom_Exception;
import com.TEAF.customUtilities.LoadCustomConfig;
import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class OrderPlacement {
	

	public static WebDriver driver = StepBase.getDriver();
	static Utilities util = new Utilities();
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(OrderPlacement.class.getName());

	@Then("^I should select edit based on credit card address$")
	public static void i_should_select_edit_based_on_credit_card_address() throws Exception {
		
		String methodName= "i_should_select_edit_based_on_credit_card_address";

		try {
			String locator = "//label[@for='davidsbridal123@dbi.com']//..//..//a";
			WebElement addressElement = driver.findElement(By.xpath(locator));
			// log.info("Found");
			WrapperFunctions.highLightElement(addressElement);
			addressElement.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	@When("^I click on accept acknowledgement check box$")
	public static void i_click_on_accept_acknowledgement() throws ECom_Exception, Exception {
		
		String methodName= "i_click_on_accept_acknowledgement";

		try {
			try {
				WebElement confirmBox = driver.findElement(By.id("SO_confirm_box"));
				WebDriverWait wb = new WebDriverWait(driver, 10);
				wb.until(ExpectedConditions.visibilityOf(confirmBox));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", confirmBox);
//			confirmBox.click();
			} catch (Exception e) {
				log.info("Accept Acknowledgement is not present");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	@When("^I select payment method '(.*)' and enter the card details with CVV number$")
	public static void i_select_payment_method_and_enter_card_details_with_cvv_number(String cardMethod)
			throws Exception {
		
		String methodName= "i_select_payment_method_and_enter_card_details_with_cvv_number '"+cardMethod+"'";

		try {
			LoadCustomConfig lc = new LoadCustomConfig();
			CommonSteps.I_should_see_on_page("PaymentSection");
			if (cardMethod.equalsIgnoreCase("visa")) {
				String property = lc.getProperty("test.VISA");
				String[] splitKey = property.split("-");
				Hooks.scenario.write(splitKey[0]);
				CommonSteps.I_enter_in_field(splitKey[0], "CreditCardField");
				CommonSteps.I_enter_in_field(splitKey[1], "CVVField");
			} else if (cardMethod.equalsIgnoreCase("dbcc")) {
				String property = lc.getProperty("test.DBCC");
				String[] splitKey = property.split("-");
				Hooks.scenario.write(splitKey[0]);
				CommonSteps.I_enter_in_field(splitKey[0], "CreditCardField");
				CommonSteps.I_click("ExpireDateText");
				Thread.sleep(2000);
				// CommonSteps.I_enter_in_field(splitKey[1], "CVVField");
			} else if (cardMethod.equalsIgnoreCase("master")) {
				String property = lc.getProperty("test.MASTER");
				String[] splitKey = property.split("-");
				Hooks.scenario.write(splitKey[0]);
				CommonSteps.I_enter_in_field(splitKey[0], "CreditCardField");
				CommonSteps.I_enter_in_field(splitKey[1], "CVVField");
			} else if (cardMethod.equalsIgnoreCase("discover")) {
				String property = lc.getProperty("test.DISCOVER");
				String[] splitKey = property.split("-");
				Hooks.scenario.write(splitKey[0]);
				CommonSteps.I_enter_in_field(splitKey[0], "CreditCardField");
				CommonSteps.I_enter_in_field(splitKey[1], "CVVField");
			} else if (cardMethod.equalsIgnoreCase("diners")) {
				String property = lc.getProperty("test.DINERS");
				String[] splitKey = property.split("-");
				Hooks.scenario.write(splitKey[0]);
				CommonSteps.I_enter_in_field(splitKey[0], "CreditCardField");
				CommonSteps.I_enter_in_field(splitKey[1], "CVVField");
			} else if (cardMethod.equalsIgnoreCase("amex")) {
				String property = lc.getProperty("test.AMEX");
				String[] splitKey = property.split("-");
				Hooks.scenario.write(splitKey[0]);
				CommonSteps.I_enter_in_field(splitKey[0], "CreditCardField");
				CommonSteps.I_enter_in_field(splitKey[1], "CVVField");
			} else {
				throw new Exception("Invalid Card type");
			}

			CommonSteps.I_clickJS("SharedShipBillAddressCheckbox");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@When("^I click on shipping delivery '(.*)'$")
	public static void i_click_on_shipping_delivery(String value) throws ECom_Exception, Exception {
		
		String methodName= "i_click_on_shipping_delivery '"+value+"'";

		WebDriverWait wb = new WebDriverWait(driver, 40);
		try {
			WebElement shipoption = driver.findElement(By.xpath("//label[contains(@for,'" + value + "')]//..//input"));
			wb.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOf(shipoption));
			shipoption.click();
		} catch (Exception e) {
			CommonSteps.I_should_see_on_page("CntnueBtnShipMethod");
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@When("^I select size '(.*)' for the product$")
	public static void i_select_color_in_pdp(String size) throws ECom_Exception, Exception {
		
		String methodName= "i_select_color_in_pdp '"+size+"'";

		try {
			WebElement selectSize = driver.findElement(By.xpath("//label[@for='detail-size--" + size + "']"));
			selectSize.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@When("^I login the paypal account with valid email and password$")
	public static void i_login_the_paypal_with_valid_email_password() throws ECom_Exception, Exception {
		
	
		WebDriverWait wb = new WebDriverWait(driver, 5);
			try {
				
				try {
					CommonSteps.i_scroll_through_top_page();
					wb.until(ExpectedConditions
							.visibilityOf(driver.findElement(GetPageObjectRead.OR_GetElement("LoginBtn_Paypal"))));
					CommonSteps.I_clickJS("LoginBtn_Paypal");
					wb.until(ExpectedConditions
							.visibilityOf(driver.findElement(GetPageObjectRead.OR_GetElement("EmailFieldPaypal"))));
					CommonSteps.I_should_see_on_page("EmailFieldPaypal");
					CommonSteps.i_clear_and_enter_the_text("pranee_1358913423_per@dbi.com", "EmailFieldPaypal");
					CommonSteps.I_click("NextBtn");
					CommonSteps.I_wait_for_visibility_of_element("PwdFieldPaypal");
					CommonSteps.I_should_see_on_page("PwdFieldPaypal");
					CommonSteps.I_enter_in_field("12345678", "PwdFieldPaypal");
					CommonSteps.I_click("LoginPaypal");
					CommonSteps.I_should_see_on_page("PayPalUserName");
				} catch (Exception e) {
					
					CommonSteps.i_scroll_through_top_page();
					wb.until(ExpectedConditions
							.visibilityOf(driver.findElement(GetPageObjectRead.OR_GetElement("LoginBtn_Paypal"))));
					CommonSteps.I_click("LoginBtn_Paypal");
					wb.until(ExpectedConditions
							.visibilityOf(driver.findElement(GetPageObjectRead.OR_GetElement("EmailFieldPaypal"))));
					CommonSteps.I_should_see_on_page("EmailFieldPaypal");
					CommonSteps.i_clear_and_enter_the_text("pranee_1358913423_per@dbi.com", "EmailFieldPaypal");
					CommonSteps.I_should_see_on_page("PwdFieldPaypal");
					CommonSteps.I_enter_in_field("12345678", "PwdFieldPaypal");
					CommonSteps.I_click("LoginPaypal");
					CommonSteps.I_should_see_on_page("PayPalUserName");
				} 
				
			} catch (Exception e) {
				
				try {
					
					
					wb.until(ExpectedConditions
							.visibilityOf(driver.findElement(GetPageObjectRead.OR_GetElement("EmailFieldPaypal"))));
					CommonSteps.I_should_see_on_page("EmailFieldPaypal");
					CommonSteps.i_clear_and_enter_the_text("pranee_1358913423_per@dbi.com", "EmailFieldPaypal");
						wb.until(ExpectedConditions
								.visibilityOf(driver.findElement(GetPageObjectRead.OR_GetElement("NextBtn"))));
						CommonSteps.I_click("NextBtn");
						CommonSteps.I_wait_for_visibility_of_element("PwdFieldPaypal");
						CommonSteps.I_should_see_on_page("PwdFieldPaypal");
						CommonSteps.I_enter_in_field("12345678", "PwdFieldPaypal");
						CommonSteps.I_clickJS("LoginPaypal");
						CommonSteps.I_should_see_on_page("PayPalUserName");
					} catch (Exception ee) {
						wb.until(ExpectedConditions
								.visibilityOf(driver.findElement(GetPageObjectRead.OR_GetElement("EmailFieldPaypal"))));
						CommonSteps.I_should_see_on_page("EmailFieldPaypal");
						CommonSteps.i_clear_and_enter_the_text("pranee_1358913423_per@dbi.com", "EmailFieldPaypal");
						CommonSteps.I_wait_for_visibility_of_element("PwdFieldPaypal");
						CommonSteps.I_should_see_on_page("PwdFieldPaypal");
						CommonSteps.I_enter_in_field("12345678", "PwdFieldPaypal");
						CommonSteps.I_click("LoginPaypal");
						CommonSteps.I_should_see_on_page("PayPalUserName");
					}
				e.printStackTrace();
			}
			
				// TODO Auto-generated catch block
				CommonSteps.I_should_see_on_page("CntnueBtnPaypal");
		
		
		}
	

	

	@When("^I switch to Iframe$")
	public static void i_switch_to_iframe() throws ECom_Exception, Exception {
		
		String methodName= "i_switch_to_iframe";

		try {
			driver.switchTo().defaultContent();
//    c8b96874844313759dc2f5d55d3e27a1        WebElement iframe = driver.findElement(By.xpath("//iframe[@id=\"envoyId\"]"));
			WebElement iframe = driver.findElement(By.xpath("//iframe[@id='envoyId']"));
			driver.switchTo().frame(iframe);
			log.info("Switching to iframe");
			WebElement findElement = driver.findElement(By.id("submit-order-btn-left"));
			boolean displayed = findElement.isDisplayed();
			System.out.println(displayed);
			findElement.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	@Then("^I switch back to MainFrame$")
	public static void i_switch_back_to_mainframe() throws ECom_Exception, Exception {
		String methodName= "i_switch_back_to_mainframe";

		try {
			driver.switchTo().parentFrame();
			Thread.sleep(2000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@When("^I update the value '(.*)' in the excel sheet '(.*)' for product '(.*)'$")
	public static void i_update_the_Value_in_the_excel_sheet_for_product(String element, String sheet, String product)
			throws Exception {
		
		String methodName= "i_update_the_Value_in_the_excel_sheet_for_product '"+element+"' '"+sheet+"' '"+product+"'";

		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element, GetPageObjectRead.OR_GetElement(element),
					50);
			java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));

			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheetAt = wb.getSheet(sheet);
			for (int i = 0; i < sheetAt.getPhysicalNumberOfRows(); i++) {
				Cell p1 = sheetAt.getRow(i).getCell(0);
				if (p1 == null) {
					continue;
				}
				String productID = null;
				if (p1.getCellType().equals(CellType.STRING)) {
					productID = p1.getStringCellValue();
				} else if (p1.getCellType().equals(CellType.NUMERIC)) {
					double d = p1.getNumericCellValue();
					long l = (long) d;
					productID = String.valueOf(l);
				}

				if (productID.equals(product)) {

					if (sheet.equals("Vendor Products")) {

						sheetAt.getRow(i).createCell(5).setCellValue(wElement.getText());
						Hooks.scenario.write(wElement.getText());
						sheetAt.getRow(i).createCell(6).setCellValue("Pass");

					} else {

						sheetAt.getRow(i).createCell(7).setCellValue(wElement.getText());
						Hooks.scenario.write(wElement.getText());
						sheetAt.getRow(i).createCell(8).setCellValue("Pass");

					}
				}
			}

			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	public static void javaScriptClick(WebElement webElement) throws Exception {
		

		try {
			WebDriverWait wb = new WebDriverWait(driver, 20);
			wb.until(ExpectedConditions.visibilityOf(webElement));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", webElement);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@When("^I select the product from result with color '(.*)' ,size '(.*)' and qty '(.*)'$")
	public static void i_select_the_product_from_result_with_color_and_size(String color, String size, String qty)
			throws ECom_Exception, Exception {
		
		String methodName= "i_select_the_product_from_result_with_color_and_size '"+color+"' '"+size+"' '"+qty+"'";

		try {
			WebDriverWait wb = new WebDriverWait(driver, 40);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			try {
				WebElement result = driver.findElement(By.xpath("//h1[contains(text(), 'Search results for')]"));
				wb.until(ExpectedConditions.visibilityOf(result));
				boolean displayed = result.isDisplayed();
				if (displayed) {
					log.info("Search Result - PLP Page");
				}
				WebElement pdImage = driver.findElement(By.xpath("(//div[@class=\"product_name\"]//a)[1]"));
				CommonSteps.I_scroll_to_element("coordinates", "0,200");
				javaScriptClick(pdImage);
				CommonSteps.I_scroll_to_element("coordinates", "0,200");
				if (!color.equalsIgnoreCase("no color")) {
//				WebElement colorelement = driver.findElement(By.id("detail__color-dropdown-select"));
					WebElement colorelement = driver.findElement(
							By.xpath("//select[@id='detail__color-dropdown-select']//option[text()='" + color + "']"));
					wb.until(ExpectedConditions.visibilityOf(colorelement));
					colorelement.click();

//				Select sc = new Select(colorelement);
//				sc.selectByVisibleText(color);
				}
				CommonSteps.I_scroll_to_element("coordinates", "0,200");
				if (!size.equalsIgnoreCase("no size")) {
					WebElement sizeElement = driver.findElement(By.xpath("//label[text()='" + size + "']"));
					wb.until(ExpectedConditions.visibilityOf(sizeElement));
					sizeElement.click();
				}
				if (!qty.equalsIgnoreCase("no qty")) {
					CommonSteps.i_clear_and_enter_the_text_js(qty, "QtyField");
				}
			} catch (org.openqa.selenium.NoSuchElementException e) {
				if (!color.equalsIgnoreCase("no color")) {
//				WebElement colorelement = driver.findElement(By.id("detail__color-dropdown-select"));
//				wb.until(ExpectedConditions.visibilityOf(colorelement));
//				Select sc = new Select(colorelement);
//				Thread.sleep(3000);
//				sc.selectByVisibleText(color);
					WebElement colorelement = driver.findElement(
							By.xpath("//select[@id='detail__color-dropdown-select']//option[text()='" + color + "']"));
					wb.until(ExpectedConditions.visibilityOf(colorelement));
					colorelement.click();
				}
				CommonSteps.I_scroll_to_element("coordinates", "0,200");
				if (!size.equalsIgnoreCase("no size")) {
					WebElement sizeElement = driver.findElement(By.xpath("//label[text()='" + size + "']"));
					wb.until(ExpectedConditions.visibilityOf(sizeElement));
					javaScriptClick(sizeElement);
				}
				if (!qty.equalsIgnoreCase("no qty")) {
					CommonSteps.i_clear_and_enter_the_text_js(qty, "QtyField");
				}

				driver.manage().timeouts().implicitlyWait(Integer.parseInt(System.getProperty("test.implicitlyWait")),
						TimeUnit.SECONDS);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@When("^I change the country to '(.*)'$")
	public static void i_chancountry_to_US(String Value) throws Exception {
		
		String methodName= "i_chancountry_to_US '"+Value+"'";

		try {
			WebDriverWait wb = new WebDriverWait(driver, 10);
			wb.until(ExpectedConditions.visibilityOf(driver.findElement(GetPageObjectRead.OR_GetElement("CountryLogo"))));
			driver.findElement(GetPageObjectRead.OR_GetElement("CountryLogo")).click();
			CommonSteps.I_scroll_to_element("window.scrollBy(0,400)", "SelectCountryText");
			CommonSteps.I_select_option_in_dd_by(Value, "ChooseCountry", "text");
			CommonSteps.I_clickJS("ProceedHomePageBtn");
			Thread.sleep(2000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	@When("^I enter Promo Code '(.*)' and apply it$")
	public static void i_enter_promo_code_and_apply_it(String code) throws ECom_Exception, Exception {
		
		String methodName= "i_enter_promo_code_and_apply_it '"+code+"'";

			try {
				WebDriverWait wb = new WebDriverWait(driver, 5);
				Thread.sleep(2000);
//		Actions ac = new Actions(driver);
//		ac.click(driver.findElement(GetPageObjectRead.OR_GetElement("PromoCtrlLink")));
						CommonSteps.I_clickJS("PromoCtrlLink");
//		try {
//			CommonSteps.i_clear_and_enter_the_text(code, "PromoCode");
//			CommonSteps.I_clickJS("ApplyButton");
//			WebElement AppliedPromo = driver.findElement(GetPageObjectRead.OR_GetElement("PromotionUsed"));
//			String CheckPromoText = AppliedPromo.getText();
//			if (CheckPromoText.contains(code)) {
//				Assert.assertTrue(true);
//			} else {
//				Assert.assertFalse(false);
//			}
//
//		} catch (Exception e) {
//			CommonSteps.I_clickJS("PromoCtrlLink");

					try {
//						CommonSteps.I_clickJS("PromoCtrlLink");
						wb.until(ExpectedConditions
								.visibilityOf(driver.findElement(GetPageObjectRead.OR_GetElement("RemoveUsedPromoCode"))));
						CommonSteps.I_clickJS("RemoveUsedPromoCode");
//						CommonSteps.I_clickJS("RemoveUsedPromoCode");
						Thread.sleep(2000);

					} catch (Exception e) {
//						CommonSteps.I_clickJS("PromoCtrlLink");
					CommonSteps.i_clear_and_enter_the_text(code, "PromoCode");
					CommonSteps.I_clickJS("ApplyButton");
					WebElement AppliedPromo = driver.findElement(GetPageObjectRead.OR_GetElement("PromotionUsed"));
					String CheckPromoText = AppliedPromo.getText();
					if (CheckPromoText.contains(code)) {
						Assert.assertTrue(true);
					} else {
						Assert.assertFalse(false);
					}
//			CommonSteps.I_should_see_text_contained_on_page_At(code, "PromotionUsed");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				new ECom_Exception(methodName, e);
			}
		}  

	public static void multipleTrytoFindElement(String element) throws ECom_Exception, Exception {
		
		String methodName= "multipleTrytoFindElement '"+element+"'";

		boolean flag = false;
		for (int i = 0; i < 3; i++) {
			if (flag) {
				break;
			}
			try {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);

				flag = true;
				log.info("Clicked on Try ->" + i);
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
				new ECom_Exception(methodName, e);

				
			}

		}
	}

	@When("^I login the application with valid username and password$")
	public static void i_login_the_application_with_valid_username_password() throws ECom_Exception, Exception {

		String methodName= "i_login_the_application_with_valid_username_password";
		try {
			try {
				WebDriverWait wb = new WebDriverWait(driver, 20);
				wb.until(ExpectedConditions
						.visibilityOf(driver.findElement(GetPageObjectRead.OR_GetElement("EmailFieldLogin"))));
				CommonSteps.I_should_see_on_page("EmailFieldLogin");
				if (System.getProperty("test.TestName").toLowerCase().contains("chrome")) {
					CommonSteps.I_enter_in_field("davidsbridal5599@gmail.com", "EmailFieldLogin");
					CommonSteps.I_enter_in_field("Cyber@123", "PwdFieldLogin");
					Hooks.scenario.write("davidsbridal5599@gmail.com , Cyber@123");
				} else if (System.getProperty("test.TestName").toLowerCase().contains("firefox")) {
					CommonSteps.I_enter_in_field("DBQATestAc3@gmail.com", "EmailFieldLogin");
					CommonSteps.I_enter_in_field("test", "PwdFieldLogin");
					Hooks.scenario.write("DBQATestAc3@gmail.com , test");
				} else if (System.getProperty("test.TestName").toLowerCase().contains("edge")) {
					CommonSteps.I_enter_in_field("DBQATestAc2@gmail.com", "EmailFieldLogin");
					CommonSteps.I_enter_in_field("test", "PwdFieldLogin");
					Hooks.scenario.write("DBQATestAc2@gmail.com , test");
				}else if (System.getProperty("test.TestName").toLowerCase().contains("safari")) {
					CommonSteps.I_enter_in_field("dbqatestac6@gmail.com", "EmailFieldLogin");
					CommonSteps.I_enter_in_field("test", "PwdFieldLogin");
					Hooks.scenario.write("dbqatestac6@gmail.com , test"); 
				} else {
					CommonSteps.I_enter_in_field("davidsbridal5599@gmail.com", "EmailFieldLogin");
					CommonSteps.I_enter_in_field("Cyber@123", "PwdFieldLogin");
					Hooks.scenario.write("davidsbridal5599@gmail.com , Cyber@123");

				}
				Thread.sleep(2000);
//            multipleTrytoFindElement("LoginSubmitBtn");
				CommonSteps.I_clickJS("LoginSubmitBtn");
//            CommonSteps.I_wait_for_visibility_of_element("UserName_Header");
				Thread.sleep(2000);
				CommonSteps.I_wait_for_visibility_of_element("HeaderPersonalNavAccount");
				CommonSteps.I_should_see_on_page("HeaderPersonalNavAccount");
			} catch (org.openqa.selenium.NoSuchElementException e) {
//            CommonSteps.I_wait_for_visibility_of_element("UserName_Header");
				CommonSteps.I_wait_for_visibility_of_element("HeaderPersonalNavAccount");
				CommonSteps.I_should_see_on_page("HeaderPersonalNavAccount");
				log.info("User Already Logged In");

			}
		}catch(Exception e) {
			e.printStackTrace();
				new ECom_Exception(methodName, e);

			}

	}

	@When("^I logout the application if it is logged in already after launching the browser$")
	public static void i_logout_the_application() throws ECom_Exception, Exception {
		
		String methodName= "i_logout_the_application";

		
			try {
				try {
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					WebElement username = driver.findElement(GetPageObjectRead.OR_GetElement("HeaderPersonalNavAccount"));
					if (username.isDisplayed()) {
						CommonSteps.I_mouse_over("HeaderPersonalNavAccount");
						CommonSteps.I_wait_for_visibility_of_element("Logout_Header");
						CommonSteps.I_clickJS("Logout_Header");
						Thread.sleep(2000);

					} else {
						log.info("User is not  Logged In already");

					}

				} catch (Exception e) {

					CommonSteps.I_should_see_on_page("LoginLink_Header");
					log.info("User is not  Logged In already");
					//e.printStackTrace();
					


				}
				driver.manage().timeouts().implicitlyWait(Long.parseLong(System.getProperty("test.implicitlyWait")),
						TimeUnit.SECONDS);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				new ECom_Exception(methodName, e);
			}
		
	}

	@Then("^I click Agree Button if it is present on the page$")
	public static void i_click_agree_button_if_it_is_present_on_the_page() throws ECom_Exception, Exception {
		
		String methodName= "i_click_agree_button_if_it_is_present_on_the_page";
		try {
			CommonSteps.I_should_see_on_page("AgreeButton");
			CommonSteps.I_click("AgreeButton");
		} catch (Exception e) {
			CommonSteps.I_wait_for_visibility_of_element("CntnueOrderReview");
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	@When("^I login the application in Checkout page with valid credentials$")
	public static void i_login_the_application_in_checkout_page_with_valid_credentials() throws ECom_Exception, Exception {

		String methodName= "i_login_the_application_in_checkout_page_with_valid_credentials";

			try {
		CommonSteps.I_should_see_on_page("SignInText_CheckoutPage");
		CommonSteps.I_should_see_on_page("LoginId");
		if (System.getProperty("test.TestName").toLowerCase().contains("chrome")) {
			CommonSteps.I_enter_in_field("davidsbridal5599@gmail.com", "LoginId");
			CommonSteps.I_enter_in_field("Cyber@123", "LoginPassword");
			Hooks.scenario.write("davidsbridal5599@gmail.com , Cyber@123");
		} else if (System.getProperty("test.TestName").toLowerCase().contains("firefox")) {
			CommonSteps.I_enter_in_field("DBQATestAc3@gmail.com", "LoginId");
			CommonSteps.I_enter_in_field("test", "LoginPassword");
			Hooks.scenario.write("DBQATestAc3@gmail.com , test");
		} else if (System.getProperty("test.TestName").toLowerCase().contains("edge")) {
			CommonSteps.I_enter_in_field("DBQATestAc2@gmail.com", "LoginId");
			CommonSteps.I_enter_in_field("test", "LoginPassword");
			Hooks.scenario.write("DBQATestAc2@gmail.com , test");
		}else if (System.getProperty("test.TestName").toLowerCase().contains("safari")) {
			CommonSteps.I_enter_in_field("dbqatestac6@gmail.com", "LoginId");
			CommonSteps.I_enter_in_field("test", "LoginPassword");
			Hooks.scenario.write("dbqatestac6@gmail.com , test");
		} else {
			CommonSteps.I_enter_in_field("davidsbridal5599@gmail.com", "LoginId");
			CommonSteps.I_enter_in_field("Cyber@123", "LoginPassword");
			Hooks.scenario.write("davidsbridal5599@gmail.com , Cyber@123");

		}
//		CommonSteps.I_enter_in_field("davidsbridal5599@gmail.com", "LoginId");
//		CommonSteps.I_enter_in_field("Cyber@123", "LoginPassword");
		CommonSteps.I_doubleClick("SignIn");
		log.info("User is Logged In");
		
//		WebElement cntnueShipping = driver.findElement(GetPageObjectRead.OR_GetElement("CntnueShipping_ShippingAddressPage"));
//		if(cntnueShipping.isDisplayed())
//		{
//			CommonSteps.I_should_see_on_page("CntnueShipping_ShippingAddressPage");
//		}else {
//			CommonSteps.I_wait_for_visibility_of_element("HeaderPersonalNavAccount");
//			CommonSteps.I_should_see_on_page("HeaderPersonalNavAccount");
//		}
		
			}catch(Exception e) {
		e.printStackTrace();
		new ECom_Exception(methodName, e);

		
	}
		

}
}
