package com.TEAF.stepDefinitions;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import cucumber.api.java.en.When;
import junit.framework.Assert;


public class APIDefinition {
	
static RestAssuredSteps rs = new RestAssuredSteps();

 	public static void getLoginApi() throws Throwable {
        rs.endPoint("https://43o8tjcr0m.execute-api.us-east-1.amazonaws.com/qa/login");
        rs.content_type("application/json");
        rs.header_key_value("username", "dss_automated_test");
        rs.header_key_value("password", "4Thebe$tre$ult$");
        rs.header_key_value("bearertoken", "1234567890");
        try {
            rs.Method("Get");
        } catch (Exception e) {
        }
        rs.print_response();
        rs.statuscode(200);
        rs.retrivedata_store_withobject_index("$..token", "0", "AuthToken");
    }

 

    public static void getTheAppointmentAndCancelTheAppointment(String confirmationNumber) throws Throwable {
        rs.endPoint("https://43o8tjcr0m.execute-api.us-east-1.amazonaws.com/qa/appointment/{confirmationNumber}");
        rs.content_type("application/json");
        rs.adding_path_param("confirmationNumber", confirmationNumber);
        rs.header_key_value("username", "dss_automated_test");
        rs.header_key_value("password", "4Thebe$tre$ult$");
        rs.header_key_value("bearertoken", "1234567890");
        rs.header_key_value("authorizationToken", "$$AuthToken");
        try {
            rs.Method("Get");
        } catch (Exception e) {
        }
        rs.print_response();
        int statusCode = rs.getResponse().getStatusCode();
        if (statusCode == 200) {
            rs.statuscode(200);
            rs.match_JSONPath_contains("$..appointment.confirmationNumber", confirmationNumber);
            rs.endPoint("https://43o8tjcr0m.execute-api.us-east-1.amazonaws.com/qa/appointment/{confirmationNumber}");
            rs.content_type("application/json");
            rs.adding_path_param("confirmationNumber", confirmationNumber);
            rs.header_key_value("username", "dss_automated_test");
            rs.header_key_value("password", "4Thebe$tre$ult$");
            rs.header_key_value("bearertoken", "1234567890");
            rs.header_key_value("authorizationToken", "$$AuthToken");
            try {
                rs.Method("Delete");
            } catch (Exception e) {
            }
            rs.print_response();
            rs.statuscode(200);
            rs.match_JSONPath_contains("$..result", "SUCCESS");

 

        } else if (statusCode == 404) {
            String object = rs.getResponse().jsonPath().get("errorMessage");
            Assert.assertTrue(object.contains("Confirmation number does not exist"));
        }

 

    }

 

    @When("^I cancel all the appointment added in the SpreadSheet$")
    public static void cancelTheAppoint() throws Throwable {
        getLoginApi();
        getAppointmentConfirmationNumber();
        for (int i = 1; i < confirNumList.size(); i++) {
            getTheAppointmentAndCancelTheAppointment(confirNumList.get(i));

 

        }
    }

 

    static List<String> confirNumList = new ArrayList<String>();

 

    public static void getAppointmentConfirmationNumber() throws IOException {
        java.io.File f = new java.io.File(
                System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\TestDatas.xlsx");

 

        FileInputStream fin = new FileInputStream(f);
        Workbook wb = new XSSFWorkbook(fin);
        Sheet sheet = wb.getSheet("Appointments");

 

        for (int i = 0; i < sheet.getPhysicalNumberOfRows(); i++) {
            Cell cell = sheet.getRow(i).getCell(5);
            if (cell == null) {
                continue;
            }
            String stconfirNum = sheet.getRow(i).getCell(5).getStringCellValue();
            confirNumList.add(stconfirNum);
        }

 

    }
    
    
    
}
 