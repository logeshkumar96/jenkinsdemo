package com.TEAF.stepDefinitions;

public class ContactMerge {

	
	
	
	private String customerCodes;
	private String ultimateMasterCode;

	public String getCustomerCodes() {
		return customerCodes;
	}

	public void setCustomerCodes(String customerCodes) {
		this.customerCodes = customerCodes;
	}

	public String getUltimateMasterCode() {
		return ultimateMasterCode;
	}

	public void setUltimateMasterCode(String ultimateMasterCode) {
		this.ultimateMasterCode = ultimateMasterCode;
	}

}