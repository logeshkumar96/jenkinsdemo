package com.TEAF.stepDefinitions;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import static io.restassured.RestAssured.given;

public class basic_auth {

//	private String path;
//    private String validRequest = "{\n" +
//            "  \"username\": \"some-user\",\n" +
//            "  \"email\": \"some-user@email.com\",\n" +
//            "  \"password\": \"Passw0rd123!\" \n}";

//    @Before
//    public void setup() {
//        RestAssured.baseURI = "https://qaawsmule.mfrm.com:9086/api/incontact-report/incontact-report-withhold";
//    }

    @Test
    public void createUser() {
        Response response = given()
                .auth()
                .preemptive()
                .basic("mulesoftqa", "MfrmMuleSoftDev")
                .header("Accept", ContentType.JSON.getAcceptHeader())
                .contentType(ContentType.JSON)
                .get("https://qaawsmule.mfrm.com:9086/api/incontact-report/incontact-report-withhold")
                .then().extract().response();
        
        System.out.println(response.getBody().prettyPrint());

//        Assertions.assertThat(201, response.getStatusCode());
    }

}
