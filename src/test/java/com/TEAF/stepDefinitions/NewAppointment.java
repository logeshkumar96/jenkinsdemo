package com.TEAF.stepDefinitions;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import com.TEAF.customUtilities.ECom_Exception;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class NewAppointment {

	public static WebDriver driver = StepBase.getDriver();
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(NewAppointment.class.getName());

	public static void i_verify_the_email_body_content(String sub, String body) throws ECom_Exception, Exception {
		
		String methodName= "i_verify_the_email_body_content '"+sub+"' '"+body+"'";

		try {
			String currentUrl = driver.getCurrentUrl();
			com.TEAF.framework.ReadingEmail.getemailRead("outlook.office365.com", "pop3", "v.pkumar@dbi.com", "Cyber@123",
					sub, body);
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.get(System.getProperty("user.dir")+"\\src\\test\\java\\com\\Resources\\EmailBody.html");
			String text = driver.findElement(By.xpath("//p[contains(text(),'Hi')]")).getText();
			text.contains(body);
			driver.findElement(By.xpath("//div[text()='STORES']")).click();
			boolean storepageTitle = driver.getTitle().contains("Find Bridal Shops in Your Area ");
			driver.navigate().back();
			driver.findElement(By.xpath("//a[text()='view all matched dresses']")).click();
			boolean viewmatcheddresscurrenturl = driver.getCurrentUrl().equals(currentUrl);
			driver.navigate().back();
			driver.findElement(By.xpath("//a[contains(text(),'an appointment')]")).click();
			boolean bookanappttitle = driver.getTitle().contains("Virtual Bridal Stylist Appointment");
			driver.navigate().back();
			driver.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

		
	}

//	public static void main(String[] args) throws ECom_Exception, Exception {
//		try {
//			i_verify_the_email_body_content("your Dress Finder results!", "view all matched dresses");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}





//	public static void main(String[] args) {
//
//		System.setProperty("webdriver.chrome.driver",
//				"D:\\Project Workspaces\\ecom-automation-digitalcommerce\\src\\test\\java\\com\\Resources\\chromedriver.exe");
//		Map<String, String> mobileEmulation = new HashMap();
//		mobileEmulation.put("deviceName", "Nexus 5");
//		ChromeOptions chromeOptions = new ChromeOptions();
//		chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
//
//		WebDriver driver = new ChromeDriver(chromeOptions);
//
//		driver.get("https://www.google.com");
//
//	}



	@When("^I get urls and verify$")
	public static void geturls_verify() throws Exception {
		
		String methodName= "geturls_verify";

		try {
			System.out.println("Iteration ");
			driver.navigate().to("https://www.google.co.in/");
			WebDriverWait wb = new WebDriverWait(driver, 30);
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\InventoryCheck.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook w = new XSSFWorkbook(fin);
			int numberOfSheets = w.getNumberOfSheets();
			Actions ac = new Actions(driver);
			for (int i = 4; i == 4; i++) {
				System.out.println(i);
				Sheet sheetAt = w.getSheetAt(3);
				boolean flag = false;
				for (int j = 1; j < sheetAt.getPhysicalNumberOfRows(); j++) {

					System.out.println(j);
					Row row = sheetAt.getRow(j);
					if (row == null) {
						continue;
					}
					Cell cell = row.getCell(0);
					if (cell == null) {
						continue;
					}
					String url = cell.getStringCellValue();
					driver.navigate().to(url);
					try {
						WebElement colortext = driver.findElement(By.xpath("(//legend[contains(text(),'Color')])[1]"));
						JavascriptExecutor js = (JavascriptExecutor) driver;
						js.executeScript("arguments[0].scrollIntoView();", colortext);
						WebElement colordd = driver
								.findElement(By.xpath("(//span[contains(@class,'detail__color-swatch-bg swatch-bg')])[1]"));
						wb.until(ExpectedConditions.attributeToBe(colordd, "class", "detail__color-swatch-bg swatch-bg"));
						wb.until(ExpectedConditions.visibilityOf(colordd));
						wb.until(ExpectedConditions.elementToBeClickable(colordd));
						ac.moveToElement(colordd).doubleClick(colordd).build().perform();
						;

						WebElement size = driver.findElement(By.xpath(
								"(//div[contains(@class,'detail__attributes-item detail__size-item')]/input[not(@disabled)]//..//label)[1]"));
						wb.until(ExpectedConditions.visibilityOf(size));
						wb.until(ExpectedConditions.elementToBeClickable(size));
						ac.moveToElement(size).doubleClick(size).build().perform();
						;

						WebElement addtobag = driver.findElement(By.id("productPageAdd2Cart"));
						wb.until(ExpectedConditions.visibilityOf(addtobag));
						wb.until(ExpectedConditions.elementToBeClickable(addtobag));
						ac.moveToElement(addtobag).doubleClick(addtobag).build().perform();
						Thread.sleep(2000);

						driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
						for (int k = 0; k < 3; k++) {

							try {
								WebDriverWait wbs = new WebDriverWait(driver, 10);

								WebElement displayed = driver.findElement(By.id("widget_minishopcart_popup_1"));
								wbs.ignoring(StaleElementReferenceException.class)
										.until(ExpectedConditions.visibilityOf(displayed));
								String text = driver.findElement(By.xpath("//span[@class='notification']")).getText();
								System.out.println(j + text);
								break;
							} catch (Exception e) {
								System.out.println("Stalee Element exception");
							}
						}
						System.out.println("Product Avaliable");
						row.createCell(1).setCellValue("Product Avaliable");

					} catch (Exception e) {
						try {
							e.printStackTrace();
							WebElement noitem = driver.findElement(By.xpath("//p[contains(text(),'sorry')]"));
							wb.until(ExpectedConditions.visibilityOf(noitem));
							noitem.isDisplayed();

							System.out.println("Product Un Avaliable");

							row.createCell(1).setCellValue("Product Unavaliable");
						} catch (Exception e1) {
							e1.printStackTrace();
							System.out.println(j + "Failure");
						}
					}
					FileOutputStream fout = new FileOutputStream(f);
					w.write(fout);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

//	 public static boolean ignoringStaleElementException(By loc) {
//	        boolean result = false;
//	        int attempts = 0;
//	        while (attempts < 3) {
//	            try {
//	                new WebDriverWait(driver, 10).ignoring(StaleElementReferenceException.class)
//	                        .until(ExpectedConditions.visibilityOf(loc));
//	                loc.isDisplayed();
//	                result = true;
//	                break;
//	            } catch (StaleElementReferenceException e) {
//	                attempts++;
//	                log.error("Attempt Count "+ attempts + " for StaleElement exception");
//	            }
//	        }
//	        return result;
//	    }
//	

	@When("^I click_ on the '(.*)'$")
	public static void i_click_on_the_element(String element) throws ECom_Exception, Exception {
		
		String methodName= "i_click_on_the_element '"+element+"'";

		try {
			WebDriverWait wb = new WebDriverWait(driver, 30);
//		Actions ac = new Actions(driver);
			By loc = GetPageObjectRead.OR_GetElement(element);
			wb.until(ExpectedConditions.presenceOfElementLocated(loc));
//		ac.click(driver.findElement(loc));
			driver.findElement(loc).click();
//		javaScriptClick(driver.findElement(loc));

//		ignoringStaleElementException(loc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}
	
	@When("^I click  the Store location '(.*)' for firefox$")
	public static void i_click_the_element(String ele) throws ECom_Exception, Exception {
		
		String methodName= "i_click_the_element '"+ele+"'";

		try {
			if (StepBase.testBrowser.equalsIgnoreCase("firefox") || System.getProperty("test.TestName").toLowerCase().contains("firefox") ) {
				WebDriverWait wb = new WebDriverWait(driver, 30);
				By element1 = GetPageObjectRead.OR_GetElement(ele);
				wb.until(ExpectedConditions.presenceOfElementLocated(element1));
				driver.findElement(element1).click();
				
			}else {
				log.info("Not a firefox driver");

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
		
		
	}

	

	@When("^I should_ see element '(.*)' present on page$")
	public static void i_should_see_the_element_present_on_page(String element) throws Exception {
		
		String methodName= "i_should_see_the_element_present_on_page '"+element+"'";

		try {
			WebDriverWait wb = new WebDriverWait(driver, 30);
			By loc = GetPageObjectRead.OR_GetElement(element);
			wb.until(ExpectedConditions.presenceOfElementLocated(loc));
			Thread.sleep(1000);
			boolean displayed = driver.findElement(loc).isDisplayed();
			
			if (displayed) {
				assertTrue(displayed);
			} else {
				throw new Exception("Element is not found! :" + element);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);

		}

	}
	
	
	
	@When("^I see options '(.*)' - '(.*)'$")
	public static void i_see_options_cheecked_unchecked(String element, String selected) throws ECom_Exception, Exception {
		
		String methodName= "i_see_options_cheecked_unchecked '"+element+"' '"+selected+"'";

		try {
			WebElement option = driver.findElement(By.xpath("//div[text()='" + element.trim() + "']//..//..//input"));
			boolean eleSelection = option.isSelected();
			if (selected.equalsIgnoreCase("checked")) {
				try {
					assertTrue(eleSelection);
				} catch (Exception e) {
					option.click();
					boolean selected2 = option.isSelected();
					if (selected2) {
						fail();
					}else {
						System.out.println("OPtions is not available to click");
					}
				}
			} else if (selected.equalsIgnoreCase("unchecked")) {
				
				try {
					assertFalse(eleSelection);
				} catch (Exception e) {
					option.click();
					boolean selected2 = option.isSelected();
					if (selected2) {
						fail();
					}else {
						System.out.println("OPtions is not available to click");
					}
					}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}
	
	@When("^I generate random value_ and enter in the field '(.*)'$")
	public static void i_generate_random_value_string(String Locator) throws ECom_Exception, Exception {
		
		String methodName= "i_generate_random_value_string '"+Locator+"'";

	try {
		WebElement TextField = driver.findElement(GetPageObjectRead.OR_GetElement(Locator));
		TextField.clear();
		TextField.click();
		final int length = 5;
		String randomstring = RandomStringUtils.randomAlphabetic(length);
		TextField.sendKeys(randomstring);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		new ECom_Exception(methodName, e);
	}
	
    
	}
	
	@When("^I generate random number and enter in the field '(.*)'$")
	public static void i_generate_random_number(String Locator) throws ECom_Exception, Exception {
		
		String methodName= "i_generate_random_number '"+Locator+"'";

		try {
			WebElement NumberField = driver.findElement(GetPageObjectRead.OR_GetElement(Locator));
			NumberField.clear();
			NumberField.click();
			long number = (long) Math.floor(Math.random() * 900000000L) + 100000000L;
//		Random randomGenerator = new Random();
//		int randomInt = randomGenerator.nextInt(1000000000);
			NumberField.sendKeys("9"+number);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}
	
	

	@When("^I Press Key down to select option '(.*)' for Store Appointment$")
	public static void i_press_key_down_to_select_store_appointment(String option) throws ECom_Exception, Exception {
		
		String methodName= "i_press_key_down_to_select_store_appointment '"+option+"'";

		try {
			Actions ac = new Actions(driver);
			Thread.sleep(1000);
			if (option.equalsIgnoreCase("Bride")) {

			} else if (option.equalsIgnoreCase("Bridesmaid")) {
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();

			} else if (option.contains("Groom")) {
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();

			} else if (option.contains("Prom")) {
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();

			} else if (option.contains("Partner")) {
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();

			}

			ac.sendKeys(Keys.ENTER).build().perform();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	@Then("^I generate random email id '(.*)' for store appointments$")
	public static void i_generate_random_email_id(String Locator) throws Throwable {
		
		String methodName= "i_generate_random_email_id '"+Locator+"'";

		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			WebElement emailTextBx = driver.findElement(GetPageObjectRead.OR_GetElement(Locator));
			emailTextBx.clear();
			wait.until(ExpectedConditions.visibilityOf(emailTextBx));
//		emailTextBx.click();
			Random randomGenerator = new Random();
			int randomInt = randomGenerator.nextInt(100000000);
			String storeEmail = "QA_StoreAppointments" + randomInt + "@gmail.com";
			emailTextBx.sendKeys(storeEmail);
			System.out.println("Email id for new user " + storeEmail);
			HashMapContainer.add("$$StoreApptEmailId", storeEmail);
			java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));

			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheetAt = wb.getSheet("StoreAppointments");
			int prows = sheetAt.getPhysicalNumberOfRows();
			sheetAt.createRow((prows + 2) - 1).createCell(0).setCellValue(storeEmail);
			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();
			fout.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	@Then("^I get text from '(.*)' and store for DBI$")
	public static void I_get_text_from(String element) throws ECom_Exception, Exception {
		
		String methodName= "I_get_text_from '"+element+"'";

		try {
			WebElement wElement = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String value = wElement.getText();
			HashMapContainer.add("$$" + element, value);

		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@When("^I store firstname, lastname, mobile number, event date, event role in the spreadsheet$")
    public static void i_get_value_in_feild_and_store() throws ECom_Exception, Exception {
		
		String methodName= "i_get_value_in_feild_and_store";

        try {

 

            String fn = driver.findElement(GetPageObjectRead.OR_GetElement("InputFirstName_SA")).getAttribute("value");
            String ln = driver.findElement(GetPageObjectRead.OR_GetElement("InputLastName_SA")).getAttribute("value");
            String em = driver.findElement(GetPageObjectRead.OR_GetElement("InputEmailFeild_SA")).getAttribute("value");
            String mb = driver.findElement(GetPageObjectRead.OR_GetElement("InputPhone_SA")).getAttribute("value");
            String ed = driver.findElement(GetPageObjectRead.OR_GetElement("EventDate_SA")).getAttribute("value");
            boolean selected = driver.findElement(By.xpath("//label[@id='smsOptin']//input")).isSelected();
            
            String er=null;
            if (StepBase.testBrowser.equalsIgnoreCase("mobile-chromeemulator")) {
                Select sc = new Select(driver.findElement(GetPageObjectRead.OR_GetElement("SelectRoleMobileDD_SA")));
                 er = sc.getFirstSelectedOption().getText();
                
            }else {
             er = driver.findElement(By.xpath("//div[contains(@class,'react-select__single-value')]")).getText();
            }
            // HashMapContainer.add("$$" + element, value);
			java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));

            FileInputStream fin = new FileInputStream(f);
            Workbook wb = new XSSFWorkbook(fin);
            Sheet sheetAt = wb.getSheet("StoreAppointments");
            for (int j = 1; j <= sheetAt.getPhysicalNumberOfRows(); j++) {
                if (sheetAt.getRow(j) == null) {
                    continue;
                }

 

                String email = sheetAt.getRow(j).getCell(0).getStringCellValue();
                System.out.println("email id " + email);
                System.out.println("email id stored " + HashMapContainer.get("$$StoreApptEmailId"));
                // System.out.println(HashMapContainer.get("$$EmailIdAppointment"));
                if (email.toLowerCase().trim().equals(HashMapContainer.get("$$StoreApptEmailId").toLowerCase().trim())) {
                    sheetAt.getRow(j).createCell(1).setCellValue(fn);
                    sheetAt.getRow(j).createCell(2).setCellValue(ln);
                    sheetAt.getRow(j).createCell(5)
                            .setCellValue(HashMapContainer.get("$$SelectedAppointmentDateInWordsSA"));
                    sheetAt.getRow(j).createCell(6).setCellValue(HashMapContainer.get("$$FirstAvailableTimeLabel_SA"));
                    sheetAt.getRow(j).createCell(3).setCellValue(mb);
                    sheetAt.getRow(j).createCell(8).setCellValue(ed);
                    sheetAt.getRow(j).createCell(9).setCellValue(er);
                    
                    sheetAt.getRow(j).createCell(10).setCellValue(selected);

 

                    if (StepBase.testBrowser.equalsIgnoreCase("mobile-chromeemulator")) {

 

                        sheetAt.getRow(j).createCell(7).setCellValue(HashMapContainer.get("$$SelectedAddressMobile_SA"));
                        
                        sheetAt.getRow(j).createCell(4).setCellValue(HashMapContainer.get("$$SelectedAppointmentTypeMobile_SA"));

 

                    }else {
                        sheetAt.getRow(j).createCell(7).setCellValue(HashMapContainer.get("$$SelectedAddress_SA"));
                        
                        sheetAt.getRow(j).createCell(4).setCellValue(HashMapContainer.get("$$SelectedAppointmentType_SA"));
                    }
                    
                    
                    
                }
                
                

            }
            
            FileOutputStream fout = new FileOutputStream(f);
            wb.write(fout);
            wb.close();
        } catch (Exception e) {
        
        	e.printStackTrace();
			new ECom_Exception(methodName, e);

        }
        
    }
	
	@Then("^I store alteration appointment address in the spreadsheet$")
	public static void i_store_alteration_appointment_address() throws ECom_Exception, Exception{
		
		String methodName= "i_store_alteration_appointment_address";

		try {
			java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));

			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheetAt = wb.getSheet("StoreAppointments");
			for (int j = 1; j <= sheetAt.getPhysicalNumberOfRows(); j++) {
			    if (sheetAt.getRow(j) == null) {
			        continue;
			    }
			    String email = sheetAt.getRow(j).getCell(0).getStringCellValue();
//			    System.out.println("email id " + email);
//			    System.out.println("email id stored " + HashMapContainer.get("$$StoreApptEmailId"));
			    if (email.toLowerCase().trim().equals(HashMapContainer.get("$$StoreApptEmailId").toLowerCase().trim())) {

			sheetAt.getRow(j).createCell(7).setCellValue(HashMapContainer.get("$$SelectedAddress_SA"));
			sheetAt.getRow(j).createCell(4).setCellValue(HashMapContainer.get("$$SelectedAppointmentType_SA"));
			    }
			}
			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);

		}
		
	}
	@When("^I store confirmation number for '(.*)' from the reschedule appointment and store in the spreadsheet$")
    public static void i_store_confirmation_number_confirmation_page(String appointmentType) throws ECom_Exception, Exception {
		
		String methodName= "i_store_confirmation_number_confirmation_page";

        try {

        	if(!appointmentType.equals("alterations")) {

            String fn = driver.findElement(GetPageObjectRead.OR_GetElement("RescheduleAppointment_SA")).getAttribute("href");
            String[] split = fn.split("=");
			java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));

            FileInputStream fin = new FileInputStream(f);
            Workbook wb = new XSSFWorkbook(fin);
            Sheet sheetAt = wb.getSheet("StoreAppointments");
            for (int j = 1; j <= sheetAt.getPhysicalNumberOfRows(); j++) {
                if (sheetAt.getRow(j) == null) {
                    continue;
                }

 

                String email = sheetAt.getRow(j).getCell(0).getStringCellValue();
                // System.out.println(HashMapContainer.get("$$EmailIdAppointment"));
                if (email.toLowerCase().trim().equals(HashMapContainer.get("$$StoreApptEmailId").toLowerCase().trim())) {
                    sheetAt.getRow(j).createCell(11).setCellValue(split[split.length-1]);
                }
                
            }
            FileOutputStream fout = new FileOutputStream(f);
            wb.write(fout);
            wb.close();
        }
        }catch (Exception e) {
        	e.printStackTrace();
			new ECom_Exception(methodName, e);

        }
    }

	@When("^I select appointment date '(.*)' after the current date in the feild '(.*)'$")
    public static void i_select_appointment_date_after_current_date_in_the_feild(String date, String element)
            throws ECom_Exception, Exception {
		
		String methodName= "i_select_appointment_date_after_current_date_in_the_feild '"+date+"' '"+element+"'";

 
		try {
        Format f = new SimpleDateFormat("MM/dd/yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, Integer.parseInt(date));
        Date currentDatePlusOne = c.getTime();
        String d = f.format(currentDatePlusOne);
        HashMapContainer.add("$$SelectedAppointmentDateSA", d);
        Actions ac = new Actions(driver);
        String addDateCount=null;
        WebElement dateInput = driver.findElement(GetPageObjectRead.OR_GetElement(element));
        if (StepBase.testBrowser.equalsIgnoreCase("firefox")) {
            ac.click(dateInput).build().perform();
            Thread.sleep(2000);
            ac.sendKeys(dateInput, d).build().perform();
        } else if (StepBase.testBrowser.equalsIgnoreCase("mobile-chromeemulator")) {
            // ac.sendKeys(dateInput, d).build().perform();
            Thread.sleep(2000);

 

//
            ac.click(dateInput).build().perform();
            Thread.sleep(2000);
            WebElement monthDD = driver.findElement(By.xpath("(//select)[1]"));
            Select sc = new Select(monthDD);
            String[] split = d.split("/");
            int parseInt = Integer.parseInt(split[0]) - 1;
            sc.selectByValue(String.valueOf(parseInt));
            Thread.sleep(1000);

 

            driver.findElement(By.xpath("//div[@aria-disabled=\"false\" and text()='" + split[1] + "']")).click();

 

        }
  
        else {
            ac.click(dateInput).build().perform();
            Thread.sleep(2000);
            WebElement monthDD = driver.findElement(By.xpath("(//select)[1]"));
            Select sc = new Select(monthDD);
            String[] split = d.split("/");
            int parseInt = Integer.parseInt(split[0]) ;
            sc.selectByValue(String.valueOf(parseInt));
            Thread.sleep(1000);

 

            WebElement dateEle = driver.findElement(By.xpath("//div[text()='"+split[1]+"']"));
            String attribute = dateEle.getAttribute("aria-disabled");
            log.info(attribute);
            int datecount = 0;
            if (attribute.equals("true")) {
                if (Integer.parseInt(split[1])<27) {
                	
                	
                    for (int i = 1; i < 5; i++) {
                        datecount = i;
                    	dateEle = driver.findElement(By.xpath("//div[text()='"+(Integer.parseInt(split[1])+i)+"']"));
                         String attribute2 = dateEle.getAttribute("aria-disabled");
                         if (attribute2.equals("true")) {
                            continue;
                        }else {
                            dateEle.click();
                            break;
                        }
                    }  
                    addDateCount=String.valueOf(datecount);
                }else {
                	 for (int i = 1; i < 5; i++) {
                		 datecount=i;
                         dateEle = driver.findElement(By.xpath("//div[text()='"+(Integer.parseInt(split[1])-i)+"']"));
                         String attribute2 = dateEle.getAttribute("aria-disabled");
                         if (attribute2.equals("true")) {
                            continue;
                        }else {
                            dateEle.click();
                            break;
                        }
                    }    
                     addDateCount="-"+String.valueOf(datecount);

                }
                

 

            }else {
                dateEle.click();
            }
 

        }
        String savedDate = d;
        DateFormat df5 = new SimpleDateFormat("E, MMM dd yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

 

        Calendar c1 = Calendar.getInstance();
        c1.setTime(sdf.parse(savedDate));
        String newDate = df5.format(c1.getTime());
        System.out.println(newDate);
        String[] split = newDate.split(",");
        String trim = split[1].trim();

 

        String[] split2 = trim.split(" ");

 

        String[] monthNames = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
                "October", "November", "December" };
        String selectedMonth = null;
        for (String x : monthNames) {
            if (split2[0].contains(x.substring(0, 3))) {
                selectedMonth = x;
            }
        }
        String formatDate = selectedMonth + " " + split2[1] + ", " + split2[2];
        System.out.println(formatDate);

if (Integer.parseInt(addDateCount)!=0) {
    df5 = new SimpleDateFormat("E, MMMM dd, yyyy");
    Calendar cal = Calendar.getInstance();
    sdf = new SimpleDateFormat("MMMM dd, yyyy");
    cal.setTime(df5.parse(newDate));
    cal.add(Calendar.DATE, Integer.parseInt(addDateCount));
    Date currentDatePlus = cal.getTime();
    formatDate = sdf.format(currentDatePlus);
    System.out.println(formatDate);
	
}
 

        HashMapContainer.add("$$SelectedAppointmentDateInWordsSA", formatDate);

 

    }catch(Exception e)
		{
    	e.printStackTrace();
		new ECom_Exception(methodName, e);

		}
		}
	
    
	@When("^I select enabled appointment date '(.*)' after the current enabled date in the feild '(.*)'$")
	    public static void i_select_appointment_date_after_current_enabled_date_in_the_feild(String date, String element)
	            throws ECom_Exception, Exception {

		String methodName= "i_select_appointment_date_after_current_enabled_date_in_the_feild '"+date+"' '"+element+"'";

		try {
	        driver.findElement(GetPageObjectRead.OR_GetElement(element)).click();;
	        int dateCount=0;
	        String selectedMonth=null;
	        String selectedDay=null;
	        for (int i = 0; i < 5; i++) {
	            List<WebElement> enabledDates = driver.findElements(By.xpath("//div[@aria-disabled='false' and not(contains(@class,'outside-month'))]"));
	            System.out.println(enabledDates.size());
	            dateCount = enabledDates.size()+dateCount;
	            if (Integer.parseInt(date) < dateCount) {
	                int dateSelect = enabledDates.size()+dateCount-enabledDates.size()-enabledDates.size();
	                System.out.println(dateSelect);
	                System.out.println(dateCount);
	                int datepresent = 1*(Integer.parseInt(date)-dateSelect);
	                if (datepresent==0) {
                		datepresent = datepresent+1;
	                }
	                
	                WebElement dateClick = driver.findElement(
	                        By.xpath("(//div[@aria-disabled='false'and not(contains(@class,'outside-month'))])[" + datepresent+"]"));
	                 selectedDay = dateClick.getText();
	                System.out.println("Selected Date "+ selectedDay);
	                WebElement month = driver.findElement(By.xpath("(//select)[1]"));
	                Select sc = new Select(month);
	                 selectedMonth = sc.getFirstSelectedOption().getText();
	                System.out.println("Selected Month "+ selectedMonth);
	                dateClick.click();
	                break;
	            }else {
	                driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();


	            }
	        }
	        


	        HashMapContainer.add("$$SelectedAppointmentDateInWordsSA", selectedMonth +" "+selectedDay+", " +"2020");
	        
	        String[] monthNames = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
	                "October", "November", "December" };
	        
	        
	        for (int i = 0; i < monthNames.length; i++) {
	            if (monthNames[i].startsWith(selectedMonth)) {
	                selectedMonth= String.valueOf(i+1);
	            }
	            
	        }

	 

	        if (selectedMonth.length()==1) {
	            selectedMonth="0"+selectedMonth;
	        }
	        HashMapContainer.add("$$SelectedAppointmentDateSA", selectedMonth +"-"+selectedDay+"-" +"2020");

	        System.out.println(selectedMonth +"-"+selectedDay+"-" +"2020");
	    }catch(Exception e) {
	    	e.printStackTrace();
	    	new ECom_Exception(methodName, e);
	    }
	}
	
	public static void javaScriptClick(WebElement webElement) throws ECom_Exception, Exception {
		
		String methodName= "javaScriptClick '"+webElement+"'";
		
		try {
			WebDriverWait wb = new WebDriverWait(driver, 20);
			wb.until(ExpectedConditions.visibilityOf(webElement));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", webElement);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	    	new ECom_Exception(methodName, e);
		}

	}
	
	@When("^I Select the Appointment Type '(.*)' and Duration '(.*)' from New Appointment$")
	public static void i_select_appointment_type_ca(String appointment, String Duration) throws Exception {
		
//		String methodName= "i_select_appointment_type_ca '"+appointment+"' '"+Duration+"'";

		try {
			WebDriverWait wb = new WebDriverWait(driver, 50);
			String option = null;
			if (appointment.contains("-")) {
				String[] split = appointment.split("-");
				appointment = split[0];
				option = split[1];
			}
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			WebElement Newtype = driver.findElement(By.xpath("//h3[text()='" + appointment + "']"));
			Thread.sleep(2000);
//		wb.until(ExpectedConditions.visibilityOf(type));
			WebElement Description = driver.findElement(By.xpath("//p[contains(text(),'" + Duration + "')]"));
			Boolean ApptDescription = Description.isDisplayed();
			if(ApptDescription)
			{
				assertTrue(ApptDescription);
			}else
			{
			  throw new Exception("Element Not Found:" + Duration);
			}
			executor.executeScript("arguments[0].click();", Newtype);
			CommonSteps.I_clickJS("ContinueBtn_SA");
//		multipleTrytoFindElement(type);

			if (appointment.equals("bridal")) {
				if (option.equals("First")) {
					WebElement FirstTime = driver.findElement(By.xpath("//h3[text()='first time bridal appointment']"));
					executor.executeScript("arguments[0].click();", FirstTime);
					CommonSteps.I_clickJS("ContinueBtn_SA");

				} else if (option.equals("Comeback")) {
					WebElement ComeBack = driver.findElement(By.xpath("//h3[text()='comeback bridal appointment']"));
					executor.executeScript("arguments[0].click();", ComeBack);
					CommonSteps.I_clickJS("ContinueBtn_SA");


				}
			}

			if (appointment.equals("alterations")) {
				if (option.equals("BridalFitting")) {
					WebElement Bridal = driver.findElement(By.xpath("//h3[text()='bridal fitting']"));
					WebElement Bridalfittingappt = driver.findElement(By.xpath("//p[contains(text(),'60 minutes to say yes to the ideal fit for your dream dress')]"));
					Boolean BridalDescription = Bridalfittingappt.isDisplayed();
					if(BridalDescription)
					{
						assertTrue(BridalDescription);
					}else
					{
					  throw new Exception("Element Not Found:");
					}
					executor.executeScript("arguments[0].click();", Bridal);
					CommonSteps.I_clickJS("ContinueBtn_SA");

				} else if (option.equals("DressFitting")) {
					WebElement Dress = driver.findElement(By.xpath("//h3[text()='dress fitting']"));
					WebElement Dressfittingappt = driver.findElement(By.xpath("//p[contains(text(),'30 minutes to have magic worked on your dress for your upcoming event ')]"));
					Boolean DressDescription = Dressfittingappt.isDisplayed();
					if(DressDescription)
					{
						assertTrue(DressDescription);
					}else
					{
					  throw new Exception("Element Not Found:");
					}
					executor.executeScript("arguments[0].click();", Dress);
					CommonSteps.I_clickJS("ContinueBtn_SA");


				}
				

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
//	    	new ECom_Exception(methodName, e);

		}

	}
	
	@When("^I select sms opt '(.*)' option from New Appointment$")
	public static void i_sms_opt_option(String option) throws ECom_Exception, Exception {
		String methodName= "i_sms_opt_option '"+option+"'";

		
		try {
			if (option.equalsIgnoreCase("yes")) {
				log.info("Sms opt checked in");
			} else if (option.equalsIgnoreCase("no")) {
				driver.findElement(GetPageObjectRead.OR_GetElement("SMSOptionCheck_SA")).click();
				log.info("Sms opt unchecked");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);		
			}

	}

	@When("^I select event date '(.*)' after the appointment date in the feild '(.*)'$")
	public static void i_select_event_date_after_appointment_date_in_the_feild(String date, String element)
			throws Throwable {
		
		String methodName= "i_select_event_date_after_appointment_date_in_the_feild '"+date+"' '"+element+"'";

		try {
		String savedDate = HashMapContainer.get("$$SelectedAppointmentDateSA").replace("/", "-");
		System.out.println(savedDate);
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(savedDate));
		c.add(Calendar.DATE, Integer.parseInt(date));
		String newDate = sdf.format(c.getTime());
		Actions ac = new Actions(driver);

		WebElement dateInput = driver.findElement(GetPageObjectRead.OR_GetElement(element));
		if (StepBase.testBrowser.equalsIgnoreCase("firefox") || System.getProperty("test.TestName").toLowerCase().contains("firefox") ) {
			ac.sendKeys(dateInput, newDate).build().perform();
			Thread.sleep(1000);
		}else if (StepBase.testBrowser.equalsIgnoreCase("mobile-chromeemulator")) {
			ac.sendKeys(dateInput, newDate).build().perform();

			ac.click(dateInput).build().perform();
			Thread.sleep(2000);
			WebElement monthDD = driver.findElement(By.xpath("(//select)[1]"));
			Select sc = new Select(monthDD);
			String[] split = newDate.split("-");
			int parseInt = Integer.parseInt(split[0])-1;
			sc.selectByValue(String.valueOf(parseInt));
			Thread.sleep(1000);

			driver.findElement(By.xpath("//div[@aria-disabled=\"false\" and text()='"+split[1]+"']")).click();
		}else {
			dateInput.click();
			Thread.sleep(2000);

			System.out.println(newDate);
			dateInput.sendKeys(newDate);
		}

	}catch(Exception e) {
		e.printStackTrace();
		new ECom_Exception(methodName, e);	
	}

}
}