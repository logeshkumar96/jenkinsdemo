package com.TEAF.stepDefinitions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.minidev.json.JSONValue;

public class CustomerUpSert {

	private String Area_Manager__c;
	private String Channel__c;
	private String Closed_Date__c;
	private String District_Manager__c;
	private String Fax;
	private String Name;
	private String Open_Date__c;
	private String RecordTypeId;
	private String Warehouse_Code__c;
	private String Warehouse__c;
	private boolean Web_Store__c;
	private String Regional_Vice_President__c;
	private String Sales_Market__c;
	private String ShippingCity;
	private String ShippingCountry;
	private String ShippingPostalCode;
	private String ShippingState;
	private String ShippingStreet;
	private String Store_Region__c;
	private String Store_Warehouse_Email__c;
	private String Store_Warehouse_Status_Code__c;
	private String MFRM_Oracle_Record_ID__c;
	private String Phone;
	private String District__c;
	private String DMA__c;
	private String Divisional_Vice_President__c;
	private String Hours_of_Operations__c;
	
	
	public String getHours_of_Operations__c() {
		return Hours_of_Operations__c;
	}
	
	public void setHours_of_Operations__c(String hours) {
		Hours_of_Operations__c = hours;
	}
	
	
	public String getDivisional_Vice_President__c() {
		return Divisional_Vice_President__c;
	}
	
	public void setDivisional_Vice_President__c(String division) {
		Divisional_Vice_President__c = division;
	}
	
	
	
	public String getDMA__c() {
		return DMA__c;
	}
	
	public void setDMA__c(String DMA) {
		DMA__c = DMA;
	}
	
	
	public String getDistrict__c() {
		return District__c;
	}
	
	public void setDistrict__c(String district__c) {
		Area_Manager__c = district__c;
	}
	
	public String getArea_Manager__c() {
		return Area_Manager__c;
	}

	public void setArea_Manager__c(String area_Manager__c) {
		Area_Manager__c = area_Manager__c;
	}

	public String getChannel__c() {
		return Channel__c;
	}

	public void setChannel__c(String channel__c) {
		Channel__c = channel__c;
	}

	public String getClosed_Date__c() {
		return Closed_Date__c;
	}

	public void setClosed_Date__c(String closed_Date__c) {
		Closed_Date__c = closed_Date__c;
	}

	public String getDistrict_Manager__c() {
		return District_Manager__c;
	}

	public void setDistrict_Manager__c(String district_Manager__c) {
		District_Manager__c = district_Manager__c;
	}

	public String getFax() {
		return Fax;
	}

	public void setFax(String fax) {
		Fax = fax;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getOpen_Date__c() {
		return Open_Date__c;
	}

	public void setOpen_Date__c(String open_Date__c) {
		Open_Date__c = open_Date__c;
	}

	public String getRecordTypeId() {
		return RecordTypeId;
	}

	public void setRecordTypeId(String recordTypeId) {
		RecordTypeId = recordTypeId;
	}

	public String getWarehouse_Code__c() {
		return Warehouse_Code__c;
	}

	public void setWarehouse_Code__c(String warehouse_Code__c) {
		Warehouse_Code__c = warehouse_Code__c;
	}

	public String getWarehouse__c() {
		return Warehouse__c;
	}

	public void setWarehouse__c(String warehouse__c) {
		Warehouse__c = warehouse__c;
	}

	public boolean getWeb_Store__c() {
		return Web_Store__c;
	}

	public void setWeb_Store__c(boolean web_Store__c) {
		Web_Store__c = web_Store__c;
	}

	public String getRegional_Vice_President__c() {
		return Regional_Vice_President__c;
	}

	public void setRegional_Vice_President__c(String regional_Vice_President__c) {
		Regional_Vice_President__c = regional_Vice_President__c;
	}

	public String getSales_Market__c() {
		return Sales_Market__c;
	}

	public void setSales_Market__c(String sales_Market__c) {
		Sales_Market__c = sales_Market__c;
	}

	public String getShippingCity() {
		return ShippingCity;
	}

	public void setShippingCity(String shippingCity) {
		ShippingCity = shippingCity;
	}

	public String getShippingCountry() {
		return ShippingCountry;
	}

	public void setShippingCountry(String shippingCountry) {
		ShippingCountry = shippingCountry;
	}

	public String getShippingPostalCode() {
		return ShippingPostalCode;
	}

	public void setShippingPostalCode(String shippingPostalCode) {
		ShippingPostalCode = shippingPostalCode;
	}

	public String getShippingState() {
		return ShippingState;
	}

	public void setShippingState(String shippingState) {
		ShippingState = shippingState;
	}

	public String getShippingStreet() {
		return ShippingStreet;
	}

	public void setShippingStreet(String shippingStreet) {
		ShippingStreet = shippingStreet;
	}

	public String getStore_Region__c() {
		return Store_Region__c;
	}

	public void setStore_Region__c(String store_Region__c) {
		Store_Region__c = store_Region__c;
	}

	public String getStore_Warehouse_Email__c() {
		return Store_Warehouse_Email__c;
	}

	public void setStore_Warehouse_Email__c(String store_Warehouse_Email__c) {
		Store_Warehouse_Email__c = store_Warehouse_Email__c;
	}

	public String getStore_Warehouse_Status_Code__c() {
		return Store_Warehouse_Status_Code__c;
	}

	public void setStore_Warehouse_Status_Code__c(String store_Warehouse_Status_Code__c) {
		Store_Warehouse_Status_Code__c = store_Warehouse_Status_Code__c;
	}

	public String getMFRM_Oracle_Record_ID__c() {
		return MFRM_Oracle_Record_ID__c;
	}

	public void setMFRM_Oracle_Record_ID__c(String mFRM_Oracle_Record_ID__c) {
		MFRM_Oracle_Record_ID__c = mFRM_Oracle_Record_ID__c;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

}
