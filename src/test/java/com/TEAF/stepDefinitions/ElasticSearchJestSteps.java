package com.TEAF.stepDefinitions;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.junit.Assert;

import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.RestAssuredUtility;
import com.TEAF.framework.Utilities;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobId;
import com.google.cloud.bigquery.JobInfo;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.Resources.CRM_SF_Payload_Pojo;
import com.Resources.Shippo_tracking_ES_to_BQ_Pojo;
import com.Resources.inContactBQ_CSVPojo_WH_EXPD_AgentList;
import com.TEAF.framework.API_UserDefined;
import com.TEAF.framework.ElasticSearchJestUtility;
import com.jayway.jsonpath.Configuration;
import com.opencsv.CSVWriter;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.deps.com.google.gson.JsonObject;
import io.cucumber.datatable.DataTable;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import io.searchbox.core.SearchResult;

public class ElasticSearchJestSteps {
	static Logger log = Logger.getLogger(ElasticSearchJestSteps.class.getName());
	static API_UserDefined ap = new API_UserDefined();

	
	@Given("^ES Search '(.*)'$")
	public static void jestSearch(String url) throws Throwable {
		ElasticSearchJestUtility.jestClient(url);
	}

	@When("^Select and Verify the index '(.*)' exists$")
	public static void select_verify_the_index(String index) throws IOException {
		ElasticSearchJestUtility.getIndicesExists(index);
	}

	@When("^Get Result as String by passing Query '(.*)'$")
	public static void get_result_as_string_by_passing_query(String query) throws Throwable {
		if (query.startsWith("$$")) {
			query = HashMapContainer.get(query);
		}
		ElasticSearchJestUtility.searchQuerywithIndex(query);
		Object read = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.total");
		log.info("No of Records found in DB : " + read);
	}
	
	@Then("^Get Result as String by passing Query with tracking number fetched from orderdelivery index$")
	public void Get_Result_as_String_by_passing_Query_with_tracking_number_fetched_from_orderdelivery_index() throws Throwable {
		String track_num = HashMapContainer.get("$$Odata_Shippo_tracking_Number");
		String query = "{\"query\":{\"match\":{\"tracking_number.keyword\":\""+track_num+"\"}}}";
		ElasticSearchJestUtility.searchQuerywithIndex(query);
		Object read = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.total");
		log.info("No of Records found in DB : " + read);
	}
	
	List<CRM_SF_Payload_Pojo> crm_SF_Payload_arraylist = new ArrayList<CRM_SF_Payload_Pojo>();
	List<String> so_ln_orderNumbers = new ArrayList<String>();
	
	@Then("^Save order numbers, item code, MFIPOSCOSTAMT and quantity from ES query response$")
	public void save_ordernumbers_itemcode_MFIPOSCOSTAMT_and_quantity_from_ES_query_response() throws Throwable {
		
		so_ln_orderNumbers = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[*]._source.DEL_DOC_NUM");
		
		for(int i=0; i<so_ln_orderNumbers.size(); i++) {
			CRM_SF_Payload_Pojo crm_SF_Payload_Pojo = new CRM_SF_Payload_Pojo();
			System.out.println("query : "+"{\"query\":{\"match\":{\"DEL_DOC_NUM.keyword\":\""+so_ln_orderNumbers.get(i)+"\"}}}");
			ElasticSearchJestUtility.searchQuerywithIndex("{\"query\":{\"match\":{\"DEL_DOC_NUM.keyword\":\""+so_ln_orderNumbers.get(i)+"\"}}}");
			
			int totalCountOfRecords = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.total");
			System.out.println("totalCountOfRecords: "+totalCountOfRecords);
			Double mfiposQty = 0.0;
			String allItm_CD = null;
			for (int j=0; j<totalCountOfRecords; j++) {
				String qtyPath = "$.hits.hits["+j+"]._source.QTY";
				String mfiPOSCOSTAMT = "$.hits.hits["+j+"]._source.MFIPOSCOSTAMT";
				String itm_CD = "$.hits.hits["+j+"]._source.ITM_CD";
				Object qty = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), qtyPath);
				Double qtyDouble = 0.0;
				if(qty instanceof Integer) {
					System.out.println("INteger qty");
					qtyDouble = Double.parseDouble(qty.toString());
				}else if (qty instanceof Double) {
					System.out.println("DOuble qty");
					qtyDouble = (Double) qty;
				}
				Object mfiposcostamt = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), mfiPOSCOSTAMT);
				Double mfiposcostamtDouble = 0.0;
				if(mfiposcostamt instanceof Integer) {
					System.out.println("INteger mfipos");
					mfiposcostamtDouble = Double.parseDouble(mfiposcostamt.toString());
				}else if (mfiposcostamt instanceof Double) {
					System.out.println("DOuble mfipos");
					mfiposcostamtDouble = (Double) mfiposcostamt;
				}
				String itm_cd = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), itm_CD);
				System.out.println("^^"+qty+" "+mfiposcostamt+" "+itm_cd);
				mfiposQty = (mfiposQty+(mfiposcostamtDouble*qtyDouble));
				System.out.println("****"+mfiposQty);
				if (allItm_CD==null) {
					allItm_CD = itm_cd;
				}else {
					allItm_CD = allItm_CD+";"+itm_cd;
				}
			}
			crm_SF_Payload_Pojo.setOrder_Number__c(so_ln_orderNumbers.get(i));
			crm_SF_Payload_Pojo.setTotal_COGS__C(mfiposQty.toString());
			crm_SF_Payload_Pojo.setItem_Number__c(allItm_CD);
			
			crm_SF_Payload_arraylist.add(crm_SF_Payload_Pojo);
		}
		
//		for (CRM_SF_Payload_Pojo a: crm_SF_Payload_arraylist) {
//			System.out.println("******"+a.getOrder_Number__c());
//			System.out.println("^^^^^^"+a.getTotal_COGS__C());
//			System.out.println("@@@@@@"+a.getItem_Number__c());
//			System.out.println("&&&&&&");
//		}
	}
	
	@Then("^Get required data by passing order numbers fetched form previous index$")
	public void Get_required_data_by_passing_order_numbers_fetched_form_previous_index() throws Throwable {
		
		for (int i=0; i<so_ln_orderNumbers.size(); i++) {
			System.out.println("so index query: "+"{\"query\":{\"match\":{\"DEL_DOC_NUM.keyword\":\""+so_ln_orderNumbers.get(i)+"\"}}}");
			ElasticSearchJestUtility.searchQuerywithIndex("{\"query\":{\"match\":{\"DEL_DOC_NUM.keyword\":\""+so_ln_orderNumbers.get(i)+"\"}}}");
			String del_DOC_NUM = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.DEL_DOC_NUM");
			System.out.println("del_DOC_NUM: "+del_DOC_NUM);
			for (CRM_SF_Payload_Pojo a: crm_SF_Payload_arraylist) {
				if(a.getOrder_Number__c().contains(del_DOC_NUM)) {
					
					int rowIndex = crm_SF_Payload_arraylist.indexOf(a);
//					CRM_SF_Payload_Pojo crm_SF_Payload_Pojo = new CRM_SF_Payload_Pojo();

					String custName = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.CUST_NAME");
					String ship_TO_ADDR1 = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.SHIP_TO_ADDR1");
					String ship_TO_ADDR2 = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.SHIP_TO_ADDR2");
					String ship_TO_CITY = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.SHIP_TO_CITY");
					String ship_TO_ST_CD = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.SHIP_TO_ST_CD");
					String ship_TO_ZIP_CD = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.SHIP_TO_ZIP_CD");
		//			String del_DOC_NUM = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.DEL_DOC_NUM");
					String description = custName+" needs Nationwide delivery at "+ship_TO_ADDR1+","+ship_TO_ADDR2+","+ship_TO_CITY+","+ship_TO_ST_CD+","+ship_TO_ZIP_CD+" with SO# ["+del_DOC_NUM+"]";
					String sales_PERSON_NAME1 = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.SALES_PERSON_NAME1");
					String ship_TO_F_NAME = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.SHIP_TO_F_NAME");
					String ship_TO_L_NAME = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.SHIP_TO_L_NAME");
					String cust_CD = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.CUST_CD");
					String email_ADDR = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.EMAIL_ADDR");
					String so_WR_DT = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.SO_WR_DT");
					String sales_PERSON_NAME2 = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.SALES_PERSON_NAME2");
					String ord_TP_CD = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.ORD_TP_CD");
					String sO_STORE_CD = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.SO_STORE_CD");
					
					a.setDescription(description);
					a.setSubject(custName);
					a.setSales_Associate_Name_1__c(sales_PERSON_NAME1);
					a.setCustomer_First_Name__c(ship_TO_F_NAME);
					a.setCustomer_Last_Name__c(ship_TO_L_NAME);
					a.setCustomer_Street__c(ship_TO_ADDR1);
					a.setCustomer_Street_Line_2__c(ship_TO_ADDR2);
					a.setCustomer_City__c(ship_TO_CITY);
					a.setCustomer_Code_Ax__c(cust_CD);
					a.setCustomer_Email_Ax__c(email_ADDR);
					a.setCustomer_State__c(ship_TO_ST_CD);
					a.setCustomer_Zip_Code__c(ship_TO_ZIP_CD);
					a.setOriginal_Order_NumberX__c(del_DOC_NUM);
					a.setOrder_Written_Date__c(so_WR_DT);
					a.setSales_Associate_Name_2__c(sales_PERSON_NAME2);
					a.setType(ord_TP_CD);
					a.setSO_STORE_CD(sO_STORE_CD);
					
					crm_SF_Payload_arraylist.set(rowIndex, a);
					break;
				}
			}
		}
		
//		for (CRM_SF_Payload_Pojo a: crm_SF_Payload_arraylist) {
//			System.out.println("getOrder_Number__c "+a.getOrder_Number__c());
//			System.out.println("getOriginal_Order_NumberX__c "+a.getOriginal_Order_NumberX__c());
//			System.out.println("getTotal_COGS__C "+a.getTotal_COGS__C());
//			System.out.println("getItem_Number__c "+a.getItem_Number__c());
//			System.out.println("getDescription "+a.getDescription());
//			System.out.println("getSubject "+a.getSubject());
//			System.out.println("getType "+a.getType());
//			System.out.println("getOrder_Written_Date__c "+a.getOrder_Written_Date__c());
//			System.out.println("getCustomer_Street__c "+a.getCustomer_Street__c());
//			System.out.println("getSO_STORE_CD "+a.getSO_STORE_CD());
//			System.out.println("&&&&&&\n");
//		}
	}
	
	@Then("^Get required data by passing store numbers fetched form previous index$")
	public void Get_required_data_by_passing_store_numbers_fetched_form_previous_index() throws Throwable {
		
		for (CRM_SF_Payload_Pojo a: crm_SF_Payload_arraylist) {
			int indexOfTheRow = crm_SF_Payload_arraylist.indexOf(a);
			System.out.println("store index query: "+"{\"query\":{\"match\":{\"Id.keyword\":\"AX-"+a.getSO_STORE_CD()+"\"}},\"_source\":{\"include\":[\"PurchaseChannel\",\"Email\"]}}");
			ElasticSearchJestUtility.searchQuerywithIndex("{\"query\":{\"match\":{\"Id.keyword\":\"AX-"+a.getSO_STORE_CD()+"\"}},\"_source\":{\"include\":[\"PurchaseChannel\",\"Email\"]}}");
			String purchaseChannel = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.PurchaseChannel");
			String email = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.Email");
			
			a.setSource__c(purchaseChannel);
			a.setStore_Email__c(email);
			
			//Hardcoded fields
			a.setOrigin("Mulesoft");
			a.setPriority("Medium");
			a.setRecordtypeid("0128A000001XqoVQAS");
			a.setStatus("New");
			
			crm_SF_Payload_arraylist.set(indexOfTheRow, a);
		}
		
//		for (CRM_SF_Payload_Pojo a: crm_SF_Payload_arraylist) {
//			System.out.println("getOrder_Number__c "+a.getOrder_Number__c());
//			System.out.println("getOriginal_Order_NumberX__c "+a.getOriginal_Order_NumberX__c());
//			System.out.println("getTotal_COGS__C "+a.getTotal_COGS__C());
//			System.out.println("getItem_Number__c "+a.getItem_Number__c());
//			System.out.println("getDescription "+a.getDescription());
//			System.out.println("getSubject "+a.getSubject());
//			System.out.println("getType "+a.getType());
//			System.out.println("getOrder_Written_Date__c "+a.getOrder_Written_Date__c());
//			System.out.println("getCustomer_Street__c "+a.getCustomer_Street__c());
//			System.out.println("getSO_STORE_CD "+a.getSO_STORE_CD());
//			System.out.println("getStore_Email__c "+a.getStore_Email__c());
//			System.out.println("getSource__c "+a.getSource__c());
//			System.out.println("\n");
//		}
	}
	
	@Then("^Get required data by passing sales person names fetched form previous index$")
	public void Get_required_data_by_passing_sales_person_names_fetched_form_previous_index() throws Throwable {
		
		for (CRM_SF_Payload_Pojo a: crm_SF_Payload_arraylist) {
			int indexOfTheRow = crm_SF_Payload_arraylist.indexOf(a);
			if (a.getSales_Associate_Name_1__c().isEmpty()) {
				a.setSales_Associate_Email_1__c(a.getSales_Associate_Name_1__c());
			}else {
				String[] salesPersonName1 = a.getSales_Associate_Name_1__c().split(" ");
				System.out.println("employee index query: "+"{\"query\":{\"bool\":{\"must\":[{\"term\":{\"First_Name.keyword\":\""+salesPersonName1[0]+"\"}},{\"term\":{\"Last_Name.keyword\":\""+salesPersonName1[1]+"\"}}]}}}");
				ElasticSearchJestUtility.searchQuerywithIndex("{\"query\":{\"bool\":{\"must\":[{\"term\":{\"First_Name.keyword\":\""+salesPersonName1[0]+"\"}},{\"term\":{\"Last_Name.keyword\":\""+salesPersonName1[1]+"\"}}]}}}");
				String salesPerson1Email = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.Email");
				a.setSales_Associate_Email_1__c(salesPerson1Email);
			}
			
			if (a.getSales_Associate_Name_2__c().isEmpty()) {
				a.setSales_Associate_Email_2__c(a.getSales_Associate_Name_2__c());
			}else {
				String[] salesPersonName2 = a.getSales_Associate_Name_2__c().split(" ");
				System.out.println("employee index query: "+"{\"query\":{\"bool\":{\"must\":[{\"term\":{\"First_Name.keyword\":\""+salesPersonName2[0]+"\"}},{\"term\":{\"Last_Name.keyword\":\""+salesPersonName2[1]+"\"}}]}}}");
				ElasticSearchJestUtility.searchQuerywithIndex("{\"query\":{\"bool\":{\"must\":[{\"term\":{\"First_Name.keyword\":\""+salesPersonName2[0]+"\"}},{\"term\":{\"Last_Name.keyword\":\""+salesPersonName2[1]+"\"}}]}}}");
				String salesPerson2Email = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.Email");
				a.setSales_Associate_Email_2__c(salesPerson2Email);
			}
			crm_SF_Payload_arraylist.set(indexOfTheRow, a);
		}
		
//		for (CRM_SF_Payload_Pojo a: crm_SF_Payload_arraylist) {
//			System.out.println("getOrder_Number__c "+a.getOrder_Number__c());
//			System.out.println("getOriginal_Order_NumberX__c "+a.getOriginal_Order_NumberX__c());
//			System.out.println("getTotal_COGS__C "+a.getTotal_COGS__C());
//			System.out.println("getItem_Number__c "+a.getItem_Number__c());
//			System.out.println("getDescription "+a.getDescription());
//			System.out.println("getSubject "+a.getSubject());
//			System.out.println("getType "+a.getType());
//			System.out.println("getOrder_Written_Date__c "+a.getOrder_Written_Date__c());
//			System.out.println("getCustomer_Street__c "+a.getCustomer_Street__c());
//			System.out.println("getSO_STORE_CD "+a.getSO_STORE_CD());
//			System.out.println("getStore_Email__c "+a.getStore_Email__c());
//			System.out.println("getSource__c "+a.getSource__c());
//			System.out.println("getSales_Associate_Email_1__c "+a.getSales_Associate_Email_1__c());
//			System.out.println("getSales_Associate_Email_2__c "+a.getSales_Associate_Email_2__c());
//			System.out.println("getOrigin "+a.getOrigin());
//			System.out.println("getPriority "+a.getPriority());
//			System.out.println("getRecordtypeid "+a.getRecordtypeid());
//			System.out.println("getStatus "+a.getStatus());
//			System.out.println("getCustomer_Code_Ax__c "+a.getCustomer_Code_Ax__c());
//			System.out.println("\n");
//		}
	}
	
	RestAssuredUtility Rest = new RestAssuredUtility();
	
	@Then("^Create API request body with customer code from so index to fetch contactID from SF$")
	public void Create_request_body_with_customer_code_from_so_index_fetch_contactID_from_SF() throws Exception {
		
		for (CRM_SF_Payload_Pojo a: crm_SF_Payload_arraylist) {
			
//			RequestSpecification spec;
			int indexValue = crm_SF_Payload_arraylist.indexOf(a);
			
			HashMap<String, String> hs1 = new HashMap<String, String>();
			HashMap<String, ArrayList<String>> hs2 = new HashMap<String, ArrayList<String>>();
//			Map<String, ArrayList<String>> hm1 = new java.util.HashMap<String, ArrayList<String>>();
			
			hs1.put("ObjectTableName", "Contact");
			String custCode = "Customer_Code__c = '"+a.getCustomer_Code_Ax__c()+"' order by LastModifiedDate desc ";
//			String custCode = "Customer_Code__c = 'AX-C015455429' order by LastModifiedDate desc ";
			System.out.println("custCode "+custCode);
			hs1.put("WhereClause", custCode);
			hs1.put("LimitCount", "2500");
			ArrayList<String> al = new ArrayList<String>();
			al.add("Id");
			hs2.put("Columns", al);
			
			JSONObject jobj = new JSONObject();
			jobj.putAll(hs1);
			jobj.putAll(hs2);
			System.out.println("^^^^^"+jobj.toString());
			
//			jobj.put("ObjectTableName", "Contact");
//			jobj.put("WhereClause", "Customer_Code__c = 'AX-C015455429' order by LastModifiedDate desc ");
//			jobj.put("LimitCount", "2500");
//			jobj.put("Columns", ["Id"]);
//			RequestSpecification spec;
//			spec = ap.getReq().body(jobj.toString());
//			ap.setReq(spec);
//			spec = ap.getReq().contentType("application/json");
//			ap.setReq(spec);
//			
//			String Path = "http://mule-worker-internal-dev-sys-sf.us-w2.cloudhub.io:8091/salesforce/dynamicquery";
////			String Path = ap.getPath();
//			System.out.println("$$$$"+Path);
//			spec = ap.getReq().log().all();
//			Response getResponse = Rest.Post(Path, spec);
//			ap.setRes(getResponse);
//			ap.setReq(given().when());
//			
//			System.out.println("*****\n"+getResponse.prettyPrint());
//			
//			Response res = ap.getRes();
//			
//			String path = "$.[0]Id";
//			RestAssuredSteps rs = new RestAssuredSteps();
//			rs.retrivedata_store(path, "contactID");
////			Object value = JsonPath.read(res.getBody().asString(), path);
//			System.out.println("%%%%"+HashMapContainer.get("contactID"));
			
			Response response22 = given().contentType("application/json").body(jobj).when().post("http://mule-worker-internal-dev-sys-sf.us-w2.cloudhub.io:8091/salesforce/dynamicquery");
			String contactId = response22.getBody().jsonPath().getJsonObject("Id").toString().replace("[", "").replace("]", "");
			System.out.println("contactId "+contactId);
			
			a.setContactid(contactId);
			
			crm_SF_Payload_arraylist.set(indexValue, a);
		}
		
		for (CRM_SF_Payload_Pojo a: crm_SF_Payload_arraylist) {
			System.out.println("getOrder_Number__c "+a.getOrder_Number__c());
			System.out.println("getOriginal_Order_NumberX__c "+a.getOriginal_Order_NumberX__c());
			System.out.println("getTotal_COGS__C "+a.getTotal_COGS__C());
			System.out.println("getItem_Number__c "+a.getItem_Number__c());
			System.out.println("getDescription "+a.getDescription());
			System.out.println("getSubject "+a.getSubject());
			System.out.println("getType "+a.getType());
			System.out.println("getOrder_Written_Date__c "+a.getOrder_Written_Date__c());
			System.out.println("getCustomer_Street__c "+a.getCustomer_Street__c());
			System.out.println("getSO_STORE_CD "+a.getSO_STORE_CD());
			System.out.println("getStore_Email__c "+a.getStore_Email__c());
			System.out.println("getSource__c "+a.getSource__c());
			System.out.println("getSales_Associate_Email_1__c "+a.getSales_Associate_Email_1__c());
			System.out.println("getSales_Associate_Email_2__c "+a.getSales_Associate_Email_2__c());
			System.out.println("getOrigin "+a.getOrigin());
			System.out.println("getPriority "+a.getPriority());
			System.out.println("getRecordtypeid "+a.getRecordtypeid());
			System.out.println("getStatus "+a.getStatus());
			System.out.println("getCustomer_Code_Ax__c "+a.getCustomer_Code_Ax__c());
			System.out.println("getContactid "+a.getContactid());
			System.out.println("\n");
		}
	}
	
//	@When("^Get Result as String by passing multiple Queries and fetch required data$")
//	public void get_result_as_string_by_passing_query(DataTable queries) throws Throwable {
//		CRM_SF_Payload_Pojo crm_SF_Payload_Pojo = new CRM_SF_Payload_Pojo();
//		List<String> querylist = queries.asList();
////		List<String> allOrderNumbers = new ArrayList<String>();
//		
//		for (String query: querylist) {
//			ElasticSearchJestUtility.searchQuerywithIndex(query);
//			List<String> orderNumbers = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[*]._source.DEL_DOC_NUM");
//			
//			for(int i=0; i<orderNumbers.size(); i++) {
//				
//				System.out.println("query : "+"{\"query\":{\"match\":{\"DEL_DOC_NUM.keyword\":\""+orderNumbers.get(i)+"\"}}}");
//				ElasticSearchJestUtility.searchQuerywithIndex("{\"query\":{\"match\":{\"DEL_DOC_NUM.keyword\":\""+orderNumbers.get(i)+"\"}}}");
//				
//				int totalCountOfRecords = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.total");
//				Double mfiposQty = 0.0;
//				String allItm_CD = null;
//				for (int j=0; j<totalCountOfRecords; j++) {
//					String qtyPath = "$.hits.hits["+j+"]._source.QTY";
//					String mfiPOSCOSTAMT = "$.hits.hits["+j+"]._source.MFIPOSCOSTAMT";
//					String itm_CD = "$.hits.hits["+j+"]._source.ITM_CD";
//					Object dd = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), qtyPath);
//					System.out.println("!!!!"+dd);
//					Double qty = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), qtyPath);
//					Double mfiposcostamt = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), mfiPOSCOSTAMT);
//					String itm_cd = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), itm_CD);
//					System.out.println("^^"+qty+" "+mfiposcostamt+" "+itm_cd);
//					mfiposQty = (mfiposQty+(mfiposcostamt*qty));
//					
//					if (allItm_CD==null) {
//						allItm_CD = itm_cd;
//					}else {
//						allItm_CD = allItm_CD+";"+itm_cd;
//					}
//				}
//				crm_SF_Payload_Pojo.setOrder_Number__c(orderNumbers.get(i));
//				crm_SF_Payload_Pojo.setTotal_COGS__C(mfiposQty.toString());
//				crm_SF_Payload_Pojo.setItem_Number__c(allItm_CD);
//				
//				crm_SF_Payload_arraylist.add(crm_SF_Payload_Pojo);
//			}
//			
//			for (CRM_SF_Payload_Pojo a: crm_SF_Payload_arraylist) {
//				System.out.println("******"+a.getOrder_Number__c());
//				System.out.println("^^^^^^"+a.getTotal_COGS__C());
//				System.out.println("@@@@@@"+a.getItem_Number__c());
//				System.out.println("&&&&&&");
//			}
//		}
//			
//			
//			
//			
////			List<String> orderNumbers = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[*]._source.DEL_DOC_NUM");
////			allOrderNumbers.addAll(orderNumbers);
////		}
////			crm_SF_Payload_Pojo.setOriginal_Order_NumberX__c(allOrderNumbers);
////			crm_SF_Payload_Pojo.setOrder_Number__c(allOrderNumbers);
////			
////			crm_SF_Payload_arraylist.add(crm_SF_Payload_Pojo);
////			
////			for (CRM_SF_Payload_Pojo a: crm_SF_Payload_arraylist) {
////				System.out.println("******"+a.getOrder_Number__c());
////				System.out.println("******"+a.getOriginal_Order_NumberX__c());
////				System.out.println("******"+a.getOrder_Number__c().size());
////				System.out.println("******"+a.getOriginal_Order_NumberX__c().size());
////				System.out.println("&&");
////			}
//	}

	public static List<Shippo_tracking_ES_to_BQ_Pojo> shippo_tracking_ES_to_BQ_Pojo_ArrayList = new ArrayList<Shippo_tracking_ES_to_BQ_Pojo>();
	
	@Then("^Get Result by passing ES query using '(.*)' , '(.*)' and store results in a pojo$")
	public void get_BQ_results_using_gte_and_lte_for_ShippoTrackingStatus(String gte, String lte) throws Throwable {
		
		if (gte.startsWith("$$")) {
			gte = HashMapContainer.get(gte);
		}
		if (lte.startsWith("$$")) {
			lte = HashMapContainer.get(lte);
		}
		//FRAMING QUERY TO FETCH SINGLE RECORD DETAILS FROM BQ
		String query = "{\r\n" + 
				"  \"query\": {\r\n" + 
				"    \"bool\": {\r\n" + 
				"      \"must\": [\r\n" + 
				"        {\r\n" + 
				"          \"range\": {\r\n" + 
				"            \"ES_MODIFIEDDATETIME\": {\r\n" + 
//				"              \"gte\": \""+gte+"\",\r\n" + 
//				"              \"lte\": \""+lte+"\"\r\n" + 
				"              \"gte\": \"2021/03/18 12:00:00\",\r\n" + 
				"              \"lte\": \"2021/03/18 14:00:00\"\r\n" + 
				"            }\r\n" + 
				"          }\r\n" + 
				"        }\r\n" + 
				"      ]\r\n" + 
				"    }\r\n" + 
				"  },\r\n" + 
				"  \"sort\": [\r\n" + 
				"    {\r\n" + 
				"      \"ES_MODIFIEDDATETIME\": {\r\n" + 
				"        \"order\": \"desc\"\r\n" + 
				"      }\r\n" + 
				"    }\r\n" + 
				"  ],\r\n" + 
				"  \"size\": 10000 \r\n"+ 
				"}";
		System.out.println("ES query executed: "+query);
		ElasticSearchJestUtility.searchQuerywithIndex(query);
		int recordsCount = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.total");
		System.out.println("No of Records found in ES : " + recordsCount);
		log.info("No of Records found in ES : " + recordsCount);
		
		for (int i=0; i<recordsCount; i++) {
//		for (int i=0; i<10; i++) {
			
			Shippo_tracking_ES_to_BQ_Pojo shippo_tracking_ES_to_BQ_Pojo = new Shippo_tracking_ES_to_BQ_Pojo();
			
			shippo_tracking_ES_to_BQ_Pojo.setTracking_number(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.tracking_number"));
			shippo_tracking_ES_to_BQ_Pojo.setCarrier(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.carrier"));
			shippo_tracking_ES_to_BQ_Pojo.setService_level_token(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.service_level_token"));
			shippo_tracking_ES_to_BQ_Pojo.setService_level_name(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.service_level_name"));
			shippo_tracking_ES_to_BQ_Pojo.setAddress_from_city(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.address_from_city"));
			shippo_tracking_ES_to_BQ_Pojo.setAddress_from_state(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.address_from_state"));
			shippo_tracking_ES_to_BQ_Pojo.setAddress_from_zip(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.address_from_zip"));
			shippo_tracking_ES_to_BQ_Pojo.setAddress_from_country(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.address_from_country"));
			shippo_tracking_ES_to_BQ_Pojo.setAddress_to_city(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.address_to_city"));
			shippo_tracking_ES_to_BQ_Pojo.setAddress_to_state(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.address_to_state"));
			shippo_tracking_ES_to_BQ_Pojo.setAddress_to_zip(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.address_to_zip"));
			shippo_tracking_ES_to_BQ_Pojo.setAddress_to_country(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.address_to_country"));
			shippo_tracking_ES_to_BQ_Pojo.setEta(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.eta"));
			shippo_tracking_ES_to_BQ_Pojo.setOriginal_eta(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.original_eta"));
			shippo_tracking_ES_to_BQ_Pojo.setStatus_date(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.status_date"));
			shippo_tracking_ES_to_BQ_Pojo.setStatus_details(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.status_details"));
			shippo_tracking_ES_to_BQ_Pojo.setLocation_city(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.location_city"));
			shippo_tracking_ES_to_BQ_Pojo.setLocation_state(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.location_state"));
			shippo_tracking_ES_to_BQ_Pojo.setLocation_zip(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.location_zip"));
			shippo_tracking_ES_to_BQ_Pojo.setLocation_country(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.location_country"));
			shippo_tracking_ES_to_BQ_Pojo.setSubstatus_code(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.substatus_code"));
			shippo_tracking_ES_to_BQ_Pojo.setSubstatus_text(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.substatus_text"));
			shippo_tracking_ES_to_BQ_Pojo.setSubstatus_action_required(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.substatus_action_required"));
			shippo_tracking_ES_to_BQ_Pojo.setObject_created_date(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.object_created_date"));
			shippo_tracking_ES_to_BQ_Pojo.setObject_updated_date(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.object_updated_date"));
			shippo_tracking_ES_to_BQ_Pojo.setObject_id(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.object_id"));
			shippo_tracking_ES_to_BQ_Pojo.setStatus(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.status"));
			shippo_tracking_ES_to_BQ_Pojo.setCreatedBy(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.createdBy"));
			shippo_tracking_ES_to_BQ_Pojo.setModifiedBy(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.modifiedBy"));
			shippo_tracking_ES_to_BQ_Pojo.setES_MODIFIEDDATETIME(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.ES_MODIFIEDDATETIME"));
			shippo_tracking_ES_to_BQ_Pojo.setCreatedDateTime(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.createdDateTime"));
			shippo_tracking_ES_to_BQ_Pojo.setModifiedDateTime(com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.modifiedDateTime"));
		
			shippo_tracking_ES_to_BQ_Pojo_ArrayList.add(shippo_tracking_ES_to_BQ_Pojo);
		}
		
		System.out.println("arraylist size: "+shippo_tracking_ES_to_BQ_Pojo_ArrayList.size());
		
	}
	
	@When("^Print ES Query Response$")
	public static void elastic_search_print_response() {
		System.out.println(ElasticSearchJestUtility.getJestResponseAsString());
		log.info(ElasticSearchJestUtility.getJestResponseAsString());
	}

	@When("^Query Key '(.*)' present in the response$")
	public void key_count_present_in_the_response(String key) {
		try {
			String read = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), key);
			if (read.length() > 1) {
				log.info("Query Key " + key + "Present in the Response");
			} else {
				log.info("Query Key " + key + " Not Present in the Response");
			}
		} catch (Exception e) {
			log.info("Query Key " + key + " Not Present in the Response");

		}
	}

	@Then("^Verify query result contains Key '(.*)' & Value '(.*)'$")
	public void match_JSONPath_contains(String path, String Expected) {
		if (Expected.startsWith("$$")) {
			Expected = HashMapContainer.get(Expected);
		}

		String Actual = ElasticSearchJestUtility.jud.getJestResponse();
		Object document = Configuration.defaultConfiguration().jsonProvider().parse(Actual);

		try {
			List<Object> actual;

			actual = com.jayway.jsonpath.JsonPath.read(document, path);
			if (actual.size() == 1) {
				String singledata = String.valueOf(actual.get(0));
				assertEquals(" Json Path Value Check ", Expected,
						singledata.toString().replace("[", "").replace("]", "").toString());

			}
			assertEquals(" Json Path Value Check ", Expected,
					actual.toString().replace("[", "").replace("]", "").toString());

		} catch (java.lang.ClassCastException e) {
			String actual;

			actual = String.valueOf(com.jayway.jsonpath.JsonPath.read(document, path));
			assertEquals(" Json Path Value Check ", Expected, actual);

		}

	}

	@When("^Records '(.*)' count present in the DB - '(.*)'$")
	public void records_count_present_in_the_response(String key, String count) {
		List<String> read = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), key);
		Assert.assertEquals(Integer.parseInt(count), read.size());
	}

	@When("^Retrieve Record '(.*)' from Query Result and store in variable '(.*)'$")
	public void retrive_query_data_store(String key, String store) throws Exception {
		Object read = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), key);
		System.out.println(read);
		if (read==null) {
			HashMapContainer.add("$$" + store, "");
		}else {
			HashMapContainer.add("$$" + store, read.toString());
		}
	}
	@Then("^Verify '(.*)' data should have the stripped content from '(.*)'$")
	public void verifyStrippedContentInField(String field,String strippedField) {
		
		List<String> fieldValues = HashMapContainer.getList(field);
		List<String> strippedFieldValue = HashMapContainer.getList(strippedField);

		for (int i = 0; i < fieldValues.size(); i++) {
			String[] strippedValue = fieldValues.get(i).split("-");
			System.out.println(strippedFieldValue.get(i).trim().equals(strippedValue[1].trim()));
			Assert.assertTrue("Field and Stripped Field does not match",
					strippedFieldValue.get(i).trim().equals(strippedValue[1].trim()));
		}
		log.info("Fields are matched");
		
	}
	@Then("^Verify records displayed '(.*)' should not be more than '(.*)'$")
	public void verifyRecordCount(String actualCount,String expectedCount) {
		List<String> actual= HashMapContainer.getList(actualCount);
		Assert.assertTrue("Total record count is more than of user defined value",Integer.parseInt(expectedCount)<=actual.size());
		log.info("Total Record Count Matched");
	}
	@Given("^ES Search with '(.*)'$")
	public static void jestSearch1(String key) throws Throwable {
		String url=Utilities.getURL(key);
		ElasticSearchJestUtility.jestClient(url);
	}
	

	
	

}
