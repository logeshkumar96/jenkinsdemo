package com.TEAF.stepDefinitions;

import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.TEAF.customUtilities.ECom_Exception;
import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.WrapperFunctions;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CommonSteps {

	static Logger log = Logger.getLogger(CommonSteps.class.getName());

	private static WebDriver driver;
	private static int elementWaitTime = Integer.parseInt(System.getProperty("test.implicitlyWait"));
	public static String appName = null;

	@Given("^My WebApp '(.*)' is open$")
	public static void my_webapp_is_open(String url) throws Exception {
		String methodName = "my_webapp_is_open '" + url + "'";
		try {

			appName = url;
			driver = StepBase.getDriver();
			log.info("Driver value: " + driver);
			driver.get(GetPageObjectRead.OR_GetURL(url));
			WrapperFunctions.waitForPageToLoad();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@When("^I navigate to '(.*)' application$")
	public static void i_navigate_to_application(String url) throws ECom_Exception, Exception {
		String methodName = "i_navigate_to_application '" + url + "'";

		try {
			driver.navigate().to(url);
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Given("^My NativeApp '(.*)' is open$")
	public static void my_nativeapp_is_open(String app) throws Exception {
		String methodName = "my_nativeapp_is_open '" + app + "'";

		try {
			driver = StepBase.getDriver();
			if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("xlsx")) {
				GetPageObjectRead.ReadExcel(app);
			}
			Thread.sleep(15000);
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I navigate to '(.*)' page$")
	public static void I_NavigateTo(String url) throws ECom_Exception, Exception {
		String methodName = "I_NavigateTo '" + url + "'";

		try {
			driver.navigate().to(HashMapContainer.getPO(url));
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I refresh the WebPage$")
	public static void I_Refresh_WebPage() throws ECom_Exception, Exception {
		String methodName = "I_Refresh_WebPage";

		try {
			driver.navigate().refresh();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I focus and click '(.*)'$")
	public static void I_focus_click(String element) throws ECom_Exception, Exception {
		String methodName = "I_focus_click '" + element + "'";

		try {
			Actions ac = new Actions(driver);
			// WebElement findElement =
			// driver.findElement(GetPageObjectRead.OR_GetElement(element));
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			ac.moveToElement(wElement).click().build().perform();

		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I wait for '(.*)' seconds$")
	public static void I_pause_for_seconds(int seconds) throws ECom_Exception, Exception {
		String methodName = "I_pause_for_seconds '" + seconds + "'";

		try {
			Thread.sleep(seconds * 1000);
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I wait for visibility of element '(.*)'$")
	public static void I_wait_for_visibility_of_element(String element) throws ECom_Exception, Exception {
		String methodName = "I_wait_for_visibility_of_element '" + element + "'";
		try {
			WrapperFunctions.waitForElementVisibility(GetPageObjectRead.OR_GetElement(element));
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);

		}
	}

	@Then("^I enter '(.*)' in field '(.*)'$")
	public static void I_enter_in_field(String value, String element) throws ECom_Exception, Exception {
		String methodName = "I_enter_in_field '" + value + "' '" + element + "'";

		try {
			if (value.length() > 3) {
				if (value.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					value = HashMapContainer.get(value);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.sendKeys(value);
			// TODO Dismiss keyboard for webView
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I enter '(.*)' in the feild '(.*)' using actions$")
	public static void i_enter_in_the_feild_using_actions(String value, String element)
			throws ECom_Exception, Exception {
		String methodName = "i_enter_in_the_feild_using_actions '" + value + "' '" + element + "'";

		try {
			WebElement inputElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (inputElement.isDisplayed() && inputElement.isEnabled()) {
				Actions ac = new Actions(driver);
				ac.sendKeys(inputElement, value).build().perform();

			} 
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);

		}
	}

	@Then("^I clear the text and enter '(.*)' in field '(.*)' by JS$")
	public static void i_clear_and_enter_the_text_js(String value, String element) throws ECom_Exception, Exception {
		String methodName = "i_clear_and_enter_the_text_js '" + value + "' '" + element + "'";

		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].setAttribute('value', '" + value + "')", wElement);

		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@When("^I should see the selected value '(.*)' in the drop down '(.*)'$")
	public static void i_should_see_selected_value_in_the_dropdown(String value, String element) throws Exception {
		String methodName = "i_should_see_selected_value_in_the_dropdown '" + value + "' '" + element + "'";

		try {
			if (value.startsWith("$$")) {
				value = HashMapContainer.get(value);
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);

			Select sc = new Select(wElement);
			String text = sc.getFirstSelectedOption().getText();
			Assert.assertEquals(value, text);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	@Then("^I clear the text and enter '(.*)' in field '(.*)'$")
	public static void i_clear_and_enter_the_text(String value, String element) throws ECom_Exception, Exception {
		String methodName = "i_clear_and_enter_the_text '" + value + "' '" + element + "'";
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);

			wElement.clear();
			if (value.length() > 3) {
				if (value.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					value = HashMapContainer.get(value);
				}
			}
			wElement.sendKeys(value);
		} catch (Exception e) {

			e.printStackTrace();

			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I clear field '(.*)'$")
	public static void I_clear_Field(String element) throws ECom_Exception, Exception {
		String methodName = "I_clear_Field '" + element + "'";
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.clear();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I hit enter-key on element '(.*)'$")
	public static void I_hit_key_on_element(String element) throws ECom_Exception, Exception {
		String methodName = "I_hit_key_on_element '" + element + "'";

		try {

			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.sendKeys(Keys.ENTER);
			// TODO Dismiss keyboard for webView
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I hit down Arrow key on element '(.*)'$")
	public static void I_hit_Downkey_on_element(String element) throws ECom_Exception, Exception {
		String methodName = "I_hit_Downkey_on_element '" + element + "'";

		try {

			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.sendKeys(Keys.ARROW_DOWN);
			// TODO Dismiss keyboard for webView
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I mouse over '(.*)'$")
	public static void I_mouse_over(String element) throws ECom_Exception, Exception {
		String methodName = "I_mouse_over '" + element + "'";

		try {
			Actions action = new Actions(driver);
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			action.moveToElement(wElement).build().perform();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I verify tool tip message '(.*)' for element '(.*)'$")
	public static void I_Verify_ToolTip_Message(String expectedMessage, String element)
			throws ECom_Exception, Exception {
		String methodName = "I_Verify_ToolTip_Message '" + expectedMessage + "' '" + element + "'";

		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			/*
			 * Actions action = new Actions(driver); WebElement we =
			 * driver.findElement(GetPageObject.OR_GetElement(element));
			 * action.moveToElement(we).build().perform();
			 */
			String ToolTipMessage = wElement.getAttribute("title");
			Assert.assertEquals(expectedMessage, ToolTipMessage);
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I verify value '(.*)' in field '(.*)'$")
	public static void I_Verify_value_inField(String expectedValue, String element) throws ECom_Exception, Exception {
		String methodName = "I_Verify_value_inField '" + expectedValue + "' '" + element + "'";
		try {

			if (expectedValue.length() > 1) {
				if (expectedValue.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					expectedValue = HashMapContainer.get(expectedValue);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);

			String ActualValue = wElement.getAttribute("value");
			Assert.assertEquals(expectedValue, ActualValue,
					"Actual value does not match the expected value in specified field!");
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I verify element '(.*)' is disabled$")
	public static void I_Verify_Element_isDisabled(String element) throws ECom_Exception, Exception {
		String methodName = "I_Verify_Element_isDisabled '" + element + "'";
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (wElement.isEnabled()) {
				throw new Exception("Element is enabled!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I enter '(.*)' into RichText Editor '(.*)'$")
	public static void I_enter_into_RichTextEditor(String value, String element) throws ECom_Exception, Exception {
		String methodName = "I_enter_into_RichTextEditor '" + value + "' '" + element + "'";

		try {
			driver.switchTo().frame("tinymce");
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.clear();
			wElement.sendKeys(value);
			driver.switchTo().defaultContent();
			// TODO Dismiss keyboard for webView
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Given("^I click '(.*)'$")
	public static void I_click(String element) throws ECom_Exception, Exception {
		String methodName = "I_click '" + element + "'";
		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.click();

		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Given("^I double-click '(.*)'$")
	public static void I_doubleClick(String element) throws ECom_Exception, Exception {
		String methodName = "I_doubleClick '" + element + "'";

		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			Actions act = new Actions(driver);
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			act.doubleClick(wElement).build().perform();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Given("^I take screenshot$")
	public static void take_Screenshot() throws ECom_Exception, Exception {
		String methodName = "take_Screenshot";

		try {

			StepBase.embedScreenshot();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Given("^I click link '(.*)'$")
	public static void I_click_link(String linkText) throws ECom_Exception, Exception {
		String methodName = "I_click_link '" + linkText + "'";

		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(linkText, By.linkText(linkText),
					elementWaitTime);
			wElement.click();
			WrapperFunctions.waitForPageToLoad();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I drag field '(.*)' and drop '(.*)'$")
	public static void I_drag_Field(String element, String element2) throws ECom_Exception, Exception {
		String methodName = "I_drag_Field '" + element + "' '" + element2 + "'";

		try {
			WebElement source = WrapperFunctions.getElementByLocator(element, GetPageObjectRead.OR_GetElement(element),
					elementWaitTime);
			WebElement target = WrapperFunctions.getElementByLocator(element2,
					GetPageObjectRead.OR_GetElement(element2), elementWaitTime);
			Actions ac = new Actions(driver);
			ac.dragAndDrop(source, target).build().perform();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Given("^I click alert accept$")
	public static void I_click_alert_accept() throws ECom_Exception, Exception {
		String methodName = "I_click_alert_accept";

		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I click by JS '(.*)'$")
	public static void I_clickJS(String element) throws ECom_Exception, Exception {
		String methodName = "I_clickJS '" + element + "'";

		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.isEnabled();
			wElement.isDisplayed();
			WrapperFunctions.clickByJS(wElement);
			StepBase.embedScreenshot();
			WrapperFunctions.waitForPageToLoad();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I should see element '(.*)' present on page$")
	public static void I_should_see_on_page(String element) throws ECom_Exception, Exception {
		String methodName = "I_should_see_on_page '" + element + "'";

		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (wElement.isDisplayed()) {
				WrapperFunctions.highLightElement(wElement);
			} else {
				throw new Exception("Element is not found! :" + element);

			}

		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);

		}
	}

	@Then("^I should see element '(.*)' is disabled on page$")
	public static void I_should_see_disbaled_on_page(String element) throws ECom_Exception, Exception {

		String methodName = "I_should_see_disbaled_on_page '" + element + "'";

		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (!wElement.isEnabled()) {
				WrapperFunctions.highLightElement(wElement);
			} else {
				throw new Exception("Element is not disabled! :" + element);
			}
//
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);

		}
	}

	@Then("^I should see element '(.*)' present on page_$")
	public static void I_should_see_on_page_AndScreenshot(String element) throws ECom_Exception, Exception {

		String methodName = "I_should_see_on_page_AndScreenshot '" + element + "'";

		try {
			try {
				if (element.length() > 1) {
					if (element.substring(0, 2).equals("$$")) {
						log.info("Fetching from HMcontainer!");
						element = HashMapContainer.get(element);
					}
				}
				WebElement wElement = WrapperFunctions.getElementByLocator(element,
						GetPageObjectRead.OR_GetElement(element), elementWaitTime);
				if (wElement.isDisplayed()) {
					WrapperFunctions.highLightElement(driver.findElement(GetPageObjectRead.OR_GetElement(element)));
					StepBase.embedScreenshot();
					// Utilities.takeScreenshot(driver);
				}

			} catch (Exception e) {

				log.info("Element is not found");
				throw new ElementNotFoundException(element, "", "");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I should not see element '(.*)' present on page$")
	public static void I_should_not_see_on_page(String element) throws Exception {

		String methodName = "I_should_not_see_on_page '" + element + "'";

		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			log.info("Element is displayed: " + wElement.isDisplayed());
			if (wElement.isDisplayed()) {
				StepBase.embedScreenshot();
				WrapperFunctions.highLightElement(driver.findElement(GetPageObjectRead.OR_GetElement(element)));
				throw new Exception("Element is found on page!");
			}

		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I should see text '(.*)' present on page$")
	public static void I_should_see_text_present_on_page(String expectedText) throws ECom_Exception, Exception {

		String methodName = "I_should_see_text_present_on_page '" + expectedText + "'";
		try {
			if (expectedText.length() > 1) {
				if (expectedText.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					expectedText = HashMapContainer.get(expectedText);
				}
			}
			if (driver.getPageSource().contains(expectedText)) {
				log.info("Text " + expectedText + " found on page!");
				StepBase.embedScreenshot();
			} else {
				throw new ElementNotFoundException(expectedText, " ", " ");
			}
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I verify checkbox '(.*)' is '(.*)'$")
	public static void I_verify_checkBox_is_Checked(String element, String status) throws ECom_Exception, Exception {

		String methodName = "I_verify_checkBox_is_Checked '" + element + "' '" + status + "'";

		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (status.equalsIgnoreCase("checked")) {
				if (!wElement.isSelected()) {
					throw new Exception("Specified Checkbox is not checked!");
				}
			} else if (status.equalsIgnoreCase("unchecked")) {
				if (wElement.isSelected()) {
					throw new Exception("Specified Checkbox is checked!");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I switch to iFrame '(.*)'$")
	public static void I_switchTo_iFrame(String FrameID) throws ECom_Exception, Exception {

		String methodName = "I_switchTo_iFrame '" + FrameID + "'";

		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(FrameID,
					GetPageObjectRead.OR_GetElement(FrameID), elementWaitTime);
			driver.switchTo().frame(wElement);
			StepBase.embedScreenshot();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	@Then("^I switch to default content$")
	public static void I_switchTo_DefaultContent() throws ECom_Exception, Exception {

		String methodName = "I_switchTo_DefaultContent";

		try {
			driver.switchTo().defaultContent();
			StepBase.embedScreenshot();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	@Then("^I switch back to Main Window$")
	public static void I_switchTo_MainWindow() throws ECom_Exception, Exception {

		String methodName = "I_switchTo_MainWindow";

		try {
			driver.switchTo().window(driver.getWindowHandle());
			StepBase.embedScreenshot();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	@Then("^I should see text '(.*)' present on page at '(.*)'$")
	public static void I_should_see_text_present_on_page_At(String expectedText, String element)
			throws ECom_Exception, Exception {

		String methodName = "I_should_see_text_present_on_page_At '" + expectedText + "' '" + element + "'";

		try {
			if (expectedText.length() > 1) {
				if (expectedText.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					expectedText = HashMapContainer.get(expectedText);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualText = wElement.getText();
			WrapperFunctions.highLightElement(wElement);
			Assert.assertEquals(expectedText, actualText);
			StepBase.embedScreenshot();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);

		}
	}

	@Then("^I should see text '(.*)' contained on page at '(.*)'$")
	public static void I_should_see_text_contained_on_page_At(String expectedText, String element)
			throws ECom_Exception, Exception {

		String methodName = "I_should_see_text_contained_on_page_At '" + expectedText + "' '" + element + "'";

		try {
			if (expectedText.length() > 1) {
				if (expectedText.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer! " + HashMapContainer.get(expectedText));
					expectedText = HashMapContainer.get(expectedText);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualText = wElement.getText();
			WrapperFunctions.highLightElement(wElement);
			log.info("Actual " + actualText + " Expected: " + expectedText);

			try {
				Assert.assertTrue(expectedText.toLowerCase().contains((actualText.toLowerCase())));
			} catch (AssertionError e) {
				Assert.assertEquals(expectedText, actualText);
			}
			StepBase.embedScreenshot();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Given("^I should see variable '(.*)' value contained in expected value '(.*)'$")
	public static void i_should_see_element_text_present_on_expected_value(String expecString, String element)
			throws ECom_Exception, Exception {

		String methodName = "i_should_see_element_text_present_on_expected_value '" + expecString + "' '" + element
				+ "'";

		try {
			if (expecString.length() > 1) {
				if (expecString.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer! " + HashMapContainer.get(expecString));

					expecString = HashMapContainer.get(expecString);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualText = wElement.getText();
			WrapperFunctions.highLightElement(wElement);
			log.info("Actual " + actualText + " Expected: " + expecString);

			try {
				Assert.assertTrue(actualText.toLowerCase().contains((expecString.toLowerCase())));
			} catch (AssertionError e) {
				Assert.assertEquals(expecString, actualText);

			}
			StepBase.embedScreenshot();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I should see text matching regx '(.*)' present on page at '(.*)'$")
	public static void I_should_see_text_matching_regularExpression(String regx, String element)
			throws ECom_Exception, Exception {

		String methodName = "I_should_see_text_matching_regularExpression '" + regx + "' '" + element + "'";

		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualText = wElement.getText();
			// Assert.assertTrue(actualText.matches(regx),"Actual Text Found:
			// "+actualText+"|");
			Assert.assertTrue(actualText.matches(regx));
			StepBase.embedScreenshot();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	@Then("^I wait '(.*)' seconds for presence of element '(.*)'$")
	public static void I_wait_for_presence_of_element(int seconds, String element) throws ECom_Exception, Exception {

		String methodName = "I_wait_for_presence_of_element '" + seconds + "' '" + element + "'";

		try {
			WrapperFunctions.waitForElementPresence(GetPageObjectRead.OR_GetElement(element), seconds);
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I switch to window with title '(.*)'$")
	public static void I_switch_to_window_with_title(String title) throws ECom_Exception, Exception {

		String methodName = "I_switch_to_window_with_title '" + title + "'";

		try {
			if (title.length() > 1) {
				if (title.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					title = HashMapContainer.get(title);
				}
			}
			WrapperFunctions.switchToWindowUsingTitle(title);
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	@Then("^I select option '(.*)' in dropdown '(.*)' by '(.*)'$")
	public static void I_select_option_in_dd_by(String option, String element, String optionType)
			throws ECom_Exception, Exception {

		String methodName = "I_select_option_in_dd_by '" + option + "' '" + element + "' '" + optionType + "'";

		try {
			Assert.assertTrue(WrapperFunctions.selectDropDownOption(GetPageObjectRead.OR_GetElement(element), option,
					optionType));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I scroll to '(.*)' - '(.*)'$")
	public static void I_scroll_to_element(String scrolltype, String element) throws ECom_Exception, Exception {

		String methodName = "I_scroll_to_element '" + scrolltype + "' '" + element + "'";

		try {
			WrapperFunctions.scroll(scrolltype, element);
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I scroll through page$")
	public static void I_scroll_thru_page() throws ECom_Exception, Exception {

		String methodName = "I_scroll_thru_page";

		try {
			for (int i = 0; i <= 16; i++) {
				WrapperFunctions.scroll("coordinates", "0,200");
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I scroll to top of the page$")
	public static void i_scroll_through_top_page() throws ECom_Exception, Exception {

		String methodName = "i_scroll_through_top_page";

		try {
			WrapperFunctions.scroll("top", "");
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I store text '(.*)' as '(.*)'$")
	public static void I_get_text_from(String TextValue, String ValueName) throws ECom_Exception, Exception {

		String methodName = "I_get_text_from '" + TextValue + "' '" + ValueName + "'";

		try {
			HashMapContainer.add("$$" + ValueName, TextValue);
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I get text from '(.*)' and store$")
	public static void I_get_text_from_store(String element) throws ECom_Exception, Exception {

		String methodName = "I_get_text_from_store '" + element + "'";

		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String value = wElement.getText();
			HashMapContainer.add("$$" + element, value);

		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I enter from stored variable '(.*)' into feild '(.*)'$")
	public static void I_enter_from_StoredValue(String StoredValue, String element) throws ECom_Exception, Exception {

		String methodName = "I_enter_from_StoredValue '" + StoredValue + "' '" + element + "'";

		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.sendKeys(HashMapContainer.get(StoredValue));
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I compare '(.*)' with stored value '(.*)'$")
	public static void I_compare_with_StoredValue(String element, String storedValue) throws ECom_Exception, Exception {

		String methodName = "I_compare_with_StoredValue '" + element + "' '" + storedValue + "'";

		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualValue = wElement.getText();
			storedValue = HashMapContainer.get(storedValue);
			Assert.assertEquals(storedValue, actualValue,
					"I Compare " + actualValue + " with expected value " + storedValue);
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I close browser$")
	public static void I_Close_Browser() throws ECom_Exception, Exception {

		String methodName = "I_Close_Browser";

		try {
			driver.close();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

}
