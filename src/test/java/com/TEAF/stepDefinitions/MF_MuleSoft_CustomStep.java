package com.TEAF.stepDefinitions;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.csv.CSVParser;
import java.io.BufferedReader;
import java.io.FileReader;
//import java.io.FileWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
//import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;
import java.util.Vector;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;

import com.Resources.InMomentAttributesUpdatePojo;
import com.Resources.Inmoment_Returns_Pojo;
import com.Resources.ReadingSuccessEmail;
import com.Resources.Saba_SFTP_SQL_CurriculaReport_pojo;
import com.Resources.Saba_SFTP_SQL_GradedSimulationsNAP_pojo;
import com.Resources.Saba_SFTP_SQL_Master_Report_Pojo;
import com.Resources.Saba_SFTP_SQL_NewHireExam_Report_Pojo;
import com.Resources.Shippo_tracking_ES_to_BQ_Pojo;
import com.Resources.inContactBQ_CSVPojo_WH_EXPD_AgentList;
import com.TEAF.Hooks.Hooks;
import com.TEAF.customUtilities.RunnableDemo;
import com.TEAF.framework.API_UserDefined;
import com.TEAF.framework.D365;
import com.TEAF.framework.ElasticSearchJestUtility;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.RestAssuredUtility;
import com.TEAF.framework.Utilities;
import com.google.api.gax.paging.Page;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobId;
import com.google.cloud.bigquery.JobInfo;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableResult;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.jayway.jsonpath.JsonPath;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
//import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.CucumberException;
import io.cucumber.datatable.DataTable;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
//import io.searchbox.client.JestClient;
//import io.searchbox.client.JestClientFactory;
//import io.searchbox.client.config.HttpClientConfig;
import net.minidev.json.JSONArray;

public class MF_MuleSoft_CustomStep {
	
	public static RequestSpecification spec = given().when();
	static JSONObject RequestParameters = new JSONObject();
	public static String Path;
	static Logger log = Logger.getLogger(MF_MuleSoft_CustomStep.class.getName());
	public static com.google.cloud.bigquery.BigQuery bigquery;
	public static TableResult BibQuery_result;
	public static ArrayList<String> BigQuery_result_records_List;
	public static int BigQuery_result_count;
	public String date_provided_in_BQ_query;
	public static Storage GCS_storage;
	static API_UserDefined ap = new API_UserDefined();
//	D365 d365 = new D365();

	@Given("^Connect to SFTP with host '(.*)', port '(.*)', user name '(.*)' and password '(.*)' then find '(.*)' for the date '(.*)' from directory '(.*)' and download to '(.*)'$")
	public void Connect_to_SFTP_with_host_port_username_and_password_and_download_file_using_name_date_directory(String host, Integer port, String username, String password, String report_name, String report_date, String SFTP_working_directory, String download_path) throws ParseException, FileNotFoundException {

		if (report_date.startsWith("$$")) {
			report_date = HashMapContainer.get(report_date);
		}
		
		JSch jsch = new JSch();
        Session session = null;
        String report_name_from_list_now = null;
        
        //CREATING SESSION FOR SFTP TO DOWNLOAD FILE
        try {
            session = jsch.getSession(username, host, port);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(password);
            session.connect();
            
            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            
            String report_name_prefix = null;
            
            //APPENDING DATE TO THE EXPECTED FILE NAME AND CREATING A FILE NAME PREFIX
            switch (report_name) {
            case "Curricula Report":
            	report_name_prefix = "Curricula Reports_"+report_date;
            	System.out.println("Report name prefix: "+report_name_prefix);
            	break;
            case "New Hire Report":
            	report_name_prefix = "New Hire Exam_"+report_date;
            	System.out.println("Report name prefix: "+report_name_prefix);
            	break;
            case "Inmoment Returns Report":
            	String month = report_date.substring(2, 4);
        		if (month.startsWith("0")) {
        			report_date = report_date.substring(0,2)+report_date.substring(3, 7);
        		}
            	report_name_prefix = "Mattress Firm Returns - Raw Data "+report_date;
            	System.out.println("Report name prefix: "+report_name_prefix);
            	break;
            case "Graded Simulations NAP":
            	report_name_prefix = "Graded Simulations NAP_"+report_date;
            	System.out.println("Report name prefix: "+report_name_prefix);
            	break;
            case "Master Report":
            	report_name_prefix = "Master Data_"+report_date;
            	System.out.println("Report name prefix: "+report_name_prefix);
            	break;
            case "MattressFirm_Response Report":
            	report_name_prefix = "MattressFirm_Response_"+report_date;
            	System.out.println("Report name prefix: "+report_name_prefix);
            	break;
            case "Selected Final MF Report":
            	report_name_prefix = "Selected_Final_MF_"+report_date;
            	System.out.println("Report name prefix: "+report_name_prefix);
            	break;
            case "InMoment Attributes Update":
            	String monthh = report_date.substring(2, 4);
        		if (monthh.startsWith("0")) {
        			report_date = report_date.substring(0,2)+report_date.substring(3, 7);
        		}
            	report_name_prefix = "Mattress Firm "+report_date+" - "+report_date;
            	System.out.println("Report name prefix: "+report_name_prefix);
            	break;
            }
            
            sftpChannel.cd(SFTP_working_directory);
            Vector<?> filelist = sftpChannel.ls(SFTP_working_directory);
            
            //CHECKING THE LIST OF FILES TO MATCH WITH FILE NAME PREFIX AND DOWNLOAD THE FILE
            for(int i=0; i<filelist.size();i++){
            	
            	LsEntry entry = (LsEntry) filelist.get(i);
            	String report_name_from_list = entry.getFilename();
            	
            	
                if (report_name_from_list.contains(report_name_prefix)) {
//                	System.out.println("attr "+entry.getAttrs().getMtimeString());
//                	SimpleDateFormat dd = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
//                	System.out.println("final date "+dd.parse(entry.getAttrs().getMtimeString()));
//                	
//                	System.out.println("attr "+entry.getAttrs().getAtimeString());
//                	System.out.println("attr "+entry.getAttrs().getMTime());
//                	System.out.println("attr "+entry.getAttrs().getATime());
                	report_name_from_list_now = report_name_from_list;
                	System.out.println("Prefix matching report found with name: "+report_name_from_list);
                	String SFTP_file_path = SFTP_working_directory+"/"+report_name_from_list;
                	
                	String download_file_path = download_path+report_name_from_list;
                	sftpChannel.get(SFTP_file_path, download_file_path);
                	System.out.println("SFTP file downloaded successfully. Downloaded file path: "+download_file_path);
                	HashMapContainer.add("$$SFTPDownloadedfilePath", download_file_path);
                }
                
            }
            sftpChannel.exit();
            session.disconnect();
            
        } catch (JSchException e) {
            e.printStackTrace();  
        } catch (SftpException e) {
            e.printStackTrace();
        }
        if (report_name_from_list_now == null) {
        	throw new FileNotFoundException();
        }
        //UTILITY TO UNZIP THE DOWNLOADED FILE IF IT IS A ZIP FILE

        if (report_name_from_list_now.endsWith(".zip")) {
			String zipFilePath = HashMapContainer.get("$$SFTPDownloadedfilePath");
			String destDir = download_path;

			Utilities util = new Utilities();
			String unZipped_File_path = util.unzip_a_file(zipFilePath, destDir);
			HashMapContainer.add("$$SFTPDownloadedfilePath", unZipped_File_path);
        }
	}
	
	@Then("^Connect to MySQL DB with host '(.*)', user name '(.*)', password '(.*)' and run the query '(.*)' and download query results to '(.*)'$")
	public void Connect_to_MySQL_DB_with_host_username_password_and_run_the_query_and_download_query_results(String host_URL, String username, String password, String SQL_query, String file_dowload_path) {
		
		//WILL CONNECT TO MYSQL DATA BASE AND RUN THE QUERY AND SAVE THE RESULTS IN A DOWNLOADED FILE
		Utilities util = new Utilities();
		util.executeSQLQuery_and_Download_result_CSV(host_URL, username, password, SQL_query, file_dowload_path);
	}
	
	@Then("^Connect to MySQL DB with host '(.*)', user name '(.*)', password '(.*)' and run the query '(.*)'$")
	public void Connect_to_MySQL_DB_with_host_username_password_and_run_the_query(String host_URL, String username, String password, String SQL_query) throws SQLException {
		
		//WILL CONNECT TO MYSQL DATA BASE AND RUN THE QUERY AND SAVE THE RESULTS IN A DOWNLOADED FILE
		Utilities util = new Utilities();
		HashMapContainer.addList("$$SQL_API_Keys", util.executeSQLQueryAndReturnSingleColumn(host_URL, username, password, SQL_query));
	}
	
	@Then("^assert SFTP CSV file '(.*)' record count with SQL result CSV file '(.*)' record count$")
	public void assert_SFTP_CSV_file_record_count_with_SQL_result_CSV_file_record_count(String sftp_path, String SQL_path) throws IOException {
		
		//TO GET THE COUNT OF RECORDS PRESENT IN THE CSV FILE
		if (sftp_path.startsWith("$$")) {
			sftp_path = HashMapContainer.get(sftp_path);
		}
		System.out.println("sftp_path "+ sftp_path);
		BufferedReader bufferedReader_SFTP = new BufferedReader(new FileReader(sftp_path));
		int SFTP_CSV_file_record_count = 0;
		while (bufferedReader_SFTP.readLine() != null) {
			SFTP_CSV_file_record_count++;
		}
		SFTP_CSV_file_record_count = SFTP_CSV_file_record_count - 2;
		bufferedReader_SFTP.close();

		//TO GET THE COUNT OF RECORDS PRESENT IN THE CSV FILE
		if (SQL_path.startsWith("$$")) {
			SQL_path = HashMapContainer.get(SQL_path);
		}
		BufferedReader bufferedReader_SQL = new BufferedReader(new FileReader(SQL_path));
		int SQL_CSV_file_record_count = 0;
		while ((bufferedReader_SQL.readLine()) != null) {
			SQL_CSV_file_record_count++;
		}
		SQL_CSV_file_record_count = SQL_CSV_file_record_count - 1;
		bufferedReader_SQL.close();
		
		System.out.println("SFTP CSV file records count : " + SFTP_CSV_file_record_count);
		System.out.println("SQL CSV file records count : " + SQL_CSV_file_record_count);
		Assert.assertEquals("Validation of SFTP file record count with SQL query result file record count: ", SFTP_CSV_file_record_count, SQL_CSV_file_record_count);
	}
	
	@Then("^assert end to end data between source SFTP CSV file '(.*)' and target SQL stage table result CSV file '(.*)' for Curricula$")
	public void assert_end_to_end_data_between_source_SFTP_CSV_file_and_target_SQL_result_CSV_file_for_Curricula(String SFTP_source_CSV_file_path, String SQL_target_CSV_file_path) throws CsvValidationException, IOException {
		
		List<Saba_SFTP_SQL_CurriculaReport_pojo> saba_SFTP_SQL_CurriculaReport_Arraylist = new ArrayList<Saba_SFTP_SQL_CurriculaReport_pojo>();
		
		if (SFTP_source_CSV_file_path.startsWith("$$")) {
			SFTP_source_CSV_file_path = HashMapContainer.get(SFTP_source_CSV_file_path);
		}
		if (SQL_target_CSV_file_path.startsWith("$$")) {
			SQL_target_CSV_file_path = HashMapContainer.get(SQL_target_CSV_file_path);
		}
		
		BufferedReader br_source = new BufferedReader(new FileReader(SFTP_source_CSV_file_path));
		String row_source;
		br_source.readLine();
		br_source.readLine();
		
		//TO STORE SFTP DATA INTO POJO CLASS
		while ((row_source = br_source.readLine()) != null) {
			String[] columns_source_SFTP = row_source.split(",");
			Saba_SFTP_SQL_CurriculaReport_pojo saba_SFTP_SQL_CurriculaReport_pojo = new Saba_SFTP_SQL_CurriculaReport_pojo();
			saba_SFTP_SQL_CurriculaReport_pojo.setCurriculum_Name(columns_source_SFTP[0]);
			saba_SFTP_SQL_CurriculaReport_pojo.setPerson_Username(columns_source_SFTP[1]);
			saba_SFTP_SQL_CurriculaReport_pojo.setStudent_Curriculum_Status(columns_source_SFTP[2]);
			saba_SFTP_SQL_CurriculaReport_pojo.setPerson_Full_Name(columns_source_SFTP[3]);
			saba_SFTP_SQL_CurriculaReport_pojo.setPerson_Person_No(columns_source_SFTP[4]);
			saba_SFTP_SQL_CurriculaReport_pojo.setManager_Full_Name(columns_source_SFTP[5]);
			saba_SFTP_SQL_CurriculaReport_pojo.setPerson_Location_Name(columns_source_SFTP[6]);
			saba_SFTP_SQL_CurriculaReport_pojo.setPerson_Parent_Organization_Name(columns_source_SFTP[7]);
			saba_SFTP_SQL_CurriculaReport_pojo.setPerson_Organization_Name(columns_source_SFTP[8]);
			saba_SFTP_SQL_CurriculaReport_pojo.setPerson_Status(columns_source_SFTP[9]);
			saba_SFTP_SQL_CurriculaReport_pojo.setPerson_Employee_Type(columns_source_SFTP[10]);
			saba_SFTP_SQL_CurriculaReport_pojo.setPerson_Company(columns_source_SFTP[11]);
			saba_SFTP_SQL_CurriculaReport_pojo.setPerson_Job_Type_Name(columns_source_SFTP[12]);
			saba_SFTP_SQL_CurriculaReport_pojo.setStudent_Curriculum_Assigned_On(columns_source_SFTP[13]);
			saba_SFTP_SQL_CurriculaReport_pojo.setStudent_Curriculum_Started_On(columns_source_SFTP[14]);
			saba_SFTP_SQL_CurriculaReport_pojo.setStudent_Curriculum_Due_Date(columns_source_SFTP[15]);
			saba_SFTP_SQL_CurriculaReport_pojo.setStudent_Curriculum_Acquired_On(columns_source_SFTP[16]);
			saba_SFTP_SQL_CurriculaReport_pojo.setCurricula_Completion_Percentage(columns_source_SFTP[17]);
			saba_SFTP_SQL_CurriculaReport_pojo.setPerson_Start_Date(columns_source_SFTP[18]);

			saba_SFTP_SQL_CurriculaReport_Arraylist.add(saba_SFTP_SQL_CurriculaReport_pojo);
		}
		br_source.close();
		
		BufferedReader br_target = new BufferedReader(new FileReader(SQL_target_CSV_file_path));
	    String row_target;
	    br_target.readLine();
	    
	    Collection<String> SQL_stage_person_Username = new ArrayList<String>();
		Collection<String> SFTP_person_Username = new ArrayList<String>();
		
		//TO ASSERT SQL RESULTS WITH CAPTURED POJO CLASS DATA
	    while ((row_target = br_target.readLine()) != null) {
	        String[] columns_target_SQL = row_target.split(",");
	        String Student_Curriculum_Status = columns_target_SQL[0];
	        String Curriculum_Name = columns_target_SQL[1];
	        String SQL_person_Username = columns_target_SQL[2];
	        SQL_stage_person_Username.add(SQL_person_Username);
	        
	        for (Saba_SFTP_SQL_CurriculaReport_pojo y: saba_SFTP_SQL_CurriculaReport_Arraylist) {
	        	SFTP_person_Username.add(y.getPerson_Username());
	        	if(y.getStudent_Curriculum_Status().contains(Student_Curriculum_Status) && y.getCurriculum_Name().contains(Curriculum_Name) && y.getPerson_Username().contains(SQL_person_Username)) {
	        		
	        		int listItemIndex = saba_SFTP_SQL_CurriculaReport_Arraylist.indexOf(y);
	        		Saba_SFTP_SQL_CurriculaReport_pojo arraylist_row = saba_SFTP_SQL_CurriculaReport_Arraylist.get(listItemIndex);
	        		
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Student_Curriculum_Status SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getStudent_Curriculum_Status(), columns_target_SQL[0]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Curriculum_Name SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getCurriculum_Name(), columns_target_SQL[1]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Person_Username SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getPerson_Username(), columns_target_SQL[2]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Person_Full_Name SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getPerson_Full_Name(), columns_target_SQL[3]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Person_No SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getPerson_Person_No(), columns_target_SQL[4]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Manager_Full_Name SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getManager_Full_Name(), columns_target_SQL[5]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Person_Location_Name SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getPerson_Location_Name(), columns_target_SQL[6]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Person_Parent_Organization_Name SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getPerson_Parent_Organization_Name(), columns_target_SQL[7]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Person_Organization_Name SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getPerson_Organization_Name(), columns_target_SQL[8]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Person_Status SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getPerson_Status(), columns_target_SQL[9]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Person_Employee_Type SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getPerson_Employee_Type(), columns_target_SQL[10]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Person_Company SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getPerson_Company(), columns_target_SQL[11]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Person_Job_Type_Name SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getPerson_Job_Type_Name(), columns_target_SQL[12]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Student_Curriculum_Assigned_On SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getStudent_Curriculum_Assigned_On(), columns_target_SQL[13]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Student_Curriculum_Started_On SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getStudent_Curriculum_Started_On(), columns_target_SQL[14]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Student_Curriculum_Due_Date SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getStudent_Curriculum_Due_Date(), columns_target_SQL[15]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Student_Curriculum_Acquired_On SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getStudent_Curriculum_Acquired_On(), columns_target_SQL[16]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Curricula_Completion_Percentage SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getCurricula_Completion_Percentage(), columns_target_SQL[17]);
	        		Assert.assertEquals("Person username: "+columns_target_SQL[2]+" --> Person_Start_Date SFTP to SQL validation_Curriculla_STG: ", arraylist_row.getPerson_Start_Date(), columns_target_SQL[18]);
	        		break;
	        	}
	        }
	    }
		br_target.close();
		
		//TO VALIDATE THAT ALL THE RECORDS GOT ASSERTED BETWEEN SFTP AND SQL
		SFTP_person_Username.removeAll(SQL_stage_person_Username);
		HashSet<String> SFTP_person_Username_hashset = new HashSet<String>();
		SFTP_person_Username_hashset.addAll(SFTP_person_Username);
		
		if (SFTP_person_Username_hashset.size()>0) {
			System.out.println("CSV comparision failed because target SQL stage result CSV file doesn't contain below person username's");
			Assert.assertNull(SFTP_person_Username_hashset);
		}
		
		System.out.println("End to End assertion between SFTP and SQL CSV files completed successfully without any errors.");
	}
	
	@And("^assert end to end data for '(.*)' records between source as SQL stage table result CSV file '(.*)' and target as SQL master table for Curricula$")
	public void assert_end_to_end_data_for_Count_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_with_desired_count_for_Saba_Curricula(Integer desired_count, String SQL_CSV_file_path) throws IOException{
		
		//ASSERTION WITH USER DEFINER COUNT OF RECORDS
		assert_end_to_end_data_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_for_Saba_Curricula(desired_count, SQL_CSV_file_path);
	}
	
	@And("^assert end to end data for all the records between source as SQL stage table result CSV file '(.*)' and target as SQL master table for Curricula$")
	public void assert_end_to_end_data_for_all_records_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_with_desired_count_for_Saba_Curricula(String SQL_CSV_file_path) throws IOException{
		
		if (SQL_CSV_file_path.startsWith("$$")) {
			SQL_CSV_file_path = HashMapContainer.get(SQL_CSV_file_path);
		}
		
		//TO FIND RECORDS COUNT IN CSV FILE
		BufferedReader br_SQL = new BufferedReader(new FileReader(SQL_CSV_file_path));
		int SQL_CSV_file_record_count = 0;
		while (br_SQL.readLine() != null) {
			SQL_CSV_file_record_count++;
		}
		SQL_CSV_file_record_count = SQL_CSV_file_record_count - 2;
		System.out.println("SQL_CSV_file_record_count "+SQL_CSV_file_record_count);
		br_SQL.close();
		
		//ASSERTION FOR ALL THE RECORDS PRESENT IN CSV FILE
		assert_end_to_end_data_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_for_Saba_Curricula(SQL_CSV_file_record_count, SQL_CSV_file_path);
	}
		
public void assert_end_to_end_data_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_for_Saba_Curricula(Integer desired_count, String SQL_CSV_file_path) throws IOException{	
		
		if (SQL_CSV_file_path.startsWith("$$")) {
			SQL_CSV_file_path = HashMapContainer.get(SQL_CSV_file_path);
		}
		
		BufferedReader bufferedReader_SQL = new BufferedReader(new FileReader(SQL_CSV_file_path));
	    String SQL_row;
	    bufferedReader_SQL.readLine();
	    for (int i=0; i<desired_count; i++) {
	    	while ((SQL_row = bufferedReader_SQL.readLine()) != null) {
		        String[] SQL_columns = SQL_row.split(",");
		        String SQL_Student_Curriculum_Status = SQL_columns[0];
		        String SQL_Curriculum_Name = SQL_columns[1];
		        String SQL_Person_Username = SQL_columns[2];
		        
		        //QUERY TO FETCH DATA FOR ONE RECORD AND ASSERT FOR ALL COLUMNS
		        String sql_query = ("SELECT * FROM [InternalProjects].[dbo].[SAB_CurriculaReports] WHERE Student_Curriculum_Status = "+SQL_Student_Curriculum_Status+" and Curriculum_Name = "+SQL_Curriculum_Name+" and Person_Username = "+SQL_Person_Username).replace("\"", "'");
		        System.out.println(sql_query);
		        Utilities util = new Utilities();
		        Map<String, String> ResultSet = util.executeSQLQuery("jdbc:sqlserver://qdbsqltab01.mfrm.com:1433", "Mule-QA", "test@123", sql_query);
				Boolean SQL_result_set = ResultSet.isEmpty();
		        if (!SQL_result_set) {
			        Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Student_Curriculum_Status SQL stage to master table validation_Curriculla: ", SQL_columns[0].replace("\"", ""), ResultSet.get("Student_Curriculum_Status"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Curriculum_Name SQL stage to master table validation_Curriculla: ", SQL_columns[1].replace("\"", ""), ResultSet.get("Curriculum_Name"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Person_Username SQL stage to master table validation_Curriculla: ", SQL_columns[2].replace("\"", ""), ResultSet.get("Person_Username"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Person_Full_Name SQL stage to master table validation_Curriculla: ", SQL_columns[3].replace("\"", ""), ResultSet.get("Person_Full_Name"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Person_No SQL stage to master table validation_Curriculla: ", SQL_columns[4].replace("\"", ""), ResultSet.get("Person_No"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Manager_Full_Name SQL stage to master table validation_Curriculla: ", SQL_columns[5].replace("\"", ""), ResultSet.get("Manager_Full_Name"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Person_Location_Name SQL stage to master table validation_Curriculla: ", SQL_columns[6].replace("\"", ""), ResultSet.get("Person_Location_Name"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Person_Parent_Organization_Name SQL stage to master table validation_Curriculla: ", SQL_columns[7].replace("\"", ""), ResultSet.get("Person_Parent_Organization_Name"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Person_Organization_Name SQL stage to master table validation_Curriculla: ", SQL_columns[8].replace("\"", ""), ResultSet.get("Person_Organization_Name"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Person_Status SQL stage to master table validation_Curriculla: ", SQL_columns[9].replace("\"", ""), ResultSet.get("Person_Status"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Person_Employee_Type SQL stage to master table validation_Curriculla: ", SQL_columns[10].replace("\"", ""), ResultSet.get("Person_Employee_Type"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Person_Company SQL stage to master table validation_Curriculla: ", SQL_columns[11].replace("\"", ""), ResultSet.get("Person_Company"));
	        		
	        		BufferedReader bufferedReader1_SQL = new BufferedReader(new FileReader(SQL_CSV_file_path));
	        		bufferedReader1_SQL.readLine();
					CSVParser parser1_SQL = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames()
							.withDelimiter(',').parse(bufferedReader1_SQL);
					String person_job_type_name = null;
					String Student_Curriculum_Assigned_On = null;
					String Student_Curriculum_Started_On = null;
					String Student_Curriculum_Due_Date = null;
					String Student_Curriculum_Acquired_On = null;
					String Curricula_Completion_Percentage = null;
					String Person_Start_Date = null;
					for (CSVRecord record : parser1_SQL) {
						if((record.get(0).replace("\"", "")).equals(SQL_Student_Curriculum_Status.replace("\"", "")) && (record.get(1).replace("\"", "")).equals(SQL_Curriculum_Name.replace("\"", "")) && (record.get(2).replace("\"", "")).equals(SQL_Person_Username.replace("\"", ""))){
							person_job_type_name = record.get(12);
							Student_Curriculum_Assigned_On = record.get(13);
							Student_Curriculum_Started_On = record.get(14);
							Student_Curriculum_Due_Date = record.get(15);
							Student_Curriculum_Acquired_On = record.get(16);
							Curricula_Completion_Percentage = record.get(17);
							Person_Start_Date = record.get(18);
						}
					}
					bufferedReader1_SQL.close();
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Person_Job_Type_Name SQL stage to master table validation_Curriculla: ", person_job_type_name, ResultSet.get("Person_Job_Type_Name"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Student_Curriculum_Assigned_On SQL stage to master table validation_Curriculla: ", Student_Curriculum_Assigned_On, ResultSet.get("Student_Curriculum_Assigned_On"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Student_Curriculum_Started_On SQL stage to master table validation_Curriculla: ", Student_Curriculum_Started_On, ResultSet.get("Student_Curriculum_Started_On"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Student_Curriculum_Due_Date SQL stage to master table validation_Curriculla: ", Student_Curriculum_Due_Date, ResultSet.get("Student_Curriculum_Due_Date"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Student_Curriculum_Acquired_On SQL stage to master table validation_Curriculla: ", Student_Curriculum_Acquired_On, ResultSet.get("Student_Curriculum_Acquired_On"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Curricula_Completion_Percentage SQL stage to master table validation_Curriculla: ", Curricula_Completion_Percentage, ResultSet.get("Curricula_Completion_Percentage"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Username+" --> Person_Start_Date SQL stage to master table validation_Curriculla: ", Person_Start_Date, ResultSet.get("Person_Start_Date"));
			        
			        break;
		        }else {
		        	System.out.println("No data present in SQL master table with combination of SQL_Student_Curriculum_Status: "+SQL_Student_Curriculum_Status.replace("\"", "'")+", SQL_Curriculum_Name: "+SQL_Curriculum_Name.replace("\"", "'")+" and SQL_Person_Username: "+SQL_Person_Username.replace("\"", "'"));
		        }
		    }
	    }
	    bufferedReader_SQL.close();
	    System.out.println("End to End assertion between SQL stage and master table completed successfully without any errors.");
	}
		
	@Then("^assert end to end data between source SFTP CSV file '(.*)' and target SQL stage table result CSV file '(.*)' for New Hire Exam$")
	public void assert_end_to_end_data_between_source_SFTP_CSV_file_and_target_SQL_result_CSV_file_for_New_Hire_Exam(String SFTP_source_CSV_file_path, String SQL_target_CSV_file_path) throws CsvValidationException, IOException {
		
		List<Saba_SFTP_SQL_NewHireExam_Report_Pojo> saba_SFTP_SQL_NewHireExam_Pojo_Arraylist = new ArrayList<Saba_SFTP_SQL_NewHireExam_Report_Pojo>();
		
		if (SFTP_source_CSV_file_path.startsWith("$$")) {
			SFTP_source_CSV_file_path = HashMapContainer.get(SFTP_source_CSV_file_path);
		}
		if (SQL_target_CSV_file_path.startsWith("$$")) {
			SQL_target_CSV_file_path = HashMapContainer.get(SQL_target_CSV_file_path);
		}
		
		BufferedReader br_source = new BufferedReader(new FileReader(SFTP_source_CSV_file_path));
		String row_source;
		br_source.readLine();
		br_source.readLine();
		
		//STORE SOURCE DATA INTO POJO
		while ((row_source = br_source.readLine()) != null) {
			String[] columns_source_SFTP = row_source.split(",");
			Saba_SFTP_SQL_NewHireExam_Report_Pojo saba_SFTP_SQL_NewHireExam_Pojo = new Saba_SFTP_SQL_NewHireExam_Report_Pojo();
			saba_SFTP_SQL_NewHireExam_Pojo.setTranscriptID(columns_source_SFTP[0]);
			saba_SFTP_SQL_NewHireExam_Pojo.setModule_name(columns_source_SFTP[1]);
			saba_SFTP_SQL_NewHireExam_Pojo.setModule_score(columns_source_SFTP[2]);
			saba_SFTP_SQL_NewHireExam_Pojo.setClass_ID(columns_source_SFTP[3]);
			saba_SFTP_SQL_NewHireExam_Pojo.setPerson_Full_Name(columns_source_SFTP[4]);
			saba_SFTP_SQL_NewHireExam_Pojo.setManager_Full_Name(columns_source_SFTP[5]);
			saba_SFTP_SQL_NewHireExam_Pojo.setPerson_Username(columns_source_SFTP[6]);
			saba_SFTP_SQL_NewHireExam_Pojo.setPerson_Status(columns_source_SFTP[7]);
			saba_SFTP_SQL_NewHireExam_Pojo.setCompleted_Courses_Transcript_Score(columns_source_SFTP[8]);
			saba_SFTP_SQL_NewHireExam_Pojo.setLearner_Module_Attempts(columns_source_SFTP[9]);
			saba_SFTP_SQL_NewHireExam_Pojo.setModule_Completed_On(columns_source_SFTP[10]);

			saba_SFTP_SQL_NewHireExam_Pojo_Arraylist.add(saba_SFTP_SQL_NewHireExam_Pojo);
		}
		br_source.close();
		
		BufferedReader br_target = new BufferedReader(new FileReader(SQL_target_CSV_file_path));
	    String row_target;
	    br_target.readLine();
	    
	    Collection<String> SQL_Transcript_ID = new ArrayList<String>();
		Collection<String> SFTP_Transcript_ID = new ArrayList<String>();
	    
	    while ((row_target = br_target.readLine()) != null) {
	        String[] columns_target_SQL = row_target.split(",");
	        String Transcript_ID = columns_target_SQL[0];
	        SQL_Transcript_ID.add(Transcript_ID);
	        
	        for (Saba_SFTP_SQL_NewHireExam_Report_Pojo y: saba_SFTP_SQL_NewHireExam_Pojo_Arraylist) {
	        	SFTP_Transcript_ID.add(y.getTranscriptID());
	        	if(y.getTranscriptID().contains(Transcript_ID)) {
	        		
			        int listItemIndex = saba_SFTP_SQL_NewHireExam_Pojo_Arraylist.indexOf(y);
			        Saba_SFTP_SQL_NewHireExam_Report_Pojo arraylist_row = saba_SFTP_SQL_NewHireExam_Pojo_Arraylist.get(listItemIndex);
			        		
			        Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> TranscriptID SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getTranscriptID(), columns_target_SQL[0]);
			        Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Module_name SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getModule_name(), columns_target_SQL[1]);
			        Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Module_score SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getModule_score(), columns_target_SQL[2]);
			        Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Class_ID SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getClass_ID(), columns_target_SQL[3]);
			        Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Person_Full_Name SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getPerson_Full_Name(), columns_target_SQL[4]);
			        Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Manager_Full_Name SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getManager_Full_Name(), columns_target_SQL[5]);
			       	Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Person_Username SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getPerson_Username(), columns_target_SQL[6]);
			       	Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Person_Status SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getPerson_Status(), columns_target_SQL[7]);
			       	Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Completed_Courses_Transcript_Score SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getCompleted_Courses_Transcript_Score(), columns_target_SQL[8]);
			       	Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Learner_Module_Attempts SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getLearner_Module_Attempts(), columns_target_SQL[9]);
			       	Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Module_Completed_On SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getModule_Completed_On(), columns_target_SQL[10]);
			       	break;
	        	}
	        }
	    }
		br_target.close();
		
		//TO VALIDATE THAT ALL THE RECORDS GOT ASSERTED BETWEEN SOURCE AND TARGET
		SFTP_Transcript_ID.removeAll(SQL_Transcript_ID);
		HashSet<String> SFTP_Transcript_ID_hashset = new HashSet<String>();
		SFTP_Transcript_ID_hashset.addAll(SFTP_Transcript_ID);
		if (SFTP_Transcript_ID_hashset.size()>0) {
			System.out.println("CSV comparision failed because target SQL stage result CSV file doesn't contain below Transcript IDs");
			Assert.assertNull(SFTP_Transcript_ID_hashset);
		}
		
		System.out.println("End to End assertion between SFTP and SQL CSV files completed successfully without any errors.");
	}
	
	@And("^assert end to end data for '(.*)' records between source as SQL stage table result CSV file '(.*)' and target as SQL master table for New Hire Exam$")
	public void assert_end_to_end_data_for_Count_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_with_desired_count_New_Hire_Exam(Integer desired_count, String SQL_CSV_file_path) throws IOException{
		
		//ASSERTION WITH USER DEFINER COUNT OF RECORDS
		assert_end_to_end_data_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_for_Saba_New_Hire_Exam(desired_count, SQL_CSV_file_path);
	}
	
	@And("^assert end to end data for all the records between source as SQL stage table result CSV file '(.*)' and target as SQL master table for New Hire Exam$")
	public void assert_end_to_end_data_for_all_records_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_with_desired_count_New_Hire_Exam(String SQL_CSV_file_path) throws IOException{
		
		if (SQL_CSV_file_path.startsWith("$$")) {
			SQL_CSV_file_path = HashMapContainer.get(SQL_CSV_file_path);
		}
		
		//TO GET COUNT OF RECORDS PRESENT IN CSV FILE
		BufferedReader br_SQL = new BufferedReader(new FileReader(SQL_CSV_file_path));
		int SQL_CSV_file_record_count = 0;
		while (br_SQL.readLine() != null) {
			SQL_CSV_file_record_count++;
		}
		SQL_CSV_file_record_count = SQL_CSV_file_record_count - 2;
		System.out.println("SQL_CSV_file_record_count "+SQL_CSV_file_record_count);
		br_SQL.close();
		
		//ASSERTION FOR ALL THE RECORDS PRESENT IN CSV FILE
		assert_end_to_end_data_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_for_Saba_New_Hire_Exam(SQL_CSV_file_record_count, SQL_CSV_file_path);
	}
		
	public void assert_end_to_end_data_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_for_Saba_New_Hire_Exam(Integer desired_count, String SQL_CSV_file_path) throws IOException{	
		
		if (SQL_CSV_file_path.startsWith("$$")) {
			SQL_CSV_file_path = HashMapContainer.get(SQL_CSV_file_path);
		}
		
		BufferedReader bufferedReader_SQL = new BufferedReader(new FileReader(SQL_CSV_file_path));
	    String SQL_row;
	    bufferedReader_SQL.readLine();
	    for (int i=0; i<desired_count; i++) {
	    	while ((SQL_row = bufferedReader_SQL.readLine()) != null) {
		        String[] SQL_columns = SQL_row.split(",");
		        String transcript_ID = SQL_columns[0];
		        
		        //QUERY TO FETCH DATA FOR ONE RECORD AND ASSERT FOR ALL COLUMNS
		        String sql_query = ("SELECT * FROM [InternalProjects].[dbo].[SAB_NewHireExam] WHERE TranscriptID = "+transcript_ID).replace("\"", "'");
		        System.out.println(sql_query);
		        Utilities util = new Utilities();
		        Map<String, String> ResultSet = util.executeSQLQuery("jdbc:sqlserver://qdbsqltab01.mfrm.com:1433", "Mule-QA", "test@123", sql_query);
				Boolean SQL_result_set = ResultSet.isEmpty();
		        if (!SQL_result_set) {
		        	
		        	Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> TranscriptID SQL stage to master table validation_New_Hire_Exam: ", SQL_columns[0].replace("\"", ""), ResultSet.get("TranscriptID"));
				    Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Module_name SQL stage to master table validation_New_Hire_Exam: ", SQL_columns[1].replace("\"", ""), ResultSet.get("Module_name"));
				    Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Module_score SQL stage to master table validation_New_Hire_Exam: ", SQL_columns[2].replace("\"", ""), ResultSet.get("Module_score"));
				    Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Class_ID SQL stage to master table validation_New_Hire_Exam: ", SQL_columns[3].replace("\"", ""), ResultSet.get("Class_ID"));
				    Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Person_Full_Name SQL stage to master table validation_New_Hire_Exam: ", SQL_columns[4].replace("\"", ""), ResultSet.get("Person_Full_Name"));
				    Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Manager_Full_Name SQL stage to master table validation_New_Hire_Exam: ", SQL_columns[5].replace("\"", ""), ResultSet.get("Manager_Full_Name"));
				    Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Person_Username SQL stage to master table validation_New_Hire_Exam: ", SQL_columns[6].replace("\"", ""), ResultSet.get("Person_Username"));
				    Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Person_Status SQL stage to master table validation_New_Hire_Exam: ", SQL_columns[7].replace("\"", ""), ResultSet.get("Person_Status"));
				    Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Completed_Courses_Transcript_Score SQL stage to master table validation_New_Hire_Exam: ", SQL_columns[8].replace("\"", ""), ResultSet.get("Completed_Courses_Transcript_Score"));
				    Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Learner_Module_Attempts SQL stage to master table validation_New_Hire_Exam: ", SQL_columns[9].replace("\"", ""), ResultSet.get("Learner_Module_Attempts"));
				    Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Module_Completed_On SQL stage to master table validation_New_Hire_Exam: ", SQL_columns[10].replace("\"", ""), ResultSet.get("Module_Completed_On"));
		        	
			        break;
		        }else {
		        	System.out.println("No data present in SQL master table for transcript_ID: "+transcript_ID.replace("\"", "'"));
		        }
		    }
	    }
	    bufferedReader_SQL.close();
	    System.out.println("End to End assertion between SQL stage and master table completed successfully without any errors.");
	}
	
	@Then("^assert end to end data between source SFTP CSV file '(.*)' and target SQL stage table result CSV file '(.*)' for Master Records$")
	public void assert_end_to_end_data_between_source_SFTP_CSV_file_and_target_SQL_result_CSV_file_for_Master_records(String SFTP_source_CSV_file_path, String SQL_target_CSV_file_path) throws CsvValidationException, IOException {
		System.out.println("Initiated end to end assertion between SFTP and SQL CSV files");
		List<Saba_SFTP_SQL_Master_Report_Pojo> saba_SFTP_SQL_Master_Pojo_Arraylist = new ArrayList<Saba_SFTP_SQL_Master_Report_Pojo>();
		if (SFTP_source_CSV_file_path.startsWith("$$")) {
			SFTP_source_CSV_file_path = HashMapContainer.get(SFTP_source_CSV_file_path);
		}
		if (SQL_target_CSV_file_path.startsWith("$$")) {
			SQL_target_CSV_file_path = HashMapContainer.get(SQL_target_CSV_file_path);
		}
		
//		SFTP_source_CSV_file_path = "F:\\Manoj Thota\\Saba\\Test data_Curriculla\\Master Data_20201014_080004 - Copy.csv";
//		SQL_target_CSV_file_path = "F:\\Manoj Thota\\Saba\\Test data_Curriculla\\SQL_Master_report_STG - Copy.csv";
		
		BufferedReader br_source = new BufferedReader(new FileReader(SFTP_source_CSV_file_path));
		String row_source;
		br_source.readLine();
		br_source.readLine();
		while ((row_source = br_source.readLine()) != null) {
			String[] columns_source_SFTP = row_source.split(",");
			Saba_SFTP_SQL_Master_Report_Pojo saba_SFTP_SQL_Master_Pojo = new Saba_SFTP_SQL_Master_Report_Pojo();
			saba_SFTP_SQL_Master_Pojo.setTranscriptID(columns_source_SFTP[0]);
			saba_SFTP_SQL_Master_Pojo.setCompletion_Status(columns_source_SFTP[1]);
			saba_SFTP_SQL_Master_Pojo.setRegistration_Status(columns_source_SFTP[2]);
			saba_SFTP_SQL_Master_Pojo.setCourse_Title(columns_source_SFTP[3]);
			saba_SFTP_SQL_Master_Pojo.setCourse_Course_ID(columns_source_SFTP[4]);
			saba_SFTP_SQL_Master_Pojo.setClass_ID(columns_source_SFTP[5]);
			saba_SFTP_SQL_Master_Pojo.setPerson_Username(columns_source_SFTP[6]);
			saba_SFTP_SQL_Master_Pojo.setPerson_Full_Name(columns_source_SFTP[7]);
			saba_SFTP_SQL_Master_Pojo.setPerson_Company(columns_source_SFTP[8]);
			saba_SFTP_SQL_Master_Pojo.setManager_Full_Name(columns_source_SFTP[9]);
			saba_SFTP_SQL_Master_Pojo.setPerson_Parent_Organization_Name(columns_source_SFTP[10]);
			saba_SFTP_SQL_Master_Pojo.setPerson_Job_Type_Name(columns_source_SFTP[11]);
			saba_SFTP_SQL_Master_Pojo.setPerson_Location_Name(columns_source_SFTP[12]);
			saba_SFTP_SQL_Master_Pojo.setCompleted_Courses_Transcript_Date_Marked_Complete(columns_source_SFTP[13]);
			saba_SFTP_SQL_Master_Pojo.setRegistration_Date(columns_source_SFTP[14]);
			saba_SFTP_SQL_Master_Pojo.setCourse_Target_Date(columns_source_SFTP[15]);
			saba_SFTP_SQL_Master_Pojo.setPerson_Employee_Type(columns_source_SFTP[16]);
			saba_SFTP_SQL_Master_Pojo.setPerson_Status(columns_source_SFTP[17]);
			saba_SFTP_SQL_Master_Pojo.setCompleted_Courses_Transcript_Score(columns_source_SFTP[18]);

			saba_SFTP_SQL_Master_Pojo_Arraylist.add(saba_SFTP_SQL_Master_Pojo);
		}
		br_source.close();
		BufferedReader br_target = new BufferedReader(new FileReader(SQL_target_CSV_file_path));
		String row_target;
		br_target.readLine();
		while ((row_target = br_target.readLine()) != null) {
			String[] columns_target_SQL = row_target.split(",");
			String Transcript_ID = columns_target_SQL[0];
			for (Saba_SFTP_SQL_Master_Report_Pojo y : saba_SFTP_SQL_Master_Pojo_Arraylist) {
				if (y.getTranscriptID().contains(Transcript_ID)) {
					int listItemIndex = saba_SFTP_SQL_Master_Pojo_Arraylist.indexOf(y);
					Saba_SFTP_SQL_Master_Report_Pojo arraylist_row = saba_SFTP_SQL_Master_Pojo_Arraylist
							.get(listItemIndex);
					
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> TranscriptID SFTP to SQL validation_Master: ", arraylist_row.getTranscriptID(), columns_target_SQL[0]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Completion_Status SFTP to SQL validation_Master: ", arraylist_row.getCompletion_Status(), columns_target_SQL[1]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Registration_Status SFTP to SQL validation_Master: ", arraylist_row.getRegistration_Status(), columns_target_SQL[2]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Course_Title SFTP to SQL validation_Master: ", arraylist_row.getCourse_Title(), columns_target_SQL[3]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Course_Course_ID SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getCourse_Course_ID(), columns_target_SQL[4]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Class_ID SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getClass_ID(), columns_target_SQL[5]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Person_Username SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getPerson_Username(), columns_target_SQL[6]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Person_Full_Name SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getPerson_Full_Name(), columns_target_SQL[7]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Person_Company SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getPerson_Company(), columns_target_SQL[8]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Manager_Full_Name SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getManager_Full_Name(), columns_target_SQL[9]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Person_Parent_Organization_Name SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getPerson_Parent_Organization_Name(), columns_target_SQL[10]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Person_Job_Type_Name SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getPerson_Job_Type_Name(), columns_target_SQL[11]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Person_Location_Name SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getPerson_Location_Name(), columns_target_SQL[12]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Completed_Courses_Transcript_Date_Marked_Complete SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getCompleted_Courses_Transcript_Date_Marked_Complete(), columns_target_SQL[13]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Registration_Date SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getRegistration_Date(), columns_target_SQL[14]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Course_Target_Date SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getCourse_Target_Date(), columns_target_SQL[15]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Person_Employee_Type SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getPerson_Employee_Type(), columns_target_SQL[16]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Person_Status SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getPerson_Status(), columns_target_SQL[17]);
					Assert.assertEquals("Transcript_ID: "+Transcript_ID+" --> Completed_Courses_Transcript_Score SFTP to SQL validation_New_Hire_Exam: ", arraylist_row.getCompleted_Courses_Transcript_Score(), columns_target_SQL[18]);
				}
			}
		}
		br_target.close();
		System.out.println(
				"End to End assertion between SFTP and SQL CSV files completed successfully without any errors.");
	}
	
	@And("^assert end to end data for '(.*)' records between source as SQL stage table result CSV file '(.*)' and target as SQL master table for Master Records$")
	public void assert_end_to_end_data_for_Count_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_with_desired_count_Master(Integer desired_count, String SQL_CSV_file_path) throws IOException{
		
		assert_end_to_end_data_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_for_Saba_Master(desired_count, SQL_CSV_file_path);
	}
	
	@And("^assert end to end data for all the records between source as SQL stage table result CSV file '(.*)' and target as SQL master table for Master Records$")
	public void assert_end_to_end_data_for_all_records_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_with_desired_count_Master(String SQL_CSV_file_path) throws IOException{
		
		if (SQL_CSV_file_path.startsWith("$$")) {
			SQL_CSV_file_path = HashMapContainer.get(SQL_CSV_file_path);
		}
//		SQL_CSV_file_path = "F:\\Manoj Thota\\Saba\\Test data_Curriculla\\Master Data_20201014_080004.csv";
		BufferedReader br_SQL = new BufferedReader(new FileReader(SQL_CSV_file_path));
		int SQL_CSV_file_record_count = 0;
		while (br_SQL.readLine() != null) {
			SQL_CSV_file_record_count++;
		}
		SQL_CSV_file_record_count = SQL_CSV_file_record_count - 1;
		System.out.println("SQL_CSV_file_record_count "+SQL_CSV_file_record_count);
		br_SQL.close();
		
		assert_end_to_end_data_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_for_Saba_Master(SQL_CSV_file_record_count, SQL_CSV_file_path);
	}
		
	public void assert_end_to_end_data_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_for_Saba_Master(Integer desired_count, String SQL_CSV_file_path) throws IOException{	
		
		if (SQL_CSV_file_path.startsWith("$$")) {
			SQL_CSV_file_path = HashMapContainer.get(SQL_CSV_file_path);
		}
		BufferedReader bufferedReader_SQL = new BufferedReader(new FileReader(SQL_CSV_file_path));
		String SQL_row;
		bufferedReader_SQL.readLine();
		for (int i = 0; i < desired_count; i++) {
			while ((SQL_row = bufferedReader_SQL.readLine()) != null) {
				String[] SQL_columns = SQL_row.split(",");
				String transcript_ID = SQL_columns[0];
				String sql_query = ("SELECT * FROM [InternalProjects].[dbo].[SAB_MasterData] WHERE TranscriptID = "+ transcript_ID).replace("\"", "'");
				System.out.println("MySQL query executed: "+sql_query);
				Utilities util = new Utilities();
				Map<String, String> ResultSet = util.executeSQLQuery("jdbc:sqlserver://qdbsqltab01.mfrm.com:1433",
						"Mule-QA", "test@123", sql_query);
				Boolean SQL_result_set = ResultSet.isEmpty();
				if (!SQL_result_set) {
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Completion_Status SQL stage to master table validation_Master: ", SQL_columns[1].replace("\"", ""), ResultSet.get("Completion_Status"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Registration_Status SQL stage to master table validation_Master: ", SQL_columns[2].replace("\"", ""), ResultSet.get("Registration_Status"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Course_Title SQL stage to master table validation_Master: ", SQL_columns[3].replace("\"", ""), ResultSet.get("Course_Title"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Course_Course_ID SQL stage to master table validation_Master: ", SQL_columns[4].replace("\"", ""), ResultSet.get("Course_Course_ID"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Class_ID SQL stage to master table validation_Master: ", SQL_columns[5].replace("\"", ""), ResultSet.get("Class_ID"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Person_Username SQL stage to master table validation_Master: ", SQL_columns[6].replace("\"", ""), ResultSet.get("Person_Username"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Person_Full_Name SQL stage to master table validation_Master: ", SQL_columns[7].replace("\"", ""), ResultSet.get("Person_Full_Name"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Person_Company SQL stage to master table validation_Master: ", SQL_columns[8].replace("\"", ""), ResultSet.get("Person_Company"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Manager_Full_Name SQL stage to master table validation_Master: ", SQL_columns[9].replace("\"", ""), ResultSet.get("Manager_Full_Name"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Person_Parent_Organization_Name SQL stage to master table validation_Master: ", SQL_columns[10].replace("\"", ""), ResultSet.get("Person_Parent_Organization_Name"));
					
					BufferedReader bufferedReader1_SQL = new BufferedReader(new FileReader(SQL_CSV_file_path));
					bufferedReader1_SQL.readLine();
					CSVParser parser1_SQL = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames()
							.withDelimiter(',').parse(bufferedReader1_SQL);
					String person_job_type_name = null;
					String Person_Location_Name = null;
					String Completed_Courses_Transcript_Date_Marked_Complete = null;
					String Registration_Date = null;
					String Course_Target_Date = null;
					String Person_Employee_Type = null;
					String Person_Status = null;
					String Completed_Courses_Transcript_Score = null;

					for (CSVRecord recordd : parser1_SQL) {
						if ((recordd.get(0).replace("\"", "")).equals(transcript_ID.replace("\"", ""))) {
							person_job_type_name = recordd.get(11);
							Person_Location_Name = recordd.get(12);
							Completed_Courses_Transcript_Date_Marked_Complete = recordd.get(13);
							Registration_Date = recordd.get(14);
							Course_Target_Date = recordd.get(15);
							Person_Employee_Type = recordd.get(16);
							Person_Status = recordd.get(17);
							Completed_Courses_Transcript_Score = recordd.get(18);
						}
					}
					bufferedReader1_SQL.close();
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Person_Job_Type_Name SQL stage to master table validation_Master: ", person_job_type_name, ResultSet.get("Person_Job_Type_Name").replace("\"", ""));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Person_Location_Name SQL stage to master table validation_Master: ", Person_Location_Name, ResultSet.get("Person_Location_Name"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Completed_Courses_Transcript_Date_Marked_Complete SQL stage to master table validation_Master: ", Completed_Courses_Transcript_Date_Marked_Complete, ResultSet.get("Completed_Courses_Transcript_Date_Marked_Complete"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Registration_Date SQL stage to master table validation_Master: ", Registration_Date, ResultSet.get("Registration_Date"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Course_Target_Date SQL stage to master table validation_Master: ", Course_Target_Date, ResultSet.get("Course_Target_Date"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Person_Employee_Type SQL stage to master table validation_Master: ", Person_Employee_Type, ResultSet.get("Person_Employee_Type"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Person_Status SQL stage to master table validation_Master: ", Person_Status, ResultSet.get("Person_Status"));
					Assert.assertEquals("Transcript_ID: "+transcript_ID+" --> Completed_Courses_Transcript_Score SQL stage to master table validation_Master: ", Completed_Courses_Transcript_Score, ResultSet.get("Completed_Courses_Transcript_Score"));
					break;
				} else {
					System.out.println("No data present in SQL master table for transcript_ID: "+ transcript_ID.replace("\"", "'"));
				}
			}
		}
		bufferedReader_SQL.close();
		System.out.println("End to End assertion between SQL stage and master table completed successfully without any errors.");
	}

	@Then("^assert if duplicate records exist in source SABA API csv file '(.*)'$")
    public void assert_if_duplicate_records_exist_in_Saba_result_csv_file(String csv_file_path) throws IOException {
      
		if (csv_file_path.startsWith("$$")) {
			csv_file_path = HashMapContainer.get(csv_file_path);
		}
	     
		//LOGIC IS TO INSERT SAME DATA IN ARRAYLIST AND HASHSET AND THEN ASSERT THE SIZE OF BOTH
		BufferedReader bufferedReader = new BufferedReader(new FileReader(csv_file_path));
		bufferedReader.readLine();
		bufferedReader.readLine();
		CSVParser parser_API = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames().withDelimiter(',')
				.parse(bufferedReader);
		ArrayList<String> firstColumnValue_Array = new ArrayList<String>();
		HashSet<String> firstColumnValue_hashset = new HashSet<String>();
		for (CSVRecord APIrecord : parser_API) {

			String first_column_value = APIrecord.get(0);
			firstColumnValue_hashset.add(first_column_value);
			firstColumnValue_Array.add(first_column_value);
		}
		bufferedReader.close();

		int distinct_hashset_firstColumnValue_count = firstColumnValue_hashset.size();
		int arraylist_firstColumnValue_count = firstColumnValue_Array.size();
		Assert.assertEquals("Duplicate records validation for source inContact API csv file: ",arraylist_firstColumnValue_count, distinct_hashset_firstColumnValue_count);
		System.out.println("Duplicate validation for first column in source CSV file is done for -->"+arraylist_firstColumnValue_count+"<-- records");
    }
	
	@Given("^capture CSV file for '(.*)' flow between '(.*)' and '(.*)' using inContact API '(.*)' and download the CSV file to the path '(.*)'$")
	public void Capture_CSV_for_inContact_API_between_date_range(String flow_name, String start_date, String end_date, String API, String file_download_path) throws InterruptedException  {
		
		//BELOW CODE IS TO SET DESIRED DOWNLOAD PATH TO THE CHROME DRIVER
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("download.default_directory", file_download_path);
		ChromeDriverService service = new ChromeDriverService.Builder()
                .usingDriverExecutable(new File(System.getProperty("user.dir")+"/src/test/java/com/Resources/chromedriver.exe"))
                .usingAnyFreePort()
                .build();
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);
//		options.CAPABILITY.;
		
		WebDriver driver = new ChromeDriver(service, options);
		
		if (start_date.startsWith("$$")) {
			start_date = HashMapContainer.get(start_date);
		}
		if (end_date.startsWith("$$")) {
			end_date = HashMapContainer.get(end_date);
		}
		
		String[] start_date_array = start_date.split("-");
		String[] start_end_array = end_date.split("-");
		
		//FRAMING COMPLETED API TO SET START AND END DATE AND TIME
		String complete_api = API+"&DateFrom="+start_date_array[0]+"%2f"+start_date_array[1]+"%2f"+start_date_array[2]+"+4%3a00%3a00+AM&DateTo="+start_end_array[0]+"%2f"+start_end_array[1]+"%2f"+start_end_array[2]+"+3%3a59%3a59+AM";
		System.out.println("inContact API used for downloading the csv file: "+complete_api);
		switch(flow_name){
			case "WithHold":
				driver.get(complete_api);
				Thread.sleep(5000);
				break;
			
			case "Expanded":
				driver.get(complete_api);
				Thread.sleep(5000);
				break;
				
			case "Agentlist":
				driver.get(complete_api);
				Thread.sleep(5000);
				break;
		}
		driver.close();
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss");  
		LocalDateTime now = LocalDateTime.now();
		String current_date_time = dtf.format(now);
		String Downloaded_file_name = null;
		switch(flow_name) {
			case "WithHold":
				Downloaded_file_name = "Data_WithHold_"+current_date_time+".csv";
				break;
			case "Expanded":
				Downloaded_file_name = "Data_Expanded_"+current_date_time+".csv";
				break;
			case "Agentlist":
				Downloaded_file_name = "Data_Agentlist_"+current_date_time+".csv";
				break;
		}
		
		//BELOW CODE IS TO CHANGE THE DOWNLOADED FILE NAME BY ADDING SYSTEM DATE AND TIME
		Path source = Paths.get(file_download_path+"\\Data.csv");
		try{
			Files.move(source, source.resolveSibling(Downloaded_file_name));
			} catch (IOException e) {
			  e.printStackTrace();
			}
		String DownloadedFilePath = file_download_path+"\\"+Downloaded_file_name;
		System.out.println("Downloaded File Path: "+DownloadedFilePath);
		HashMapContainer.add("$$inContactAPIResultCSVFilePath", DownloadedFilePath);
	}
	
	@Then("get the count of records available in inContact csv file {string}")
	public void get_the_count_of_records_available_in_inContact_csv_file(String csv_file_path) throws IOException {
		
		if (csv_file_path.startsWith("$$")) {
			csv_file_path = HashMapContainer.get(csv_file_path);
		}
		BufferedReader bufferedReader = new BufferedReader(new FileReader(csv_file_path));
	     int CSV_file_record_count = 0;
	     while(bufferedReader.readLine() != null)
	     {
	    	 CSV_file_record_count++;
	     }
	     CSV_file_record_count = CSV_file_record_count-1;
	     System.out.println("CSV file records count : "+CSV_file_record_count);
	     bufferedReader.close();
	}
	
	@Then("^assert if duplicate records exist in source inContact API result csv file '(.*)'$")
	public void assert_if_duplicate_records_exist_in_inContact_API_result_csv_file(String csv_file_path) throws IOException {
		
		if (csv_file_path.startsWith("$$")) {
			csv_file_path = HashMapContainer.get(csv_file_path);
		}
	     
		//LOGIC IS TO INSERT SAME DATA IN ARRAYLIST AND HASHSET AND THEN ASSERT THE SIZE OF BOTH
		BufferedReader bufferedReader = new BufferedReader(new FileReader(csv_file_path));
		bufferedReader.readLine();
		CSVParser parser_API = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames().withDelimiter(',')
				.parse(bufferedReader);
		ArrayList<String> firstColumnValue_Array = new ArrayList<String>();
		HashSet<String> firstColumnValue_hashset = new HashSet<String>();
		for (CSVRecord GCPrecord : parser_API) {

			String first_column_value = GCPrecord.get(0);
			firstColumnValue_hashset.add(first_column_value);
			firstColumnValue_Array.add(first_column_value);
		}
		bufferedReader.close();

		int distinct_hashset_firstColumnValue_count = firstColumnValue_hashset.size();
		int arraylist_firstColumnValue_count = firstColumnValue_Array.size();
		Assert.assertEquals("Duplicate records validation for source inContact API csv file: ",arraylist_firstColumnValue_count, distinct_hashset_firstColumnValue_count);
		System.out.println("Duplicate validation for first column in source CSV file is done for -->"+arraylist_firstColumnValue_count+"<-- records");
	}
	
	@Then("^assert end to end data between source CSV file '(.*)' and target CSV file '(.*)' for WithHold$")
	public void assert_end_to_end_data_between_source_CSV_file_and_target_CSV_file_for_WithHold(String source_CSV_file_path, String target_CSV_file_path) throws IOException {
		System.out.println("Initiated End to End data validation between InContact API result CSV file and GCP CSV file");
		List<inContactBQ_CSVPojo_WH_EXPD_AgentList> inContactBQ_CSV_WH_EXPD_arraylist = new ArrayList<inContactBQ_CSVPojo_WH_EXPD_AgentList>();
		if (source_CSV_file_path.startsWith("$$")) {
			source_CSV_file_path = HashMapContainer.get(source_CSV_file_path);
		}
		if (target_CSV_file_path.startsWith("$$")) {
			target_CSV_file_path = HashMapContainer.get(target_CSV_file_path);
		}
		
		//TO GET COUNT OF SOURCE AND TARGET CSV FILE RECORDS
		BufferedReader bufferedReader_source = new BufferedReader(new FileReader(source_CSV_file_path));
		int source_CSV_file_record_count = 0;
		while ((bufferedReader_source.readLine()) != null) {
			source_CSV_file_record_count++;
		}
		source_CSV_file_record_count = source_CSV_file_record_count - 1;
		System.out.println("Source_CSV_file_record_count: "+source_CSV_file_record_count);
		bufferedReader_source.close();
		
		BufferedReader bufferedReader_target = new BufferedReader(new FileReader(target_CSV_file_path));
		int target_CSV_file_record_count = 0;
		while ((bufferedReader_target.readLine()) != null) {
			target_CSV_file_record_count++;
		}
		target_CSV_file_record_count = target_CSV_file_record_count - 1;
		System.out.println("Target_CSV_file_record_count: "+target_CSV_file_record_count);
		bufferedReader_target.close();
		
		//CONDITION TO MATCH THE COUNT
		if(source_CSV_file_record_count==target_CSV_file_record_count) {
		
		BufferedReader bufferedReader_API_CSV = new BufferedReader(new FileReader(source_CSV_file_path));
		bufferedReader_API_CSV.readLine();
		CSVParser parser_inContactAPI_CSV = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames().withDelimiter(',')
				.parse(bufferedReader_API_CSV);
		
		//TO STORE END TO END DATA PRESENT IN SOURCE CSV FILE INTO POJO
		for (CSVRecord inContactAPI_CSV_columns : parser_inContactAPI_CSV) {
			
			inContactBQ_CSVPojo_WH_EXPD_AgentList inContactFM = new inContactBQ_CSVPojo_WH_EXPD_AgentList();
			
			inContactFM.setContact_id(inContactAPI_CSV_columns.get(0));
			inContactFM.setMaster_contact_id(inContactAPI_CSV_columns.get(1));
			inContactFM.setContact_Code(inContactAPI_CSV_columns.get(2));
			inContactFM.setMedia_name(inContactAPI_CSV_columns.get(3));
			inContactFM.setContact_name(inContactAPI_CSV_columns.get(4));
			inContactFM.setANI_DIALNUM(inContactAPI_CSV_columns.get(5));
			inContactFM.setSkill_no(inContactAPI_CSV_columns.get(6));
			inContactFM.setSkill_name(inContactAPI_CSV_columns.get(7));
			inContactFM.setCampaign_no(inContactAPI_CSV_columns.get(8));
			inContactFM.setCampaign_name(inContactAPI_CSV_columns.get(9));
			inContactFM.setAgent_no(inContactAPI_CSV_columns.get(10));
			inContactFM.setAgent_name(inContactAPI_CSV_columns.get(11));
			inContactFM.setTeam_no(inContactAPI_CSV_columns.get(12));
			inContactFM.setTeam_name(inContactAPI_CSV_columns.get(13));
			inContactFM.setDisposition_code(inContactAPI_CSV_columns.get(14));
			inContactFM.setSLA(inContactAPI_CSV_columns.get(15));
			inContactFM.setStart_date(inContactAPI_CSV_columns.get(16));
			inContactFM.setStart_time(inContactAPI_CSV_columns.get(17));
			inContactFM.setPreQueue(inContactAPI_CSV_columns.get(18));
			inContactFM.setInQueue(inContactAPI_CSV_columns.get(19));
			inContactFM.setAgent_Time(inContactAPI_CSV_columns.get(20));
			inContactFM.setPostQueue(inContactAPI_CSV_columns.get(21));
			inContactFM.setTotal_Time(inContactAPI_CSV_columns.get(22));
			inContactFM.setAbandon_Time(inContactAPI_CSV_columns.get(23));
			inContactFM.setRouting_Time(inContactAPI_CSV_columns.get(24));
			inContactFM.setAbandon(inContactAPI_CSV_columns.get(25));
			inContactFM.setCallback_time(inContactAPI_CSV_columns.get(26));
			inContactFM.setLogged(inContactAPI_CSV_columns.get(27));
			inContactFM.setHold_Time(inContactAPI_CSV_columns.get(28));

			inContactBQ_CSV_WH_EXPD_arraylist.add(inContactFM);
		}
		bufferedReader_API_CSV.close();
		Collection<String> GCP_contact_ID = new ArrayList<String>();
		Collection<String> API_contact_ID = new ArrayList<String>();
		
		BufferedReader bufferedReader_GCP_CSV = new BufferedReader(new FileReader(target_CSV_file_path));
		bufferedReader_GCP_CSV.readLine();
		CSVParser parser_GCP_CSV = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames().withDelimiter(',')
				.parse(bufferedReader_GCP_CSV);
		
		for (CSVRecord GCP_CSV_columns : parser_GCP_CSV) {
			
			String target_conatct_ID = GCP_CSV_columns.get(0);
			GCP_contact_ID.add(target_conatct_ID);
			for(inContactBQ_CSVPojo_WH_EXPD_AgentList x: inContactBQ_CSV_WH_EXPD_arraylist) {
				API_contact_ID.add(x.getContact_id());
				//CONDITION TO CHECK MATCHING RECORD BETWEEN POJO AND TARGET CSV FILE DATA
				if(x.getContact_id().contains(target_conatct_ID)) {
					int listItemIndex = inContactBQ_CSV_WH_EXPD_arraylist.indexOf(x);
					inContactBQ_CSVPojo_WH_EXPD_AgentList arraylist_row = inContactBQ_CSV_WH_EXPD_arraylist.get(listItemIndex);
					
					//ASSERTION OF END TO END DATA FOR ALL COLUMNS
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> ContactID CSV to GCP Bucket validation_WithHold: ", arraylist_row.getContact_id(), GCP_CSV_columns.get(0));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Master ContactID CSV to GCP Bucket validation_WithHold: ", arraylist_row.getMaster_contact_id(), GCP_CSV_columns.get(1));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Contact code CSV to GCP Bucket validation_WithHold: ",arraylist_row.getContact_Code(), GCP_CSV_columns.get(2));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Media name CSV to GCP Bucket validation_WithHold: ",arraylist_row.getMedia_name(), GCP_CSV_columns.get(3));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Contact name CSV to GCP Bucket validation_WithHold: ",arraylist_row.getContact_name(), GCP_CSV_columns.get(4));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> ANI DialNum CSV to GCP Bucket validation_WithHold: ",arraylist_row.getANI_DIALNUM(), GCP_CSV_columns.get(5));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Skill No CSV to GCP Bucket validation_WithHold: ",arraylist_row.getSkill_no(), GCP_CSV_columns.get(6));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Skill name CSV to GCP Bucket validation_WithHold: ",arraylist_row.getSkill_name(), GCP_CSV_columns.get(7));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Campaign no CSV to GCP Bucket validation_WithHold: ",arraylist_row.getCampaign_no(), GCP_CSV_columns.get(8));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Campaign name CSV to GCP Bucket validation_WithHold: ",arraylist_row.getCampaign_name(), GCP_CSV_columns.get(9));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Agent No CSV to GCP Bucket validation_WithHold: ",arraylist_row.getAgent_no(), GCP_CSV_columns.get(10));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Agent Name CSV to GCP Bucket validation_WithHold: ",arraylist_row.getAgent_name(), GCP_CSV_columns.get(11));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Team no CSV to GCP Bucket validation_WithHold: ",arraylist_row.getTeam_no(), GCP_CSV_columns.get(12));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Team Name CSV to GCP Bucket validation_WithHold: ",arraylist_row.getTeam_name(), GCP_CSV_columns.get(13));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Disposition code CSV to GCP Bucket validation_WithHold: ",arraylist_row.getDisposition_code(), GCP_CSV_columns.get(14));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> SLA CSV to GCP Bucket validation_WithHold: ",arraylist_row.getSLA(), GCP_CSV_columns.get(15));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Start Date CSV to GCP Bucket validation_WithHold: ",arraylist_row.getStart_date(), GCP_CSV_columns.get(16));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Start Time CSV to GCP Bucket validation_WithHold: ",arraylist_row.getStart_time(), GCP_CSV_columns.get(17));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> PreQueue CSV to GCP Bucket validation_WithHold: ",arraylist_row.getPreQueue(), GCP_CSV_columns.get(18));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> InQueue CSV to GCP Bucket validation_WithHold: ",arraylist_row.getInQueue(), GCP_CSV_columns.get(19));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Agent Time CSV to GCP Bucket validation_WithHold: ",arraylist_row.getAgent_Time(), GCP_CSV_columns.get(20));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Post Queue CSV to GCP Bucket validation_WithHold: ",arraylist_row.getPostQueue(), GCP_CSV_columns.get(21));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Total Time CSV to GCP Bucket validation_WithHold: ",arraylist_row.getTotal_Time(), GCP_CSV_columns.get(22));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Abandon Time CSV to GCP Bucket validation_WithHold: ",arraylist_row.getAbandon_Time(), GCP_CSV_columns.get(23));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Routing Time CSV to GCP Bucket validation_WithHold: ",arraylist_row.getRouting_Time(), GCP_CSV_columns.get(24));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Abandon CSV to GCP Bucket validation_WithHold: ",arraylist_row.getAbandon(), GCP_CSV_columns.get(25));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Callback Time CSV to GCP Bucket validation_WithHold: ",arraylist_row.getCallback_time(), GCP_CSV_columns.get(26));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Logged CSV to GCP Bucket validation_WithHold: ",arraylist_row.getLogged(), GCP_CSV_columns.get(27));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Hold Time CSV to GCP Bucket validation_WithHold: ",arraylist_row.getHold_Time(), GCP_CSV_columns.get(28));
					break;
				}
			}
		}
		bufferedReader_GCP_CSV.close();
		
		//CODE FOR GETTING NON MATCHING/NON EXISTING RECORDS IN TARGET
		API_contact_ID.removeAll(GCP_contact_ID);
		HashSet<String> API_contact_IDs_hashset = new HashSet<String>();
		API_contact_IDs_hashset.addAll(API_contact_ID);
		if (API_contact_IDs_hashset.size()>0) {
			System.out.println("CSV comparision failed because target GCP CSV file doesn't contain below contact ID's");
			Assert.assertNull(API_contact_IDs_hashset);
		}
		}else {
			System.out.println("Source and Target CSV files record count mismatch");
		}
		System.out.println("End to End data validation between InContact API result CSV file and GCP CSV file is successfull");
	}
	
	@Then("^assert end to end data between source CSV file '(.*)' and target CSV file '(.*)' for Expanded$")
	public void assert_end_to_end_data_between_source_CSV_file_and_target_CSV_file_for_Expanded(String source_CSV_file_path, String target_CSV_file_path) throws IOException {
		System.out.println("Initiated End to End data validation between InContact API result CSV file and GCP CSV file");
		List<inContactBQ_CSVPojo_WH_EXPD_AgentList> inContactBQ_CSV_WH_EXPD_arraylist = new ArrayList<inContactBQ_CSVPojo_WH_EXPD_AgentList>();
		if (source_CSV_file_path.startsWith("$$")) {
			source_CSV_file_path = HashMapContainer.get(source_CSV_file_path);
		}
		if (target_CSV_file_path.startsWith("$$")) {
			target_CSV_file_path = HashMapContainer.get(target_CSV_file_path);
		}
		
		//TO GET COUNT OF SOURCE AND TARGET CSV FILE RECORDS
		BufferedReader bufferedReader_source = new BufferedReader(new FileReader(source_CSV_file_path));
		int source_CSV_file_record_count = 0;
		while (bufferedReader_source.readLine() != null) {
			source_CSV_file_record_count++;
		}
		source_CSV_file_record_count = source_CSV_file_record_count - 1;
		System.out.println("source_CSV_file_record_count: "+source_CSV_file_record_count);
		bufferedReader_source.close();
		
		BufferedReader bufferedReader_target = new BufferedReader(new FileReader(target_CSV_file_path));
		int target_CSV_file_record_count = 0;
		while (bufferedReader_target.readLine() != null) {
			target_CSV_file_record_count++;
		}
		target_CSV_file_record_count = target_CSV_file_record_count - 1;
		System.out.println("target_CSV_file_record_count: "+target_CSV_file_record_count);
		bufferedReader_target.close();
		
		//CONDITION TO MATCH THE COUNT
		if(source_CSV_file_record_count==target_CSV_file_record_count) {
		
		BufferedReader bufferedReader_API_CSV = new BufferedReader(new FileReader(source_CSV_file_path));
		bufferedReader_API_CSV.readLine();
		CSVParser parser_inContactAPI_CSV = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames().withDelimiter(',')
				.parse(bufferedReader_API_CSV);
		
		//TO STORE END TO END DATA PRESENT IN SOURCE CSV FILE INTO POJO
		for (CSVRecord inContactAPI_CSV_columns : parser_inContactAPI_CSV) {
			
			inContactBQ_CSVPojo_WH_EXPD_AgentList inContactFM = new inContactBQ_CSVPojo_WH_EXPD_AgentList();
			inContactFM.setContact_id(inContactAPI_CSV_columns.get(0));
			inContactFM.setMaster_contact_id(inContactAPI_CSV_columns.get(1));
			inContactFM.setMedia_name(inContactAPI_CSV_columns.get(2));
			inContactFM.setContact_name(inContactAPI_CSV_columns.get(3));
			inContactFM.setANI(inContactAPI_CSV_columns.get(4));
			inContactFM.setDNIS(inContactAPI_CSV_columns.get(5));
			inContactFM.setSkill_no(inContactAPI_CSV_columns.get(6));
			inContactFM.setSkill_name(inContactAPI_CSV_columns.get(7));
			inContactFM.setCampaign_no(inContactAPI_CSV_columns.get(8));
			inContactFM.setCampaign_name(inContactAPI_CSV_columns.get(9));
			inContactFM.setAgent_no(inContactAPI_CSV_columns.get(10));
			inContactFM.setAgent_name(inContactAPI_CSV_columns.get(11));
			inContactFM.setTeam_no(inContactAPI_CSV_columns.get(12));
			inContactFM.setTeam_name(inContactAPI_CSV_columns.get(13));
			inContactFM.setSLA(inContactAPI_CSV_columns.get(14));
			inContactFM.setStart_date(inContactAPI_CSV_columns.get(15));
			inContactFM.setStart_time(inContactAPI_CSV_columns.get(16));
			inContactFM.setPreQueue(inContactAPI_CSV_columns.get(17));
			inContactFM.setInQueue(inContactAPI_CSV_columns.get(18));
			inContactFM.setAgent_Time(inContactAPI_CSV_columns.get(19));
			inContactFM.setPostQueue(inContactAPI_CSV_columns.get(20));
			inContactFM.setTotal_Time(inContactAPI_CSV_columns.get(21));
			inContactFM.setAbandon_Time(inContactAPI_CSV_columns.get(22));
			inContactFM.setAbandon(inContactAPI_CSV_columns.get(23));
			inContactFM.setACW_Seconds(inContactAPI_CSV_columns.get(24));
			inContactFM.setACW_Time(inContactAPI_CSV_columns.get(25));

			inContactBQ_CSV_WH_EXPD_arraylist.add(inContactFM);
		}
		bufferedReader_API_CSV.close();
		
		Collection<String> GCP_contact_ID = new ArrayList<String>();
		Collection<String> API_contact_ID = new ArrayList<String>();
		
		BufferedReader bufferedReader_GCP_CSV = new BufferedReader(new FileReader(target_CSV_file_path));
		bufferedReader_GCP_CSV.readLine();
		CSVParser parser_GCP_CSV = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames().withDelimiter(',')
				.parse(bufferedReader_GCP_CSV);
		
		for (CSVRecord GCP_CSV_columns : parser_GCP_CSV) {
			
			String target_conatct_ID = GCP_CSV_columns.get(0);
			GCP_contact_ID.add(target_conatct_ID);
		
			for(inContactBQ_CSVPojo_WH_EXPD_AgentList x: inContactBQ_CSV_WH_EXPD_arraylist) {
			API_contact_ID.add(x.getContact_id());
				
			//CONDITION TO CHECK MATCHING RECORD BETWEEN POJO AND TARGET CSV FILE DATA
			if(x.getContact_id().contains(target_conatct_ID)) {
				int listItemIndex = inContactBQ_CSV_WH_EXPD_arraylist.indexOf(x);
				inContactBQ_CSVPojo_WH_EXPD_AgentList arraylist_row = inContactBQ_CSV_WH_EXPD_arraylist.get(listItemIndex);
				
					//ASSERTION OF END TO END DATA FOR ALL COLUMNS
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> ContactID: CSV to GCP Bucket validation_Expanded: ", arraylist_row.getContact_id(), GCP_CSV_columns.get(0));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Master ContactID: CSV to GCP Bucket validation_Expanded: ", arraylist_row.getMaster_contact_id(), GCP_CSV_columns.get(1));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Media name: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getMedia_name(), GCP_CSV_columns.get(2));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Contact name: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getContact_name(), GCP_CSV_columns.get(3));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> ANI: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getANI(), GCP_CSV_columns.get(4));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> DNIS DialNum: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getDNIS(), GCP_CSV_columns.get(5));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Skill No: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getSkill_no(), GCP_CSV_columns.get(6));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Skill name: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getSkill_name(), GCP_CSV_columns.get(7));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Campaign no: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getCampaign_no(), GCP_CSV_columns.get(8));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Campaign name: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getCampaign_name(), GCP_CSV_columns.get(9));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Agent No: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getAgent_no(), GCP_CSV_columns.get(10));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Agent Name: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getAgent_name(), GCP_CSV_columns.get(11));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Team no: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getTeam_no(), GCP_CSV_columns.get(12));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Team Name: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getTeam_name(), GCP_CSV_columns.get(13));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> SLA: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getSLA(), GCP_CSV_columns.get(14));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Start Date: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getStart_date(), GCP_CSV_columns.get(15));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Start Time: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getStart_time(), GCP_CSV_columns.get(16));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> PreQueue: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getPreQueue(), GCP_CSV_columns.get(17));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> InQueue: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getInQueue(), GCP_CSV_columns.get(18));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Agent Time: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getAgent_Time(), GCP_CSV_columns.get(19));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Post Queue: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getPostQueue(), GCP_CSV_columns.get(20));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Total Time: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getTotal_Time(), GCP_CSV_columns.get(21));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Abandon Time: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getAbandon_Time(), GCP_CSV_columns.get(22));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> Abandon: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getAbandon(), GCP_CSV_columns.get(23));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> ACW Seconds: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getACW_Seconds(), GCP_CSV_columns.get(24));
					Assert.assertEquals("ContactID: "+GCP_CSV_columns.get(0)+" --> ACW Time: CSV to GCP Bucket validation_Expanded: ",arraylist_row.getACW_Time(), GCP_CSV_columns.get(25));
					break;
				}
			}
		}
		bufferedReader_GCP_CSV.close();
		
		//CODE FOR GETTING NON MATCHING/NON EXISTING RECORDS IN TARGET
		API_contact_ID.removeAll(GCP_contact_ID);
		HashSet<String> API_contact_ID_hashset = new HashSet<String>();
		API_contact_ID_hashset.addAll(API_contact_ID);
		if (API_contact_ID_hashset.size()>0) {
			System.out.println("CSV comparision failed because target GCP CSV file doesn't contain below Contact ID's");
			Assert.assertNull(API_contact_ID_hashset);
		}
		}else {
			System.out.println("Source and Target CSV files record count mismatch");
		}
		System.out.println("End to End data validation between InContact API result CSV file and GCP CSV file is successfull");
	}
	
	@Then("^assert end to end data between source CSV file '(.*)' and target CSV file '(.*)' for Agentlist$")
	public void assert_end_to_end_data_between_source_CSV_file_and_target_CSV_file_for_Agentlist(String source_inContact_CSV_file_path, String target_inContact_CSV_file_path) throws IOException {
		System.out.println("Initiated End to End data assertion between inContact API result CSV file and GCP CSV file content");
		List<inContactBQ_CSVPojo_WH_EXPD_AgentList> inContact_AgentList_pojo_arraylist = new ArrayList<inContactBQ_CSVPojo_WH_EXPD_AgentList>();
		if (source_inContact_CSV_file_path.startsWith("$$")) {
			source_inContact_CSV_file_path = HashMapContainer.get(source_inContact_CSV_file_path);
		}
		if (target_inContact_CSV_file_path.startsWith("$$")) {
			target_inContact_CSV_file_path = HashMapContainer.get(target_inContact_CSV_file_path);
		}
		
		//TO GET COUNT OF SOURCE AND TARGET CSV FILE RECORDS
		BufferedReader br_source_inContact_API = new BufferedReader(new FileReader(source_inContact_CSV_file_path));
		int source_inContact_API_CSV_file_record_count = 0;
		while (br_source_inContact_API.readLine() != null) {
			source_inContact_API_CSV_file_record_count++;
		}
		source_inContact_API_CSV_file_record_count = source_inContact_API_CSV_file_record_count - 1;
		System.out.println("Source_inContact_APICSV_file_record_count: "+source_inContact_API_CSV_file_record_count);
		br_source_inContact_API.close();
		
		BufferedReader br_target_inContact_API = new BufferedReader(new FileReader(target_inContact_CSV_file_path));
		int target_inContact_API_CSV_file_record_count = 0;
		while (br_target_inContact_API.readLine() != null) {
			target_inContact_API_CSV_file_record_count++;
		}
		target_inContact_API_CSV_file_record_count = target_inContact_API_CSV_file_record_count - 1;
		System.out.println("Target_inContact_API_CSV_file_record_count: "+target_inContact_API_CSV_file_record_count);
		br_target_inContact_API.close();
		
		//CONDITION TO MATCH THE COUNT
		if(source_inContact_API_CSV_file_record_count==target_inContact_API_CSV_file_record_count) {
			
			BufferedReader br_source_inContact_API_CSV_file = new BufferedReader(new FileReader(source_inContact_CSV_file_path));
			br_source_inContact_API_CSV_file.readLine();
		    
		    CSVParser parser_source_inContact_API_CSV = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames()
					.withDelimiter(',').parse(br_source_inContact_API_CSV_file);
		    
		  //TO STORE END TO END DATA PRESENT IN SOURCE CSV FILE INTO POJO
			for (CSVRecord source_inContact_API_CSV_record : parser_source_inContact_API_CSV) {
				
				inContactBQ_CSVPojo_WH_EXPD_AgentList inContact_Agentlist_FM = new inContactBQ_CSVPojo_WH_EXPD_AgentList();
				inContact_Agentlist_FM.setAgent_No_AL(source_inContact_API_CSV_record.get(0));
				inContact_Agentlist_FM.setTeam_No_AL(source_inContact_API_CSV_record.get(1));
				inContact_Agentlist_FM.setFirst_Name_AL(source_inContact_API_CSV_record.get(2));
				inContact_Agentlist_FM.setLast_Name_AL(source_inContact_API_CSV_record.get(3));
				inContact_Agentlist_FM.setEmail_AL(source_inContact_API_CSV_record.get(4));
				inContact_Agentlist_FM.setMod_Datetime_AL(source_inContact_API_CSV_record.get(5));
//				Below assertion is commented as we are not asserting Last login because it gets changed every time
//		    	inContact_Agentlist_FM.setLast_Login_AL(source_inContact_API_CSV_record.get(6));
				inContact_Agentlist_FM.setStatus_AL(source_inContact_API_CSV_record.get(7));
				inContact_Agentlist_FM.setReports_To_AL(source_inContact_API_CSV_record.get(8));
				inContact_Agentlist_FM.setMiddle_Name_AL(source_inContact_API_CSV_record.get(9));
				inContact_Agentlist_FM.setUsername_AL(source_inContact_API_CSV_record.get(10));
				inContact_Agentlist_FM.setTime_Zone_AL(source_inContact_API_CSV_record.get(11));
				inContact_Agentlist_FM.setChat_Timeout_AL(source_inContact_API_CSV_record.get(12));
				inContact_Agentlist_FM.setPhone_Timeout_AL(source_inContact_API_CSV_record.get(13));
				inContact_Agentlist_FM.setVoice_Timeout_AL(source_inContact_API_CSV_record.get(14));
				inContact_Agentlist_FM.setLocation_AL(source_inContact_API_CSV_record.get(15));
				inContact_Agentlist_FM.setHire_Date_AL(source_inContact_API_CSV_record.get(16));
				inContact_Agentlist_FM.setTermination_Date_AL(source_inContact_API_CSV_record.get(17));
				inContact_Agentlist_FM.setHourly_Cost_AL(source_inContact_API_CSV_record.get(18));
				inContact_Agentlist_FM.setRehire_Status_AL(source_inContact_API_CSV_record.get(19));
				inContact_Agentlist_FM.setEmployment_Type_AL(source_inContact_API_CSV_record.get(20));
				inContact_Agentlist_FM.setReferral_AL(source_inContact_API_CSV_record.get(21));
				inContact_Agentlist_FM.setAt_Home_Worker_AL(source_inContact_API_CSV_record.get(22));
				inContact_Agentlist_FM.setHiring_Source_AL(source_inContact_API_CSV_record.get(23));
				inContact_Agentlist_FM.setNtLoginName_AL(source_inContact_API_CSV_record.get(24));
				inContact_Agentlist_FM.setCustom1_AL(source_inContact_API_CSV_record.get(25));
				inContact_Agentlist_FM.setCustom2_AL(source_inContact_API_CSV_record.get(26));
				inContact_Agentlist_FM.setCustom3_AL(source_inContact_API_CSV_record.get(27));
				inContact_Agentlist_FM.setCustom4_AL(source_inContact_API_CSV_record.get(28));
				inContact_Agentlist_FM.setCustom5_AL(source_inContact_API_CSV_record.get(29));

				inContact_AgentList_pojo_arraylist.add(inContact_Agentlist_FM);
			}
			br_source_inContact_API_CSV_file.close();

			Collection<String> GCP_agent_numbers = new ArrayList<String>();
			Collection<String> API_agent_numbers = new ArrayList<String>();
			
			BufferedReader br_target_GCP_CSV_file = new BufferedReader(new FileReader(target_inContact_CSV_file_path));
			br_target_GCP_CSV_file.readLine();

			CSVParser parser_target_GCP_CSV = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames()
					.withDelimiter(',').parse(br_target_GCP_CSV_file);

			for (CSVRecord target_GCP_CSV_record : parser_target_GCP_CSV) {

				String GCP_CSV_agent_number = target_GCP_CSV_record.get(0);
				GCP_agent_numbers.add(GCP_CSV_agent_number);
		    	
			    for(inContactBQ_CSVPojo_WH_EXPD_AgentList x: inContact_AgentList_pojo_arraylist) {
			    	API_agent_numbers.add(x.getAgent_No_AL());
			    	
			    	//CONDITION TO CHECK MATCHING RECORD BETWEEN POJO AND TARGET CSV FILE DATA
					if(x.getAgent_No_AL().contains(GCP_CSV_agent_number)) {
						int listItemIndex = inContact_AgentList_pojo_arraylist.indexOf(x);
						inContactBQ_CSVPojo_WH_EXPD_AgentList arraylist_row = inContact_AgentList_pojo_arraylist.get(listItemIndex);
						
						//ASSERTION OF END TO END DATA FOR ALL COLUMNS
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Agent_No: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getAgent_No_AL(), target_GCP_CSV_record.get(0));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Team_No: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getTeam_No_AL(), target_GCP_CSV_record.get(1));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> First_Name: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getFirst_Name_AL(), target_GCP_CSV_record.get(2));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Last_Name: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getLast_Name_AL(), target_GCP_CSV_record.get(3));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Email: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getEmail_AL(), target_GCP_CSV_record.get(4));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Mod_Datetime: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getMod_Datetime_AL(), target_GCP_CSV_record.get(5));
//						Below assertion is commented as we are not asserting Last login because it gets changed every time
//						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Last_Login: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getLast_Login_AL(), target_GCP_CSV_record.get(6));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Status: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getStatus_AL(), target_GCP_CSV_record.get(7));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Reports_To: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getReports_To_AL(), target_GCP_CSV_record.get(8));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Middle_Name: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getMiddle_Name_AL(), target_GCP_CSV_record.get(9));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Username: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getUsername_AL(), target_GCP_CSV_record.get(10));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Time_Zone: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getTime_Zone_AL(), target_GCP_CSV_record.get(11));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Chat_Timeout: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getChat_Timeout_AL(), target_GCP_CSV_record.get(12));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Phone_Timeout: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getPhone_Timeout_AL(), target_GCP_CSV_record.get(13));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Voice_Timeout: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getVoice_Timeout_AL(), target_GCP_CSV_record.get(14));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Location: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getLocation_AL(), target_GCP_CSV_record.get(15));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Hire_Date: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getHire_Date_AL(), target_GCP_CSV_record.get(16));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Termination_Date: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getTermination_Date_AL(), target_GCP_CSV_record.get(17));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Hourly_Cost: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getHourly_Cost_AL(), target_GCP_CSV_record.get(18));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Rehire_Status: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getRehire_Status_AL(), target_GCP_CSV_record.get(19));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Employment_Type: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getEmployment_Type_AL(), target_GCP_CSV_record.get(20));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Referral: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getReferral_AL(), target_GCP_CSV_record.get(21));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> At_Home_Worker: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getAt_Home_Worker_AL(), target_GCP_CSV_record.get(22));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Hiring_Source: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getHiring_Source_AL(), target_GCP_CSV_record.get(23));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> NtLoginName: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getNtLoginName_AL(), target_GCP_CSV_record.get(24));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Custom1: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getCustom1_AL(), target_GCP_CSV_record.get(25));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Custom2: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getCustom2_AL(), target_GCP_CSV_record.get(26));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Custom3: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getCustom3_AL(), target_GCP_CSV_record.get(27));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Custom4: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getCustom4_AL(), target_GCP_CSV_record.get(28));
						Assert.assertEquals("Agent Number: "+GCP_CSV_agent_number+" --> Custom5: inContact API result CSV to GCP Bucket CSV validation_AgentList: ", arraylist_row.getCustom5_AL(), target_GCP_CSV_record.get(29));
						break;
					}
				}
			}
			br_target_GCP_CSV_file.close();
			
			//CODE FOR GETTING NON MATCHING/NON EXISTING RECORDS IN TARGET
			API_agent_numbers.removeAll(GCP_agent_numbers);
			HashSet<String> API_agent_numbers_hashset = new HashSet<String>();
			API_agent_numbers_hashset.addAll(API_agent_numbers);
			if (API_agent_numbers_hashset.size()>0) {
				System.out.println("CSV comparision failed because target GCP CSV file doesn't contain below Agent numbers");
				Assert.assertNull(API_agent_numbers_hashset);
			}
		}else {
			System.out.println("Source and Target CSV files record count mismatch");
		}
		System.out.println("Completed End to End data assertion between inContact API result CSV file and GCP CSV file successfully without any errors");
	}
	
	@And("^assert if duplicate records exist in target GCP Bucket result CSV file '(.*)'$")
	public void assert_duplicate_records_exist_in_target_GCP_Bucket_result_CSV_file(String GCP_CSV_file_path) throws IOException {
		
		if (GCP_CSV_file_path.startsWith("$$")) {
			GCP_CSV_file_path = HashMapContainer.get(GCP_CSV_file_path);
		}
		
		//LOGIC IS TO INSERT SAME DATA IN ARRAYLIST AND HASHSET AND THEN ASSERT THE SIZE OF BOTH
		BufferedReader bufferedReader = new BufferedReader(new FileReader(GCP_CSV_file_path));
		bufferedReader.readLine();
		CSVParser parser_GCP = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames().withDelimiter(',')
				.parse(bufferedReader);
		
		ArrayList<String> firstColumnValue_arraylist = new ArrayList<String>();
		HashSet<String> firstColumnValue_hashset = new HashSet<String>();
		for (CSVRecord GCPrecord : parser_GCP) {
			String first_column_value = GCPrecord.get(0);
			firstColumnValue_hashset.add(first_column_value);
			firstColumnValue_arraylist.add(first_column_value);
		}
		bufferedReader.close();
		int distinct_firstColumnValue_hashset_count = firstColumnValue_hashset.size();
		int firstColumnValue_arraylist_count = firstColumnValue_arraylist.size();

		Assert.assertEquals("Duplicate records validation for target GCP Bucket downloaded csv file: ",
				firstColumnValue_arraylist_count, distinct_firstColumnValue_hashset_count);
		System.out.println("Duplicate validation for first column of target CSV file is done for -->"+firstColumnValue_arraylist_count+"<-- records");
	}
	
	@And("^assert inContact CSV file '(.*)' record count with GCP Bucket CSV file '(.*)' record count$")
	public void assert_inContact_record_count_with_GCP_Bucket_CSV_file_record_count(String inContact_CSV_file_path, String GCS_CSV_file_path) throws IOException {
		
		if (inContact_CSV_file_path.startsWith("$$")) {
			inContact_CSV_file_path = HashMapContainer.get(inContact_CSV_file_path);
		}
		//TO GET THE COUNT OF RECORDS PRESENT IN THE CSV FILE
		BufferedReader bufferedReader_incontact = new BufferedReader(new FileReader(inContact_CSV_file_path));
		int CSV_file_record_count = 0;
		while (bufferedReader_incontact.readLine() != null) {
			CSV_file_record_count++;
		}
		CSV_file_record_count = CSV_file_record_count - 1;
		System.out.println("inContact API result CSV file records count : " + CSV_file_record_count);
		bufferedReader_incontact.close();

		if (GCS_CSV_file_path.startsWith("$$")) {
			GCS_CSV_file_path = HashMapContainer.get(GCS_CSV_file_path);
		}
		//TO GET THE COUNT OF RECORDS PRESENT IN THE CSV FILE
		BufferedReader bufferedReader = new BufferedReader(new FileReader(GCS_CSV_file_path));
		int GCS_CSV_file_record_count = 0;
		while ((bufferedReader.readLine()) != null) {
			GCS_CSV_file_record_count++;
		}
		GCS_CSV_file_record_count = GCS_CSV_file_record_count - 1;
		System.out.println("GCS CSV file records count : " + GCS_CSV_file_record_count);
		bufferedReader.close();
		Assert.assertEquals("Validation of BigQuery record count with GCP Bucket file record count: ",CSV_file_record_count, GCS_CSV_file_record_count);
		System.out.println("Records count validation between source and target CSV files is done successfully");
	}
	
	@And("^assert SFTP CSV file '(.*)' record count with GCP Bucket CSV file '(.*)' record count$")
	public void assert_SFTP_CSV_file_record_count_with_GCP_Bucket_CSV_file_record_count(String SFTP_CSV_file_path,
			String GCP_CSV_file_path) throws IOException {

		if (SFTP_CSV_file_path.startsWith("$$")) {
			SFTP_CSV_file_path = HashMapContainer.get(SFTP_CSV_file_path);
		}
		//TO GET THE COUNT OF RECORDS PRESENT IN THE CSV FILE
		BufferedReader bufferedReader_SFTP = new BufferedReader(new FileReader(SFTP_CSV_file_path));
		int SFTP_CSV_file_record_count = 0;
		while (bufferedReader_SFTP.readLine() != null) {
			SFTP_CSV_file_record_count++;
		}
		SFTP_CSV_file_record_count = SFTP_CSV_file_record_count - 2;
		bufferedReader_SFTP.close();

		if (GCP_CSV_file_path.startsWith("$$")) {
			GCP_CSV_file_path = HashMapContainer.get(GCP_CSV_file_path);
		}
		//TO GET THE COUNT OF RECORDS PRESENT IN THE CSV FILE
		BufferedReader bufferedReader_GCP = new BufferedReader(new FileReader(GCP_CSV_file_path));
		int GCP_CSV_file_record_count = 0;
		while ((bufferedReader_GCP.readLine()) != null) {
			GCP_CSV_file_record_count++;
		}
		GCP_CSV_file_record_count = GCP_CSV_file_record_count - 1;
		bufferedReader_GCP.close();
		System.out.println("SFTP CSV file record count: "+SFTP_CSV_file_record_count);
		System.out.println("GCP CSV file record count: "+GCP_CSV_file_record_count);
		Assert.assertEquals("Validation of SFTP record count with GCP Bucket file record count: ",
				SFTP_CSV_file_record_count, GCP_CSV_file_record_count);
	}
	
	@Then("^assert end to end data between source CSV file '(.*)' and target CSV file '(.*)' for Inmoment Returns$")
	public void assert_end_to_end_data_between_source_CSV_file_and_target_CSV_file_for_Inmoment_Returns(String SFTP_source_CSV_file_path,
			String GCP_target_CSV_file_path) throws IOException {
		System.out.println("Initiated End to End assertion between SFTP and GCP CSV files");
		List<Inmoment_Returns_Pojo> inmoment_Returns_Pojo_Arraylist = new ArrayList<Inmoment_Returns_Pojo>();
		
		if (SFTP_source_CSV_file_path.startsWith("$$")) {
			SFTP_source_CSV_file_path = HashMapContainer.get(SFTP_source_CSV_file_path);
		}
		if (GCP_target_CSV_file_path.startsWith("$$")) {
			GCP_target_CSV_file_path = HashMapContainer.get(GCP_target_CSV_file_path);
		}
		
		BufferedReader br_source = new BufferedReader(new FileReader(SFTP_source_CSV_file_path));
		br_source.readLine();
		br_source.readLine();
		
		CSVParser parser_SFTP = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames()
				.withDelimiter('|').parse(br_source);
		
		//TO STORE END TO END DATA PRESENT IN SOURCE CSV FILE INTO POJO
		for (CSVRecord record : parser_SFTP) {
			Inmoment_Returns_Pojo inmoment_Returns_Pojo = new Inmoment_Returns_Pojo();
			inmoment_Returns_Pojo.setDate_of_Service(record.get(0));
			inmoment_Returns_Pojo.setReturn_Order_Number(record.get(1));
			inmoment_Returns_Pojo.setStore_hash(record.get(2));
			inmoment_Returns_Pojo.setLocation(record.get(3));
			inmoment_Returns_Pojo.setReason_for_Return(record.get(4));
			inmoment_Returns_Pojo.setArea_of_Discomfort(record.get(5));
			inmoment_Returns_Pojo.setHow_Damaged(record.get(6));
			inmoment_Returns_Pojo.setDamaged_Mentioned_at_Delivery(record.get(7));
			inmoment_Returns_Pojo.setWas_Product_Sealed(record.get(8));
			inmoment_Returns_Pojo.setComfort_Match_In_Store_Experience(record.get(9));
			inmoment_Returns_Pojo.setComplete_Comfort_Test_Online_Store(record.get(10));
			inmoment_Returns_Pojo.setTry_Product_Pre_Purchase(record.get(11));
			inmoment_Returns_Pojo.setNot_Match_Expectations(record.get(12));
			inmoment_Returns_Pojo.setRecommend_Comment(record.get(13));
			inmoment_Returns_Pojo.setAddress(record.get(14));
			inmoment_Returns_Pojo.setCity(record.get(15));
			inmoment_Returns_Pojo.setState(record.get(16));
			inmoment_Returns_Pojo.setZip_Code(record.get(17));
			inmoment_Returns_Pojo.setStore_Name(record.get(18));
			inmoment_Returns_Pojo.setEmail_Address(record.get(19));
			inmoment_Returns_Pojo.setFirst_Name(record.get(20));
			inmoment_Returns_Pojo.setPhone_Number(record.get(21));
			inmoment_Returns_Pojo.setLast_Name(record.get(22));
			inmoment_Returns_Pojo.setProcessDateTime(record.get(23));
			inmoment_Returns_Pojo.setSales_Person_1(record.get(24));
			inmoment_Returns_Pojo.setSales_Person_2(record.get(25));
			inmoment_Returns_Pojo.setProduct_Name_1(record.get(26));
			inmoment_Returns_Pojo.setProduct_Variant_1(record.get(27));
			inmoment_Returns_Pojo.setItemNum_1(record.get(28));
			inmoment_Returns_Pojo.setProduct_Description_1(record.get(29));
			inmoment_Returns_Pojo.setSize_1(record.get(30));
			inmoment_Returns_Pojo.setQty_1(record.get(31));
			inmoment_Returns_Pojo.setReturn_Price_1(record.get(32));
			inmoment_Returns_Pojo.setProduct_Category_1(record.get(33));
			inmoment_Returns_Pojo.setProduct_Name_2(record.get(34));
			inmoment_Returns_Pojo.setProduct_Variant_2(record.get(35));
			inmoment_Returns_Pojo.setItemNum_2(record.get(36));
			inmoment_Returns_Pojo.setProduct_Description_2(record.get(37));
			inmoment_Returns_Pojo.setSize_2(record.get(38));
			inmoment_Returns_Pojo.setQty_2(record.get(39));
			inmoment_Returns_Pojo.setReturn_Price_2(record.get(40));
			inmoment_Returns_Pojo.setProduct_Category_2(record.get(41));
			inmoment_Returns_Pojo.setProduct_Name_3(record.get(42));
			inmoment_Returns_Pojo.setProduct_Variant_3(record.get(43));
			inmoment_Returns_Pojo.setItemNum_3(record.get(44));
			inmoment_Returns_Pojo.setProduct_Description_3(record.get(45));
			inmoment_Returns_Pojo.setSize_3(record.get(46));
			inmoment_Returns_Pojo.setQty_3(record.get(47));
			inmoment_Returns_Pojo.setReturn_Price_3(record.get(48));
			inmoment_Returns_Pojo.setProduct_Category_3(record.get(49));
			inmoment_Returns_Pojo.setMode(record.get(50));
			inmoment_Returns_Pojo.setResponse_ID(record.get(51));

			inmoment_Returns_Pojo_Arraylist.add(inmoment_Returns_Pojo);
		}
		br_source.close();
		
		BufferedReader br_target = new BufferedReader(new FileReader(GCP_target_CSV_file_path));
	    br_target.readLine();
	    
	    CSVParser parser_GCP = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames()
				.withDelimiter(',').parse(br_target);
	    
	    Collection<String> GCP_response_IDs = new ArrayList<String>();
	    Collection<String> SFTP_response_IDs = new ArrayList<String>();
	    
		for (CSVRecord GCP_record : parser_GCP) {
			
	        String return_Order_Number = GCP_record.get(1);
	        String responseID = GCP_record.get(51);
	        GCP_response_IDs.add(responseID);
	        
	        for (Inmoment_Returns_Pojo y: inmoment_Returns_Pojo_Arraylist) {
	        	SFTP_response_IDs.add(y.getResponse_ID());
	        	
	        	//CONDITION TO CHECK MATCHING RECORD BETWEEN POJO AND TARGET CSV FILE DATA
	        	if(y.getReturn_Order_Number().contains(return_Order_Number) && y.getResponse_ID().contains(responseID)) {
	        		
	        		int listItemIndex = inmoment_Returns_Pojo_Arraylist.indexOf(y);
			        Inmoment_Returns_Pojo arraylist_row = inmoment_Returns_Pojo_Arraylist.get(listItemIndex);
			        	
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Date_of_Service SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getDate_of_Service(), GCP_record.get(0));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Return_Order_Number SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getReturn_Order_Number(), GCP_record.get(1));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Store_hash SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getStore_hash(), GCP_record.get(2));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Location SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getLocation(), GCP_record.get(3));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Reason_for_Return SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getReason_for_Return(), GCP_record.get(4));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Area_of_Discomfort SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getArea_of_Discomfort(), GCP_record.get(5));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> How_Damaged SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getHow_Damaged(), GCP_record.get(6));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Damaged_Mentioned_at_Delivery SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getDamaged_Mentioned_at_Delivery(), GCP_record.get(7));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Was_Product_Sealed SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getWas_Product_Sealed(), GCP_record.get(8));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Comfort_Match_In_Store_Experience SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getComfort_Match_In_Store_Experience(), GCP_record.get(9));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Complete_Comfort_Test_Online_Store SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getComplete_Comfort_Test_Online_Store(), GCP_record.get(10));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Try_Product_Pre_Purchase SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getTry_Product_Pre_Purchase(), GCP_record.get(11));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Not_Match_Expectations SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getNot_Match_Expectations(), GCP_record.get(12));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Recommend_Comment SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getRecommend_Comment(), GCP_record.get(13));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Address SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getAddress(), GCP_record.get(14));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> City SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getCity(), GCP_record.get(15));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> State SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getState(), GCP_record.get(16));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Zip_Code SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getZip_Code(), GCP_record.get(17));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Store_Name SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getStore_Name(), GCP_record.get(18));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Email_Address SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getEmail_Address(), GCP_record.get(19));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> First_Name SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getFirst_Name(), GCP_record.get(20));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Phone_Number SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getPhone_Number(), GCP_record.get(21));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Last_Name SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getLast_Name(), GCP_record.get(22));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> ProcessDateTime SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getProcessDateTime(), GCP_record.get(23));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Sales_Person_1 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getSales_Person_1(), GCP_record.get(24));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Sales_Person_2 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getSales_Person_2(), GCP_record.get(25));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Product_Name_1 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getProduct_Name_1(), GCP_record.get(26));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Product_Variant_1 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getProduct_Variant_1(), GCP_record.get(27));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> ItemNum_1 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getItemNum_1(), GCP_record.get(28));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Product_Description_1 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getProduct_Description_1(), GCP_record.get(29));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Size_1 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getSize_1(), GCP_record.get(30));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Qty_1 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getQty_1(), GCP_record.get(31));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Return_Price_1 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getReturn_Price_1(), GCP_record.get(32));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Product_Category_1 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getProduct_Category_1(), GCP_record.get(33));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Product_Name_2 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getProduct_Name_2(), GCP_record.get(34));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Product_Variant_2 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getProduct_Variant_2(), GCP_record.get(35));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> ItemNum_2 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getItemNum_2(), GCP_record.get(36));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Product_Description_2 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getProduct_Description_2(), GCP_record.get(37));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Size_2 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getSize_2(), GCP_record.get(38));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Qty_2 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getQty_2(), GCP_record.get(39));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Return_Price_2 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getReturn_Price_2(), GCP_record.get(40));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Product_Category_2 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getProduct_Category_2(), GCP_record.get(41));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Product_Name_3 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getProduct_Name_3(), GCP_record.get(42));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Product_Variant_3 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getProduct_Variant_3(), GCP_record.get(43));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> ItemNum_3 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getItemNum_3(), GCP_record.get(44));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Product_Description_3 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getProduct_Description_3(), GCP_record.get(45));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Size_3 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getSize_3(), GCP_record.get(46));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Qty_3 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getQty_3(), GCP_record.get(47));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Return_Price_3 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getReturn_Price_3(), GCP_record.get(48));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Product_Category_3 SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getProduct_Category_3(), GCP_record.get(49));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Mode SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getMode(), GCP_record.get(50));
			        Assert.assertEquals("Return_Order_Number: "+return_Order_Number+" --> Response_ID SFTP to GCP validation for Inmoment Returns: ", arraylist_row.getResponse_ID(), GCP_record.get(51));
			        break;
	        	}
	        }
	    }
		br_target.close();
		
		//CODE FOR GETTING NON MATCHING/NON EXISTING RECORDS IN TARGET
		SFTP_response_IDs.removeAll(GCP_response_IDs);
		HashSet<String> SFTP_response_IDs_hashset = new HashSet<String>();
		SFTP_response_IDs_hashset.addAll(SFTP_response_IDs);
		if (SFTP_response_IDs_hashset.size()>0) {
			System.out.println("CSV comparision failed because target GCP CSV file doesn't contain below response ID's");
			Assert.assertNull(SFTP_response_IDs_hashset);
		}
		System.out.println("End to End assertion between SFTP and GCP CSV files completed successfully without any errors.");
	}
	
	@Given("Connect to BigQuery with project ID {string}")
	public static void connect_with_BigQuery_with_project_ID_and_Key_file(String projectID) throws FileNotFoundException, IOException {
		
		switch(projectID) {
		
			case "qa-mfrm-data":
				bigquery = BigQueryOptions.newBuilder().setProjectId(projectID)
				.setCredentials(ServiceAccountCredentials
				.fromStream(new FileInputStream(System.getProperty("user.dir")+"/src/test/java/com/Resources/svc-devops@qa-mfrm-data.iam.gserviceaccount.com.json")))
//				.fromStream(new FileInputStream(System.getProperty("user.dir")+"/src/test/java/com/Resources/svc-mulesoft@qa-mfrm-data.iam.gserviceaccount.com.json")))
				.build().getService();
				break;
				
			case "dev-mfrm-data":
				bigquery = BigQueryOptions.newBuilder().setProjectId(projectID)
				.setCredentials(ServiceAccountCredentials
				.fromStream(new FileInputStream(System.getProperty("user.dir")+"/src/test/java/com/Resources/svc-devops@dev-mfrm-data.iam.gserviceaccount.com.json")))
				.build().getService();
				break;
				
			case "qa-mfrm-data_Shippo-Tracking-ES-to-BQ":
				bigquery = BigQueryOptions.newBuilder().setProjectId("qa-mfrm-data")
				.setCredentials(ServiceAccountCredentials
//				.fromStream(new FileInputStream(System.getProperty("user.dir")+"/src/test/java/com/Resources/svc-devops@qa-mfrm-data.iam.gserviceaccount.com.json")))
				.fromStream(new FileInputStream(System.getProperty("user.dir")+"/src/test/java/com/Resources/svc-mulesoft@qa-mfrm-data.iam.gserviceaccount.com.json")))
				.build().getService();
				break;
		}
	}
	
	@Then("fetch BigQuery results for Start date {string} using query {string} for InContact")
	public void fetch_BigQuery_records_count_from_query(String BQ_query_date, String base_query) throws InterruptedException {
		
		if (BQ_query_date.startsWith("$$")) {
			BQ_query_date = HashMapContainer.get(BQ_query_date);
		}
		//FRAMING QUERY TO FETCH SINGLE RECORD DETAILS FROM BQ
//		String DateUsedForFetchingBQresults = HashMapContainer.get("$$DateUsedForFetchingBQresults");
		String BQ_query = base_query+" WHERE StartDate = '"+BQ_query_date+"'";

//		String BQ_query = "SELECT DISTINCT sm.StoreID, StoreName, Phone, StoreAddress, City, State, Zip, Latitude, Longitude, DistanceinMeters as Distance, StoreHoursOfOperation as Hours FROM `dev-mfrm-data.mfrm_sales.store_master` sm JOIN `dev-mfrm-data.mfrm_sales.nearest_store_by_zipcode` ns ON sm.StoreID = ns.StoreID WHERE ns.ZipCode = \"11714\" AND SortOrder <= 4";
//		String BQ_query = "select * from `dev-mfrm-data.mfrm_sales.store_master`";
//		String BQ_query = "select * from `qa-mfrm-data.mfrm_reporting_dataset.competitor_product_pricing`";
		System.out.println("BigQuery executed: "+BQ_query);
		QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(BQ_query)
		        .setUseLegacySql(false)
		        .build();
	
		JobId jobId = JobId.of(UUID.randomUUID().toString());
		Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
		queryJob = queryJob.waitFor();

		if (queryJob == null) {
			throw new RuntimeException("Job no longer exists");
		} else if (queryJob.getStatus().getError() != null) {
			throw new RuntimeException(queryJob.getStatus().getError().toString());
		}

		BibQuery_result = queryJob.getQueryResults();
		System.out.println("BigQuery executed successfully");
		
		//To be used in future step definitions
		date_provided_in_BQ_query = BQ_query_date;
		
		//Need to be tested - Writing BQ result to CSV file
//		File file = new File("F:\\Manoj Thota\\Wiser\\BQresults.csv");
//		try {
//			FileWriter outputfile = new FileWriter(file);
//			CSVWriter writer = new CSVWriter(outputfile);
//			writer.writeAll((Iterable<String[]>) BibQuery_result);
//			writer.close();
//		}
//		catch (IOException e) { 
//	        e.printStackTrace(); 
//	    } 
	}
	
	@Then("fetch BigQuery results using query {string}")
	public void fetch_BigQuery_results_using_query(String query) throws InterruptedException {
		
		//EXECUTING BIGQUERY TO FETCH RECORDS FROM BQ
		System.out.println("BigQuery executed: "+query);
		QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(query)
		        .setUseLegacySql(false)
		        .build();
	
		JobId jobId = JobId.of(UUID.randomUUID().toString());
		Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
		queryJob = queryJob.waitFor();

		if (queryJob == null) {
			throw new RuntimeException("Job no longer exists");
		} else if (queryJob.getStatus().getError() != null) {
			throw new RuntimeException(queryJob.getStatus().getError().toString());
		}

		BibQuery_result = queryJob.getQueryResults();
		System.out.println("BigQuery executed successfully");
	}
	
	@Then("^assert ES shippo tracking status data with BQ shippo tracking status stage table$")
	public void assert_ES_shippo_tracking_status_data_with_BQ_shippo_tracking_status_stage_table() {
		
		System.out.println("Initiating assertion between ES shippo staus and BQ shippo status stage table for "+BibQuery_result.getTotalRows()+" records.");
		
		ElasticSearchJestSteps elasticSearchJestSteps = new ElasticSearchJestSteps();
		int ES_records_size = elasticSearchJestSteps.shippo_tracking_ES_to_BQ_Pojo_ArrayList.size();
		System.out.println("ES_records_size: "+ES_records_size);
		System.out.println("BibQuery_result size: "+BibQuery_result.getTotalRows());
		for (Shippo_tracking_ES_to_BQ_Pojo ES_record: elasticSearchJestSteps.shippo_tracking_ES_to_BQ_Pojo_ArrayList) {
			for(FieldValueList row : BibQuery_result.iterateAll()) {
				System.out.println("ES Tracking number: "+ES_record.getTracking_number());
				System.out.println("BQ Trackign number: "+row.get("TrackingNumber").getStringValue());
				if (ES_record.getTracking_number().equals(row.get("TrackingNumber").getStringValue())) {
					System.out.println("Asserting end to end data for the tracking number: "+ES_record.getTracking_number());
					Object trackingNumber = row.get("TrackingNumber").getValue();
					if (trackingNumber==null)
						trackingNumber = "";
					Assert.assertEquals("Asserting Tracking Number for the trackingNumber: "+trackingNumber+" --> ", ES_record.getTracking_number(), trackingNumber);
					Object carrier = row.get("Carrier").getValue();
					if (carrier==null)
						carrier = "";
					Assert.assertEquals("Asserting Carrier for the trackingNumber: "+trackingNumber+" --> ", ES_record.getCarrier(), carrier);
					Object serviceLevelToken = row.get("ServiceLevelToken").getValue();
					if (serviceLevelToken==null)
						serviceLevelToken = "";
					Assert.assertEquals("Asserting ServiceLevelToken for the trackingNumber: "+trackingNumber+" --> ", ES_record.getService_level_token(), serviceLevelToken);
					Object serviceLevelName = row.get("ServiceLevelName").getValue();
					if (serviceLevelName==null)
						serviceLevelName = "";
					Assert.assertEquals("Asserting ServiceLevelName for the trackingNumber: "+trackingNumber+" --> ", ES_record.getService_level_name(), serviceLevelName);
					Object addressFromCity = row.get("AddressFromCity").getValue();
//					if (addressFromCity==null)
//						addressFromCity = "";
					Assert.assertEquals("Asserting AddressFromCity for the trackingNumber: "+trackingNumber+" --> ", ES_record.getAddress_from_city(), addressFromCity);
					Object addressFromState = row.get("AddressFromState").getValue();
//					if (addressFromState==null)
//						addressFromState = "";
					Assert.assertEquals("Asserting AddressFromState for the trackingNumber: "+trackingNumber+" --> ", ES_record.getAddress_from_state(), addressFromState);
					
					Object addressFromZip = row.get("AddressFromZip").getValue();
//					System.out.println("addressFromZip "+addressFromZip);
//					if (addressFromZip==null)
//						addressFromZip = "";
//					System.out.println(ES_record.getAddress_from_zip());
//					System.out.println("addressFromZip after "+addressFromZip);
					
//					Assert.assertEquals("Asserting AddressFromZip for the trackingNumber: "+trackingNumber+" --> ", ES_record.getAddress_from_zip(), addressFromZip);

					Object addressFromCountry = row.get("AddressFromCountry").getValue();
//					if (addressFromCountry==null)
//						addressFromCountry = "";
					Assert.assertEquals("Asserting AddressFromCountry for the trackingNumber: "+trackingNumber+" --> ", ES_record.getAddress_from_country(), addressFromCountry);
					Object addressToCity = row.get("AddressToCity").getValue();
//					if (addressToCity==null)
//						addressToCity = "";
					Assert.assertEquals("Asserting AddressToCity for the trackingNumber: "+trackingNumber+" --> ", ES_record.getAddress_to_city(), addressToCity);
					Object addressToState = row.get("AddressToState").getValue();
//					if (addressToState==null)
//						addressToState = "";
					Assert.assertEquals("Asserting AddressToState for the trackingNumber: "+trackingNumber+" --> ", ES_record.getAddress_to_state(), addressToState);
					Object addressToZip = row.get("AddressToZip").getValue();
//					if (addressToZip==null)
//						addressToZip = "";
					Assert.assertEquals("Asserting AddressToZip for the trackingNumber: "+trackingNumber+" --> ", ES_record.getAddress_to_zip(), addressToZip);
					Object addressToCountry = row.get("AddressToCountry").getValue();
//					if (addressToCountry==null)
//						addressToCountry = "";
					Assert.assertEquals("Asserting AddressToCountry for the trackingNumber: "+trackingNumber+" --> ", ES_record.getAddress_to_country(), addressToCountry);
					Object eTA = ES_record.getEta();
//					System.out.println("eTA before "+eTA);
					if (eTA==null) {
//						eTA = "";
//						System.out.println("eTA after "+eTA);
						Assert.assertEquals("Asserting ETA for the trackingNumber: "+trackingNumber+" --> ", eTA, row.get("ETA").getValue());
					}else {
						Date eTA_modified = null;
						String eTA_final = null;
						SimpleDateFormat source_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				        try {
				        	eTA_modified = source_format.parse(eTA.toString());
				        	SimpleDateFormat target_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				        	eTA_final = target_format.format(eTA_modified);
						} catch (ParseException e) {
							e.printStackTrace();
						}
//				        System.out.println("eTA_modified after "+eTA_final);
						Assert.assertEquals("Asserting ETA for the trackingNumber: "+trackingNumber+" --> ", eTA_final, row.get("ETA").getValue());
					}
					
					Object originalETA = ES_record.getOriginal_eta();
//					System.out.println("originalETA before "+originalETA);
					if (originalETA==null) {
						originalETA = "";
//						System.out.println("originalETA after "+originalETA);
						Assert.assertEquals("Asserting OriginalETA for the trackingNumber: "+trackingNumber+" --> ", originalETA, row.get("OriginalETA").getValue());
					}else {
						Date originalETA_modified = null;
						String originalETA_final = null;
						SimpleDateFormat source_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				        try {
				        	originalETA_modified = source_format.parse(originalETA.toString());
				        	SimpleDateFormat target_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				        	originalETA_final = target_format.format(originalETA_modified);
						} catch (ParseException e) {
							e.printStackTrace();
						}
//				        System.out.println("originalETA_modified after "+originalETA_final);
						Assert.assertEquals("Asserting ETA for the trackingNumber: "+trackingNumber+" --> ", originalETA_final, row.get("OriginalETA").getValue());
					}
					
					Object statusDate = ES_record.getStatus_date();
//					System.out.println("statusDate before "+statusDate);
					if (statusDate==null) {
						statusDate = "";
//						System.out.println("statusDate after "+statusDate);
						Assert.assertEquals("Asserting StatusDate for the trackingNumber: "+trackingNumber+" --> ", statusDate, row.get("StatusDate").getValue());
					}else {
						Date statusDate_modified = null;
						String statusDate_final = null;
						SimpleDateFormat source_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				        try {
				        	statusDate_modified = source_format.parse(statusDate.toString());
				        	SimpleDateFormat target_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				        	statusDate_final = target_format.format(statusDate_modified);
						} catch (ParseException e) {
							e.printStackTrace();
						}
//				        System.out.println("statusDate_modified after "+statusDate_final);
						Assert.assertEquals("Asserting StatusDate for the trackingNumber: "+trackingNumber+" --> ", statusDate_final, row.get("StatusDate").getValue());
					}
					
					Object statusDetails = row.get("StatusDetails").getValue();
					if (statusDetails==null)
						statusDetails = "";
					Assert.assertEquals("Asserting StatusDetails for the trackingNumber: "+trackingNumber+" --> ", ES_record.getStatus_details(), statusDetails);
					Object locationCity = row.get("LocationCity").getValue();
//					if (locationCity==null)
//						locationCity = "";
					Assert.assertEquals("Asserting LocationCity for the trackingNumber: "+trackingNumber+" --> ", ES_record.getLocation_city(), locationCity);
					Object locationState = row.get("LocationState").getValue();
//					if (locationState==null)
//						locationState = "";
					Assert.assertEquals("Asserting LocationState for the trackingNumber: "+trackingNumber+" --> ", ES_record.getLocation_state(), locationState);
//					Object locationZip = row.get("LocationZip").getValue();
//					if (locationZip==null)
//						locationZip = "";
//					Assert.assertEquals("Asserting LocationZip for the trackingNumber: "+trackingNumber+" --> ", ES_record.getLocation_zip(), locationZip);
					Object locationCountry = row.get("LocationCountry").getValue();
//					if (locationCountry==null)
//						locationCountry = "";
					Assert.assertEquals("Asserting LocationCountry for the trackingNumber: "+trackingNumber+" --> ", ES_record.getLocation_country(), locationCountry);
					Object subStatusCode = row.get("SubStatusCode").getValue();
					if (subStatusCode==null)
						subStatusCode = "";
					Assert.assertEquals("Asserting SubStatusCode for the trackingNumber: "+trackingNumber+" --> ", ES_record.getSubstatus_code(), subStatusCode);
					Object subStatusText = row.get("SubStatusText").getValue();
					if (subStatusText==null)
						subStatusText = "";
					Assert.assertEquals("Asserting SubStatusText for the trackingNumber: "+trackingNumber+" --> ", ES_record.getSubstatus_text(), subStatusText);
					Object subStatusActionRequired = row.get("SubStatusActionRequired").getValue();
					if (subStatusActionRequired==null)
						subStatusActionRequired = "";
					Assert.assertEquals("Asserting SubStatusActionRequired for the trackingNumber: "+trackingNumber+" --> ", ES_record.getSubstatus_action_required().toString(), subStatusActionRequired);
					
					Object objectCreatedDateBQ = row.get("ObjectCreatedDate").getValue();
					if (objectCreatedDateBQ==null)
						objectCreatedDateBQ = "";
					Object objectCreatedDateES = ES_record.getObject_created_date();
//					System.out.println("objectCreatedDateES before "+objectCreatedDateES);
					if (objectCreatedDateES==null) {
						objectCreatedDateES = "";
//						System.out.println("objectCreatedDateES after "+objectCreatedDateES);
						Assert.assertEquals("Asserting ObjectCreatedDate for the trackingNumber: "+trackingNumber+" --> ", objectCreatedDateES, objectCreatedDateBQ);
					}else {
						int ESrecordSize = objectCreatedDateES.toString().length();
//						System.out.println("ESrecordSize "+ESrecordSize);
						if(ESrecordSize<=20) {
							
						}else if(ESrecordSize<=21) {
							objectCreatedDateES = objectCreatedDateES.toString().substring(0, 20)+"000000";
						}else if(ESrecordSize<=22) {
							objectCreatedDateES = objectCreatedDateES.toString().substring(0, 21)+"00000";
						}else if(ESrecordSize<=23) {
							objectCreatedDateES = objectCreatedDateES.toString().substring(0, 22)+"0000";
						}else if(ESrecordSize<=24) {
							objectCreatedDateES = objectCreatedDateES.toString().substring(0, 23)+"000";
						}else if(ESrecordSize<=25) {
							objectCreatedDateES = objectCreatedDateES.toString().substring(0, 24)+"00";
						}else if(ESrecordSize<=26) {
							objectCreatedDateES = objectCreatedDateES.toString().substring(0, 25)+"0";
						}
//						System.out.println("objectCreatedDateES after "+objectCreatedDateES);
						Assert.assertEquals("Asserting ObjectCreatedDate for the trackingNumber: "+trackingNumber+" --> ", objectCreatedDateES, objectCreatedDateBQ);
					}
					
					Object objectUpdatedDateBQ = row.get("ObjectUpdatedDate").getValue();
					if (objectUpdatedDateBQ==null)
						objectUpdatedDateBQ = "";
					Object objectUpdatedDateES = ES_record.getObject_updated_date();
//					System.out.println("objectUpdatedDateES before "+objectUpdatedDateES);
					if (objectUpdatedDateES==null) {
						objectUpdatedDateES = "";
//						System.out.println("objectUpdatedDateES after "+objectUpdatedDateES);
						Assert.assertEquals("Asserting ObjectUpdatedDate for the trackingNumber: "+trackingNumber+" --> ", objectUpdatedDateES, objectUpdatedDateBQ);
					}else {
						int ESrecordSize = objectUpdatedDateES.toString().length();
//						System.out.println("ESrecordSize "+ESrecordSize);
						if(ESrecordSize<=20) {
							
						}else if(ESrecordSize<=21) {
							objectUpdatedDateES = objectUpdatedDateES.toString().substring(0, 20)+"000000";
						}else if(ESrecordSize<=22) {
							objectUpdatedDateES = objectUpdatedDateES.toString().substring(0, 21)+"00000";
						}else if(ESrecordSize<=23) {
							objectUpdatedDateES = objectUpdatedDateES.toString().substring(0, 22)+"0000";
						}else if(ESrecordSize<=24) {
							objectUpdatedDateES = objectUpdatedDateES.toString().substring(0, 23)+"000";
						}else if(ESrecordSize<=25) {
							objectUpdatedDateES = objectUpdatedDateES.toString().substring(0, 24)+"00";
						}else if(ESrecordSize<=26) {
							objectUpdatedDateES = objectUpdatedDateES.toString().substring(0, 25)+"0";
						}
//						System.out.println("objectUpdatedDateES after "+objectUpdatedDateES);
						Assert.assertEquals("Asserting ObjectUpdatedDate for the trackingNumber: "+trackingNumber+" --> ", objectUpdatedDateES, objectUpdatedDateBQ);
					}
					
					Object objectID = row.get("ObjectID").getValue();
					if (objectID==null)
						objectID = "";
					Assert.assertEquals("Asserting ObjectID for the trackingNumber: "+trackingNumber+" --> ", ES_record.getObject_id(), objectID);
					Object status = row.get("Status").getValue();
					if (status==null)
						status = "";
					Assert.assertEquals("Asserting Status for the trackingNumber: "+trackingNumber+" --> ", ES_record.getStatus(), status);
					Object createdBy = row.get("CreatedBy").getValue();
					if (createdBy==null)
						createdBy = "";
					Assert.assertEquals("Asserting CreatedBy for the trackingNumber: "+trackingNumber+" --> ", ES_record.getCreatedBy(), createdBy);
					Object modifiedBy = row.get("ModifiedBy").getValue();
					if (modifiedBy==null)
						modifiedBy = "";
					Assert.assertEquals("Asserting ModifiedBy for the trackingNumber: "+trackingNumber+" --> ", ES_record.getModifiedBy(), modifiedBy);
					
					Object eSModifiedDateTimeBQ = row.get("ESModifiedDateTime").getValue();
//					System.out.println("eSModifiedDateTimeBQ before "+eSModifiedDateTimeBQ);
					if (eSModifiedDateTimeBQ==null)
						eSModifiedDateTimeBQ = "";
//					System.out.println("eSModifiedDateTimeBQ after "+eSModifiedDateTimeBQ);
					Object eSModifiedDateTimeES = ES_record.getES_MODIFIEDDATETIME();
//					System.out.println("eSModifiedDateTime before "+eSModifiedDateTimeES);
					Date eSModifiedDateTime_modified = null;
					String eSModifiedDateTime_final = null;
					SimpleDateFormat source_format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			        try {
			        	eSModifiedDateTime_modified = source_format.parse(eSModifiedDateTimeES.toString());
			        	SimpleDateFormat target_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			        	eSModifiedDateTime_final = target_format.format(eSModifiedDateTime_modified);
					} catch (ParseException e) {
						e.printStackTrace();
					}
//			        System.out.println("eSModifiedDateTime after "+eSModifiedDateTime_final);
					Assert.assertEquals("Asserting ESModifiedDateTime for the trackingNumber: "+trackingNumber+" --> ", eSModifiedDateTime_final, eSModifiedDateTimeBQ);
					
					
					Object createdDateTimeBQ = row.get("CreatedDateTime").getValue();
//					System.out.println("createdDateTimeBQ before "+createdDateTimeBQ);
					if (createdDateTimeBQ==null)
						createdDateTimeBQ = "";
//					System.out.println("createdDateTimeBQ after "+createdDateTimeBQ);
					Object createdDateTimeES = ES_record.getCreatedDateTime();
//					System.out.println("createdDateTimeES before "+createdDateTimeES);
					Date createdDateTimeES_modified = null;
					String createdDateTimeES_final = null;
					SimpleDateFormat createdDateTimeES_source_format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			        try {
			        	createdDateTimeES_modified = createdDateTimeES_source_format.parse(createdDateTimeES.toString());
			        	SimpleDateFormat target_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			        	createdDateTimeES_final = target_format.format(createdDateTimeES_modified);
					} catch (ParseException e) {
						e.printStackTrace();
					}
//			        System.out.println("createdDateTimeES after "+createdDateTimeES_final);
					Assert.assertEquals("Asserting CreatedDateTime for the trackingNumber: "+trackingNumber+" --> ", createdDateTimeES_final, createdDateTimeBQ);
					
					
					Object modifiedDateTimeBQ = row.get("ModifiedDateTime").getValue();
//					System.out.println("modifiedDateTimeBQ before "+modifiedDateTimeBQ);
					if (modifiedDateTimeBQ==null)
						modifiedDateTimeBQ = "";
//					System.out.println("modifiedDateTimeBQ after "+modifiedDateTimeBQ);
					Object modifiedDateTimeES = ES_record.getModifiedDateTime();
//					System.out.println("modifiedDateTimeES before "+modifiedDateTimeES);
					Date modifiedDateTimeES_modified = null;
					String modifiedDateTimeES_final = null;
					SimpleDateFormat modifiedDateTime_source_format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			        try {
			        	modifiedDateTimeES_modified = modifiedDateTime_source_format.parse(modifiedDateTimeES.toString());
			        	SimpleDateFormat target_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			        	modifiedDateTimeES_final = target_format.format(modifiedDateTimeES_modified);
					} catch (ParseException e) {
						e.printStackTrace();
					}
//			        System.out.println("modifiedDateTimeES after "+modifiedDateTimeES_final);
					Assert.assertEquals("Asserting ModifiedDateTime for the trackingNumber: "+trackingNumber+" --> ", modifiedDateTimeES_final, modifiedDateTimeBQ);
					break;
				}
			}
		}
	}
	
	@Then("^assert BQ shippo tracking status stage table data with status master table$")
	public void assert_BQ_shippo_tracking_status_stage_table_data_with_status_master_table() throws InterruptedException {
		
		List<Shippo_tracking_ES_to_BQ_Pojo> shippo_track_ES_to_BQ_Pojo_AL = new ArrayList<Shippo_tracking_ES_to_BQ_Pojo>();
		
		for(FieldValueList row : BibQuery_result.iterateAll()) {
			
			Shippo_tracking_ES_to_BQ_Pojo shippo_track_ES_to_BQ_Pojo = new Shippo_tracking_ES_to_BQ_Pojo();
			
			shippo_track_ES_to_BQ_Pojo.setES_MODIFIEDDATETIME(row.get("ESModifiedDateTime").getValue().toString());
			if (row.get("AddressFromCity").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setAddress_from_city("");
			else
				shippo_track_ES_to_BQ_Pojo.setAddress_from_city(row.get("AddressFromCity").getValue().toString());
			if (row.get("AddressFromCountry").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setAddress_from_country("");
			else
				shippo_track_ES_to_BQ_Pojo.setAddress_from_country(row.get("AddressFromCountry").getValue().toString());
			if (row.get("AddressFromState").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setAddress_from_state("");
			else
				shippo_track_ES_to_BQ_Pojo.setAddress_from_state(row.get("AddressFromState").getValue().toString());
			if (row.get("AddressFromZip").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setAddress_from_zip("");
			else 
				shippo_track_ES_to_BQ_Pojo.setAddress_from_zip(row.get("AddressFromZip").getValue().toString());
			if (row.get("AddressToCity").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setAddress_to_city("");
			else
				shippo_track_ES_to_BQ_Pojo.setAddress_to_city(row.get("AddressToCity").getValue().toString());
			if (row.get("AddressToCountry").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setAddress_to_country("");
			else
				shippo_track_ES_to_BQ_Pojo.setAddress_to_country(row.get("AddressToCountry").getValue().toString());
			if (row.get("AddressToState").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setAddress_to_state("");
			else
				shippo_track_ES_to_BQ_Pojo.setAddress_to_state(row.get("AddressToState").getValue().toString());
			if (row.get("AddressToZip").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setAddress_to_zip("");
			else
				shippo_track_ES_to_BQ_Pojo.setAddress_to_zip(row.get("AddressToZip").getValue().toString());
			if (row.get("Carrier").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setCarrier("");
			else
				shippo_track_ES_to_BQ_Pojo.setCarrier(row.get("Carrier").getValue().toString());
			if (row.get("ETA").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setEta("");
			else
				shippo_track_ES_to_BQ_Pojo.setEta(row.get("ETA").getValue().toString());
			if (row.get("LocationCity").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setLocation_city("");
			else
				shippo_track_ES_to_BQ_Pojo.setLocation_city(row.get("LocationCity").getValue().toString());
			if (row.get("LocationCountry").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setLocation_country("");
			else
				shippo_track_ES_to_BQ_Pojo.setLocation_country(row.get("LocationCountry").getValue().toString());
			if (row.get("LocationState").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setLocation_state("");
			else
				shippo_track_ES_to_BQ_Pojo.setLocation_state(row.get("LocationState").getValue().toString());
			if (row.get("LocationZip").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setLocation_zip("");
			else
				shippo_track_ES_to_BQ_Pojo.setLocation_zip(row.get("LocationZip").getValue().toString());
			if (row.get("ObjectCreatedDate").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setObject_created_date("");
			else
				shippo_track_ES_to_BQ_Pojo.setObject_created_date(row.get("ObjectCreatedDate").getValue().toString());
			if (row.get("ObjectID").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setObject_id("");
			else
				shippo_track_ES_to_BQ_Pojo.setObject_id(row.get("ObjectID").getValue().toString());
			if (row.get("ObjectUpdatedDate").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setObject_updated_date("");
			else
				shippo_track_ES_to_BQ_Pojo.setObject_updated_date(row.get("ObjectUpdatedDate").getValue().toString());
			if (row.get("OriginalETA").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setOriginal_eta("");
			else
				shippo_track_ES_to_BQ_Pojo.setOriginal_eta(row.get("OriginalETA").getValue().toString());
			if (row.get("ServiceLevelName").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setService_level_name("");
			else
				shippo_track_ES_to_BQ_Pojo.setService_level_name(row.get("ServiceLevelName").getValue().toString());
			if (row.get("ServiceLevelToken").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setService_level_token("");
			else
				shippo_track_ES_to_BQ_Pojo.setService_level_token(row.get("ServiceLevelToken").getValue().toString());
			if (row.get("Status").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setStatus("");
			else
				shippo_track_ES_to_BQ_Pojo.setStatus(row.get("Status").getValue().toString());
			if (row.get("StatusDate").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setStatus_date("");
			else
				shippo_track_ES_to_BQ_Pojo.setStatus_date(row.get("StatusDate").getValue().toString());
			if (row.get("StatusDetails").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setStatus_details("");
			else
				shippo_track_ES_to_BQ_Pojo.setStatus_details(row.get("StatusDetails").getValue().toString());
			shippo_track_ES_to_BQ_Pojo.setSubstatus_action_required(row.get("SubStatusActionRequired").getBooleanValue());
			if (row.get("SubStatusCode").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setSubstatus_code("");
			else
				shippo_track_ES_to_BQ_Pojo.setSubstatus_code(row.get("SubStatusCode").getValue().toString());
			if (row.get("SubStatusText").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setSubstatus_text("");
			else
				shippo_track_ES_to_BQ_Pojo.setSubstatus_text(row.get("SubStatusText").getValue().toString());
			if (row.get("TrackingNumber").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setTracking_number("");
			else
				shippo_track_ES_to_BQ_Pojo.setTracking_number(row.get("TrackingNumber").getValue().toString());
			if (row.get("ModifiedBy").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setModifiedBy("");
			else
				shippo_track_ES_to_BQ_Pojo.setModifiedBy(row.get("ModifiedBy").getValue().toString());
			if (row.get("ModifiedDateTime").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setModifiedDateTime("");
			else
				shippo_track_ES_to_BQ_Pojo.setModifiedDateTime(row.get("ModifiedDateTime").getValue().toString());
			if (row.get("CreatedBy").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setCreatedBy("");
			else
				shippo_track_ES_to_BQ_Pojo.setCreatedBy(row.get("CreatedBy").getValue().toString());
			if (row.get("CreatedDateTime").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setCreatedDateTime("");
			else
				shippo_track_ES_to_BQ_Pojo.setCreatedDateTime(row.get("CreatedDateTime").getValue().toString());
			
			shippo_track_ES_to_BQ_Pojo_AL.add(shippo_track_ES_to_BQ_Pojo);
		}
		
		System.out.println("shippo_track_ES_to_BQ_Pojo_AL size: "+shippo_track_ES_to_BQ_Pojo_AL.size());
		
		for (Shippo_tracking_ES_to_BQ_Pojo BQ_stg_row: shippo_track_ES_to_BQ_Pojo_AL) {
			
			String query = "SELECT * FROM `qa-mfrm-data.mfrm_reporting_dataset.shippo_tracking_status` WHERE TrackingNumber = '"+BQ_stg_row.getTracking_number()+"'";
			System.out.println("BigQuery executed: "+query);
			QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(query)
			        .setUseLegacySql(false)
			        .build();
		
			JobId jobId = JobId.of(UUID.randomUUID().toString());
			Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
			queryJob = queryJob.waitFor();

			if (queryJob == null) {
				throw new RuntimeException("Job no longer exists");
			} else if (queryJob.getStatus().getError() != null) {
				throw new RuntimeException(queryJob.getStatus().getError().toString());
			}

			BibQuery_result = queryJob.getQueryResults();
			
			for(FieldValueList BQ_master_row : BibQuery_result.iterateAll()) {
				
				Object eSModifiedDateTime = BQ_master_row.get("ESModifiedDateTime").getValue();
				if (eSModifiedDateTime==null)
					eSModifiedDateTime = "";
				Assert.assertEquals("Assert ESModifiedDateTime for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getES_MODIFIEDDATETIME(), eSModifiedDateTime);
				Object addressFromCity = BQ_master_row.get("AddressFromCity").getValue();
				if (addressFromCity==null)
					addressFromCity = "";
				Assert.assertEquals("Assert AddressFromCity for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getAddress_from_city(), addressFromCity);
				Object addressFromCountry = BQ_master_row.get("AddressFromCountry").getValue();
				if (addressFromCountry==null)
					addressFromCountry = "";
				Assert.assertEquals("Assert AddressFromCountry for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getAddress_from_country(), addressFromCountry);
				Object addressFromState = BQ_master_row.get("AddressFromState").getValue();
				if (addressFromState==null)
					addressFromState = "";
				Assert.assertEquals("Assert AddressFromState for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getAddress_from_state(), addressFromState);
				Object addressFromZip = BQ_master_row.get("AddressFromZip").getValue();
				if (addressFromZip==null)
					addressFromZip = "";
				Assert.assertEquals("Assert AddressFromZip for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getAddress_from_zip(), addressFromZip);
				Object addressToCity = BQ_master_row.get("AddressToCity").getValue();
				if (addressToCity==null)
					addressToCity = "";
				Assert.assertEquals("Assert AddressToCity for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getAddress_to_city(), addressToCity);
				Object addressToCountry = BQ_master_row.get("AddressToCountry").getValue();
				if (addressToCountry==null)
					addressToCountry = "";
				Assert.assertEquals("Assert AddressToCountry for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getAddress_to_country(), addressToCountry);
				Object addressToState = BQ_master_row.get("AddressToState").getValue();
				if (addressToState==null)
					addressToState = "";
				Assert.assertEquals("Assert AddressToState for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getAddress_to_state(), addressToState);
				Object addressToZip = BQ_master_row.get("AddressToZip").getValue();
				if (addressToZip==null)
					addressToZip = "";
				Assert.assertEquals("Assert AddressToZip for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getAddress_to_zip(), addressToZip);
				Object carrier = BQ_master_row.get("Carrier").getValue();
				if (carrier==null)
					carrier = "";
				Assert.assertEquals("Assert Carrier for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getCarrier(), carrier);
				Object eTA = BQ_master_row.get("ETA").getValue();
				if (eTA==null)
					eTA = "";
				Assert.assertEquals("Assert ETA for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getEta(), eTA);
				Object locationCity = BQ_master_row.get("LocationCity").getValue();
				if (locationCity==null)
					locationCity = "";
				Assert.assertEquals("Assert LocationCity for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getLocation_city(), locationCity);
				Object locationCountry = BQ_master_row.get("LocationCountry").getValue();
				if (locationCountry==null)
					locationCountry = "";
				Assert.assertEquals("Assert LocationCountry for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getLocation_country(), locationCountry);
				Object locationState = BQ_master_row.get("LocationState").getValue();
				if (locationState==null)
					locationState = "";
				Assert.assertEquals("Assert LocationState for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getLocation_state(), locationState);
				Object locationZip = BQ_master_row.get("LocationZip").getValue();
				if (locationZip==null)
					locationZip = "";
				Assert.assertEquals("Assert LocationZip for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getLocation_zip(), locationZip);
				Object objectCreatedDate = BQ_master_row.get("ObjectCreatedDate").getValue();
				if (objectCreatedDate==null)
					objectCreatedDate = "";
				Assert.assertEquals("Assert ObjectCreatedDate for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getObject_created_date(), objectCreatedDate);
				Object objectID = BQ_master_row.get("ObjectID").getValue();
				if (objectID==null)
					objectID = "";
				Assert.assertEquals("Assert ObjectID for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getObject_id(), objectID);
				Object objectUpdatedDate = BQ_master_row.get("ObjectUpdatedDate").getValue();
				if (objectUpdatedDate==null)
					objectUpdatedDate = "";
				Assert.assertEquals("Assert ObjectUpdatedDate for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getObject_updated_date(), objectUpdatedDate);
				Object originalETA = BQ_master_row.get("OriginalETA").getValue();
				if (originalETA==null)
					originalETA = "";
				Assert.assertEquals("Assert OriginalETA for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getOriginal_eta(), originalETA);
				Object serviceLevelName = BQ_master_row.get("ServiceLevelName").getValue();
				if (serviceLevelName==null)
					serviceLevelName = "";
				Assert.assertEquals("Assert ServiceLevelName for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getService_level_name(), serviceLevelName);
				Object serviceLevelToken = BQ_master_row.get("ServiceLevelToken").getValue();
				if (serviceLevelToken==null)
					serviceLevelToken = "";
				Assert.assertEquals("Assert ServiceLevelToken for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getService_level_token(), serviceLevelToken);
				Object status = BQ_master_row.get("Status").getValue();
				if (status==null)
					status = "";
				Assert.assertEquals("Assert Status for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getStatus(), status);
				Object statusDate = BQ_master_row.get("StatusDate").getValue();
				if (statusDate==null)
					statusDate = "";
				Assert.assertEquals("Assert StatusDate for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getStatus_date(), statusDate);
				Object statusDetails = BQ_master_row.get("StatusDetails").getValue();
				if (statusDetails==null)
					statusDetails = "";
				Assert.assertEquals("Assert StatusDetails for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getStatus_details(), statusDetails);
				Object subStatusActionRequired = BQ_master_row.get("SubStatusActionRequired").getValue();
				if (subStatusActionRequired==null)
					subStatusActionRequired = "";
				Assert.assertEquals("Assert SubStatusActionRequired for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getSubstatus_action_required().toString(), subStatusActionRequired);
				Object subStatusCode = BQ_master_row.get("SubStatusCode").getValue();
				if (subStatusCode==null)
					subStatusCode = "";
				Assert.assertEquals("Assert SubStatusCode for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getSubstatus_code(), subStatusCode);
				Object subStatusText = BQ_master_row.get("SubStatusText").getValue();
				if (subStatusText==null)
					subStatusText = "";
				Assert.assertEquals("Assert SubStatusText for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getSubstatus_text(), subStatusText);
				Object trackingNumber = BQ_master_row.get("TrackingNumber").getValue();
				if (trackingNumber==null)
					trackingNumber = "";
				Assert.assertEquals("Assert TrackingNumber for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getTracking_number(), trackingNumber);
				Object modifiedBy = BQ_master_row.get("ModifiedBy").getValue();
				if (modifiedBy==null)
					modifiedBy = "";
				Assert.assertEquals("Assert ModifiedBy for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getModifiedBy(), modifiedBy);
				Object modifiedDateTime = BQ_master_row.get("ModifiedDateTime").getValue();
				if (modifiedDateTime==null)
					modifiedDateTime = "";
				Assert.assertEquals("Assert ModifiedDateTime for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getModifiedDateTime(), modifiedDateTime);
				Object createdBy = BQ_master_row.get("CreatedBy").getValue();
				if (createdBy==null)
					createdBy = "";
				Assert.assertEquals("Assert CreatedBy for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getCreatedBy(), createdBy);
				Object createdDateTime = BQ_master_row.get("CreatedDateTime").getValue();
				if (createdDateTime==null)
					createdDateTime = "";
				Assert.assertEquals("Assert CreatedDateTime for tracking number: "+BQ_stg_row.getTracking_number(), BQ_stg_row.getCreatedDateTime(), createdDateTime);
			}
		}
	}
	
	@Then("^assert BQ shippo tracking status stage table tracking numbers count with history stage table tracking numbers count$")
	public void assert_BQ_shippo_status_stage_tracking_numbers_count_with_history_stage_table_tracking_numbers_count() throws InterruptedException {
		
		QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder("SELECT count(distinct(TrackingNumber)) as StatusStgCount FROM `qa-mfrm-data.mfrm_staging_dataset.shippo_tracking_status_stg`")
		        .setUseLegacySql(false)
		        .build();
	
		JobId jobId = JobId.of(UUID.randomUUID().toString());
		Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
		queryJob = queryJob.waitFor();

		if (queryJob == null) {
			throw new RuntimeException("Job no longer exists");
		} else if (queryJob.getStatus().getError() != null) {
			throw new RuntimeException(queryJob.getStatus().getError().toString());
		}

		BibQuery_result = queryJob.getQueryResults();
		String noOfRecordsInStg_Status = null;
		for (FieldValueList row : BibQuery_result.iterateAll()) {
			noOfRecordsInStg_Status = row.get("StatusStgCount").getStringValue();
		}
		
		QueryJobConfiguration queryConfigg = QueryJobConfiguration.newBuilder("SELECT count(distinct(TrackingNumber)) as HistoryStgCount FROM `qa-mfrm-data.mfrm_staging_dataset.shippo_tracking_history_stg`")
		        .setUseLegacySql(false)
		        .build();
	
		JobId jobIdd = JobId.of(UUID.randomUUID().toString());
		Job queryJobb = bigquery.create(JobInfo.newBuilder(queryConfigg).setJobId(jobIdd).build());
		queryJobb = queryJobb.waitFor();

		if (queryJobb == null) {
			throw new RuntimeException("Job no longer exists");
		} else if (queryJobb.getStatus().getError() != null) {
			throw new RuntimeException(queryJobb.getStatus().getError().toString());
		}

		BibQuery_result = queryJobb.getQueryResults();
		String noOfRecordsInStg_History = null;
		for (FieldValueList row : BibQuery_result.iterateAll()) {
			noOfRecordsInStg_History = row.get("HistoryStgCount").getStringValue();
		}
		Assert.assertEquals("Asserting count of tracking numbers between BQ status stg and history stg tables: ", noOfRecordsInStg_Status, noOfRecordsInStg_History);
	}

	
	@Then("^assert BQ shippo tracking history stage table data with history master table$")
	public void assert_BQ_shippo_tracking_history_stage_table_data_with_history_master_table() throws InterruptedException {
		
		QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder("SELECT * FROM `qa-mfrm-data.mfrm_staging_dataset.shippo_tracking_history_stg`")
		        .setUseLegacySql(false)
		        .build();
	
		JobId jobId = JobId.of(UUID.randomUUID().toString());
		Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
		queryJob = queryJob.waitFor();

		if (queryJob == null) {
			throw new RuntimeException("Job no longer exists");
		} else if (queryJob.getStatus().getError() != null) {
			throw new RuntimeException(queryJob.getStatus().getError().toString());
		}

		BibQuery_result = queryJob.getQueryResults();
		
		List<Shippo_tracking_ES_to_BQ_Pojo> shippo_track_ES_to_BQ_Pojo_AL = new ArrayList<Shippo_tracking_ES_to_BQ_Pojo>();
		for (FieldValueList row: BibQuery_result.iterateAll()) {
			
			Shippo_tracking_ES_to_BQ_Pojo shippo_track_ES_to_BQ_Pojo = new Shippo_tracking_ES_to_BQ_Pojo();
			
			if (row.get("LocationCity").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setLocation_city("");
			else
				shippo_track_ES_to_BQ_Pojo.setLocation_city(row.get("LocationCity").getValue().toString());
			if (row.get("LocationCountry").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setLocation_country("");
			else
				shippo_track_ES_to_BQ_Pojo.setLocation_country(row.get("LocationCountry").getValue().toString());
			if (row.get("LocationState").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setLocation_state("");
			else
				shippo_track_ES_to_BQ_Pojo.setLocation_state(row.get("LocationState").getValue().toString());
			if (row.get("LocationZip").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setLocation_zip("");
			else
				shippo_track_ES_to_BQ_Pojo.setLocation_zip(row.get("LocationZip").getValue().toString());
			if (row.get("ObjectCreatedDate").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setObject_created_date("");
			else
				shippo_track_ES_to_BQ_Pojo.setObject_created_date(row.get("ObjectCreatedDate").getValue().toString());
			if (row.get("ObjectID").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setObject_id("");
			else
				shippo_track_ES_to_BQ_Pojo.setObject_id(row.get("ObjectID").getValue().toString());
			if (row.get("ObjectUpdatedDate").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setObject_updated_date("");
			else
				shippo_track_ES_to_BQ_Pojo.setObject_updated_date(row.get("ObjectUpdatedDate").getValue().toString());
			if (row.get("Status").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setStatus("");
			else
				shippo_track_ES_to_BQ_Pojo.setStatus(row.get("Status").getValue().toString());
			if (row.get("StatusDate").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setStatus_date("");
			else
				shippo_track_ES_to_BQ_Pojo.setStatus_date(row.get("StatusDate").getValue().toString());
			if (row.get("StatusDetails").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setStatus_details("");
			else
				shippo_track_ES_to_BQ_Pojo.setStatus_details(row.get("StatusDetails").getValue().toString());
			shippo_track_ES_to_BQ_Pojo.setSubstatus_action_required(row.get("SubStatusActionRequired").getBooleanValue());
			if (row.get("SubStatusCode").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setSubstatus_code("");
			else
				shippo_track_ES_to_BQ_Pojo.setSubstatus_code(row.get("SubStatusCode").getValue().toString());
			if (row.get("SubStatusText").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setSubstatus_text("");
			else
				shippo_track_ES_to_BQ_Pojo.setSubstatus_text(row.get("SubStatusText").getValue().toString());
			if (row.get("TrackingNumber").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setTracking_number("");
			else
				shippo_track_ES_to_BQ_Pojo.setTracking_number(row.get("TrackingNumber").getValue().toString());
			if (row.get("ESModifiedDateTime").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setES_MODIFIEDDATETIME("");
			else
				shippo_track_ES_to_BQ_Pojo.setES_MODIFIEDDATETIME(row.get("ESModifiedDateTime").getValue().toString());
			if (row.get("CreatedBy").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setCreatedBy("");
			else
				shippo_track_ES_to_BQ_Pojo.setCreatedBy(row.get("CreatedBy").getValue().toString());
			if (row.get("CreatedDateTime").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setCreatedDateTime("");
			else
				shippo_track_ES_to_BQ_Pojo.setCreatedDateTime(row.get("CreatedDateTime").getValue().toString());
			if (row.get("ModifiedBy").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setModifiedBy("");
			else
				shippo_track_ES_to_BQ_Pojo.setModifiedBy(row.get("ModifiedBy").getValue().toString());
			if (row.get("ModifiedDateTime").getValue()==null)
				shippo_track_ES_to_BQ_Pojo.setModifiedDateTime("");
			else
				shippo_track_ES_to_BQ_Pojo.setModifiedDateTime(row.get("ModifiedDateTime").getValue().toString());
			
			shippo_track_ES_to_BQ_Pojo_AL.add(shippo_track_ES_to_BQ_Pojo);
		}
		System.out.println("shippo_track_ES_to_BQ_Pojo_AL size: "+shippo_track_ES_to_BQ_Pojo_AL.size());
		for (Shippo_tracking_ES_to_BQ_Pojo history_stg: shippo_track_ES_to_BQ_Pojo_AL) {
			
			QueryJobConfiguration queryConfigg = QueryJobConfiguration.newBuilder("SELECT * FROM `qa-mfrm-data.mfrm_reporting_dataset.shippo_tracking_history` WHERE ObjectID = '"+history_stg.getObject_id()+"'")
			        .setUseLegacySql(false)
			        .build();
		
			JobId jobIdd = JobId.of(UUID.randomUUID().toString());
			Job queryJobb = bigquery.create(JobInfo.newBuilder(queryConfigg).setJobId(jobIdd).build());
			queryJobb = queryJobb.waitFor();

			if (queryJobb == null) {
				throw new RuntimeException("Job no longer exists");
			} else if (queryJobb.getStatus().getError() != null) {
				throw new RuntimeException(queryJobb.getStatus().getError().toString());
			}

			BibQuery_result = queryJobb.getQueryResults();
			
			for (FieldValueList history_master: BibQuery_result.iterateAll()) {
				
				System.out.println("Perform Assertion for Object ID: "+history_stg.getObject_id());
				Object locationCity = history_master.get("LocationCity").getValue();
				if (locationCity==null)
					locationCity = "";
				Assert.assertEquals("Asserting LocationCity for the Object ID: "+history_stg.getObject_id(), history_stg.getLocation_city(), locationCity);
				Object locationCountry = history_master.get("LocationCountry").getValue();
				if (locationCountry==null)
					locationCountry = "";
				Assert.assertEquals("Asserting LocationCountry for the Object ID: "+history_stg.getObject_id(), history_stg.getLocation_country(), locationCountry);
				Object locationState = history_master.get("LocationState").getValue();
				if (locationState==null)
					locationState = "";
				Assert.assertEquals("Asserting LocationState for the Object ID: "+history_stg.getObject_id(), history_stg.getLocation_state(), locationState);
				Object locationZip = history_master.get("LocationZip").getValue();
				if (locationZip==null)
					locationZip = "";
				Assert.assertEquals("Asserting LocationZip for the Object ID: "+history_stg.getObject_id(), history_stg.getLocation_zip(), locationZip);
				Object objectCreatedDate = history_master.get("ObjectCreatedDate").getValue();
				if (objectCreatedDate==null)
					objectCreatedDate = "";
				Assert.assertEquals("Asserting ObjectCreatedDate for the Object ID: "+history_stg.getObject_id(), history_stg.getObject_created_date(), objectCreatedDate);
				Object ObjectID = history_master.get("ObjectID").getValue();
				if (ObjectID==null)
					ObjectID = "";
				Assert.assertEquals("Asserting ObjectID for the Object ID: "+history_stg.getObject_id(), history_stg.getObject_id(), ObjectID);
				Object objectUpdatedDate = history_master.get("ObjectUpdatedDate").getValue();
				if (objectUpdatedDate==null)
					objectUpdatedDate = "";
				Assert.assertEquals("Asserting ObjectUpdatedDate for the Object ID: "+history_stg.getObject_id(), history_stg.getObject_updated_date(), objectUpdatedDate);
				Object status = history_master.get("Status").getValue();
				if (status==null)
					status = "";
				Assert.assertEquals("Asserting Status for the Object ID: "+history_stg.getObject_id(), history_stg.getStatus(), status);
				Object statusDate = history_master.get("StatusDate").getValue();
				if (statusDate==null)
					statusDate = "";
				Assert.assertEquals("Asserting StatusDate for the Object ID: "+history_stg.getObject_id(), history_stg.getStatus_date(), statusDate);
				Object statusDetails = history_master.get("StatusDetails").getValue();
				if (statusDetails==null)
					statusDetails = "";
				Assert.assertEquals("Asserting StatusDetails for the Object ID: "+history_stg.getObject_id(), history_stg.getStatus_details(), statusDetails);
				Object subStatusActionRequired = history_master.get("SubStatusActionRequired").getValue();
				if (subStatusActionRequired==null)
					subStatusActionRequired = "";
				Assert.assertEquals("Asserting SubStatusActionRequired for the Object ID: "+history_stg.getObject_id(), history_stg.getSubstatus_action_required().toString(), subStatusActionRequired);
				Object subStatusCode = history_master.get("SubStatusCode").getValue();
				if (subStatusCode==null)
					subStatusCode = "";
				Assert.assertEquals("Asserting SubStatusCode for the Object ID: "+history_stg.getObject_id(), history_stg.getSubstatus_code(), subStatusCode);
				Object subStatusText = history_master.get("SubStatusText").getValue();
				if (subStatusText==null)
					subStatusText = "";
				Assert.assertEquals("Asserting SubStatusText for the Object ID: "+history_stg.getObject_id(), history_stg.getSubstatus_text(), subStatusText);
				Object trackingNumber = history_master.get("TrackingNumber").getValue();
				if (trackingNumber==null)
					trackingNumber = "";
				Assert.assertEquals("Asserting TrackingNumber for the Object ID: "+history_stg.getObject_id(), history_stg.getTracking_number(), trackingNumber);
				Object eSModifiedDateTime = history_master.get("ESModifiedDateTime").getValue();
				if (eSModifiedDateTime==null)
					eSModifiedDateTime = "";
				Assert.assertEquals("Asserting ESModifiedDateTime for the Object ID: "+history_stg.getObject_id(), history_stg.getES_MODIFIEDDATETIME(), eSModifiedDateTime);
				Object createdBy = history_master.get("CreatedBy").getValue();
				if (createdBy==null)
					createdBy = "";
				Assert.assertEquals("Asserting CreatedBy for the Object ID: "+history_stg.getObject_id(), history_stg.getCreatedBy(), createdBy);
				Object createdDateTime = history_master.get("CreatedDateTime").getValue();
				if (createdDateTime==null)
					createdDateTime = "";
				Assert.assertEquals("Asserting CreatedDateTime for the Object ID: "+history_stg.getObject_id(), history_stg.getCreatedDateTime(), createdDateTime);
				Object modifiedBy = history_master.get("ModifiedBy").getValue();
				if (modifiedBy==null)
					modifiedBy = "";
				Assert.assertEquals("Asserting ModifiedBy for the Object ID: "+history_stg.getObject_id(), history_stg.getModifiedBy(), modifiedBy);
				Object modifiedDateTime = history_master.get("ModifiedDateTime").getValue();
				if (modifiedDateTime==null)
					modifiedDateTime = "";
				Assert.assertEquals("Asserting ModifiedDateTime for the Object ID: "+history_stg.getObject_id(), history_stg.getModifiedDateTime(), modifiedDateTime);
			}
		}
	}
	
	@Then("^assert BQ shippo tracking history stage table data with ES using tracking numbers fetched from BQ status stage table$")
	public void assert_BQ_shippo_tracking_history_stg_data_with_ES_using_trackingNo_fetched_from_BQ_status_stg_table() throws Throwable {
		
		QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder("SELECT * FROM `qa-mfrm-data.mfrm_staging_dataset.shippo_tracking_status_stg`")
		        .setUseLegacySql(false)
		        .build();
	
		JobId jobId = JobId.of(UUID.randomUUID().toString());
		Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
		queryJob = queryJob.waitFor();

		if (queryJob == null) {
			throw new RuntimeException("Job no longer exists");
		} else if (queryJob.getStatus().getError() != null) {
			throw new RuntimeException(queryJob.getStatus().getError().toString());
		}

		BibQuery_result = queryJob.getQueryResults();
		
		fetch_records_and_count("TrackingNumber");
		
		for (String TrackingNo_StatusStg: BigQuery_result_records_List) {
			
			QueryJobConfiguration queryConfigg = QueryJobConfiguration.newBuilder("SELECT * FROM `qa-mfrm-data.mfrm_staging_dataset.shippo_tracking_history_stg` where TrackingNumber = '"+TrackingNo_StatusStg+"'")
			        .setUseLegacySql(false)
			        .build();
		
			JobId jobIdd = JobId.of(UUID.randomUUID().toString());
			Job queryJobb = bigquery.create(JobInfo.newBuilder(queryConfigg).setJobId(jobIdd).build());
			queryJobb = queryJobb.waitFor();

			if (queryJobb == null) {
				throw new RuntimeException("Job no longer exists");
			} else if (queryJobb.getStatus().getError() != null) {
				throw new RuntimeException(queryJobb.getStatus().getError().toString());
			}

			BibQuery_result = queryJobb.getQueryResults();
			
			List<Shippo_tracking_ES_to_BQ_Pojo> shippo_track_ES_to_BQ_Pojo_AL = new ArrayList<Shippo_tracking_ES_to_BQ_Pojo>();
			for (FieldValueList row: BibQuery_result.iterateAll()) {
				
				Shippo_tracking_ES_to_BQ_Pojo shippo_track_ES_to_BQ_Pojo = new Shippo_tracking_ES_to_BQ_Pojo();
				
				if (row.get("LocationCity").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setLocation_city("");
				else
					shippo_track_ES_to_BQ_Pojo.setLocation_city(row.get("LocationCity").getValue().toString());
				if (row.get("LocationCountry").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setLocation_country("");
				else
					shippo_track_ES_to_BQ_Pojo.setLocation_country(row.get("LocationCountry").getValue().toString());
				if (row.get("LocationState").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setLocation_state("");
				else
					shippo_track_ES_to_BQ_Pojo.setLocation_state(row.get("LocationState").getValue().toString());
				if (row.get("LocationZip").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setLocation_zip("");
				else
					shippo_track_ES_to_BQ_Pojo.setLocation_zip(row.get("LocationZip").getValue().toString());
				if (row.get("ObjectCreatedDate").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setObject_created_date("");
				else
					shippo_track_ES_to_BQ_Pojo.setObject_created_date(row.get("ObjectCreatedDate").getValue().toString());
				if (row.get("ObjectID").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setObject_id("");
				else
					shippo_track_ES_to_BQ_Pojo.setObject_id(row.get("ObjectID").getValue().toString());
				if (row.get("ObjectUpdatedDate").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setObject_updated_date("");
				else
					shippo_track_ES_to_BQ_Pojo.setObject_updated_date(row.get("ObjectUpdatedDate").getValue().toString());
				if (row.get("Status").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setStatus("");
				else
					shippo_track_ES_to_BQ_Pojo.setStatus(row.get("Status").getValue().toString());
				if (row.get("StatusDate").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setStatus_date("");
				else
					shippo_track_ES_to_BQ_Pojo.setStatus_date(row.get("StatusDate").getValue().toString());
				if (row.get("StatusDetails").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setStatus_details("");
				else
					shippo_track_ES_to_BQ_Pojo.setStatus_details(row.get("StatusDetails").getValue().toString());
				shippo_track_ES_to_BQ_Pojo.setSubstatus_action_required(row.get("SubStatusActionRequired").getBooleanValue());
				if (row.get("SubStatusCode").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setSubstatus_code("");
				else
					shippo_track_ES_to_BQ_Pojo.setSubstatus_code(row.get("SubStatusCode").getValue().toString());
				if (row.get("SubStatusText").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setSubstatus_text("");
				else
					shippo_track_ES_to_BQ_Pojo.setSubstatus_text(row.get("SubStatusText").getValue().toString());
				if (row.get("TrackingNumber").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setTracking_number("");
				else
					shippo_track_ES_to_BQ_Pojo.setTracking_number(row.get("TrackingNumber").getValue().toString());
				if (row.get("ESModifiedDateTime").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setES_MODIFIEDDATETIME("");
				else
					shippo_track_ES_to_BQ_Pojo.setES_MODIFIEDDATETIME(row.get("ESModifiedDateTime").getValue().toString());
				if (row.get("CreatedBy").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setCreatedBy("");
				else
					shippo_track_ES_to_BQ_Pojo.setCreatedBy(row.get("CreatedBy").getValue().toString());
				if (row.get("CreatedDateTime").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setCreatedDateTime("");
				else
					shippo_track_ES_to_BQ_Pojo.setCreatedDateTime(row.get("CreatedDateTime").getValue().toString());
				if (row.get("ModifiedBy").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setModifiedBy("");
				else
					shippo_track_ES_to_BQ_Pojo.setModifiedBy(row.get("ModifiedBy").getValue().toString());
				if (row.get("ModifiedDateTime").getValue()==null)
					shippo_track_ES_to_BQ_Pojo.setModifiedDateTime("");
				else
					shippo_track_ES_to_BQ_Pojo.setModifiedDateTime(row.get("ModifiedDateTime").getValue().toString());
				
				shippo_track_ES_to_BQ_Pojo_AL.add(shippo_track_ES_to_BQ_Pojo);
			}
			System.out.println("shippo_track_ES_to_BQ_Pojo_AL size: "+shippo_track_ES_to_BQ_Pojo_AL.size());
			
			for (Shippo_tracking_ES_to_BQ_Pojo bQ_History_Stg: shippo_track_ES_to_BQ_Pojo_AL) {
				
				String ES_query = "{\r\n" + 
						"  \"query\": {\r\n" + 
						"    \"match\": {\r\n" + 
						"      \"object_id.keyword\": \""+bQ_History_Stg.getObject_id()+"\"\r\n" + 
						"    }\r\n" + 
						"  }\r\n" + 
						"}";
				System.out.println("ES query executed: "+ES_query);
				ElasticSearchJestUtility.searchQuerywithIndex(ES_query);
				
				String ES_tracking_number = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.tracking_number");
				String ES_status_date = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.status_date");
				String ES_status_details = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.status_details");
				String ES_location_city = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.location_city");
				String ES_location_state = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.location_state");
				String ES_location_zip = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.location_zip");
				String ES_location_country = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.location_country");
				String ES_substatus_code = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.substatus_code");
				String ES_substatus_text = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.substatus_text");
				String ES_substatus_action_required = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.substatus_action_required");
				String ES_object_created_date = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.object_created_date");
				String ES_object_updated_date = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.object_updated_date");
				String ES_object_id = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.object_id");
				String ES_status = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.status");
				String ES_createdBy = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.createdBy");
				String ES_modifiedBy = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.modifiedBy");
				String ES_ES_MODIFIEDDATETIME = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.ES_MODIFIEDDATETIME");
				String ES_createdDateTime = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.createdDateTime");
				String ES_modifiedDateTime = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.modifiedDateTime");
				
				Assert.assertEquals("Asserting tracking_number for the Object ID: "+bQ_History_Stg.getObject_id(), ES_tracking_number, bQ_History_Stg.getTracking_number());
				Assert.assertEquals("Asserting status_date for the Object ID: "+bQ_History_Stg.getObject_id(), ES_status_date, bQ_History_Stg.getStatus_date());
				Assert.assertEquals("Asserting status_details for the Object ID: "+bQ_History_Stg.getObject_id(), ES_status_details, bQ_History_Stg.getStatus_details());
				Assert.assertEquals("Asserting location_city for the Object ID: "+bQ_History_Stg.getObject_id(), ES_location_city, bQ_History_Stg.getLocation_city());
				Assert.assertEquals("Asserting location_state for the Object ID: "+bQ_History_Stg.getObject_id(), ES_location_state, bQ_History_Stg.getLocation_state());
				Assert.assertEquals("Asserting location_zip for the Object ID: "+bQ_History_Stg.getObject_id(), ES_location_zip, bQ_History_Stg.getLocation_zip());
				Assert.assertEquals("Asserting location_country for the Object ID: "+bQ_History_Stg.getObject_id(), ES_location_country, bQ_History_Stg.getLocation_country());
				Assert.assertEquals("Asserting substatus_code for the Object ID: "+bQ_History_Stg.getObject_id(), ES_substatus_code, bQ_History_Stg.getSubstatus_code());
				Assert.assertEquals("Asserting substatus_text for the Object ID: "+bQ_History_Stg.getObject_id(), ES_substatus_text, bQ_History_Stg.getSubstatus_text());
				Assert.assertEquals("Asserting substatus_action_required for the Object ID: "+bQ_History_Stg.getObject_id(), ES_substatus_action_required, bQ_History_Stg.getSubstatus_action_required());
				Assert.assertEquals("Asserting object_created_date for the Object ID: "+bQ_History_Stg.getObject_id(), ES_object_created_date, bQ_History_Stg.getObject_created_date());
				Assert.assertEquals("Asserting object_updated_date for the Object ID: "+bQ_History_Stg.getObject_id(), ES_object_updated_date, bQ_History_Stg.getObject_updated_date());
				Assert.assertEquals("Asserting object_id for the Object ID: "+bQ_History_Stg.getObject_id(), ES_object_id, bQ_History_Stg.getObject_id());
				Assert.assertEquals("Asserting status for the Object ID: "+bQ_History_Stg.getObject_id(), ES_status, bQ_History_Stg.getStatus());
				Assert.assertEquals("Asserting createdBy for the Object ID: "+bQ_History_Stg.getObject_id(), ES_createdBy, bQ_History_Stg.getCreatedBy());
				Assert.assertEquals("Asserting modifiedBy for the Object ID: "+bQ_History_Stg.getObject_id(), ES_modifiedBy, bQ_History_Stg.getModifiedBy());
				Assert.assertEquals("Asserting ES_MODIFIEDDATETIME for the Object ID: "+bQ_History_Stg.getObject_id(), ES_ES_MODIFIEDDATETIME, bQ_History_Stg.getES_MODIFIEDDATETIME());
				Assert.assertEquals("Asserting createdDateTime for the Object ID: "+bQ_History_Stg.getObject_id(), ES_createdDateTime, bQ_History_Stg.getCreatedDateTime());
				Assert.assertEquals("Asserting modifiedDateTime for the Object ID: "+bQ_History_Stg.getObject_id(), ES_modifiedDateTime, bQ_History_Stg.getModifiedDateTime());
			}
		}
	}
	
	@Then("^assert end to end data between source SFTP CSV file '(.*)' and target SQL stage table result CSV file '(.*)' for InMoment Attributes Updates$")
	public void assert_end_to_end_data_between_source_SFTP_CSV_file_and_target_SQL_result_CSV_file_for_InMoment_Attributes_Updates(String SFTP_source_CSV_file_path, String SQL_target_CSV_file_path) throws CsvValidationException, IOException {
		
		List<InMomentAttributesUpdatePojo> InMomentAttributesUpdatePojo_Arraylist = new ArrayList<InMomentAttributesUpdatePojo>();
		
		if (SFTP_source_CSV_file_path.startsWith("$$")) {
			SFTP_source_CSV_file_path = HashMapContainer.get(SFTP_source_CSV_file_path);
		}
		if (SQL_target_CSV_file_path.startsWith("$$")) {
			SQL_target_CSV_file_path = HashMapContainer.get(SQL_target_CSV_file_path);
		}
		
		BufferedReader br_source = new BufferedReader(new FileReader(SFTP_source_CSV_file_path));
		String row_source;
		br_source.readLine();
		br_source.readLine();
		
		//TO STORE SFTP DATA INTO POJO CLASS
		while ((row_source = br_source.readLine()) != null) {
//			System.out.println("row_source "+row_source);
			String[] columns_source_SFTP = row_source.split("\\|");
//			System.out.println("Preparing arraylist for: "+columns_source_SFTP[2]);
			InMomentAttributesUpdatePojo inMomentAttributesUpdatePojo = new InMomentAttributesUpdatePojo();
			inMomentAttributesUpdatePojo.setLocation(columns_source_SFTP[0]);
			inMomentAttributesUpdatePojo.setDate_of_Survey(columns_source_SFTP[1]);
			inMomentAttributesUpdatePojo.setResponse_ID(columns_source_SFTP[2]);
			inMomentAttributesUpdatePojo.setTicket_No(columns_source_SFTP[3]);
			inMomentAttributesUpdatePojo.setLast_Name(columns_source_SFTP[4]);
			inMomentAttributesUpdatePojo.setFirst_Name(columns_source_SFTP[5]);
			inMomentAttributesUpdatePojo.setHome_Phone(columns_source_SFTP[6]);
			inMomentAttributesUpdatePojo.setAddress(columns_source_SFTP[7]);
			inMomentAttributesUpdatePojo.setCity(columns_source_SFTP[8]);
			inMomentAttributesUpdatePojo.setState(columns_source_SFTP[9]);
			inMomentAttributesUpdatePojo.setZip_Code(columns_source_SFTP[10]);
			inMomentAttributesUpdatePojo.setLocation_No(columns_source_SFTP[11]);
			inMomentAttributesUpdatePojo.setStore_Name(columns_source_SFTP[12]);
			inMomentAttributesUpdatePojo.setDate_of_Service(columns_source_SFTP[13]);
			inMomentAttributesUpdatePojo.setFinal_Date(columns_source_SFTP[14]);
			inMomentAttributesUpdatePojo.setPU_DEL(columns_source_SFTP[15]);
			inMomentAttributesUpdatePojo.setEmail_Address(columns_source_SFTP[16]);
			inMomentAttributesUpdatePojo.setSales_Person_1(columns_source_SFTP[17]);
			inMomentAttributesUpdatePojo.setSales_Person_2(columns_source_SFTP[18]);
			inMomentAttributesUpdatePojo.setDelivering_DC(columns_source_SFTP[19]);
			inMomentAttributesUpdatePojo.setDelivery_Truck_No(columns_source_SFTP[20]);
			inMomentAttributesUpdatePojo.setDelivery_Market(columns_source_SFTP[21]);
			inMomentAttributesUpdatePojo.setOrder_Value(columns_source_SFTP[22]);
			inMomentAttributesUpdatePojo.setDelivery_Mode(columns_source_SFTP[23]);
			inMomentAttributesUpdatePojo.setTruck_Driver_ID(columns_source_SFTP[24]);
			inMomentAttributesUpdatePojo.setTruck_Driver_Name(columns_source_SFTP[25]);
			inMomentAttributesUpdatePojo.setTruck_Contractor(columns_source_SFTP[26]);
			inMomentAttributesUpdatePojo.setDate_of_Sale(columns_source_SFTP[27]);
			inMomentAttributesUpdatePojo.setTime_of_Sale(columns_source_SFTP[28]);
			inMomentAttributesUpdatePojo.setScheduled_Delivery_Date(columns_source_SFTP[29]);
			inMomentAttributesUpdatePojo.setScheduled_Delivery_Time(columns_source_SFTP[30]);
			inMomentAttributesUpdatePojo.setActual_Delivery_Date(columns_source_SFTP[31]);
			inMomentAttributesUpdatePojo.setDelivery_Time(columns_source_SFTP[32]);
			inMomentAttributesUpdatePojo.setPurchase_Channel(columns_source_SFTP[33]);
			inMomentAttributesUpdatePojo.setVendor_ID(columns_source_SFTP[34]);
			inMomentAttributesUpdatePojo.setVendor_Name(columns_source_SFTP[35]);
			inMomentAttributesUpdatePojo.setRecommend(columns_source_SFTP[36]);
			String recommend_Comment = columns_source_SFTP[37];
            /* From ISO-8859-1 to UTF-8 */
//    		String decrptRecommend_Comment = new String(recommend_Comment.getBytes("ISO-8859-1"), "UTF-8");
			inMomentAttributesUpdatePojo.setRecommend_Comment(recommend_Comment);
			String video_Feedback_Comment = columns_source_SFTP[38];
            /* From ISO-8859-1 to UTF-8 */
//    		String decryptVideo_Feedback_Comment = new String(video_Feedback_Comment.getBytes("ISO-8859-1"), "UTF-8");
			inMomentAttributesUpdatePojo.setVideo_Feedback_Comment(video_Feedback_Comment);
			inMomentAttributesUpdatePojo.setDelivery_Satisfaction(columns_source_SFTP[39]);
			inMomentAttributesUpdatePojo.setPoor_Experience_Location(columns_source_SFTP[40]);
			inMomentAttributesUpdatePojo.setPoor_Experience_Location_Other(columns_source_SFTP[41]);
			inMomentAttributesUpdatePojo.setContact_Request(columns_source_SFTP[42]);
			inMomentAttributesUpdatePojo.setContact_Method(columns_source_SFTP[43]);
			inMomentAttributesUpdatePojo.setContact_Email_Address(columns_source_SFTP[44]);
			inMomentAttributesUpdatePojo.setContact_Phone_Number(columns_source_SFTP[45]);
			inMomentAttributesUpdatePojo.setNPS_Score(columns_source_SFTP[46]);
			inMomentAttributesUpdatePojo.setMode(columns_source_SFTP[47]);
			inMomentAttributesUpdatePojo.setDistrict(columns_source_SFTP[48]);
			inMomentAttributesUpdatePojo.setBrand(columns_source_SFTP[49]);
			inMomentAttributesUpdatePojo.setSurvey_Attempt(columns_source_SFTP[50]);
			inMomentAttributesUpdatePojo.setIP_Address(columns_source_SFTP[51]);
			inMomentAttributesUpdatePojo.setDevice_Type(columns_source_SFTP[52]);
			
			InMomentAttributesUpdatePojo_Arraylist.add(inMomentAttributesUpdatePojo);
		}
		br_source.close();
		System.out.println("InMomentAttributesUpdatePojo_Arraylist size: "+InMomentAttributesUpdatePojo_Arraylist.size());
		
		BufferedReader bufferedReader_SQL = new BufferedReader(new FileReader(SQL_target_CSV_file_path));
//	    String row_target;
	    bufferedReader_SQL.readLine();
	    
	    Collection<String> SQL_stage_Response_ID = new ArrayList<String>();
		Collection<String> SFTP_file_Response_ID = new ArrayList<String>();
		
		CSVParser parser_SQL = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames()
				.withDelimiter(',').parse(bufferedReader_SQL);
		
		//TO ASSERT SQL RESULTS WITH CAPTURED POJO CLASS DATA
		for (CSVRecord record : parser_SQL) {
//	    while ((row_target = br_target.readLine()) != null) {
//	    	row_target = row_target.replace("\"", "");
//	        String[] columns_target_SQL = row_target.split(",");
	        String response_ID_SQL = record.get(2);
	        
	        SQL_stage_Response_ID.add(response_ID_SQL);
	        
	        for (InMomentAttributesUpdatePojo y: InMomentAttributesUpdatePojo_Arraylist) {
	        	SFTP_file_Response_ID.add(y.getResponse_ID());
	        	
	        	System.out.println("y.getResponse_ID()"+y.getResponse_ID());
	        	System.out.println("respponse_ID_SQL "+response_ID_SQL);
	        	if(y.getResponse_ID().contains(response_ID_SQL)) {
	        		
	        		int listItemIndex = InMomentAttributesUpdatePojo_Arraylist.indexOf(y);
	        		InMomentAttributesUpdatePojo sFTP_arraylist_row = InMomentAttributesUpdatePojo_Arraylist.get(listItemIndex);

	        		System.out.println("Performing assertion for the Response ID: "+response_ID_SQL);
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Location SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getLocation(), record.get(0));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Date_of_Survey SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getDate_of_Survey(), record.get(1));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Response_ID SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getResponse_ID(), record.get(2));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Ticket_No SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getTicket_No(), record.get(3));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Last_Name SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getLast_Name(), record.get(4));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> First_Name SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getFirst_Name(), record.get(5));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Home_Phone SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getHome_Phone(), record.get(6));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Address SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getAddress(), record.get(7));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> City SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getCity(), record.get(8));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> State SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getState(), record.get(9));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Zip_Code SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getZip_Code(), record.get(10));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Location_No SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getLocation_No(), record.get(11));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Store_Name SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getStore_Name(), record.get(12));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Date_of_Service SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getDate_of_Service(), record.get(13));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Final_Date SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getFinal_Date(), record.get(14));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> PU_DEL SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getPU_DEL(), record.get(15));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Email_Address SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getEmail_Address(), record.get(16));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Sales_Person_1 SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getSales_Person_1(), record.get(17));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Sales_Person_2 SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getSales_Person_2(), record.get(18));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Delivering_DC SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getDelivering_DC(), record.get(19));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Delivery_Truck_No SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getDelivery_Truck_No(), record.get(20));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Delivery_Market SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getDelivery_Market(), record.get(21));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Order_Value SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getOrder_Value(), record.get(22));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Delivery_Mode SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getDelivery_Mode(), record.get(23));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Truck_Contractor SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getTruck_Contractor(), record.get(24));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Time_of_Sale SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getTime_of_Sale(), record.get(25));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Delivery_Time SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getDelivery_Time(), record.get(26));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Purchase_Channel SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getPurchase_Channel(), record.get(27));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Recommend SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getRecommend(), record.get(28));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Recommend_Comment SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getRecommend_Comment(), record.get(29));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Video_Feedback_Comment SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getVideo_Feedback_Comment(), record.get(30));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Delivery_Satisfaction SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getDelivery_Satisfaction(), record.get(31));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Poor_Experience_Location SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getPoor_Experience_Location(), record.get(32));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Poor_Experience_Location_Other SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getPoor_Experience_Location_Other(), record.get(33));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Contact_Request SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getContact_Request(), record.get(34));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Contact_Method SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getContact_Method(), record.get(35));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Contact_Email_Address SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getContact_Email_Address(), record.get(36));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Contact_Phone_Number SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getContact_Phone_Number(), record.get(37));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> NPS_Score SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getNPS_Score(), record.get(38));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Mode SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getMode(), record.get(39));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> District SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getDistrict(), record.get(40));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Brand SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getBrand(), record.get(41));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Survey_Attempt SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getSurvey_Attempt(), record.get(42));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> IP_Address SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getIP_Address(), record.get(43));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Device_Type SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getDevice_Type(), record.get(44));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Vendor_ID SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getVendor_ID(), record.get(45));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Vendor_Name SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getVendor_Name(), record.get(46));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Scheduled_Delivery_Date SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getScheduled_Delivery_Date(), record.get(47));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Scheduled_Delivery_Time SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getScheduled_Delivery_Time(), record.get(48));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Actual_Delivery_Date SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getActual_Delivery_Date(), record.get(49));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Date_of_Sale SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getDate_of_Sale(), record.get(50));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Truck_Driver_ID SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getTruck_Driver_ID(), record.get(51));
	        		Assert.assertEquals("Assertion for Response ID: "+response_ID_SQL+" --> Truck_Driver_Name SFTP to SQL validation_Curriculla_STG: ", sFTP_arraylist_row.getTruck_Driver_Name(), record.get(52));
	        		break;
	        	}
	        }
	    }
		bufferedReader_SQL.close();
		
		//TO VALIDATE THAT ALL THE RECORDS GOT ASSERTED BETWEEN SFTP AND SQL
		SFTP_file_Response_ID.removeAll(SQL_stage_Response_ID);
		HashSet<String> SFTP_response_ID_hashset = new HashSet<String>();
		SFTP_response_ID_hashset.addAll(SFTP_file_Response_ID);
		
		if (SFTP_response_ID_hashset.size()>0) {
			System.out.println("CSV comparision failed because target SQL stage result CSV file doesn't contain below Response ID's");
			Assert.assertNull(SFTP_response_ID_hashset);
		}
		
		System.out.println("End to End assertion between SFTP and SQL CSV files completed successfully without any errors.");
	}
	
	@And("^assert end to end data for '(.*)' records between source as SQL stage table result CSV file '(.*)' and target as SQL master table for InMoment Attributes Updates$")
	public void assert_end_to_end_data_for_Count_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_with_desired_count_InMoment_Attributes_Updates(Integer desired_count, String SQL_CSV_file_path) throws IOException, ParseException{
		SQL_CSV_file_path = "F:\\Manoj Thota\\InMoment attributes update\\SQL_files\\SQLInMomentSurveyStg_05-04-2021_09-07-46 - Copy.csv";
		assert_end_to_end_data_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_for_InMoment_Attributes_Updates(desired_count, SQL_CSV_file_path);
	}
	
	@And("^assert end to end data for all the records between source as SQL stage table result CSV file '(.*)' and target as SQL master table for InMoment Attributes Updates$")
	public void assert_end_to_end_data_for_all_records_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_with_desired_count_InMoment_Attributes_Updates(String SQL_CSV_file_path) throws IOException, ParseException{
		
		if (SQL_CSV_file_path.startsWith("$$")) {
			SQL_CSV_file_path = HashMapContainer.get(SQL_CSV_file_path);
		}
//		SQL_CSV_file_path = "F:\\Manoj Thota\\InMoment attributes update\\SQL_files\\SQLInMomentSurveyStg_05-04-2021_09-07-46 - Copy.csv";
		BufferedReader br_SQL = new BufferedReader(new FileReader(SQL_CSV_file_path));
		int SQL_CSV_file_record_count = 0;
		while (br_SQL.readLine() != null) {
			SQL_CSV_file_record_count++;
		}
		SQL_CSV_file_record_count = SQL_CSV_file_record_count - 1;
		System.out.println("SQL_CSV_file_record_count "+SQL_CSV_file_record_count);
		br_SQL.close();
		
		assert_end_to_end_data_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_for_InMoment_Attributes_Updates(SQL_CSV_file_record_count, SQL_CSV_file_path);
	}
		
	public void assert_end_to_end_data_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_for_InMoment_Attributes_Updates(Integer desired_count, String SQL_CSV_file_path) throws IOException, ParseException{	
		
		if (SQL_CSV_file_path.startsWith("$$")) {
			SQL_CSV_file_path = HashMapContainer.get(SQL_CSV_file_path);
		}
		BufferedReader bufferedReader_SQL = new BufferedReader(new FileReader(SQL_CSV_file_path));
		bufferedReader_SQL.readLine();
		CSVParser parser1_SQL = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames().withDelimiter(',').parse(bufferedReader_SQL);
		for (int i = 0; i < desired_count; i++) {
			for (CSVRecord SQL_row : parser1_SQL) {
				String response_ID_SQL_stg = SQL_row.get(2).replace("\"", "'");
				String sql_query = "SELECT * FROM [ThirdPartyProj].[dbo].[INM_InMomentSurveyData] WHERE Response_ID = '"+response_ID_SQL_stg+"'";
				System.out.println("MySQL query executed: "+sql_query);
				Utilities util = new Utilities();
				Map<String, String> ResultSet = util.executeSQLQuery("jdbc:sqlserver://qdbsqltab01.mfrm.com:1433",
						"Mule-QA", "test@123", sql_query);
				Boolean SQL_result_set = ResultSet.isEmpty();
				if (!SQL_result_set) {

					System.out.println("Doing assertion for the Response ID: "+response_ID_SQL_stg);
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Location SQL stage to master table validation: ", SQL_row.get(0).replace("\"", ""), ResultSet.get("Location"));
					SimpleDateFormat source_format = new SimpleDateFormat("M/dd/yy");
			        Date date = source_format.parse(SQL_row.get(1).replace("\"", ""));
			        SimpleDateFormat target_format = new SimpleDateFormat("yyyy-MM-dd");
			        String mod_Datetime = target_format.format(date);
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Date_of_Survey SQL stage to master table validation: ", mod_Datetime, ResultSet.get("Date_of_Survey"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Response_ID SQL stage to master table validation: ", SQL_row.get(2).replace("\"", ""), ResultSet.get("Response_ID"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Ticket# SQL stage to master table validation: ", SQL_row.get(3).replace("\"", ""), ResultSet.get("Ticket#"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Last_Name SQL stage to master table validation: ", SQL_row.get(4).replace("\"", ""), ResultSet.get("Last_Name"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> First_Name SQL stage to master table validation: ", SQL_row.get(5).replace("\"", ""), ResultSet.get("First_Name"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Home_Phone SQL stage to master table validation: ", SQL_row.get(6).replace("\"", ""), ResultSet.get("Home_Phone"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Address SQL stage to master table validation: ", SQL_row.get(7).replace("\"", ""), ResultSet.get("Address"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> City SQL stage to master table validation: ", SQL_row.get(8).replace("\"", ""), ResultSet.get("City"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> State SQL stage to master table validation: ", SQL_row.get(9).replace("\"", ""), ResultSet.get("State"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Zip_Code SQL stage to master table validation: ", SQL_row.get(10).replace("\"", ""), ResultSet.get("Zip_Code"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Location_No SQL stage to master table validation: ", SQL_row.get(11).replace("\"", ""), ResultSet.get("Location_No"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Store_Name SQL stage to master table validation: ", SQL_row.get(12).replace("\"", ""), ResultSet.get("Store_Name"));
					SimpleDateFormat source_formatt = new SimpleDateFormat("M/dd/yy");
			        Date datee = source_formatt.parse(SQL_row.get(13).replace("\"", ""));
			        SimpleDateFormat target_formatt = new SimpleDateFormat("yyyy-MM-dd");
			        String mod_Datetimee = target_formatt.format(datee);
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Date_of_Service SQL stage to master table validation: ", mod_Datetimee, ResultSet.get("Date_of_Service"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Final_Date SQL stage to master table validation: ", SQL_row.get(14).replace("\"", ""), ResultSet.get("Final_Date"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> PU_DEL SQL stage to master table validation: ", SQL_row.get(15).replace("\"", ""), ResultSet.get("PU_DEL"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Email_Address SQL stage to master table validation: ", SQL_row.get(16).replace("\"", ""), ResultSet.get("Email_Address"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Sales_Person_1 SQL stage to master table validation: ", SQL_row.get(17).replace("\"", ""), ResultSet.get("Sales_Person_1"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Sales_Person_2 SQL stage to master table validation: ", SQL_row.get(18).replace("\"", ""), ResultSet.get("Sales_Person_2"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Delivering_DC SQL stage to master table validation: ", SQL_row.get(19).replace("\"", ""), ResultSet.get("Delivering_DC"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Delivery_Truck# SQL stage to master table validation: ", SQL_row.get(20).replace("\"", ""), ResultSet.get("Delivery_Truck#"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Delivery_Market SQL stage to master table validation: ", SQL_row.get(21).replace("\"", ""), ResultSet.get("Delivery_Market"));
					String order_value = SQL_row.get(22).replace("\"", "");
					if (order_value.isEmpty()) {
						order_value = null;
					}
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Order_Value SQL stage to master table validation: ", order_value, ResultSet.get("Order_Value"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Delivery_Mode SQL stage to master table validation: ", SQL_row.get(23).replace("\"", ""), ResultSet.get("Delivery_Mode"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Truck_Contractor SQL stage to master table validation: ", SQL_row.get(24).replace("\"", ""), ResultSet.get("Truck_Contractor"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Time_of_Sale SQL stage to master table validation: ", SQL_row.get(25).replace("\"", ""), ResultSet.get("Time_of_Sale"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Delivery_Time SQL stage to master table validation: ", SQL_row.get(26).replace("\"", ""), ResultSet.get("Delivery_Time"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Purchase_Channel SQL stage to master table validation: ", SQL_row.get(27).replace("\"", ""), ResultSet.get("Purchase_Channel"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Recommend SQL stage to master table validation: ", SQL_row.get(28).replace("\"", ""), ResultSet.get("Recommend"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Recommend_Comment SQL stage to master table validation: ", SQL_row.get(29).replace("\"", ""), ResultSet.get("Recommend_Comment"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Video_Feedback_Comment SQL stage to master table validation: ", SQL_row.get(30).replace("\"", ""), ResultSet.get("Video_Feedback_Comment"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Delivery_Satisfaction SQL stage to master table validation: ", SQL_row.get(31).replace("\"", ""), ResultSet.get("Delivery_Satisfaction"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Poor_Experience_Location SQL stage to master table validation: ", SQL_row.get(32).replace("\"", ""), ResultSet.get("Poor_Experience_Location"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Poor_Experience_Location_Other SQL stage to master table validation: ", SQL_row.get(33).replace("\"", ""), ResultSet.get("Poor_Experience_Location_Other"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Contact_Request SQL stage to master table validation: ", SQL_row.get(34).replace("\"", ""), ResultSet.get("Contact_Request"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Contact_Method SQL stage to master table validation: ", SQL_row.get(35).replace("\"", ""), ResultSet.get("Contact_Method"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Contact_Email_Address SQL stage to master table validation: ", SQL_row.get(36).replace("\"", ""), ResultSet.get("Contact_Email_Address"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Contact_Phone_Number SQL stage to master table validation: ", SQL_row.get(37).replace("\"", ""), ResultSet.get("Contact_Phone_Number"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> NPS_Score SQL stage to master table validation: ", SQL_row.get(38).replace("\"", ""), ResultSet.get("NPS_Score"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Mode SQL stage to master table validation: ", SQL_row.get(39).replace("\"", ""), ResultSet.get("Mode"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> District SQL stage to master table validation: ", SQL_row.get(40).replace("\"", ""), ResultSet.get("District"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Brand SQL stage to master table validation: ", SQL_row.get(41).replace("\"", ""), ResultSet.get("Brand"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Survey_Attempt SQL stage to master table validation: ", SQL_row.get(42).replace("\"", ""), ResultSet.get("Survey_Attempt"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> IP_Address SQL stage to master table validation: ", SQL_row.get(43).replace("\"", ""), ResultSet.get("IP_Address"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Device_Type SQL stage to master table validation: ", SQL_row.get(44).replace("\"", ""), ResultSet.get("Device_Type"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Vendor_ID SQL stage to master table validation: ", SQL_row.get(45).replace("\"", ""), ResultSet.get("Vendor_ID"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Vendor_Name SQL stage to master table validation: ", SQL_row.get(46).replace("\"", ""), ResultSet.get("Vendor_Name"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Scheduled_Delivery_Date SQL stage to master table validation: ", SQL_row.get(47).replace("\"", ""), ResultSet.get("Scheduled_Delivery_Date"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Scheduled_Delivery_Time SQL stage to master table validation: ", SQL_row.get(48).replace("\"", ""), ResultSet.get("Scheduled_Delivery_Time"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Actual_Delivery_Date SQL stage to master table validation: ", SQL_row.get(49).replace("\"", ""), ResultSet.get("Actual_Delivery_Date"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Date_of_Sale SQL stage to master table validation: ", SQL_row.get(50).replace("\"", ""), ResultSet.get("Date_of_Sale"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Truck_Driver_ID SQL stage to master table validation: ", SQL_row.get(51).replace("\"", ""), ResultSet.get("Truck_Driver_ID"));
					Assert.assertEquals("Assertion for the Response ID: "+response_ID_SQL_stg+" --> Truck_Driver_Name SQL stage to master table validation: ", SQL_row.get(52).replace("\"", ""), ResultSet.get("Truck_Driver_Name"));
					break;
				} else {
					System.out.println("No data present in SQL master table for the Response_ID: "+ response_ID_SQL_stg);
				}
			}
		}
		bufferedReader_SQL.close();
		System.out.println("End to End assertion between SQL stage and master table completed successfully without any errors.");
	}
	
	@Then("^capture visitTrend, trendTimeBucketsUserTimezone, siteEngagedDwellMins and siteDwellTrendMins from visits API response$")
	public void capture_details_from_visits_API_response_Aislelabs() {
		
		HashMapContainer.addList("$$visitTrends", com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), "$.response.flowVisits[0].visitTrend[*]"));
		HashMapContainer.addList("$$trendTimeBucketsUserTimezones", com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), "$.response.flowVisits[0].trendTimeBucketsUserTimezone[*]"));
		HashMapContainer.addList("$$siteDwellTrendMins", com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), "$.response.flowVisits[0].siteDwellTrendMins[*]"));
		HashMapContainer.add("$$siteEngagedDwellMins", JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), "$.response.flowVisits[0].siteEngagedDwellMins").toString());
	}
	
	@Then("^assert visits API response with aislelabs_30_min_interval_visits BQ table content$")
	public void assert_visits_API_response_with_aislelabs_30_min_interval_visits_BQ_table_content_aislelabs() throws InterruptedException, ParseException {
		
		System.out.println("Initiated assertion of visits API response with aislelabs_30_min_interval_visits BQ table content");
		List<String> values = HashMapContainer.getList("$$TDIDs");
		Object firstValue = values.get(0);
		String TDID = null;
		if (firstValue instanceof Integer) {
			TDID = firstValue.toString();
		}
		List<String> TDIDnames = HashMapContainer.getList("$$TDIDnames");
		String TDIDname = TDIDnames.get(0);
		String store_id = TDIDname.substring(5, 11);
		String store_name = TDIDname.substring(12);
		System.out.println("store id: "+store_id+" and store name: "+store_name);
		String siteEngagedDwellMins = HashMapContainer.get("$$siteEngagedDwellMins");
		List<String> visitTrends = HashMapContainer.getList("$$visitTrends");
		List<String> siteDwellTrendMins = HashMapContainer.getList("$$siteDwellTrendMins");
		List<String> trendTimeBucketsUserTimezones = HashMapContainer.getList("$$trendTimeBucketsUserTimezones");
		for (int i=0; i<trendTimeBucketsUserTimezones.size(); i++) {
			
			String query = "SELECT * FROM `qa-mfrm-data.mfrm_customer_and_social.aislelabs_30_min_interval_visits` WHERE TdId = "+TDID+" and StartDateTimeCST = '"+trendTimeBucketsUserTimezones.get(i)+":00'";
			System.out.println("BigQuery executed: "+query);
			QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(query)
			        .setUseLegacySql(false)
			        .build();
		
			JobId jobId = JobId.of(UUID.randomUUID().toString());
			Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
			queryJob = queryJob.waitFor();
	
			if (queryJob == null) {
				throw new RuntimeException("Job no longer exists");
			} else if (queryJob.getStatus().getError() != null) {
				throw new RuntimeException(queryJob.getStatus().getError().toString());
			}
	
			BibQuery_result = queryJob.getQueryResults();
			
			for(FieldValueList BQ_result_row : BibQuery_result.iterateAll()) {
				System.out.println("Performing assertion for the time: "+trendTimeBucketsUserTimezones.get(i));
				Assert.assertEquals(store_id, BQ_result_row.get("StoreNumber").getStringValue());
				Assert.assertEquals(store_name, BQ_result_row.get("StoreName").getStringValue());
				Assert.assertEquals("Mule", BQ_result_row.get("CreatedBy").getStringValue());
				if (siteEngagedDwellMins.equals("0.0")) {
					Assert.assertEquals("0.0", BQ_result_row.get("SiteEngagedDwellMins").getStringValue());
				}else {
					Object siteDwellTrendMin = siteDwellTrendMins.get(i);
					String siteDwellTrendMinString = null;
					if (siteDwellTrendMin instanceof Double) {
						siteDwellTrendMinString = Double.toString((double) siteDwellTrendMin);
					}
					Assert.assertEquals(siteDwellTrendMinString, BQ_result_row.get("SiteEngagedDwellMins").getStringValue());
				}
				Assert.assertEquals(TDID, BQ_result_row.get("TdId").getStringValue());
				Object siteDwellTrendMin = siteDwellTrendMins.get(i);
				String siteDwellTrendMinString = null;
				if (siteDwellTrendMin instanceof Double) {
					siteDwellTrendMinString = Double.toString((double) siteDwellTrendMin);
				}
				Assert.assertEquals(siteDwellTrendMinString, BQ_result_row.get("SiteDwellMins").getStringValue());
				Object visitTrend = visitTrends.get(i);
				String visitTrendString = null;
				if (visitTrend instanceof Integer) {
					visitTrendString = Integer.toString((int) visitTrend);
				}
				Assert.assertEquals(visitTrendString, BQ_result_row.get("Visits").getStringValue());
				Assert.assertEquals(visitTrendString, BQ_result_row.get("UniqueVisitors").getStringValue());
				Assert.assertEquals(trendTimeBucketsUserTimezones.get(i)+":00", BQ_result_row.get("StartDateTimeCST").getStringValue());
				String starttime = trendTimeBucketsUserTimezones.get(i);
				int mins = Integer.parseInt(starttime.substring(14));
				int mins_modified = mins+29;
				String endtime = starttime.substring(0, 14)+mins_modified+":00";
				Assert.assertEquals(endtime, BQ_result_row.get("EndDateTimeCST").getStringValue());
		        SimpleDateFormat source_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		        Date startDate = source_format.parse(trendTimeBucketsUserTimezones.get(i)+":00");
		        DateFormat target_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
		        target_format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
				String startDateTimeUTC = target_format.format(startDate);
				String startDateTimeUTC_BQ = BQ_result_row.get("StartDateTimeUTC").getStringValue();
				double db = Double.parseDouble(startDateTimeUTC_BQ);
				Date date = new Date((long) (db * 1000L));
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
				format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
				startDateTimeUTC_BQ = format.format(date);
//				Assert.assertEquals(startDateTimeUTC, startDateTimeUTC_BQ);
				Date endDate = source_format.parse(endtime);
				String endDateTimeUTC = target_format.format(endDate);
				String endDateTimeUTC_BQ = BQ_result_row.get("EndDateTimeUTC").getStringValue();
				double bq = Double.parseDouble(endDateTimeUTC_BQ);
				Date date_convert = new Date((long) (bq * 1000L));
				endDateTimeUTC_BQ = format.format(date_convert);
//				Assert.assertEquals(endDateTimeUTC, endDateTimeUTC_BQ);
			}
		}
		System.out.println("Completed assertion between visits API response with aislelabs_30_min_interval_visits BQ table content");
	}
	
	@Then("fetch '(.*)' records and count from BigQuery results")
	public ArrayList<String> fetch_records_and_count(String column_name) {
		
		//TO FETCH ONE SINGLE COLUMN VALUE
		BigQuery_result_records_List = new ArrayList<String>();
		for (FieldValueList row : BibQuery_result.iterateAll()) {
			
			String result_value = row.get(column_name).getStringValue();
			BigQuery_result_records_List.add(result_value);
		}
		BigQuery_result_count = BigQuery_result_records_List.size();
		System.out.println("BigQuery records Count: "+BigQuery_result_count);
		return BigQuery_result_records_List;
	}
	
	@And("^assert if duplicate records exist in target BigQuery results$")
	public void assert_if_duplicate_records_exist_in_target_BigQuery_results() {
		
		//DUPLICATE VALIDATION BY USING HASHSET
		int BQResults_total_count = BigQuery_result_records_List.size();
		HashSet<String> hashSet_BQResults = new HashSet<String>();
		hashSet_BQResults.addAll(BigQuery_result_records_List);
		int hashSet_BQResults_count = hashSet_BQResults.size();
		Assert.assertEquals("Duplicate records validation for target BigQuery results: ", BQResults_total_count, hashSet_BQResults_count);
		System.out.println("Duplicate target Contact ID's validation is done for -->"+BQResults_total_count+"<-- records");
	}
	
	@And("assert BigQuery record count with CSV file '(.*)' record count")
	public void assert_BigQuery_record_count_with_CSV_file_record_count(String inContactAPIResultCSVFilePath) throws IOException {
		
		if (inContactAPIResultCSVFilePath.startsWith("$$")) {
			inContactAPIResultCSVFilePath = HashMapContainer.get(inContactAPIResultCSVFilePath);
		}
		BufferedReader bufferedReader = new BufferedReader(new FileReader(inContactAPIResultCSVFilePath));
		int CSV_file_record_count = 0;
		while (bufferedReader.readLine() != null) {
			CSV_file_record_count++;
		}
		CSV_file_record_count = CSV_file_record_count - 1;
		bufferedReader.close();
		Assert.assertEquals("Validation of BigQuery record count with CSV file record count: ", BigQuery_result_count, CSV_file_record_count);
	}
	
	@And("^assert all column values for count of '(.*)' rows between BigQuery and CSV file '(.*)' for WithHold$")
	public void assert_all_column_values_for_count_between_BigQuery_and_CSV_file_records_for_WithHold(Integer rows_count,String csv_file_path) throws IOException, InterruptedException {
		
		//ASSERTION WITH USER DEFINER COUNT OF RECORDS
		CSV_and_BQ_records_assertion_WithHold(rows_count, csv_file_path);
	}
	
	@And("^assert all column values for all rows between BigQuery and CSV file '(.*)' for WithHold$")
	public void assert_all_column_values_for_all_rows_between_BigQuery_and_CSV_file_records_for_WithHold(String csv_file_path) throws IOException, InterruptedException {
		
		if (csv_file_path.startsWith("$$")) {
			csv_file_path = HashMapContainer.get(csv_file_path);
		}
		// TO FIND RECORDS COUNT IN CSV FILE
		BufferedReader bufferedReader = new BufferedReader(new FileReader(csv_file_path));
		int CSV_file_record_count = 0;
		while (bufferedReader.readLine() != null) {
			CSV_file_record_count++;
		}
		CSV_file_record_count = CSV_file_record_count - 1;
		bufferedReader.close();

		//ASSERTION FOR ALL THE RECORDS PRESENT IN CSV FILE
		CSV_and_BQ_records_assertion_WithHold(CSV_file_record_count, csv_file_path);
	}
	
	public void CSV_and_BQ_records_assertion_WithHold(Integer desired_rows_count,String APIResultCSVFilePath) throws IOException, InterruptedException {
		System.out.println("Initiated End to End data validation between BigQuery and InContact API result CSV file for "+desired_rows_count+" records");
//		if (APIResultCSVFilePath.startsWith("$$")) {
//			APIResultCSVFilePath = HashMapContainer.get(APIResultCSVFilePath);
//		}
		APIResultCSVFilePath = "F:\\Manoj Thota\\InContact\\WithHold\\WithHold_API_CSV_files\\Data_WithHold_2020_09_29_14_09_44.csv";
		BufferedReader br_APIResultCSVFile = new BufferedReader(new FileReader(APIResultCSVFilePath));
		br_APIResultCSVFile.readLine();
		CSVParser parser_APIResultCSVFile = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames().withDelimiter(',')
				.parse(br_APIResultCSVFile);
		
		//RUNNING LOOP FOR EVERY RECORD TO GET DATA FROM BQ AND ASSERT WITH API RESULT CSV FILE
		for (int i=0; i<desired_rows_count; i++) {
			for (CSVRecord APIResultCSVFile_record : parser_APIResultCSVFile) {
			
				String rowValue_ContactID = APIResultCSVFile_record.get(0);
//				String Bquery = "select * from `qa-mfrm-data.mfrm_customer_and_social.incontact_comp_with_hold_call` WHERE  StartDate = '"+date_provided_in_BQ_query+"' and ContactId = '"+rowValue_ContactID+"' LIMIT 1";
				String Bquery = "select * from `qa-mfrm-data.mfrm_customer_and_social.incontact_comp_with_hold_call` WHERE  StartDate = '2020-09-28' and ContactId = '"+rowValue_ContactID+"' LIMIT 1";
				
				System.out.println("Query to fetch single record: "+Bquery);
				QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(Bquery)
						.setUseLegacySql(false)
						.build();
				JobId jobId = JobId.of(UUID.randomUUID().toString());
				Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
				queryJob = queryJob.waitFor();

				if (queryJob == null) {
					throw new RuntimeException("Job no longer exists");
				} else if (queryJob.getStatus().getError() != null) {
					throw new RuntimeException(queryJob.getStatus().getError().toString());
				}
	
				BibQuery_result = queryJob.getQueryResults();
			
				for(FieldValueList row : BibQuery_result.iterateAll()) {
					
					System.out.println(row.get("ContactId").getStringValue());
					Assert.assertEquals("(WithHold) Contact ID validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(0), row.get("ContactId").getStringValue());
					Assert.assertEquals("(WithHold) Master Contact ID validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(1), row.get("MasterContactId").getStringValue());
					Assert.assertEquals("(WithHold) Contact code validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(2), row.get("ContactCode").getStringValue());
					Assert.assertEquals("(WithHold) Media name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(3), row.get("MediaName").getStringValue());
					Assert.assertEquals("(WithHold) Contact name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(4), row.get("ContactName").getStringValue());
					Assert.assertEquals("(WithHold) ANI DialNum validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(5), row.get("ANIDialnum").getStringValue());
					Assert.assertEquals("(WithHold) Skill No validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(6), row.get("SkillNo").getStringValue());
					Assert.assertEquals("(WithHold) Skill name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(7), row.get("SkillName").getStringValue());
					Assert.assertEquals("(WithHold) Campaign no validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(8), row.get("CampaignNo").getStringValue());
					Assert.assertEquals("(WithHold) Campaign name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(9), row.get("CampaignName").getStringValue());
					Assert.assertEquals("(WithHold) Agent No validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(10), row.get("AgentNo").getStringValue());
					Assert.assertEquals("(WithHold) Agent Name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(11), row.get("AgentName").getStringValue());
					Assert.assertEquals("(WithHold) Team no validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(12), row.get("TeamNo").getStringValue());
					Assert.assertEquals("(WithHold) Team Name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(13), row.get("TeamName").getStringValue());
					
					String disposition_code = APIResultCSVFile_record.get(14);
					if(disposition_code.isEmpty()) {
						disposition_code = null;
					}
					Assert.assertEquals("(WithHold) Disposition code validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", disposition_code, row.get("DispositionCode").getValue());
					Assert.assertEquals("(WithHold) SLA validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(15), row.get("SLA").getStringValue());
					
					String[] csv_start_date = APIResultCSVFile_record.get(16).split("/");
					String start_date = csv_start_date[2]+"-"+csv_start_date[0]+"-"+csv_start_date[1];
					
					Assert.assertEquals("(WithHold) Start Date validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", start_date, row.get("StartDate").getStringValue());
					Assert.assertEquals("(WithHold) Start Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(17), row.get("StartTime").getStringValue());
					Assert.assertEquals("(WithHold) PreQueue validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(18), row.get("PreQueue").getStringValue());
					Assert.assertEquals("(WithHold) InQueue validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(19), row.get("InQueue").getStringValue());
					Assert.assertEquals("(WithHold) Agent Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(20), row.get("AgentTime").getStringValue());
					Assert.assertEquals("(WithHold) Post Queue validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(21), row.get("PostQueue").getStringValue());
					Assert.assertEquals("(WithHold) Total Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(22), row.get("TotalTime").getStringValue());
					Assert.assertEquals("(WithHold) Abandon Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(23), row.get("AbandonTime").getStringValue());
					Assert.assertEquals("(WithHold) Routing Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(24), row.get("RoutingTime").getStringValue());
					Assert.assertEquals("(WithHold) Abandon validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(25), row.get("Abandon").getStringValue());
					Assert.assertEquals("(WithHold) Callback Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(26), row.get("CallbackTime").getStringValue());
					Assert.assertEquals("(WithHold) Logged validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(27), row.get("Logged").getStringValue());
					Assert.assertEquals("(WithHold) Hold Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(28), row.get("HoldTime").getStringValue());
				}
				break;
			}
		}
		System.out.println("Completed End to End data validation between BigQuery and InContact API result CSV file successfully for "+desired_rows_count+" records without any error");
	}
	
	@And("^assert all column values for count of '(.*)' rows between BigQuery and CSV file '(.*)' for Expanded$")
	public void assert_all_column_values_for_count_between_BigQuery_and_CSV_file_records_for_Expanded(Integer rows_count,String csv_file_path) throws IOException, InterruptedException {
		
		//ASSERTION WITH USER DEFINER COUNT OF RECORDS
		CSV_and_BQ_records_assertion_Expanded(rows_count, csv_file_path);
	}
	
	@And("^assert all column values for all rows between BigQuery and CSV file '(.*)' for Expanded$")
	public void assert_all_column_values_for_all_rows_between_BigQuery_and_CSV_file_records_for_Expanded(String csv_file_path) throws IOException, InterruptedException {
		
		if (csv_file_path.startsWith("$$")) {
			csv_file_path = HashMapContainer.get(csv_file_path);
		}
		// TO FIND RECORDS COUNT IN CSV FILE
		BufferedReader bufferedReader = new BufferedReader(new FileReader(csv_file_path));
		int CSV_file_record_count = 0;
		while (bufferedReader.readLine() != null) {
			CSV_file_record_count++;
		}
		CSV_file_record_count = CSV_file_record_count - 1;
		bufferedReader.close();

		//ASSERTION FOR ALL THE RECORDS PRESENT IN CSV FILE
		CSV_and_BQ_records_assertion_Expanded(CSV_file_record_count, csv_file_path);
	}
	
	public void CSV_and_BQ_records_assertion_Expanded(Integer desired_rows_count,String APIResultCSVFilePath) throws IOException, InterruptedException {
		System.out.println("Initiated End to End data validation between BigQuery and InContact API result CSV file for "+desired_rows_count+" records");
		if (APIResultCSVFilePath.startsWith("$$")) {
			APIResultCSVFilePath = HashMapContainer.get(APIResultCSVFilePath);
		}
		
		BufferedReader br_APIResultCSVFile = new BufferedReader(new FileReader(APIResultCSVFilePath));
		br_APIResultCSVFile.readLine();
		CSVParser parser_APIResultCSVFile = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames().withDelimiter(',')
				.parse(br_APIResultCSVFile);
		
		//RUNNING LOOP FOR EVERY RECORD TO GET DATA FROM BQ AND ASSERT WITH API RESULT CSV FILE
		for(int i=0; i<desired_rows_count; i++) {
			for (CSVRecord APIResultCSVFile_record : parser_APIResultCSVFile) {
				
				String rowValue_ContactID = APIResultCSVFile_record.get(0);
				String Bquery = "select * from `qa-mfrm-data.mfrm_customer_and_social.incontact_expanded_call` WHERE  StartDate = '"+date_provided_in_BQ_query+"' and ContactId = '"+rowValue_ContactID+"' LIMIT 1";
				System.out.println("Query to fetch single record: "+Bquery);
				QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(Bquery)
						.setUseLegacySql(false)
						.build();
				JobId jobId = JobId.of(UUID.randomUUID().toString());
				Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
				queryJob = queryJob.waitFor();

				if (queryJob == null) {
					throw new RuntimeException("Job no longer exists");
				} else if (queryJob.getStatus().getError() != null) {
					throw new RuntimeException(queryJob.getStatus().getError().toString());
				}
	
				BibQuery_result = queryJob.getQueryResults();
			
				for(FieldValueList row : BibQuery_result.iterateAll()) {
					
					Assert.assertEquals("(Expanded) Contact ID validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(0), row.get("ContactId").getStringValue());
					Assert.assertEquals("(Expanded) Master ContactID validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(1), row.get("MasterContactId").getStringValue());
					Assert.assertEquals("(Expanded) Media name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(2), row.get("MediaName").getStringValue());
					Assert.assertEquals("(Expanded) Contact name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(3), row.get("ContactName").getStringValue());
					Assert.assertEquals("(Expanded) ANI validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(4), row.get("ANI").getStringValue());
					Assert.assertEquals("(Expanded) DNIS validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(5), row.get("DNIS").getStringValue());
					Assert.assertEquals("(Expanded) Skill No validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(6), row.get("SkillNo").getStringValue());
					Assert.assertEquals("(Expanded) Skill name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(7), row.get("SkillName").getStringValue());
					Assert.assertEquals("(Expanded) Campaign no validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(8), row.get("CampaignNo").getStringValue());
					Assert.assertEquals("(Expanded) Campaign name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(9), row.get("CampaignName").getStringValue());
					Assert.assertEquals("(Expanded) Agent No validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(10), row.get("AgentNo").getStringValue());
					Assert.assertEquals("(Expanded) Agent Name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(11), row.get("AgentName").getStringValue());
					Assert.assertEquals("(Expanded) Team no validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(12), row.get("TeamNo").getStringValue());
					Assert.assertEquals("(Expanded) Team Name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(13), row.get("TeamName").getStringValue());
					Assert.assertEquals("(Expanded) SLA validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(14), row.get("SLA").getStringValue());
					
					String[] csv_start_date = APIResultCSVFile_record.get(15).split("/");
					String start_date = csv_start_date[2]+"-"+csv_start_date[0]+"-"+csv_start_date[1];
					
					Assert.assertEquals("(Expanded) Start Date validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", start_date, row.get("StartDate").getStringValue());
					Assert.assertEquals("(Expanded) Start Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(16), row.get("StartTime").getStringValue());
					Assert.assertEquals("(Expanded) PreQueue validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(17), row.get("PreQueue").getStringValue());
					Assert.assertEquals("(Expanded) InQueue validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(18), row.get("InQueue").getStringValue());
					Assert.assertEquals("(Expanded) Agent Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(19), row.get("AgentTime").getStringValue());
					Assert.assertEquals("(Expanded) Post Queue validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(20), row.get("PostQueue").getStringValue());
					Assert.assertEquals("(Expanded) Total Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(21), row.get("TotalTime").getStringValue());
					Assert.assertEquals("(Expanded) Abandon Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(22), row.get("AbandonTime").getStringValue());
					Assert.assertEquals("(Expanded) Abandon validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(23), row.get("Abandon").getStringValue());
					Assert.assertEquals("(Expanded) ACW Seconds validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(24), row.get("ACWSeconds").getStringValue());
					Assert.assertEquals("(Expanded) ACW Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(25), row.get("ACWTime").getStringValue());
				}
				break;
			}
		}
		System.out.println("End to End data validation between BigQuery and InContact API result CSV file is successfull for "+desired_rows_count+" records without any error");
	}
	
	@And("^assert all column values for count of '(.*)' rows between BigQuery and CSV file '(.*)' for Agentlist$")
	public void assert_all_column_values_for_count_between_BigQuery_and_CSV_file_records_for_Agentlist(Integer rows_count,String csv_file_path) throws IOException, InterruptedException, ParseException {
		
		//ASSERTION WITH USER DEFINER COUNT OF RECORDS
		CSV_and_BQ_records_assertion_Agentlist(rows_count, csv_file_path);
	}
	
	@And("^assert all column values for all rows between BigQuery and CSV file '(.*)' for Agentlist$")
	public void assert_all_column_values_for_all_rows_between_BigQuery_and_CSV_file_records_for_Agentlist(String csv_file_path) throws IOException, InterruptedException, ParseException {
		
		if (csv_file_path.startsWith("$$")) {
			csv_file_path = HashMapContainer.get(csv_file_path);
		}
		//TO FIND RECORDS COUNT IN CSV FILE
		BufferedReader bufferedReader = new BufferedReader(new FileReader(csv_file_path));
		int CSV_file_record_count = 0;
		while (bufferedReader.readLine() != null) {
			CSV_file_record_count++;
		}
		CSV_file_record_count = CSV_file_record_count - 1;
		bufferedReader.close();

		//ASSERTION FOR ALL THE RECORDS PRESENT IN CSV FILE
		CSV_and_BQ_records_assertion_Agentlist(CSV_file_record_count, csv_file_path);
	}
	
	public void CSV_and_BQ_records_assertion_Agentlist(Integer desired_rows_count,String APIResultCSVFilePath) throws IOException, InterruptedException, ParseException {
		System.out.println("Initiated End to End data assertion between inContact API and BigQuery results for "+desired_rows_count+" records");
		if (APIResultCSVFilePath.startsWith("$$")) {
			APIResultCSVFilePath = HashMapContainer.get(APIResultCSVFilePath);
		}
		
		BufferedReader br_APIResultCSVFile = new BufferedReader(new FileReader(APIResultCSVFilePath));
		br_APIResultCSVFile.readLine();
		CSVParser parser_APIResultCSVFile = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames()
				.withDelimiter(',').parse(br_APIResultCSVFile);
	    
		//RUNNING LOOP FOR EVERY RECORD TO GET DATA FROM BQ AND ASSERT WITH API RESULT CSV FILE
	    for (int i=0; i<desired_rows_count; i++) {
	    	for (CSVRecord APIResultCSVFile_record : parser_APIResultCSVFile) {
	    		
		        String AgentNo_from_API_result = APIResultCSVFile_record.get(0);
		        String Bquery = "select * from `dev-mfrm-data.mfrm_customer_and_social.incontact_agent_list_extended` where AgentNo = "+AgentNo_from_API_result;
		        System.out.println("Query to fetch single record: "+Bquery);
		        QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(Bquery)
		        		.setUseLegacySql(false)
		        		.build();
				JobId jobId = JobId.of(UUID.randomUUID().toString());
				Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
				queryJob = queryJob.waitFor();
	
				if (queryJob == null) {
					throw new RuntimeException("Job no longer exists");
				} else if (queryJob.getStatus().getError() != null) {
					throw new RuntimeException(queryJob.getStatus().getError().toString());
				}
	
				BibQuery_result = queryJob.getQueryResults();
			
				for(FieldValueList row : BibQuery_result.iterateAll()) {
					
					String agent_No = APIResultCSVFile_record.get(0);
					if (agent_No.isEmpty()) {
						agent_No = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Agent_No: inContact API result CSV to BigQuery result validation_AgentList: ", agent_No, row.get("AgentNo").getValue());
					String team_No = APIResultCSVFile_record.get(1);
					if (team_No.isEmpty()) {
						team_No = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Team_No: inContact API result CSV to BigQuery result validation_AgentList: ", team_No, row.get("TeamNo").getValue());
					String first_Name = APIResultCSVFile_record.get(2);
					if (first_Name.isEmpty()) {
						first_Name = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> First_Name: inContact API result CSV to BigQuery result validation_AgentList: ", first_Name, row.get("FirstName").getValue());
					String last_Name = APIResultCSVFile_record.get(3);
					if (last_Name.isEmpty()) {
						last_Name = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Last_Name: inContact API result CSV to BigQuery result validation_AgentList: ", last_Name, row.get("LastName").getValue());
					String email = APIResultCSVFile_record.get(4);
					if (email.isEmpty()) {
						email = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Email: inContact API result CSV to BigQuery result validation_AgentList: ", email, row.get("Email").getValue());
					String mod_Datetime = APIResultCSVFile_record.get(5);
					if (mod_Datetime.isEmpty()) {
						mod_Datetime = null;
					}else {
						SimpleDateFormat source_format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
				        Date date = source_format.parse(mod_Datetime);
				        SimpleDateFormat target_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				        mod_Datetime = target_format.format(date);
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Mod_Datetime: inContact API result CSV to BigQuery result validation_AgentList: ", mod_Datetime, row.get("ModDatetime").getValue());
//					Below assertion is commented as we are not asserting Last login because it gets changed every time
//					String last_Login = APIResultCSVFile_record.get(6);
//					if (last_Login.isEmpty()) {
//						last_Login = null;
//					}
//					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Last_Login: inContact API result CSV to BigQuery result validation_AgentList: ", last_Login, row.get("LastLogin").getValue());
					String status = APIResultCSVFile_record.get(7);
					if (status.isEmpty()) {
						status = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Status: inContact API result CSV to BigQuery result validation_AgentList: ", status, row.get("Status").getValue());
					String reports_To = APIResultCSVFile_record.get(8);
					if (reports_To.isEmpty()) {
						reports_To = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Reports_To: inContact API result CSV to BigQuery result validation_AgentList: ", reports_To, row.get("ReportsTo").getValue());
					String middle_Name = APIResultCSVFile_record.get(9);
					if (middle_Name.isEmpty()) {
						middle_Name = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Middle_Name: inContact API result CSV to BigQuery result validation_AgentList: ", middle_Name, row.get("MiddleName").getValue());
					String username = APIResultCSVFile_record.get(10);
					if (username.isEmpty()) {
						username = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Username: inContact API result CSV to BigQuery result validation_AgentList: ", username, row.get("Username").getValue());
					String time_Zone = APIResultCSVFile_record.get(11);
					if (time_Zone.isEmpty()) {
						time_Zone = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Time_Zone: inContact API result CSV to BigQuery result validation_AgentList: ", time_Zone, row.get("TimeZone").getValue());
					String chat_Timeout = APIResultCSVFile_record.get(12);
					if (chat_Timeout.isEmpty()) {
						chat_Timeout = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Chat_Timeout: inContact API result CSV to BigQuery result validation_AgentList: ", chat_Timeout, row.get("ChatTimeout").getValue());
					String phone_Timeout = APIResultCSVFile_record.get(13);
					if (phone_Timeout.isEmpty()) {
						phone_Timeout = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Phone_Timeout: inContact API result CSV to BigQuery result validation_AgentList: ", phone_Timeout, row.get("PhoneTimeout").getValue());
					String voice_Timeout = APIResultCSVFile_record.get(14);
					if (voice_Timeout.isEmpty()) {
						voice_Timeout = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Voice_Timeout: inContact API result CSV to BigQuery result validation_AgentList: ", voice_Timeout, row.get("VoiceTimeout").getValue());
					String location = APIResultCSVFile_record.get(15);
					if (location.isEmpty()) {
						location = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Location: inContact API result CSV to BigQuery result validation_AgentList: ", location, row.get("Location").getValue());
					String hire_Date = APIResultCSVFile_record.get(16);
					if (hire_Date.isEmpty()) {
						hire_Date = null;
					}else {
						SimpleDateFormat source_format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
				        Date date = source_format.parse(hire_Date);
				        SimpleDateFormat target_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				        hire_Date = target_format.format(date);
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Hire_Date: inContact API result CSV to BigQuery result validation_AgentList: ", hire_Date, row.get("HireDate").getValue());
					String termination_Date = APIResultCSVFile_record.get(17);
					if (termination_Date.isEmpty()) {
						termination_Date = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Termination_Date: inContact API result CSV to BigQuery result validation_AgentList: ", termination_Date, row.get("TerminationDate").getValue());
					String hourly_Cost = APIResultCSVFile_record.get(18);
					if (hourly_Cost.isEmpty()) {
						hourly_Cost = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Hourly_Cost: inContact API result CSV to BigQuery result validation_AgentList: ", hourly_Cost, row.get("HourlyCost").getValue());
					String rehire_Status = APIResultCSVFile_record.get(19);
					if (rehire_Status.isEmpty()) {
						rehire_Status = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> : inContact API result CSV to BigQuery result validation_AgentList: ", rehire_Status, row.get("RehireStatus").getValue());
					String employment_Type = APIResultCSVFile_record.get(20);
					if (employment_Type.isEmpty()) {
						employment_Type = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Employment_Type: inContact API result CSV to BigQuery result validation_AgentList: ", employment_Type, row.get("EmploymentType").getValue());
					String referral = APIResultCSVFile_record.get(21);
					if (referral.isEmpty()) {
						referral = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Referral: inContact API result CSV to BigQuery result validation_AgentList: ", referral, row.get("Referral").getValue());
					String at_Home_Worker = APIResultCSVFile_record.get(22);
					if (at_Home_Worker.isEmpty()) {
						at_Home_Worker = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> At_Home_Worker: inContact API result CSV to BigQuery result validation_AgentList: ", at_Home_Worker, row.get("AtHomeWorker").getValue());
					String hiring_Source = APIResultCSVFile_record.get(23);
					if (hiring_Source.isEmpty()) {
						hiring_Source = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Hiring_Source: inContact API result CSV to BigQuery result validation_AgentList: ", hiring_Source, row.get("HiringSource").getValue());
					String ntLoginName = APIResultCSVFile_record.get(24);
					if (ntLoginName.isEmpty()) {
						ntLoginName = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> NtLoginName: inContact API result CSV to BigQuery result validation_AgentList: ", ntLoginName, row.get("NtLoginName").getValue());
					String custom1 = APIResultCSVFile_record.get(25);
					if (custom1.isEmpty()) {
						custom1 = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Custom1: inContact API result CSV to BigQuery result validation_AgentList: ", custom1, row.get("Custom1").getValue());
					String custom2 = APIResultCSVFile_record.get(26);
					if (custom2.isEmpty()) {
						custom2 = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Custom2: inContact API result CSV to BigQuery result validation_AgentList: ", custom2, row.get("Custom2").getValue());
					String custom3 = APIResultCSVFile_record.get(27);
					if (custom3.isEmpty()) {
						custom3 = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Custom3: inContact API result CSV to BigQuery result validation_AgentList: ", custom3, row.get("Custom3").getValue());
					String custom4 = APIResultCSVFile_record.get(28);
					if (custom4.isEmpty()) {
						custom4 = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Custom4: inContact API result CSV to BigQuery result validation_AgentList: ", custom4, row.get("Custom4").getValue());
					String custom5 = APIResultCSVFile_record.get(29);
					if (custom5.isEmpty()) {
						custom5 = null;
					}
					Assert.assertEquals("Agent Number: "+AgentNo_from_API_result+" --> Custom5: inContact API result CSV to BigQuery result validation_AgentList: ", custom5, row.get("Custom5").getValue());
				}
				break;
			}
	    }
	    System.out.println("Completed End to End assertion between inContact API and BigQuery results successfully without any errors for "+desired_rows_count+" records.");
	}
	
	@And("^assert all column values for count of '(.*)' rows between BigQuery and SFTP CSV file '(.*)' for Inmoment Returns$")
	public void assert_all_column_values_for_count_between_BigQuery_and_SFTP_CSV_file_records_for_Inmoment_Returns(Integer rows_count,String csv_file_path) throws IOException, InterruptedException, ParseException {
		
		//ASSERTION WITH USER DEFINER COUNT OF RECORDS
		assert_all_column_values_for_count_of_rows_between_BigQuery_and_SFTP_CSV_file_for_Inmoment_Returns(rows_count, csv_file_path);
	}
	
	@And("^assert all column values for all rows between BigQuery and SFTP CSV file '(.*)' for Inmoment Returns$")
	public void assert_all_column_values_for_all_rows_between_BigQuery_and_SFTP_CSV_file_records_for_Inmoment_Returns(String csv_file_path) throws IOException, InterruptedException, ParseException {
		
		if (csv_file_path.startsWith("$$")) {
			csv_file_path = HashMapContainer.get(csv_file_path);
		}
		// TO FIND RECORDS COUNT IN CSV FILE
		BufferedReader bufferedReader = new BufferedReader(new FileReader(csv_file_path));
		int CSV_file_record_count = 0;
		while (bufferedReader.readLine() != null) {
			CSV_file_record_count++;
		}
		CSV_file_record_count = CSV_file_record_count - 2;
		bufferedReader.close();

		// ASSERTION FOR ALL THE RECORDS PRESENT IN CSV FILE
		assert_all_column_values_for_count_of_rows_between_BigQuery_and_SFTP_CSV_file_for_Inmoment_Returns(CSV_file_record_count, csv_file_path);
	}
	
	public void assert_all_column_values_for_count_of_rows_between_BigQuery_and_SFTP_CSV_file_for_Inmoment_Returns(Integer desired_rows_count,String SFTP_csv_file_path) throws IOException, InterruptedException, ParseException {
	
		System.out.println("Initiated End to End assertion between SFTP and BigQuery for "+desired_rows_count+" records");
		if (SFTP_csv_file_path.startsWith("$$")) {
			SFTP_csv_file_path = HashMapContainer.get(SFTP_csv_file_path);
		}
		
		BufferedReader bufferedReader_SFTP = new BufferedReader(new FileReader(SFTP_csv_file_path));
	    bufferedReader_SFTP.readLine();
	    bufferedReader_SFTP.readLine();
	    
	    CSVParser parser_SFTP = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames()
				.withDelimiter('|').parse(bufferedReader_SFTP);
	    
	  //RUNNING LOOP FOR EVERY RECORD TO GET DATA FROM BQ AND ASSERT WITH API RESULT CSV FILE
	    for (int i=0; i<desired_rows_count; i++) {
	    	for (CSVRecord SFTPrecord : parser_SFTP) {
	    		
		        String return_order_number = SFTPrecord.get(1);
		        String response_ID_for_query = SFTPrecord.get(51);
				if (response_ID_for_query.isEmpty()) {
					response_ID_for_query = null;
				}else {
					response_ID_for_query = response_ID_for_query.replace(",", "");
				}
	    		
		        String Bquery = "select * from `qa-mfrm-data.mfrm_customer_and_social.inmoment_return_survey` where Return_Order_number = '"+return_order_number+"' and Response_ID = "+response_ID_for_query;
				System.out.println("Query to fetch single record: "+Bquery);
				QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(Bquery)
				        .setUseLegacySql(false)
				        .build();
				JobId jobId = JobId.of(UUID.randomUUID().toString());
				Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
				queryJob = queryJob.waitFor();

				if (queryJob == null) {
					throw new RuntimeException("Job no longer exists");
				} else if (queryJob.getStatus().getError() != null) {
					throw new RuntimeException(queryJob.getStatus().getError().toString());
				}

				BibQuery_result = queryJob.getQueryResults();
				
				for(FieldValueList row : BibQuery_result.iterateAll()) {
				
					String date_of_Service = SFTPrecord.get(0);
					if (date_of_Service.isEmpty()) {
						date_of_Service = null;
					}else {
						Date date=new SimpleDateFormat("M/dd/yy").parse(date_of_Service);
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
						date_of_Service= formatter.format(date);
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Date_of_Service SFTP to BQ validation for Inmoment Returns: ", date_of_Service, row.get("Date_of_Service").getValue());
					String return_Order_Number = SFTPrecord.get(1);
					if (return_Order_Number.isEmpty()) {
						return_Order_Number = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Return_Order_Number SFTP to BQ validation for Inmoment Returns: ", return_Order_Number, row.get("Return_Order_Number").getValue());
					String store = SFTPrecord.get(2);
					if (store.isEmpty()) {
						store = null;
					}
//					Assert.assertTrue("Return_Order_Number: "+return_order_number+" --> Store__ SFTP to BQ validation for Inmoment Returns", store.contains((CharSequence) row.get("Store__").getValue()));
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Store__ SFTP to BQ validation for Inmoment Returns", store, row.get("Store__").getValue());
					String location = SFTPrecord.get(3);
					if (location.isEmpty()) {
						location = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Location SFTP to BQ validation for Inmoment Returns: ", location, row.get("Location").getValue());
					String reasonForReturn = SFTPrecord.get(4);
					if (reasonForReturn.isEmpty()) {
						reasonForReturn = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Reason_for_Return SFTP to BQ validation for Inmoment Returns: ", reasonForReturn, row.get("Reason_for_Return").getValue());
					String area_of_discomfort = SFTPrecord.get(5);
					if (area_of_discomfort.isEmpty()) {
						area_of_discomfort = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Area_of_Discomfort SFTP to BQ validation for Inmoment Returns: ", area_of_discomfort, row.get("Area_of_Discomfort").getValue());
					String howDamaged = SFTPrecord.get(6);
					if (howDamaged.isEmpty()) {
						howDamaged = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> How_Damaged SFTP to BQ validation for Inmoment Returns: ", howDamaged, row.get("How_Damaged").getValue());
					String damaged_Mentioned_at_Delivery = SFTPrecord.get(7);
					if (damaged_Mentioned_at_Delivery.isEmpty()) {
						damaged_Mentioned_at_Delivery = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Damaged_Mentioned_at_Delivery SFTP to BQ validation for Inmoment Returns: ", damaged_Mentioned_at_Delivery, row.get("Damaged_Mentioned_at_Delivery").getValue());
					String was_Product_Sealed = SFTPrecord.get(8);
					if (was_Product_Sealed.isEmpty()) {
						was_Product_Sealed = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Was_Product_Sealed SFTP to BQ validation for Inmoment Returns: ", was_Product_Sealed, row.get("Was_Product_Sealed").getValue());
					String comfortMatchInStoreExperience = SFTPrecord.get(9);
					if (comfortMatchInStoreExperience.isEmpty()) {
						comfortMatchInStoreExperience = null;
					}else if (comfortMatchInStoreExperience.equals("Yes") || comfortMatchInStoreExperience.equals("Y") || comfortMatchInStoreExperience.equals("y")) {
						comfortMatchInStoreExperience = "true";
					}else if (comfortMatchInStoreExperience.equals("No") || comfortMatchInStoreExperience.equals("N") || comfortMatchInStoreExperience.equals("n")) {
						comfortMatchInStoreExperience = "false";
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Comfort_Match_In_Store_Experience SFTP to BQ validation for Inmoment Returns: ", comfortMatchInStoreExperience, row.get("Comfort_Match_In_Store_Experience").getValue());
					String complete_Comfort_Test_Online_Store = SFTPrecord.get(10);
					if (complete_Comfort_Test_Online_Store.isEmpty()) {
						complete_Comfort_Test_Online_Store = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Complete_Comfort_Test_Online_Store SFTP to BQ validation for Inmoment Returns: ", complete_Comfort_Test_Online_Store, row.get("Complete_Comfort_Test_Online_Store").getValue());
					String try_Product_Pre_Purchase = SFTPrecord.get(11);
					if (try_Product_Pre_Purchase.isEmpty()) {
						try_Product_Pre_Purchase = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Try_Product_Pre_Purchase SFTP to BQ validation for Inmoment Returns: ", try_Product_Pre_Purchase, row.get("Try_Product_Pre_Purchase").getValue());
					String not_Match_Expectations = SFTPrecord.get(12);
					if (not_Match_Expectations.isEmpty()) {
						not_Match_Expectations = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Not_Match_Expectations SFTP to BQ validation for Inmoment Returns: ", not_Match_Expectations, row.get("Not_Match_Expectations").getValue());
					String recommend_Comment_SFTP = SFTPrecord.get(13);
					String replaced_recommend_Comment_SFTP = null;
					if (recommend_Comment_SFTP.isEmpty()) {
						recommend_Comment_SFTP = null;
					}else {
						replaced_recommend_Comment_SFTP = recommend_Comment_SFTP.replace("Ã¢â‚¬â„¢", "'")
								.replace("Ã¢â‚¬Å“", "â€œ").replace("Ã¢â‚¬?", "â€�").replace("aâ‚¬â„¢", "'");
					}
					String recommend_Comment_BQ = (String) row.get("Recommend_Comment").getValue();
					if (recommend_Comment_BQ == null) {
						recommend_Comment_BQ = null;
					}else {
						recommend_Comment_BQ = recommend_Comment_BQ.replace("â€™", "'");
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Recommend_Comment SFTP to BQ validation for Inmoment Returns: ", replaced_recommend_Comment_SFTP, recommend_Comment_BQ);
					String address = SFTPrecord.get(14);
					if (address.isEmpty()) {
						address = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Address SFTP to BQ validation for Inmoment Returns: ", address, row.get("Address").getValue());
					String city = SFTPrecord.get(15);
					if (city.isEmpty()) {
						city = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> City SFTP to BQ validation for Inmoment Returns: ", city, row.get("City").getValue());
					String state = SFTPrecord.get(16);
					if (state.isEmpty()) {
						state = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> State SFTP to BQ validation for Inmoment Returns: ", state, row.get("State").getValue());
					String zip_Code_SFTP = SFTPrecord.get(17);
					if (zip_Code_SFTP.isEmpty()) {
						zip_Code_SFTP = null;
					}
					String zip_Code_BQ = (String) row.get("Zip_Code").getValue();
					if (zip_Code_BQ == null) {
						Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Zip_Code SFTP to BQ validation for Inmoment Returns: ", zip_Code_SFTP, zip_Code_BQ);
					}else {
						Assert.assertTrue("Return_Order_Number: "+return_order_number+" --> Zip_Code SFTP to BQ validation for Inmoment Returns", zip_Code_SFTP.contains((CharSequence) zip_Code_BQ));
					}
					
					String store_Name = SFTPrecord.get(18);
					if (store_Name.isEmpty()) {
						store_Name = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Store_Name SFTP to BQ validation for Inmoment Returns: ", store_Name, row.get("Store_Name").getValue());
					String email_Address = SFTPrecord.get(19);
					if (email_Address.isEmpty()) {
						email_Address = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Email_Address SFTP to BQ validation for Inmoment Returns: ", email_Address, row.get("Email_Address").getValue());
					String first_Name = SFTPrecord.get(20);
					if (first_Name.isEmpty()) {
						first_Name = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> First_Name SFTP to BQ validation for Inmoment Returns: ", first_Name, row.get("First_Name").getValue());
					String phone_Number = SFTPrecord.get(21);
					if (phone_Number.isEmpty()) {
						phone_Number = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Phone_Number SFTP to BQ validation for Inmoment Returns: ", phone_Number, row.get("Phone_Number").getValue());
					String last_Name = SFTPrecord.get(22);
					if (last_Name.isEmpty()) {
						last_Name = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Last_Name SFTP to BQ validation for Inmoment Returns: ", last_Name, row.get("Last_Name").getValue());
					String processDateTime_SFTP = SFTPrecord.get(23);
					if (processDateTime_SFTP.isEmpty()) {
						processDateTime_SFTP = null;
					}else {
						SimpleDateFormat currentDateFormat_SFTP = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S");
						processDateTime_SFTP = currentDateFormat_SFTP.parse(processDateTime_SFTP).toString();
					}
					String processDateTime_BQ = row.get("ProcessDateTime").getStringValue();
					if (processDateTime_BQ.isEmpty()) {
						processDateTime_BQ = null;
					}else {
						
						//Below code used to assert including milli seconds but in some rare cases, unable to assert with exact values because zero is getting trimmed
						double db = Double.parseDouble(processDateTime_BQ);
						Date date = new Date((long) (db * 1000L));
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S");
						format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
						processDateTime_BQ = format.format(date);
						
						//Below code will assert excluding milli seconds
						SimpleDateFormat currentDateFormat_BQ = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S");
						processDateTime_BQ = currentDateFormat_BQ.parse(processDateTime_BQ).toString();
					}
					Assert.assertTrue("Return_Order_Number: "+return_order_number+" --> ProcessDateTime SFTP to BQ validation for Inmoment Returns: ", processDateTime_BQ.contains(processDateTime_SFTP));		
					String sales_Person_1 = SFTPrecord.get(24);
					if (sales_Person_1.isEmpty()) {
						sales_Person_1 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Sales_Person_1 SFTP to BQ validation for Inmoment Returns: ", sales_Person_1, row.get("Sales_Person_1").getValue());
					String sales_Person_2 = SFTPrecord.get(25);
					if (sales_Person_2.isEmpty()) {
						sales_Person_2 = null;
					}
					String sales_person2_BQ = (String) row.get("Sales_Person_2").getValue();
					if (sales_person2_BQ == null) {
						Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Sales_Person_2 SFTP to BQ validation for Inmoment Returns: ", sales_Person_2, sales_person2_BQ);
					}else {
						Assert.assertTrue("Return_Order_Number: "+return_order_number+" --> Sales_Person_2 SFTP to BQ validation for Inmoment Returns", sales_Person_2.contains((CharSequence) sales_person2_BQ));
					}
					String product_Name_1 = SFTPrecord.get(26);
					if (product_Name_1.isEmpty()) {
						product_Name_1 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Product_Name_1 SFTP to BQ validation for Inmoment Returns: ", product_Name_1, row.get("Product_Name_1").getValue());
					String product_Variant_1 = SFTPrecord.get(27);
					if (product_Variant_1.isEmpty()) {
						product_Variant_1 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Product_Variant_1 SFTP to BQ validation for Inmoment Returns: ", product_Variant_1, row.get("Product_Variant_1").getValue());
					String itemNum_1 = SFTPrecord.get(28);
					if (itemNum_1.isEmpty()) {
						itemNum_1 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> ItemNum_1 SFTP to BQ validation for Inmoment Returns: ", itemNum_1, row.get("ItemNum_1").getValue());
					String product_Description_1 = SFTPrecord.get(29);
					if (product_Description_1.isEmpty()) {
						product_Description_1 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Product_Description_1 SFTP to BQ validation for Inmoment Returns: ", product_Description_1, row.get("Product_Description_1").getValue());
					String size_1 = SFTPrecord.get(30);
					if (size_1.isEmpty()) {
						size_1 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Size_1 SFTP to BQ validation for Inmoment Returns: ", size_1, row.get("Size_1").getValue());
					String qty_1 = SFTPrecord.get(31);
					if (qty_1.isEmpty()) {
						qty_1 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Qty_1 SFTP to BQ validation for Inmoment Returns: ", qty_1, row.get("Qty_1").getValue());
					String return_Price_1 = SFTPrecord.get(32);
					if (return_Price_1.isEmpty()) {
						return_Price_1 = null;
					}else {
						Float f = Float.parseFloat(return_Price_1);
						return_Price_1 = f.toString();
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Return_Price_1 SFTP to BQ validation for Inmoment Returns: ", return_Price_1, row.get("Return_Price_1").getValue());
					String product_Category_1 = SFTPrecord.get(33);
					if (product_Category_1.isEmpty()) {
						product_Category_1 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Product_Category_1 SFTP to BQ validation for Inmoment Returns: ", product_Category_1, row.get("Product_Category_1").getValue());
					String product_Name_2 = SFTPrecord.get(34);
					if (product_Name_2.isEmpty()) {
						product_Name_2 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Product_Name_2 SFTP to BQ validation for Inmoment Returns: ", product_Name_2, row.get("Product_Name_2").getValue());
					String product_Variant_2 = SFTPrecord.get(35);
					if (product_Variant_2.isEmpty()) {
						product_Variant_2 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Product_Variant_2 SFTP to BQ validation for Inmoment Returns: ", product_Variant_2, row.get("Product_Variant_2").getValue());
					String itemNum_2 = SFTPrecord.get(36);
					if (itemNum_2.isEmpty()) {
						itemNum_2 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> ItemNum_2 SFTP to BQ validation for Inmoment Returns: ", itemNum_2, row.get("ItemNum_2").getValue());
					String product_Description_2 = SFTPrecord.get(37);
					if (product_Description_2.isEmpty()) {
						product_Description_2 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Product_Description_2 SFTP to BQ validation for Inmoment Returns: ", product_Description_2, row.get("Product_Description_2").getValue());
					String size_2 = SFTPrecord.get(38);
					if (size_2.isEmpty()) {
						size_2 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Size_2 SFTP to BQ validation for Inmoment Returns: ", size_2, row.get("Size_2").getValue());
					String qty_2 = SFTPrecord.get(39);
					if (qty_2.isEmpty()) {
						qty_2 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Qty_2 SFTP to BQ validation for Inmoment Returns: ", qty_2, row.get("Qty_2").getValue());
					String return_Price_2 = SFTPrecord.get(40);
					if (return_Price_2.isEmpty()) {
						return_Price_2 = null;
					}else {
						double return_parse2 = Double.parseDouble(return_Price_2);
						return_Price_2 = Double.toString(return_parse2);
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Return_Price_2 SFTP to BQ validation for Inmoment Returns: ", return_Price_2, row.get("Return_Price_2").getValue());
					String product_Category_2 = SFTPrecord.get(41);
					if (product_Category_2.isEmpty()) {
						product_Category_2 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Product_Category_2 SFTP to BQ validation for Inmoment Returns: ", product_Category_2, row.get("Product_Category_2").getValue());
					String product_Name_3 = SFTPrecord.get(42);
					if (product_Name_3.isEmpty()) {
						product_Name_3 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Product_Name_3 SFTP to BQ validation for Inmoment Returns: ", product_Name_3, row.get("Product_Name_3").getValue());
					String product_Variant_3 = SFTPrecord.get(43);
					if (product_Variant_3.isEmpty()) {
						product_Variant_3 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Product_Variant_3 SFTP to BQ validation for Inmoment Returns: ", product_Variant_3, row.get("Product_Variant_3").getValue());
					String itemNum_3 = SFTPrecord.get(44);
					if (itemNum_3.isEmpty()) {
						itemNum_3 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> ItemNum_3 SFTP to BQ validation for Inmoment Returns: ", itemNum_3, row.get("ItemNum_3").getValue());
					String product_Description_3 = SFTPrecord.get(45);
					if (product_Description_3.isEmpty()) {
						product_Description_3 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Product_Description_3 SFTP to BQ validation for Inmoment Returns: ", product_Description_3, row.get("Product_Description_3").getValue());
					String Size_3 = SFTPrecord.get(46);
					if (Size_3.isEmpty()) {
						Size_3 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Size_3 SFTP to BQ validation for Inmoment Returns: ", Size_3, row.get("Size_3").getValue());
					String qty_3 = SFTPrecord.get(47);
					if (qty_3.isEmpty()) {
						qty_3 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Qty_3 SFTP to BQ validation for Inmoment Returns: ", qty_3, row.get("Qty_3").getValue());
					String return_Price_3 = SFTPrecord.get(48);
					if (return_Price_3.isEmpty()) {
						return_Price_3 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Return_Price_3 SFTP to BQ validation for Inmoment Returns: ", return_Price_3, row.get("Return_Price_3").getValue());
					String product_Category_3 = SFTPrecord.get(49);
					if (product_Category_3.isEmpty()) {
						product_Category_3 = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Product_Category_3 SFTP to BQ validation for Inmoment Returns: ", product_Category_3, row.get("Product_Category_3").getValue());
					String mode = SFTPrecord.get(50);
					if (mode.isEmpty()) {
						mode = null;
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Mode SFTP to BQ validation for Inmoment Returns: ", mode, row.get("Mode").getValue());
					String response_ID = SFTPrecord.get(51);
					if (response_ID.isEmpty()) {
						response_ID = null;
					}else {
						response_ID = response_ID.replace(",", "");
					}
					Assert.assertEquals("Return_Order_Number: "+return_order_number+" --> Response_ID SFTP to BQ validation for Inmoment Returns: ", response_ID, row.get("Response_ID").getValue());
				}
		        break;
	    	}
	    }
	    bufferedReader_SFTP.close();
	    System.out.println("Completed End to End assertion between SFTP and BigQuery records successfully without any errors for "+desired_rows_count+" records.");
	}
	
	@Given("^Connect to GCS Bucket with project ID '(.*)'$")
	public void Connect_to_GCS_Bucket_with_project_ID_and_Key_file(String project_ID) throws FileNotFoundException, IOException {
		
		switch(project_ID) {
			case "qa-mfrm-data":
				GCS_storage = StorageOptions.newBuilder().setProjectId(project_ID)
				.setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream(System.getProperty("user.dir")+"/src/test/java/com/Resources/svc-devops@qa-mfrm-data.iam.gserviceaccount.com.json")))
				.build().getService();
				break;
				
			case "dev-mfrm-data":
				GCS_storage = StorageOptions.newBuilder().setProjectId(project_ID)
				.setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream(System.getProperty("user.dir")+"/src/test/java/com/Resources/svc-devops@dev-mfrm-data.iam.gserviceaccount.com.json")))
				.build().getService();
				break;
		}
	}
	
	@Then("^for '(.*)', open bucket '(.*)' and Download CSV file for the date '(.*)' to the local path '(.*)'$")
	public void open_bucket_and_Download_CSV_file_for_the_date_to_the_local_path(String flow_name, String bucket_name, String desired_date, String file_save_path) {
		
		Bucket bucket = GCS_storage.get(bucket_name);
		Page<Blob> objects = bucket.list();

		if (desired_date.startsWith("$$")) {
			desired_date = HashMapContainer.get(desired_date);
		}

		//CREATING PROFIX TO CHECK AND DOWNLOAD GCP FILES
		String desiredCSV = null;
		switch (flow_name) {
		case "WithHold":
			desiredCSV = "GCPInbound/Incontact/incontact-comp-withhold-report" + desired_date;
			break;
		case "Expanded":
			desiredCSV = "GCPInbound/Incontact/incontact-comp-expanded-report" + desired_date;
			break;
		case "Agentlist":
			desiredCSV = "GCPInbound/Incontact/incontact-comp-agentlist-extended-report" + desired_date;
			break;
		case "Inmoment Returns Report":
			desiredCSV = "GCPInbound/InMoment/ReturnSurvey/Mattress Firm Returns - Raw Data " + desired_date;
			break;
		}
		
		//FINDING DESIRED FILE AND DOWNLOAD TO THE SPECIFIED PATH
		String file_download_status = null;
		for (Blob object : objects.iterateAll()) {
			String objname = object.getName();
			if (objname.contains(desiredCSV)) {
				System.out.println("File name prefix: " + desiredCSV);
				System.out.println("File name matching with the prefix and to be downloaded: " + objname);
				Path path = Paths.get(file_save_path + "\\" + objname.replace("/", "_").replace(":", "-"));
				object.downloadTo(path);
				file_download_status = "Downloaded";
				String GCS_downloaded_CSV_file_path = file_save_path + "\\"+ objname.replace("/", "_").replace(":", "-");
				HashMapContainer.add("$$GCSDownloadedCSVfilePath", GCS_downloaded_CSV_file_path);
			}
		}
//		if (file_download_status.equals("Downloaded")) {
		if (file_download_status.isEmpty()) {
//			System.out.println("GCS Bucket File downloaded successfully.");
			System.out.println("No file found for given date.");
		} else {
//			System.out.println("No file found for given date.");
			System.out.println("GCS Bucket File downloaded successfully.");
		}
	}

	@Then("^assert end to end data between source SFTP CSV file '(.*)' and target SQL stage table result CSV file '(.*)' for GradedSimulation$")
	public void assert_end_to_end_data_between_source_SFTP_CSV_file_and_target_SQL_result_CSV_file_for_GradedSimulation(String SFTP_source_CSV_file_path, String SQL_target_CSV_file_path) throws CsvValidationException, IOException {
		
		List<Saba_SFTP_SQL_GradedSimulationsNAP_pojo> saba_SFTP_SQL_GradedSimulationsNAP_pojo_Arraylist = new ArrayList<Saba_SFTP_SQL_GradedSimulationsNAP_pojo>();
//		String path_SFTP = "F:\\Manoj Thota\\Saba\\Test data_Curriculla\\Curricula Reports_20200819_163405.csv";
//		String path_SQL = "F:\\Manoj Thota\\Saba\\Test data_Curriculla\\SQLCurriculla_STG_test.csv";
		
		if (SFTP_source_CSV_file_path.startsWith("$$")) {
			SFTP_source_CSV_file_path = HashMapContainer.get(SFTP_source_CSV_file_path);
		}
		if (SQL_target_CSV_file_path.startsWith("$$")) {
			SQL_target_CSV_file_path = HashMapContainer.get(SQL_target_CSV_file_path);
		}
		
		BufferedReader br_source = new BufferedReader(new FileReader(SFTP_source_CSV_file_path));
		String row_source;
		br_source.readLine();
		br_source.readLine();
		
		while ((row_source = br_source.readLine()) != null) {
			String[] columns_source_SFTP = row_source.split(",");
			Saba_SFTP_SQL_GradedSimulationsNAP_pojo saba_SFTP_SQL_GradedSimulationsNAP_pojo = new Saba_SFTP_SQL_GradedSimulationsNAP_pojo();
			saba_SFTP_SQL_GradedSimulationsNAP_pojo.setTranscript_Internal_ID(columns_source_SFTP[0]);
			saba_SFTP_SQL_GradedSimulationsNAP_pojo.setClass_Name(columns_source_SFTP[1]);
			saba_SFTP_SQL_GradedSimulationsNAP_pojo.setAssessment_Attempt_Number(columns_source_SFTP[2]);
			saba_SFTP_SQL_GradedSimulationsNAP_pojo.setAssessment_Version(columns_source_SFTP[3]);
			saba_SFTP_SQL_GradedSimulationsNAP_pojo.setLearner_Module_Attempts(columns_source_SFTP[4]);
			saba_SFTP_SQL_GradedSimulationsNAP_pojo.setAssessment_Completion_Date(columns_source_SFTP[5]);
			saba_SFTP_SQL_GradedSimulationsNAP_pojo.setClass_ID(columns_source_SFTP[6]);
			saba_SFTP_SQL_GradedSimulationsNAP_pojo.setPerson_Full_Name(columns_source_SFTP[7]);
			saba_SFTP_SQL_GradedSimulationsNAP_pojo.setManager_Full_Name(columns_source_SFTP[8]);
			saba_SFTP_SQL_GradedSimulationsNAP_pojo.setPerson_Username(columns_source_SFTP[9]);
			saba_SFTP_SQL_GradedSimulationsNAP_pojo.setPerson_Status(columns_source_SFTP[10]);
			saba_SFTP_SQL_GradedSimulationsNAP_pojo.setAssessment_Question_Response(columns_source_SFTP[11]);
			

			saba_SFTP_SQL_GradedSimulationsNAP_pojo_Arraylist.add(saba_SFTP_SQL_GradedSimulationsNAP_pojo);
		}
		br_source.close();
		
		BufferedReader br_target = new BufferedReader(new FileReader(SQL_target_CSV_file_path));
	    String row_target;
	    br_target.readLine();
	    while ((row_target = br_target.readLine()) != null) {
	        String[] columns_target_SQL = row_target.split(",");
	        String Transcript_Internal_ID = columns_target_SQL[0];
	        String Class_Name = columns_target_SQL[1];
	        String Assessment_Attempt_Number = columns_target_SQL[2];
	        String Assessment_Version = columns_target_SQL[3];
	     
	        
	        for (Saba_SFTP_SQL_GradedSimulationsNAP_pojo y: saba_SFTP_SQL_GradedSimulationsNAP_pojo_Arraylist) {
	        	
	        	if(y.getTranscript_Internal_ID().contains(Transcript_Internal_ID)&&
	        			y.getClass_Name().contains(Class_Name)&&
	        			y.getAssessment_Attempt_Number().contains(Assessment_Attempt_Number)&&
	        			y.getAssessment_Version().contains(Assessment_Version)) {
	        		
			        System.out.println("---------y.getTranscript_Internal_ID()---------"+y.getTranscript_Internal_ID());	
	        		int listItemIndex = saba_SFTP_SQL_GradedSimulationsNAP_pojo_Arraylist.indexOf(y);
			        		Saba_SFTP_SQL_GradedSimulationsNAP_pojo arraylist_row = saba_SFTP_SQL_GradedSimulationsNAP_pojo_Arraylist.get(listItemIndex);
			        		
			        		Assert.assertEquals("Transcript_Internal_ID: "+columns_target_SQL[0], arraylist_row.getTranscript_Internal_ID(), columns_target_SQL[0]);
			        		Assert.assertEquals("Transcript_Internal_ID: "+columns_target_SQL[0], arraylist_row.getClass_Name(), columns_target_SQL[1]);
			        		Assert.assertEquals("Transcript_Internal_ID: "+columns_target_SQL[0], arraylist_row.getAssessment_Attempt_Number(), columns_target_SQL[2]);
			        		Assert.assertEquals("Transcript_Internal_ID: "+columns_target_SQL[0], arraylist_row.getAssessment_Version(), columns_target_SQL[3]);
			        		Assert.assertEquals("Transcript_Internal_ID: "+columns_target_SQL[0], arraylist_row.getLearner_Module_Attempts(), columns_target_SQL[4]);
			        		Assert.assertEquals("Transcript_Internal_ID: "+columns_target_SQL[0], arraylist_row.getAssessment_Completion_Date(), columns_target_SQL[5]);
			        		Assert.assertEquals("Transcript_Internal_ID: "+columns_target_SQL[0], arraylist_row.getClass_ID(), columns_target_SQL[6]);
			        		Assert.assertEquals("Transcript_Internal_ID: "+columns_target_SQL[0], arraylist_row.getPerson_Full_Name(), columns_target_SQL[7]);
			        		Assert.assertEquals("Transcript_Internal_ID: "+columns_target_SQL[0], arraylist_row.getManager_Full_Name(), columns_target_SQL[8]);
			        		Assert.assertEquals("Transcript_Internal_ID: "+columns_target_SQL[0], arraylist_row.getPerson_Username(), columns_target_SQL[9]);
			        		Assert.assertEquals("Transcript_Internal_ID: "+columns_target_SQL[0], arraylist_row.getPerson_Status(), columns_target_SQL[10]);
			        		Assert.assertEquals("Transcript_Internal_ID: "+columns_target_SQL[0], arraylist_row.getAssessment_Question_Response(), columns_target_SQL[11]);
			        		break;
	        	}
	        }
	    }
		br_target.close();
		
		System.out.println("End to End assertion between SFTP and SQL CSV files completed successfully without any errors.");
		
		
}
	@And("^assert end to end data for '(.*)' records between source as SQL stage table result CSV file '(.*)' and target as SQL master table for Graded Simulation$")
	public void assert_end_to_end_data_for_Count_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_For_Graded_Simulation(Integer desired_count, String SQL_CSV_file_path) throws IOException{
		
		assert_end_to_end_data_for_Count_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_For_Graded_Simulation_with_desired_count(desired_count, SQL_CSV_file_path);
	}

	public void assert_end_to_end_data_for_Count_between_source_as_SQL_stage_table_result_CSV_file_and_target_as_SQL_master_table_For_Graded_Simulation_with_desired_count(Integer desired_count, String SQL_CSV_file_path) throws IOException{	
		
//		String SQL_file_path = HashMapContainer.get(SQL_CSV_file_path);
//		String SQL_file_path = "F:\\Manoj Thota\\Saba\\Test data_Curriculla\\SQLCurriculla_STG.csv";
		
		if (SQL_CSV_file_path.startsWith("$$")) {
			SQL_CSV_file_path = HashMapContainer.get(SQL_CSV_file_path);
		}
		
		BufferedReader bufferedReader_SQL = new BufferedReader(new FileReader(SQL_CSV_file_path));
	    String SQL_row;
	    bufferedReader_SQL.readLine();
	    for (int i=0; i<desired_count; i++) {
	    	while ((SQL_row = bufferedReader_SQL.readLine()) != null) {
		        String[] SQL_columns = SQL_row.split(",");
		        String SQL_Transcript_Internal_ID = SQL_columns[0];
		        String SQL_Class_Name = SQL_columns[1];
		        String SQL_Assessment_Attempt_Number = SQL_columns[2];
		        String SQL_Assessment_Version = SQL_columns[3];
		        String SQL_Person_Full_Name = SQL_columns[7];
		        
		        String sql_query = ("SELECT  * FROM [InternalProjects].[dbo].[SAB_GradedSimulationsNAP] Where Transcript_Internal_ID='"+SQL_Transcript_Internal_ID+"'and Class_Name='"+SQL_Class_Name+"' and Assessment_Attempt_Number='"+SQL_Assessment_Attempt_Number+"' and Assessment_Version='"+SQL_Assessment_Version+"'").replace("\"", "");
		        System.out.println(sql_query);
		        Utilities util = new Utilities();
		        Map<String, String> ResultSet = util.executeSQLQuery("jdbc:sqlserver://qdbsqltab01.mfrm.com:1433", "Mule-QA", "test@123", sql_query);
				Boolean SQL_result_set = ResultSet.isEmpty();
		        if (!SQL_result_set) {
			        Assert.assertEquals("Person username: "+SQL_Person_Full_Name, SQL_columns[0].replace("\"", ""), ResultSet.get("Transcript_Internal_ID"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Full_Name, SQL_columns[1].replace("\"", ""), ResultSet.get("Class_Name"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Full_Name, SQL_columns[2].replace("\"", ""), ResultSet.get("Assessment_Attempt_Number"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Full_Name, SQL_columns[3].replace("\"", ""), ResultSet.get("Assessment_Version"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Full_Name, SQL_columns[4].replace("\"", ""), ResultSet.get("Learner_Module_Attempts"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Full_Name, SQL_columns[5].replace("\"", ""), ResultSet.get("Assessment_Completion_Date"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Full_Name, SQL_columns[6].replace("\"", ""), ResultSet.get("Class_ID"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Full_Name, SQL_columns[7].replace("\"", ""), ResultSet.get("Person_Full_Name"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Full_Name, SQL_columns[8].replace("\"", ""), ResultSet.get("Manager_Full_Name"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Full_Name, SQL_columns[9].replace("\"", ""), ResultSet.get("Person_Username"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Full_Name, SQL_columns[10].replace("\"", ""), ResultSet.get("Person_Status"));
	        		Assert.assertEquals("Person username: "+SQL_Person_Full_Name, SQL_columns[11].replace("\"", ""), ResultSet.get("Assessment_Question_Response"));
		        
		        }
	        		
	        		else {
		        	System.out.println("No data present in SQL master table with combination of Transcript_Internal_ID: "+SQL_Transcript_Internal_ID.replace("\"", "'")+", SQL_Class_Name: "+SQL_Class_Name.replace("\"", "'")+" and SQL_Assessment_Attempt_Number: "+SQL_Assessment_Attempt_Number.replace("\"", "'")+"and SQL_Assessment_Version:"+SQL_Assessment_Version.replace("\"", "'"));
		        	
	        		}
		    }
	    }
	    bufferedReader_SQL.close();
	    System.out.println("End to End assertion between SQL stage and master table completed successfully without any errors.");
	}
	@Given("fetch service run for the flow {string}")
	public void fetch_service_run_for_the_flow(String flowName) {
		String sql_query = "SELECT [LAST_RUNTIME] FROM [SFDC_MuleSoftDB].[dbo].[SERVICE_RUN] WHERE SUBSCRIBER_NAME='"
				+ flowName + "'";
		System.out.println(sql_query);
		Utilities util = new Utilities();
		Map<String, String> ResultSet = util.executeSQLQuery("jdbc:sqlserver://sdbsqltab01.mfrm.com:1433", "MuleS-Dev",
				"test@1235", sql_query);
		System.out.println(ResultSet.get("LAST_RUNTIME"));
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        Assert.assertTrue("***** Job not triggered today for the "+flowName+" *****",ResultSet.get("LAST_RUNTIME").contains(dateFormat.format(date).toString()));

	}


	
	@When("^I validate email notification received with body contains '(.*)' in '(.*)' position of email inbox$")
    public void I_validate_email_notification_received_with_body_contains_in_position_of_email_inbox(String expected_subject, int email_position) {
		
		//UTILITY TO VALIDATE EMAIL BODY ALONG WITH EMAIL POSITION IN THE INBOX
		ReadingSuccessEmail.getemailRead(expected_subject,email_position);
    }
	
//	   private static Thread t;
//	   private static String threadName;
//	   
//	   public static void RunnableDemo( String name) {
//	      threadName = name;
//	      System.out.println("Creating " +  threadName );
//	   }
//	   
//	   public void run() {
//		      System.out.println("Running " +  threadName );
//		      try {
//		         for(int i = 4; i > 0; i--) {
//		            System.out.println("Thread: " + threadName + ", " + i);
//		            // Let the thread sleep for a while.
//		            Thread.sleep(50);
//		         }
//		      } catch (InterruptedException e) {
//		         System.out.println("Thread " +  threadName + " interrupted.");
//		      }
//		      System.out.println("Thread " +  threadName + " exiting.");
//		   }
//		   
//		   public static void start () {
//		      System.out.println("Starting " +  threadName );
//		      if (t == null) {
//		         t = new Thread (threadName);
//		         t.start ();
//		      }
//		   }
	
	public void CSV_and_BQ_records_assertion_MultiThread(String filePath, Integer startCount,Integer endCount) throws IOException, InterruptedException {
		int recordsToRun = endCount-startCount+1;
		System.out.println("Initiated End to End data validation between BigQuery and InContact API result CSV file for "+recordsToRun+" records");
		if (filePath.startsWith("$$")) {
			filePath = HashMapContainer.get(filePath);
		}
		
		BufferedReader br_APIResultCSVFile = new BufferedReader(new FileReader(filePath));
//		br_APIResultCSVFile.readLine();
		CSVParser parser_APIResultCSVFile = CSVFormat.DEFAULT.withQuote('"').withAllowMissingColumnNames().withDelimiter(',')
				.parse(br_APIResultCSVFile);
		
		for (int i=0; i<startCount-1; i++)
		{
			br_APIResultCSVFile.readLine();
		}
		String fileContent;
		for (int j=0; j<=recordsToRun; j++) {
			while ((fileContent = br_APIResultCSVFile.readLine()) != null) {
		
//		for (int i=0; i<desired_rows_count; i++) {
			for (CSVRecord APIResultCSVFile_record : parser_APIResultCSVFile) {
				
//				CSVRecord APIResultCSVFile_record = (CSVRecord) parser_APIResultCSVFile;
			
				String rowValue_ContactID = APIResultCSVFile_record.get(0);
//				String Bquery = "select * from `qa-mfrm-data.mfrm_customer_and_social.incontact_comp_with_hold_call` WHERE  StartDate = '"+date_provided_in_BQ_query+"' and ContactId = '"+rowValue_ContactID+"' LIMIT 1";
				
				//below query is for testing purpose and above query should be used for automation
				String Bquery = "select * from `qa-mfrm-data.mfrm_customer_and_social.incontact_comp_with_hold_call` WHERE  StartDate = '2020-09-28' and ContactId = '"+rowValue_ContactID+"' LIMIT 1";
				
				System.out.println("Query to fetch single record: "+Bquery);
				QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(Bquery)
						.setUseLegacySql(false)
						.build();
				JobId jobId = JobId.of(UUID.randomUUID().toString());
				Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
				queryJob = queryJob.waitFor();

				if (queryJob == null) {
					throw new RuntimeException("Job no longer exists");
				} else if (queryJob.getStatus().getError() != null) {
					throw new RuntimeException(queryJob.getStatus().getError().toString());
				}
	
				BibQuery_result = queryJob.getQueryResults();
			
				for(FieldValueList row : BibQuery_result.iterateAll()) {
					
					System.out.println(row.get("ContactId").getStringValue());
					Assert.assertEquals("(WithHold) Contact ID validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(0), row.get("ContactId").getStringValue());
					Assert.assertEquals("(WithHold) Master Contact ID validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(1), row.get("MasterContactId").getStringValue());
//					System.out.println(row.get("ContactCode").getStringValue());
					Assert.assertEquals("(WithHold) Contact code validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(2), row.get("ContactCode").getStringValue());
					System.out.println(APIResultCSVFile_record.get(3));
					System.out.println(row.get("MediaName").getStringValue());
					Assert.assertEquals("(WithHold) Media name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(3), row.get("MediaName").getStringValue());
					Assert.assertEquals("(WithHold) Contact name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(4), row.get("ContactName").getStringValue());
					Assert.assertEquals("(WithHold) ANI DialNum validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(5), row.get("ANIDialnum").getStringValue());
					Assert.assertEquals("(WithHold) Skill No validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(6), row.get("SkillNo").getStringValue());
					Assert.assertEquals("(WithHold) Skill name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(7), row.get("SkillName").getStringValue());
					Assert.assertEquals("(WithHold) Campaign no validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(8), row.get("CampaignNo").getStringValue());
					Assert.assertEquals("(WithHold) Campaign name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(9), row.get("CampaignName").getStringValue());
					Assert.assertEquals("(WithHold) Agent No validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(10), row.get("AgentNo").getStringValue());
					Assert.assertEquals("(WithHold) Agent Name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(11), row.get("AgentName").getStringValue());
					Assert.assertEquals("(WithHold) Team no validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(12), row.get("TeamNo").getStringValue());
					Assert.assertEquals("(WithHold) Team Name validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(13), row.get("TeamName").getStringValue());
					
					String disposition_code = APIResultCSVFile_record.get(14);
					if(disposition_code.isEmpty()) {
						disposition_code = null;
					}
					Assert.assertEquals("(WithHold) Disposition code validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", disposition_code, row.get("DispositionCode").getValue());
					Assert.assertEquals("(WithHold) SLA validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(15), row.get("SLA").getStringValue());
					
					String[] csv_start_date = APIResultCSVFile_record.get(16).split("/");
					String start_date = csv_start_date[2]+"-"+csv_start_date[0]+"-"+csv_start_date[1];
					
					Assert.assertEquals("(WithHold) Start Date validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", start_date, row.get("StartDate").getStringValue());
					Assert.assertEquals("(WithHold) Start Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(17), row.get("StartTime").getStringValue());
					Assert.assertEquals("(WithHold) PreQueue validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(18), row.get("PreQueue").getStringValue());
					Assert.assertEquals("(WithHold) InQueue validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(19), row.get("InQueue").getStringValue());
					Assert.assertEquals("(WithHold) Agent Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(20), row.get("AgentTime").getStringValue());
					Assert.assertEquals("(WithHold) Post Queue validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(21), row.get("PostQueue").getStringValue());
					Assert.assertEquals("(WithHold) Total Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(22), row.get("TotalTime").getStringValue());
					Assert.assertEquals("(WithHold) Abandon Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(23), row.get("AbandonTime").getStringValue());
					Assert.assertEquals("(WithHold) Routing Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(24), row.get("RoutingTime").getStringValue());
					Assert.assertEquals("(WithHold) Abandon validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(25), row.get("Abandon").getStringValue());
					Assert.assertEquals("(WithHold) Callback Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(26), row.get("CallbackTime").getStringValue());
//					System.out.println(row.get("Logged").getStringValue());
					Assert.assertEquals("(WithHold) Logged validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(27), row.get("Logged").getStringValue());
//					System.out.println(row.get("HoldTime").getStringValue());
					Assert.assertEquals("(WithHold) Hold Time validation between inContact API CSV and BQ for Contact ID: "+rowValue_ContactID+" --> ", APIResultCSVFile_record.get(28), row.get("HoldTime").getStringValue());
				}
				break;
			}break;
			}
		}
		System.out.println("Completed End to End data validation between BigQuery and InContact API result CSV file successfully for "+recordsToRun+" records without any error");
	}
	
	public static void main(String[] args) throws Throwable {
		
//		String DATE_FORMAT = "yyyy-MM-dd'T'hh:mm";
//		LocalDateTime ldt = LocalDateTime.parse(dateInString, DateTimeFormatter.ofPattern(DATE_FORMAT));
//		System.out.println(ldt);
		
//		ZoneId singaporeZoneId = ZoneId.of("UTC");
//        System.out.println("TimeZone : " + singaporeZoneId);
        
//		String dateInString = "2021-04-11T23:59:00";
//        SimpleDateFormat source_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//        Date dd = source_format.parse(dateInString);
//        System.out.println(dd);
//        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
//		format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
//		String processDateTime_BQ = format.format(dd);
//		System.out.println(processDateTime_BQ);
		
		String processDateTime_BQ = "1618117200.0";
		if (processDateTime_BQ.isEmpty()) {
			processDateTime_BQ = null;
		}else {
			
			//Below code used to assert including milli seconds but in some rare cases, unable to assert with exact values because zero is getting trimmed
			double db = Double.parseDouble(processDateTime_BQ);
			Date date = new Date((long) (db * 1000L));
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
			format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
			processDateTime_BQ = format.format(date);
			System.out.println(processDateTime_BQ);
			
			//Below code will assert excluding milli seconds
//			SimpleDateFormat currentDateFormat_BQ = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S");
//			processDateTime_BQ = currentDateFormat_BQ.parse(processDateTime_BQ).toString();
		}
        
//		String filePath1 = "F:\\Manoj Thota\\POC\\Multi threading\\samplecount1.csv";
//		String filePath2 = "F:\\Manoj Thota\\POC\\Multi threading\\samplecount2.csv";
//		String filePath3 = "F:\\Manoj Thota\\POC\\Multi threading\\samplecount3.csv";
//		String filePath4 = "F:\\Manoj Thota\\POC\\Multi threading\\samplecount4.csv";
//		String filePath5 = "F:\\Manoj Thota\\POC\\Multi threading\\samplecount5.csv";
//		String APIResultCSVFilePath = "F:\\Manoj Thota\\POC\\Multi threading\\Data_WithHold_2020_09_29_14_09_44.csv";
//		
//		BufferedReader sourceFile = new BufferedReader(new FileReader(APIResultCSVFilePath));
//		int sourceFileCount = 0;
//		while (sourceFile.readLine() != null) {
//			sourceFileCount++;
//		}
//		sourceFileCount = sourceFileCount-1;
//		System.out.println("Total number of records found: "+sourceFileCount);
//		sourceFile.close();
//		
//		connect_with_BigQuery_with_project_ID_and_Key_file("qa-mfrm-data");
//		
////		sourceFileCount = 201;
//		if (sourceFileCount >=1 && sourceFileCount <= 100) {
//			System.out.println("Number of Threads: 1");
//			RunnableDemo R1 = new RunnableDemo("Thread 1", APIResultCSVFilePath, 1, 100);
//			R1.start();
//		}
//		else if (sourceFileCount >=501 && sourceFileCount <=1000) {
//			System.out.println("Number of Threads: 2");
//			RunnableDemo R1 = new RunnableDemo("Thread 1", APIResultCSVFilePath, 1, 50);
//			RunnableDemo R2 = new RunnableDemo("Thread 2", APIResultCSVFilePath, 51, 100);
//			R1.start();
//			R2.start();
//		}
//		else if (sourceFileCount >=4001 && sourceFileCount <6000) {
//			System.out.println("Number of Threads: 3");
//			RunnableDemo R1 = new RunnableDemo("Thread 1", filePath1, 1, 2000);
//			RunnableDemo R2 = new RunnableDemo("Thread 2", filePath1, 2001, 4000);
//			RunnableDemo R3 = new RunnableDemo("Thread 3", filePath1, 4001, 6000);
//			R1.start();
//			R2.start();
//			R3.start();
//		}else if (sourceFileCount >=6001 && sourceFileCount <=8000) {
//			System.out.println("Number of Threads: 4");
//			RunnableDemo R1 = new RunnableDemo("Thread 1", filePath1, 1, 2000);
//			RunnableDemo R2 = new RunnableDemo("Thread 2", filePath1, 2001, 4000);
//			RunnableDemo R3 = new RunnableDemo("Thread 3", filePath1, 4001, 6000);
//			RunnableDemo R4 = new RunnableDemo("Thread 4", filePath1, 6001, 8000);
//			R1.start();
//			R2.start();
//			R3.start();
//			R4.start();
//		}else if (sourceFileCount >=8001 && sourceFileCount <=10000) {
//			System.out.println("Number of Threads: 5");
//			RunnableDemo R1 = new RunnableDemo("Thread 1", filePath1, 1, 2000);
//			RunnableDemo R2 = new RunnableDemo("Thread 2", filePath2, 2001, 4000);
//			RunnableDemo R3 = new RunnableDemo("Thread 3", filePath3, 4001, 6000);
//			RunnableDemo R4 = new RunnableDemo("Thread 4", filePath4, 6001, 8000);
//			RunnableDemo R5 = new RunnableDemo("Thread 5", filePath5, 8001, 10000);
//			R1.start();
//			R2.start();
//			R3.start();
//			R4.start();
//			R5.start();
//		}
		
//		String report_date ="4-04-21";
//		String month = report_date.substring(2, 4);
////		String[] sd = ss.split("\\|");
//		if (month.startsWith("0")) {
//			report_date = report_date.substring(0,2)+report_date.substring(3, 7);
//			System.out.println("report_date "+report_date);
//		}
		
		
//		RunnableDemo R1 = new RunnableDemo("thread 1");
//		RunnableDemo R2 = new RunnableDemo("thread 2");
//		RunnableDemo R3 = new RunnableDemo("thread 4");
//		RunnableDemo R4 = new RunnableDemo("thread 5");
//		R1.start();
//		R2.start();
//		R3.start();
//		R4.start();
		
//		Thread t1 = new Thread();
//		t1.start();

//		RunnableDemo R2 = new RunnableDemo("Thread 2");
//		R2.start();
		
//		String url=Utilities.getURL("odata_custinfo_uat");
//		ElasticSearchJestUtility.jestClient(url);
//		JestClientFactory factory = new JestClientFactory();
//		factory.setHttpClientConfig(new HttpClientConfig.Builder(url).multiThreaded(true).build());
////		JestClient client = factory.getObject();
////		jud.setClient(client);
//		for(String a : args) {
//			System.out.println("*****"+a);
//		}
		
//		String ss = "2021-04-08T00:30";
//		int dd = Integer.parseInt(ss.substring(14));
//		int df = dd+29;
//		String er = ss.substring(0, 14)+df+":00";
//		System.out.println(er);
//		
//		String we = "MFRM 029163 Crossroads Town Center";
//		System.out.println(we.substring(5, 11)+"*");
		
		
//		String input ="We didn[â€™]t feel";
//        /* From ISO-8859-1 to UTF-8 */
//       String Decryptedvalue = new String(input.getBytes("ISO-8859-1"), "UTF-8");
////      Assert.assertEquals("LeadCustomerName",Decryptedvalue,row.get("LeadCustomerName").getValue());
////		String Decryptedvalue = getUTF8Encoded(input);
//		System.out.println(Decryptedvalue);
	}
//	private final static Charset UTF8_CHARSET = Charset.forName("UTF-8");
//	public static String getUTF8Encoded(String targetString) {
//	    String resultant = "";
//	    try {
//	        return new String(encodeUTF8(targetString), UTF8_CHARSET);
//	    } catch (Exception e) {
//	        e.printStackTrace();
//	        return resultant;
//	    }
//	}
//	private static final byte[] encodeUTF8(String string) {
//	    return string.getBytes(UTF8_CHARSET);
//	}
	
	@Then("^generate date '(.*)' and time '(.*)' in format '(.*)' and store as '(.*)'$")
    public void I_generateDateAndTime_and_Store_as(String dateInput, String timeInput, String format, String variableName) {
        try {
            Utilities util = new Utilities();
            String dateAndTime = util.generateDateAndTime(dateInput, timeInput, format);
            System.out.println(dateAndTime);
            HashMapContainer.add("$$"+variableName, dateAndTime);
        } catch (Exception e) {
        	e.printStackTrace();
            throw new CucumberException(e.getMessage(), e);
        }
    }
	
	@Then("^I generate date '(.*)' in format '(.*)' and store as '(.*)'$")
    public void I_generateDate_and_Store_as(String dateInput, String format, String variableName) {
        try {
            Utilities util = new Utilities();
            String date = util.generateDate(dateInput, format);
            
            HashMapContainer.add("$$"+variableName, date);
        } catch (Exception e) {
        	e.printStackTrace();
            throw new CucumberException(e.getMessage(), e);
        }
    }
	
	@When("Retrive data exists in all the fields from API XML response and store in variable for order header")
	public void XML_APIResponseStoringInVariableForOrderHeader() {
//		Response res = ap.getRes();
		Response res = RestAssuredSteps.getResponse();
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.BALANCE_DUE", "API_BALANCE_DUE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.BRAND", "API_BRAND");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.CO_CD", "API_CO_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.CUST_CD", "API_CUST_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.CUST_CD_STRIPPED", "API_CUST_CD_STRIPPED");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.CUST_NAME", "API_CUST_NAME");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.DEL_CHG", "API_DEL_CHG");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.DEL_DOC_NUM", "API_DEL_DOC_NUM");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.DEL_DOC_NUM_STRIPPED", "API_DEL_DOC_NUM_STRIPPED");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.DELIVERYDATE", "API_DELIVERYDATE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.DO_NOT_CONTACT", "API_DO_NOT_CONTACT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.EMAIL_ADDR", "API_EMAIL_ADDR");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.EMAIL_ADDR_SHIP_TO", "API_EMAIL_ADDR_SHIP_TO");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.ES_MODIFIEDDATETIME", "API_ES_MODIFIEDDATETIME");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.FINAL_DT", "API_FINAL_DT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.FINANCE_AMOUNT", "API_FINANCE_AMOUNT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.MERGED_CUST_CD", "API_MERGED_CUST_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.MFIISRETURNORDER", "API_MFIISRETURNORDER");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.ORD_TP_CD", "API_ORD_TP_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.ORDER_ORDER_NUM", "API_ORDER_ORDER_NUM");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.ORDER_TYPE", "API_ORDER_TYPE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.PAYMENTS", "API_PAYMENTS");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.PCT_OF_SALE1", "API_PCT_OF_SALE1");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.PCT_OF_SALE2", "API_PCT_OF_SALE2");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.PROMO_CODE", "API_PROMO_CODE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.PROMO_CODE_DESCRIPTION", "API_PROMO_CODE_DESCRIPTION");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.PU_DEL_DT", "API_PU_DEL_DT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.RETURNREASONCODEID", "API_RETURNREASONCODEID");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.RETURNSTATUS", "API_RETURNSTATUS");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SALES_PERSON_NAME1", "API_SALES_PERSON_NAME1");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SALES_PERSON_NAME2", "API_SALES_PERSON_NAME2");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SALES_STATUS_DESCRIPTION", "API_SALES_STATUS_DESCRIPTION");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SALESFORCE_LINKID", "API_SALESFORCE_LINKID");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SALESORIGINID", "API_SALESORIGINID");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SALESSTATUS", "API_SALESSTATUS");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SETUP_CHG", "API_SETUP_CHG");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_ADDR1", "API_SHIP_TO_ADDR1");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_ADDR2", "API_SHIP_TO_ADDR2");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_B_PHONE", "API_SHIP_TO_B_PHONE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_B_PHONE1", "API_SHIP_TO_B_PHONE1");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_CITY", "API_SHIP_TO_CITY");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_F_NAME", "API_SHIP_TO_F_NAME");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_H_PHONE", "API_SHIP_TO_H_PHONE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_H_PHONE1", "API_SHIP_TO_H_PHONE1");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_L_NAME", "API_SHIP_TO_L_NAME");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_ST_CD", "API_SHIP_TO_ST_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_ZIP_CD", "API_SHIP_TO_ZIP_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIPCARRIERACCOUNT", "API_SHIPCARRIERACCOUNT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIPCARRIERDELIVERYCONTACT", "API_SHIPCARRIERDELIVERYCONTACT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIPCARRIERDLVTYPE", "API_SHIPCARRIERDLVTYPE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIPCARRIERID", "API_SHIPCARRIERID");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIPCARRIERNAME", "API_SHIPCARRIERNAME");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SO_EMP_SLSP_CD1", "API_SO_EMP_SLSP_CD1");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SO_EMP_SLSP_CD2", "API_SO_EMP_SLSP_CD2");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SO_SEQ_NUM", "API_SO_SEQ_NUM");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SO_STORE_CD", "API_SO_STORE_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SO_WR_DT", "API_SO_WR_DT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.STAT_CD", "API_STAT_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.TAX_CHG", "API_TAX_CHG");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.TOTAL_AMOUNT", "API_TOTAL_AMOUNT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.TOTAL_SALE", "API_TOTAL_SALE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.ULTIMATE_CUST_CD", "API_ULTIMATE_CUST_CD");
	}

	@And("Retrive list of data in all the fields from BQ JSON response and store in variable for order header")
	public void JSON_BQResponseStoringInVariableForOrderHeader() {
		
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.BALANCE_DUE", "DB_BALANCE_DUE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.BRAND", "DB_BRAND");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.CO_CD", "DB_CO_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.CUST_CD", "DB_CUST_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.CUST_CD_STRIPPED", "DB_CUST_CD_STRIPPED");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.CUST_NAME", "DB_CUST_NAME");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.DEL_CHG", "DB_DEL_CHG");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.DEL_DOC_NUM", "DB_DEL_DOC_NUM");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.DEL_DOC_NUM_STRIPPED", "DB_DEL_DOC_NUM_STRIPPED");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.DELIVERYDATE", "DB_DELIVERYDATE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.DO_NOT_CONTACT", "DB_DO_NOT_CONTACT");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.EMAIL_ADDR", "DB_EMAIL_ADDR");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.EMAIL_ADDR_SHIP_TO", "DB_EMAIL_ADDR_SHIP_TO");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.ES_MODIFIEDDATETIME", "DB_ES_MODIFIEDDATETIME");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.FINAL_DT", "DB_FINAL_DT");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.FINANCE_AMOUNT", "DB_FINANCE_AMOUNT");
//		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.MERGED_CUST_CD", "DB_MERGED_CUST_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.MFIISRETURNORDER", "DB_MFIISRETURNORDER");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.ORD_TP_CD", "DB_ORD_TP_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.ORDER_ORDER_NUM", "DB_ORDER_ORDER_NUM");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.ORDER_TYPE", "DB_ORDER_TYPE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.PAYMENTS", "DB_PAYMENTS");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.PCT_OF_SALE1", "DB_PCT_OF_SALE1");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.PCT_OF_SALE2", "DB_PCT_OF_SALE2");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.PROMO_CODE", "DB_PROMO_CODE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.PROMO_CODE_DESCRIPTION", "DB_PROMO_CODE_DESCRIPTION");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.PU_DEL_DT", "DB_PU_DEL_DT");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.RETURNREASONCODEID", "DB_RETURNREASONCODEID");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.RETURNSTATUS", "DB_RETURNSTATUS");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SALES_PERSON_NAME1", "DB_SALES_PERSON_NAME1");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SALES_PERSON_NAME2", "DB_SALES_PERSON_NAME2");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SALESFORCE_LINKID", "DB_SALESFORCE_LINKID");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SALESORIGINID", "DB_SALESORIGINID");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SALESSTATUS", "DB_SALESSTATUS");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SETUP_CHG", "DB_SETUP_CHG");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_ADDR1", "DB_SHIP_TO_ADDR1");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_ADDR2", "DB_SHIP_TO_ADDR2");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_B_PHONE", "DB_SHIP_TO_B_PHONE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_B_PHONE1", "DB_SHIP_TO_B_PHONE1");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_CITY", "DB_SHIP_TO_CITY");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_F_NAME", "DB_SHIP_TO_F_NAME");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_H_PHONE", "DB_SHIP_TO_H_PHONE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_H_PHONE1", "DB_SHIP_TO_H_PHONE1");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_L_NAME", "DB_SHIP_TO_L_NAME");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_ST_CD", "DB_SHIP_TO_ST_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_ZIP_CD", "DB_SHIP_TO_ZIP_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIPCARRIERACCOUNT", "DB_SHIPCARRIERACCOUNT");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIPCARRIERDELIVERYCONTACT", "DB_SHIPCARRIERDELIVERYCONTACT");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIPCARRIERDLVTYPE", "DB_SHIPCARRIERDLVTYPE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIPCARRIERID", "DB_SHIPCARRIERID");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIPCARRIERNAME", "DB_SHIPCARRIERNAME");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SO_EMP_SLSP_CD1", "DB_SO_EMP_SLSP_CD1");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SO_EMP_SLSP_CD2", "DB_SO_EMP_SLSP_CD2");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SO_SEQ_NUM", "DB_SO_SEQ_NUM");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SO_STORE_CD", "DB_SO_STORE_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SO_WR_DT", "DB_SO_WR_DT");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.STAT_CD", "DB_STAT_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.TAX_CHG", "DB_TAX_CHG");
//		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.TOTAL_AMOUNT", "DB_TOTAL_AMOUNT");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.TOTAL_SALE", "DB_TOTAL_SALE");
//		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.ULTIMATE_CUST_CD", "DB_ULTIMATE_CUST_CD");
	}
	
	@Then("Verify API response values and DB values are equal for order header")
	public void VerifyAPIandDBValuesOrderHeader() {
		
		String [] API_keys = {"API_BALANCE_DUE","API_BRAND","API_CO_CD","API_CUST_CD","API_CUST_CD_STRIPPED","API_CUST_NAME","API_DEL_CHG",
				"API_DEL_DOC_NUM","API_DEL_DOC_NUM_STRIPPED","API_DELIVERYDATE","API_DO_NOT_CONTACT","API_EMAIL_ADDR","API_EMAIL_ADDR_SHIP_TO",
				"API_ES_MODIFIEDDATETIME","API_FINAL_DT","API_FINANCE_AMOUNT","API_MFIISRETURNORDER","API_ORD_TP_CD",
				"API_ORDER_ORDER_NUM","API_ORDER_TYPE","API_PAYMENTS","API_PCT_OF_SALE1","API_PCT_OF_SALE2","API_PROMO_CODE","API_PROMO_CODE_DESCRIPTION",
				"API_PU_DEL_DT","API_RETURNREASONCODEID","API_RETURNSTATUS","API_SALES_PERSON_NAME1","API_SALES_PERSON_NAME2",
				"API_SALESFORCE_LINKID","API_SALESORIGINID","API_SALESSTATUS","API_SETUP_CHG","API_SHIP_TO_ADDR1","API_SHIP_TO_ADDR2","API_SHIP_TO_B_PHONE",
				"API_SHIP_TO_B_PHONE1","API_SHIP_TO_CITY","API_SHIP_TO_F_NAME","API_SHIP_TO_H_PHONE","API_SHIP_TO_H_PHONE1","API_SHIP_TO_L_NAME",
				"API_SHIP_TO_ST_CD","API_SHIP_TO_ZIP_CD","API_SHIPCARRIERACCOUNT","API_SHIPCARRIERDELIVERYCONTACT","API_SHIPCARRIERDLVTYPE",
				"API_SHIPCARRIERID","API_SHIPCARRIERNAME","API_SO_EMP_SLSP_CD1","API_SO_EMP_SLSP_CD2","API_SO_SEQ_NUM","API_SO_STORE_CD","API_SO_WR_DT",
				"API_STAT_CD","API_TAX_CHG","API_TOTAL_SALE","API_TOTAL_AMOUNT","API_SALES_STATUS_DESCRIPTION"};
		
		String [] DB_keys = {"DB_BALANCE_DUE","DB_BRAND","DB_CO_CD","DB_CUST_CD","DB_CUST_CD_STRIPPED","DB_CUST_NAME","DB_DEL_CHG",
				"DB_DEL_DOC_NUM","DB_DEL_DOC_NUM_STRIPPED","DB_DELIVERYDATE","DB_DO_NOT_CONTACT","DB_EMAIL_ADDR","DB_EMAIL_ADDR_SHIP_TO",
				"DB_ES_MODIFIEDDATETIME","DB_FINAL_DT","DB_FINANCE_AMOUNT","DB_MFIISRETURNORDER","DB_ORD_TP_CD",
				"DB_ORDER_ORDER_NUM","DB_ORDER_TYPE","DB_PAYMENTS","DB_PCT_OF_SALE1","DB_PCT_OF_SALE2","DB_PROMO_CODE","DB_PROMO_CODE_DESCRIPTION",
				"DB_PU_DEL_DT","DB_RETURNREASONCODEID","DB_RETURNSTATUS","DB_SALES_PERSON_NAME1","DB_SALES_PERSON_NAME2",
				"DB_SALESFORCE_LINKID","DB_SALESORIGINID","DB_SALESSTATUS","DB_SETUP_CHG","DB_SHIP_TO_ADDR1","DB_SHIP_TO_ADDR2","DB_SHIP_TO_B_PHONE",
				"DB_SHIP_TO_B_PHONE1","DB_SHIP_TO_CITY","DB_SHIP_TO_F_NAME","DB_SHIP_TO_H_PHONE","DB_SHIP_TO_H_PHONE1","DB_SHIP_TO_L_NAME",
				"DB_SHIP_TO_ST_CD","DB_SHIP_TO_ZIP_CD","DB_SHIPCARRIERACCOUNT","DB_SHIPCARRIERDELIVERYCONTACT","DB_SHIPCARRIERDLVTYPE",
				"DB_SHIPCARRIERID","DB_SHIPCARRIERNAME","DB_SO_EMP_SLSP_CD1","DB_SO_EMP_SLSP_CD2","DB_SO_SEQ_NUM","DB_SO_STORE_CD","DB_SO_WR_DT",
				"DB_STAT_CD","DB_TAX_CHG","DB_TOTAL_SALE"};
		
		for (int i=0; i<API_keys.length; i++) {
			if (API_keys[i] == "API_TOTAL_AMOUNT") {
				List<String> api_totalAmountList = HashMapContainer.getList("$$API_TOTAL_AMOUNT");
				List<String> so_TotalSale = HashMapContainer.getList("$$DB_TOTAL_SALE");
				List<String> so_taxCharge = HashMapContainer.getList("$$DB_TAX_CHG");
				System.out.println(API_keys[i] + ":" + api_totalAmountList);
				System.out.println("DB_TOTAL_SALE : " + so_TotalSale);
				System.out.println("DB_TAX_CHG : " + so_taxCharge);
				for (int k = 0; k < api_totalAmountList.size(); k++) {
					Object obj_totalsale = so_TotalSale.get(k);
					Object obj_tax = so_taxCharge.get(k);
					float api_totalamount =Float.parseFloat(api_totalAmountList.get(k));
					float so_totalsale_float = 0;
					float so_taxcharge = 0;
					
					if (obj_totalsale instanceof Integer) {
						so_totalsale_float = ((Integer) obj_totalsale).floatValue();
					}else if (obj_totalsale instanceof Double) {
						so_totalsale_float = ((Double) obj_totalsale).floatValue();
					}
					
					if (obj_tax instanceof Integer) {
						so_taxcharge = ((Integer) obj_tax).floatValue();
					}else if (obj_tax instanceof Double) {
						so_taxcharge = ((Double) obj_tax).floatValue();
					}
					
					float totalsaleandtax = so_totalsale_float + so_taxcharge;
					System.out.println("totalsaleandtax "+totalsaleandtax);
					
					Assert.assertEquals(api_totalamount, totalsaleandtax, 0.01);
					
					
//					if (obj_TOTAL_SALE instanceof Integer) {
//						System.out.println("******DB_TOTAL_SALE******");
//						float so_float =Float.parseFloat(sourceList.get(j));
//						float ta_float = ((Integer) obj_TOTAL_SALE).floatValue();
//						Assert.assertEquals(so_float, ta_float, 0.0001);
//					}
				}
			}else if (API_keys[i] == "API_SALES_STATUS_DESCRIPTION"){
//				System.out.println("inside API_SALES_STATUS_DESCRIPTION");
				List<String> api_SALES_STATUS_DESCRIPTION = HashMapContainer.getList("$$API_SALES_STATUS_DESCRIPTION");
				List<String> so_SALESSTATUS = HashMapContainer.getList("$$DB_SALESSTATUS");
//				System.out.println(API_keys[i] + ":" + api_SALES_STATUS_DESCRIPTION);
//				System.out.println("DB_SALESSTATUS : " + so_SALESSTATUS);
				for (int m = 0; m < api_SALES_STATUS_DESCRIPTION.size(); m++) {
//					System.out.println("inside for loop");
					String so_description = null;
//					String so_SALESSTATUS1 = so_SALESSTATUS.get(m);
//					System.out.println("*****"+so_SALESSTATUS1);
					if (so_SALESSTATUS.get(m).equals("1")) {
						so_description = "Open Order";
						System.out.println("DB_SALESSTATUS 1 = Open Order");
					}else if (so_SALESSTATUS.get(m).equals("2")) {
						so_description = "Delivered";
						System.out.println("DB_SALESSTATUS 2 = Delivered");
					}else if (so_SALESSTATUS.get(m).equals("3")) {
						so_description = "Invoiced";
						System.out.println("DB_SALESSTATUS 3 = Invoiced");
					}else if (so_SALESSTATUS.get(m).equals("4")) {
						so_description = "Canceled";
						System.out.println("DB_SALESSTATUS 4 = Canceled");
					}
					Assert.assertEquals(api_SALES_STATUS_DESCRIPTION.get(m), so_description);
				}
			}else {
			List<String> sourceList = HashMapContainer.getList("$$" + API_keys[i]);
			List<String> targetList = HashMapContainer.getList("$$" + DB_keys[i]);
			System.out.println(API_keys[i] + ":" + sourceList);
			System.out.println(DB_keys[i] + ":" + targetList);
			for (int j = 0; j < sourceList.size(); j++) {
				
				switch(DB_keys[i]) {
					case "DB_BALANCE_DUE":
						Object obj_BALANCE_DUE = targetList.get(j);
						if (obj_BALANCE_DUE instanceof Integer) {
							System.out.println("*****DB_BALANCE_DUE********");
							float so_float =Float.parseFloat(sourceList.get(j));
							float ta_float = ((Integer) obj_BALANCE_DUE).floatValue();
							Assert.assertEquals(so_float, ta_float, 0.0001);
						}
						break;
						
					case "DB_DEL_CHG":
//						List<String> sourceList = HashMapContainer.getList("$$" + API_keys[i]);
//						List<String> targetList = HashMapContainer.getList("$$" + DB_keys[i]);
//						System.out.println(API_keys[i] + ":" + sourceList);
//						System.out.println(DB_keys[i] + ":" + targetList);
//						for (int j = 0; j < sourceList.size(); j++) {
							String source_value = sourceList.get(j).trim();
//							System.out.println("source_value "+source_value);
							if (source_value.isEmpty()) {
								source_value = null;
//								System.out.println("source_value inside IF "+source_value);
							}
//							System.out.println("target "+targetList.get(j));
							String target = targetList.get(j);
							Assert.assertEquals(source_value,target);
							break;
//						}
							
					case "DB_DELIVERYDATE":
						String converted_date = targetList.get(j).replace("\"", "").replace("/", "-");
//						System.out.println(converted_date);
						Assert.assertTrue(sourceList.get(j).contains(converted_date));
						break;
						
					case "DB_ES_MODIFIEDDATETIME":
						String es_ModDateTime = targetList.get(j).replace("\"", "").replace("/", "-");
						System.out.println(es_ModDateTime);
						String s1= es_ModDateTime.substring(0,10);
						String s2= es_ModDateTime.substring(es_ModDateTime.length()-8);
						String s3= s1+"T"+s2;
						System.out.println(s3);
//						String[] ES_modDatetime = es_ModDateTime.split(" ");
//						String final
						Assert.assertEquals(sourceList.get(j).trim(), s3);
						break;
						
					case "DB_FINAL_DT":
						String converted_final_date = targetList.get(j).replace("\"", "").replace("/", "-");
//						System.out.println(converted_date);
						Assert.assertTrue(sourceList.get(j).contains(converted_final_date));
						break;
						
					case "DB_FINANCE_AMOUNT":
						Object obj_FINANCE_AMOUNT = targetList.get(j);
						if (obj_FINANCE_AMOUNT instanceof Integer) {
							System.out.println("*****DB_FINANCE_AMOUNT*******");
							float so_float =Float.parseFloat(sourceList.get(j));
							float ta_float = ((Integer) obj_FINANCE_AMOUNT).floatValue();
							Assert.assertEquals(so_float, ta_float, 0.0001);
						}
						break;
						
					case "DB_MFIISRETURNORDER":
						Object obj_MFIISRETURNORDER = targetList.get(j);
						if (obj_MFIISRETURNORDER instanceof Integer) {
							System.out.println("*****DB_MFIISRETURNORDER*******");
							Assert.assertEquals(Integer.parseInt(sourceList.get(j)), (targetList.get(j)));
						}
						break;
						
					case "DB_PAYMENTS":
						Object obj_PAYMENTS = targetList.get(j);
						if (obj_PAYMENTS instanceof Double) {
							System.out.println("****DB_PAYMENTS*****");
							Assert.assertEquals(Double.parseDouble(sourceList.get(j)), (targetList.get(j)));
						}
//						String so = sourceList.get(j);
//						Double ta = Double.valueOf(targetList.get(j));
////						System.out.println(so+" "+ta);
//						Assert.assertEquals(so, ta);
						break;
						
					case "DB_PCT_OF_SALE1":
						Object obj_PCT_OF_SALE1 = targetList.get(j);
						if (obj_PCT_OF_SALE1 instanceof Integer) {
							System.out.println("******DB_PCT_OF_SALE1******");
							float so_float =Float.parseFloat(sourceList.get(j));
							float ta_float = ((Integer) obj_PCT_OF_SALE1).floatValue();
							Assert.assertEquals(so_float, ta_float, 0.0001);
							
//							System.out.println(Double.parseDouble(targetList.get(j)));
//							Assert.assertEquals(sourceList.get(j), Double.parseDouble(targetList.get(j)));
						}
//						Assert.assertTrue(sourceList.get(j).equals(targetList.get(j)));
						break;
						
					case "DB_PCT_OF_SALE2":
//						Assert.assertEquals((sourceList.get(j)), (targetList.get(j)));
						Object obj_PCT_OF_SALE2 = targetList.get(j);
						if (obj_PCT_OF_SALE2 instanceof Integer) {
							System.out.println("*****DB_PCT_OF_SALE2*******");
							float so_float =Float.parseFloat(sourceList.get(j));
							float ta_float = ((Integer) obj_PCT_OF_SALE2).floatValue();
							Assert.assertEquals(so_float, ta_float, 0.0001);
//							Assert.assertEquals(Double.parseDouble(sourceList.get(j)), (targetList.get(j)));
						}
//						Assert.assertTrue(sourceList.get(j).equals(targetList.get(j)));
						break;
						
					case "DB_PU_DEL_DT":
							String source_Pu_Del_Dt = sourceList.get(j).trim();
							if (source_Pu_Del_Dt.isEmpty()) {
								source_Pu_Del_Dt = null;
							}
							String target_Pu_Del_Dt = targetList.get(j);
							Assert.assertEquals(source_Pu_Del_Dt,target_Pu_Del_Dt);
							break;
							
					case "DB_RETURNSTATUS":
						Object obj_RETURNSTATUS = targetList.get(j);
						if (obj_RETURNSTATUS instanceof Integer) {
							System.out.println("*****DB_RETURNSTATUS*******");
							Assert.assertEquals(Integer.parseInt(sourceList.get(j)), (targetList.get(j)));
						}
						break;
							
					case "DB_SETUP_CHG":
						String source_SETUP_CHG = sourceList.get(j).trim();
						if (source_SETUP_CHG.isEmpty()) {
							source_SETUP_CHG = null;
						}
						String target_SETUP_CHG = targetList.get(j);
						Assert.assertEquals(source_SETUP_CHG,target_SETUP_CHG);
						break;
						
					case "DB_SHIPCARRIERDLVTYPE":
						Object obj_SHIPCARRIERDLVTYPE = targetList.get(j);
						if(obj_SHIPCARRIERDLVTYPE instanceof String) {
							Assert.assertTrue(sourceList.get(j).contains(targetList.get(j)));
						}else if (obj_SHIPCARRIERDLVTYPE instanceof Integer) {
							System.out.println("******DB_SHIPCARRIERDLVTYPE********");
							Assert.assertEquals(Integer.parseInt(sourceList.get(j)), (targetList.get(j)));
						}
//						Assert.assertTrue(sourceList.get(j).contains(targetList.get(j)));
						break;
						
					case "DB_SO_WR_DT":
						String SO_WR_DT = targetList.get(j).replace("\"", "").replace("/", "-");
						System.out.println("&&&&&"+SO_WR_DT);
						String ss1= SO_WR_DT.substring(0,10);
						String ss2= SO_WR_DT.substring(SO_WR_DT.length()-8);
						String ss3= ss1+"T"+ss2+"Z";
						System.out.println("^^^^^^"+ss3);
//						String[] ES_modDatetime = es_ModDateTime.split(" ");
//						String final
						Assert.assertEquals(sourceList.get(j).trim(), ss3);
						break;
						
					case "DB_TAX_CHG":
						Object obj_TAX_CHG = targetList.get(j);
						if (obj_TAX_CHG instanceof Double) {
							System.out.println("****DB_TAX_CHG*****");
							Assert.assertEquals(Double.parseDouble(sourceList.get(j)), (targetList.get(j)));
						}
						break;
						
					case "DB_TOTAL_SALE":
						Object obj_TOTAL_SALE = targetList.get(j);
						if (obj_TOTAL_SALE instanceof Integer) {
							System.out.println("******DB_TOTAL_SALE******");
							float so_float =Float.parseFloat(sourceList.get(j));
							float ta_float = ((Integer) obj_TOTAL_SALE).floatValue();
							Assert.assertEquals(so_float, ta_float, 0.0001);
						}
						break;
						
//					case "DB_CUST_CD_STRIPPED":
//						
//						break;
						
					default:
						System.out.println("insisde default");
//						System.out.println(sourceList.get(j));
//						System.out.println(targetList.get(j));
						Assert.assertTrue(sourceList.get(j).equals(targetList.get(j)));
				}
				}
			}
		}
	}
	
	@Then("Verify MERGED_CUST_CD and ULTIMATE_CUST_CD field values from cross ref index")
	public void FieldValidationsFromCrossRefIndex() throws Throwable {
		List<String> so_custCodes = HashMapContainer.getList("$$DB_CUST_CD");
		List<String> api_MERGED_CUST_CD = HashMapContainer.getList("$$API_MERGED_CUST_CD");
		List<String> api_CUST_CD = HashMapContainer.getList("$$API_CUST_CD");
		List<String> api_ULTIMATE_CUST_CD = HashMapContainer.getList("$$API_ULTIMATE_CUST_CD");
		for (int i=0; i<so_custCodes.size(); i++) {
			System.out.println("cust codee "+so_custCodes.get(i));
			String cross_ref_query = "{\r\n" + 
					"  \"query\": {\r\n" + 
					"    \"match\": {\r\n" + 
					"      \"customerCode.keyword\": \""+so_custCodes.get(i)+"\"\r\n" + 
					"    }\r\n" + 
					"  }\r\n" + 
					"}";
			System.out.println("^^^"+cross_ref_query);
			String url=Utilities.getURL("odata_custinfo_uat");
			ElasticSearchJestUtility.jestClient(url);
			ElasticSearchJestUtility.getIndicesExists("customer_cross_ref");
			ElasticSearchJestUtility.searchQuerywithIndex(cross_ref_query);
			Object read = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.total");
			log.info("No of Records found in DB : " + read);
			log.info(ElasticSearchJestUtility.getJestResponseAsString());
			
			List<String> UMCC = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[*]._source.ultimateMasterCustomerCode");
			for (String s:UMCC) {
				System.out.println("*****"+s);
			}
			if (UMCC.size() == 0) {
//				String api_merged_cust_CD_ID = api_MERGED_CUST_CD.get(i);
				System.out.println("no data in cross ref inside if validation");
				Assert.assertTrue(api_MERGED_CUST_CD.get(i).isEmpty());
				Assert.assertEquals(api_ULTIMATE_CUST_CD.get(i), api_CUST_CD.get(i));
			}else {
				System.out.println("Data exists in cross ref inside else");
				System.out.println(api_MERGED_CUST_CD.get(i) + api_CUST_CD.get(i));
				System.out.println(api_ULTIMATE_CUST_CD.get(i) + UMCC.get(i));
				Assert.assertEquals(api_MERGED_CUST_CD.get(i), api_CUST_CD.get(i));
				Assert.assertEquals(api_ULTIMATE_CUST_CD.get(i), UMCC.get(i));
			}
		}
	}
	
	@When("Retrive data exists in all the fields from API XML response and store in variable for order lines")
	public void XML_APIResponseStoringInVariableForOrderLines() {
//		Response res = ap.getRes();
		Response res = RestAssuredSteps.getResponse();
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.ACTIVATION_DT", "Lines_API_ACTIVATION_DT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.ACTIVATION_PHONE", "Lines_API_ACTIVATION_PHONE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.CARRIER", "Lines_API_CARRIER");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.CO_CD", "Lines_API_CO_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.COD_AMT", "Lines_API_COD_AMT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.COMM_CD", "Lines_API_COMM_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.COMM_ON_DEL_CHG", "Lines_API_COMM_ON_DEL_CHG");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.COMM_ON_SETUP_CHG", "Lines_API_COMM_ON_SETUP_CHG");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.COSTPRICE", "Lines_API_COSTPRICE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.CUST_TAX_CHG", "Lines_API_CUST_TAX_CHG");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.DEL_DOC_LN_NUM", "Lines_API_DEL_DOC_LN_NUM");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.DEL_DOC_NUM", "Lines_API_DEL_DOC_NUM");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.DEL_DOC_NUM_STRIPPED", "Lines_API_DEL_DOC_NUM_STRIPPED");
//		Delivery date validation is in pending to confirm from Raajvi
//		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.DELIVERY_DT", "Lines_API_DELIVERY_DT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.DISC_AMT", "Lines_API_DISC_AMT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.DLVMODE", "Lines_API_DLVMODE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.ES_MODIFIEDDATETIME", "Lines_API_ES_MODIFIEDDATETIME");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.FILL_DT", "Lines_API_FILL_DT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.FINAL_DT", "Lines_API_FINAL_DT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.ITM_CD", "Lines_API_ITM_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.ITM_CD_STRIPPED", "Lines_API_ITM_CD_STRIPPED");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.LOC_CD", "Lines_API_LOC_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.LV_IN_CARTON", "Lines_API_LV_IN_CARTON");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.MFIEXTENDEDPRICE", "Lines_API_MFIEXTENDEDPRICE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.OOC_QTY", "Lines_API_OOC_QTY");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.OUT_CD", "Lines_API_OUT_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.OUT_ID_CD", "Lines_API_OUT_ID_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.PCT_OF_SALE1", "Lines_API_PCT_OF_SALE1");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.PCT_OF_SALE2", "Lines_API_PCT_OF_SALE2");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.PRC_CHG_APP_CD", "Lines_API_PRC_CHG_APP_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.PURCHASE_ORDER_NUM", "Lines_API_PURCHASE_ORDER_NUM");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.QTY", "Lines_API_QTY");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.REF_SER_NUM", "Lines_API_REF_SER_NUM");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SALESPRICE", "Lines_API_SALESPRICE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SER_NUM", "Lines_API_SER_NUM");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SET_UP", "Lines_API_SET_UP");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_GROUP", "Lines_API_SHIP_GROUP");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_CITY", "Lines_API_SHIP_TO_CITY");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_COUNTRY", "Lines_API_SHIP_TO_COUNTRY");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_COUNTY", "Lines_API_SHIP_TO_COUNTY");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_STATE", "Lines_API_SHIP_TO_STATE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_STREET", "Lines_API_SHIP_TO_STREET");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIP_TO_ZIP_CD", "Lines_API_SHIP_TO_ZIP_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIPCARRIERACCOUNT", "Lines_API_SHIPCARRIERACCOUNT");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIPCARRIERACCOUNTCODE", "Lines_API_SHIPCARRIERACCOUNTCODE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIPCARRIERDLVTYPE", "Lines_API_SHIPCARRIERDLVTYPE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIPCARRIERID", "Lines_API_SHIPCARRIERID");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SHIPCARRIERNAME", "Lines_API_SHIPCARRIERNAME");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SIZE", "Lines_API_SIZE");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SO_EMP_SLSP_CD1", "Lines_API_SO_EMP_SLSP_CD1");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SO_EMP_SLSP_CD2", "Lines_API_SO_EMP_SLSP_CD2");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.SPIFF", "Lines_API_SPIFF");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.STORE_CD", "Lines_API_STORE_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.STORE_TAX_CHG", "Lines_API_STORE_TAX_CHG");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.TAKEN_WITH", "Lines_API_TAKEN_WITH");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.TAX_BASIS", "Lines_API_TAX_BASIS");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.TAX_CD", "Lines_API_TAX_CD");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.TAX_RESP", "Lines_API_TAX_RESP");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.Total_Amount", "Lines_API_Total_Amount");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.TRACKING_NUM", "Lines_API_TRACKING_NUM");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.TREATED_BY_LN_NUM", "Lines_API_TREATED_BY_LN_NUM");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.UNIT_PRC", "Lines_API_UNIT_PRC");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.VOID_FLAG", "Lines_API_VOID_FLAG");
		RestAssuredUtility.retriveDataFromXMLResponse(res, "feed.entry.content.properties.WARRANTED_BY_LN_NUM", "Lines_API_WARRANTED_BY_LN_NUM");
	}

	@And("Retrive list of data in all the fields from BQ JSON response and store in variable for order lines")
	public void JSON_BQResponseStoringInVariableForOrderLines() {
		
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.ACTIVATION_DT", "Lines_DB_ACTIVATION_DT");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.ACTIVATION_PHONE", "Lines_DB_ACTIVATION_PHONE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.CARRIER", "Lines_DB_CARRIER");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.CO_CD", "Lines_DB_CO_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.COD_AMT", "Lines_DB_COD_AMT");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.COMM_CD", "Lines_DB_COMM_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.COMM_ON_DEL_CHG", "Lines_DB_COMM_ON_DEL_CHG");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.COMM_ON_SETUP_CHG", "Lines_DB_COMM_ON_SETUP_CHG");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.COSTPRICE", "Lines_DB_COSTPRICE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.CUST_TAX_CHG", "Lines_DB_CUST_TAX_CHG");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.DEL_DOC_LN_NUM", "Lines_DB_DEL_DOC_LN_NUM");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.DEL_DOC_NUM", "Lines_DB_DEL_DOC_NUM");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.DEL_DOC_NUM_STRIPPED", "Lines_DB_DEL_DOC_NUM_STRIPPED");
//		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.DELIVERY_DT", "Lines_DB_DELIVERY_DT");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.DISC_AMT", "Lines_DB_DISC_AMT");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.DLVMODE", "Lines_DB_DLVMODE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.ES_MODIFIEDDATETIME", "Lines_DB_ES_MODIFIEDDATETIME");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.FILL_DT", "Lines_DB_FILL_DT");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.FINAL_DT", "Lines_DB_FINAL_DT");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.ITM_CD", "Lines_DB_ITM_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.ITM_CD_STRIPPED", "Lines_DB_ITM_CD_STRIPPED");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.LOC_CD", "Lines_DB_LOC_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.LV_IN_CARTON", "Lines_DB_LV_IN_CARTON");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.MFIEXTENDEDPRICE", "Lines_DB_MFIEXTENDEDPRICE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.OOC_QTY", "Lines_DB_OOC_QTY");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.OUT_CD", "Lines_DB_OUT_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.OUT_ID_CD", "Lines_DB_OUT_ID_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.PCT_OF_SALE1", "Lines_DB_PCT_OF_SALE1");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.PCT_OF_SALE2", "Lines_DB_PCT_OF_SALE2");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.PRC_CHG_APP_CD", "Lines_DB_PRC_CHG_APP_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.PURCHASE_ORDER_NUM", "Lines_DB_PURCHASE_ORDER_NUM");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.QTY", "Lines_DB_QTY");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.REF_SER_NUM", "Lines_DB_REF_SER_NUM");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SALESPRICE", "Lines_DB_SALESPRICE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SER_NUM", "Lines_DB_SER_NUM");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SET_UP", "Lines_DB_SET_UP");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_GROUP", "Lines_DB_SHIP_GROUP");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_CITY", "Lines_DB_SHIP_TO_CITY");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_COUNTRY", "Lines_DB_SHIP_TO_COUNTRY");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_COUNTY", "Lines_DB_SHIP_TO_COUNTY");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_STATE", "Lines_DB_SHIP_TO_STATE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_STREET", "Lines_DB_SHIP_TO_STREET");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIP_TO_ZIP_CD", "Lines_DB_SHIP_TO_ZIP_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIPCARRIERACCOUNT", "Lines_DB_SHIPCARRIERACCOUNT");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIPCARRIERACCOUNTCODE", "Lines_DB_SHIPCARRIERACCOUNTCODE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIPCARRIERDLVTYPE", "Lines_DB_SHIPCARRIERDLVTYPE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIPCARRIERID", "Lines_DB_SHIPCARRIERID");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SHIPCARRIERNAME", "Lines_DB_SHIPCARRIERNAME");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.PRODUCT_SIZE", "Lines_DB_SIZE");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SO_EMP_SLSP_CD1", "Lines_DB_SO_EMP_SLSP_CD1");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SO_EMP_SLSP_CD2", "Lines_DB_SO_EMP_SLSP_CD2");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.SPIFF", "Lines_DB_SPIFF");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.STORE_CD", "Lines_DB_STORE_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.STORE_TAX_CHG", "Lines_DB_STORE_TAX_CHG");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.TAKEN_WITH", "Lines_DB_TAKEN_WITH");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.TAX_BASIS", "Lines_DB_TAX_BASIS");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.TAX_CD", "Lines_DB_TAX_CD");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.TAX_RESP", "Lines_DB_TAX_RESP");
//		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.Total_Amount", "Lines_DB_Total_Amount");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.TRACKING_NUM", "Lines_DB_TRACKING_NUM");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.TREATED_BY_LN_NUM", "Lines_DB_TREATED_BY_LN_NUM");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.UNIT_PRC", "Lines_DB_UNIT_PRC");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.VOID_FLAG", "Lines_DB_VOID_FLAG");
		RestAssuredUtility.retriveDataFromJSONResponse("$.hits.hits[*]._source.WARRANTED_BY_LN_NUM", "Lines_DB_WARRANTED_BY_LN_NUM");
	}
	
	@Then("Verify API response values and DB values are equal for order lines")
	public void VerifyAPIandDBValuesOrderLines() {
		
		String [] API_keys = {"Lines_API_ACTIVATION_DT","Lines_API_ACTIVATION_PHONE","Lines_API_CARRIER","Lines_API_CO_CD","Lines_API_COD_AMT",
				"Lines_API_COMM_CD","Lines_API_COMM_ON_DEL_CHG","Lines_API_COMM_ON_SETUP_CHG","Lines_API_COSTPRICE","Lines_API_CUST_TAX_CHG",
				"Lines_API_DEL_DOC_LN_NUM","Lines_API_DEL_DOC_NUM","Lines_API_DEL_DOC_NUM_STRIPPED",
//				"Lines_API_DELIVERY_DT",
				"Lines_API_DISC_AMT","Lines_API_DLVMODE","Lines_API_ES_MODIFIEDDATETIME","Lines_API_FILL_DT","Lines_API_FINAL_DT","Lines_API_ITM_CD","Lines_API_ITM_CD_STRIPPED",
				"Lines_API_LOC_CD","Lines_API_LV_IN_CARTON","Lines_API_MFIEXTENDEDPRICE","Lines_API_OOC_QTY","Lines_API_OUT_CD","Lines_API_OUT_ID_CD",
				"Lines_API_PCT_OF_SALE1","Lines_API_PCT_OF_SALE2","Lines_API_PRC_CHG_APP_CD","Lines_API_PURCHASE_ORDER_NUM","Lines_API_QTY",
				"Lines_API_REF_SER_NUM","Lines_API_SALESPRICE","Lines_API_SER_NUM","Lines_API_SET_UP","Lines_API_SHIP_GROUP","Lines_API_SHIP_TO_CITY",
				"Lines_API_SHIP_TO_COUNTRY","Lines_API_SHIP_TO_COUNTY","Lines_API_SHIP_TO_STATE","Lines_API_SHIP_TO_STREET","Lines_API_SHIP_TO_ZIP_CD",
				"Lines_API_SHIPCARRIERACCOUNT","Lines_API_SHIPCARRIERACCOUNTCODE","Lines_API_SHIPCARRIERDLVTYPE","Lines_API_SHIPCARRIERID",
				"Lines_API_SHIPCARRIERNAME","Lines_API_SIZE","Lines_API_SO_EMP_SLSP_CD1","Lines_API_SO_EMP_SLSP_CD2","Lines_API_SPIFF","Lines_API_STORE_CD",
				"Lines_API_STORE_TAX_CHG","Lines_API_TAKEN_WITH","Lines_API_TAX_BASIS","Lines_API_TAX_CD","Lines_API_TAX_RESP","Lines_API_TRACKING_NUM",
				"Lines_API_TREATED_BY_LN_NUM","Lines_API_UNIT_PRC","Lines_API_VOID_FLAG","Lines_API_WARRANTED_BY_LN_NUM","Lines_API_Total_Amount"};
		
		String [] DB_keys = {"Lines_DB_ACTIVATION_DT","Lines_DB_ACTIVATION_PHONE","Lines_DB_CARRIER","Lines_DB_CO_CD","Lines_DB_COD_AMT",
				"Lines_DB_COMM_CD","Lines_DB_COMM_ON_DEL_CHG","Lines_DB_COMM_ON_SETUP_CHG","Lines_DB_COSTPRICE","Lines_DB_CUST_TAX_CHG",
				"Lines_DB_DEL_DOC_LN_NUM","Lines_DB_DEL_DOC_NUM","Lines_DB_DEL_DOC_NUM_STRIPPED",
//				"Lines_DB_DELIVERY_DT",
				"Lines_DB_DISC_AMT","Lines_DB_DLVMODE","Lines_DB_ES_MODIFIEDDATETIME","Lines_DB_FILL_DT","Lines_DB_FINAL_DT","Lines_DB_ITM_CD","Lines_DB_ITM_CD_STRIPPED",
				"Lines_DB_LOC_CD","Lines_DB_LV_IN_CARTON","Lines_DB_MFIEXTENDEDPRICE","Lines_DB_OOC_QTY","Lines_DB_OUT_CD","Lines_DB_OUT_ID_CD",
				"Lines_DB_PCT_OF_SALE1","Lines_DB_PCT_OF_SALE2","Lines_DB_PRC_CHG_APP_CD","Lines_DB_PURCHASE_ORDER_NUM","Lines_DB_QTY",
				"Lines_DB_REF_SER_NUM","Lines_DB_SALESPRICE","Lines_DB_SER_NUM","Lines_DB_SET_UP","Lines_DB_SHIP_GROUP","Lines_DB_SHIP_TO_CITY",
				"Lines_DB_SHIP_TO_COUNTRY","Lines_DB_SHIP_TO_COUNTY","Lines_DB_SHIP_TO_STATE","Lines_DB_SHIP_TO_STREET","Lines_DB_SHIP_TO_ZIP_CD",
				"Lines_DB_SHIPCARRIERACCOUNT","Lines_DB_SHIPCARRIERACCOUNTCODE","Lines_DB_SHIPCARRIERDLVTYPE","Lines_DB_SHIPCARRIERID",
				"Lines_DB_SHIPCARRIERNAME","Lines_DB_SIZE","Lines_DB_SO_EMP_SLSP_CD1","Lines_DB_SO_EMP_SLSP_CD2","Lines_DB_SPIFF","Lines_DB_STORE_CD",
				"Lines_DB_STORE_TAX_CHG","Lines_DB_TAKEN_WITH","Lines_DB_TAX_BASIS","Lines_DB_TAX_CD","Lines_DB_TAX_RESP",
				"Lines_DB_TRACKING_NUM","Lines_DB_TREATED_BY_LN_NUM","Lines_DB_UNIT_PRC","Lines_DB_VOID_FLAG","Lines_DB_WARRANTED_BY_LN_NUM"};
		
		for (int i=0; i<API_keys.length; i++) {
			if (API_keys[i] == "Lines_API_Total_Amount") {
				List<String> api_totalAmountList = HashMapContainer.getList("$$Lines_API_Total_Amount");
				List<String> so_unitPrice = HashMapContainer.getList("$$Lines_DB_UNIT_PRC");
				List<String> so_Qty = HashMapContainer.getList("$$Lines_DB_QTY");
				System.out.println(API_keys[i] + ":" + api_totalAmountList);
				System.out.println("Lines_DB_UNIT_PRC : " + so_unitPrice);
				System.out.println("Lines_DB_QTY : " + so_Qty);
				for (int k = 0; k < api_totalAmountList.size(); k++) {
					Object obj_unitPrice = so_unitPrice.get(k);
					Object obj_Qty = so_Qty.get(k);
					float api_totalamount =Float.parseFloat(api_totalAmountList.get(k));
					float so_unitPrice_float = 0;
					float so_Qty_float = 0;
					
					if (obj_unitPrice instanceof Integer) {
						so_unitPrice_float = ((Integer) obj_unitPrice).floatValue();
					}else if (obj_unitPrice instanceof Double) {
						so_unitPrice_float = ((Double) obj_unitPrice).floatValue();
					}
					
					if (obj_Qty instanceof Integer) {
						so_Qty_float = ((Integer) obj_Qty).floatValue();
					}else if (obj_Qty instanceof Double) {
						so_Qty_float = ((Double) obj_Qty).floatValue();
					}
					
					float unitpriceandqty = so_unitPrice_float * so_Qty_float;
					System.out.println("unitpriceandqty "+unitpriceandqty);
					
					Assert.assertEquals(api_totalamount, unitpriceandqty, 0.01);
				}
			}
			
			else {
			List<String> sourceList = HashMapContainer.getList("$$" + API_keys[i]);
			List<String> targetList = HashMapContainer.getList("$$" + DB_keys[i]);
			System.out.println(API_keys[i] + ":" + sourceList);
			System.out.println(DB_keys[i] + ":" + targetList);
			for (int j = 0; j < sourceList.size(); j++) {
				
				switch(DB_keys[i]) {
				
				case "Lines_DB_ACTIVATION_DT":
					System.out.println("****Lines_DB_ACTIVATION_DT******");
					String source_SETUP_CHG = sourceList.get(j).trim();
					if (source_SETUP_CHG.isEmpty()) {
						source_SETUP_CHG = null;
					}
					String target_SETUP_CHG = targetList.get(j);
					Assert.assertEquals(source_SETUP_CHG,target_SETUP_CHG);
					break;
					
				case "Lines_DB_COD_AMT":
					System.out.println("******Lines_DB_COD_AMT******");
					String source_COD_AMT = sourceList.get(j).trim();
					if (source_COD_AMT.isEmpty()) {
						source_COD_AMT = null;
					}
					String target_COD_AMT = targetList.get(j);
					Assert.assertEquals(source_COD_AMT,target_COD_AMT);
					break;
					
				case "Lines_DB_COSTPRICE":
					Object obj_COSTPRICE = targetList.get(j);
					if (obj_COSTPRICE instanceof Double) {
						System.out.println("*****Lines_DB_COSTPRICE********");
						float so_float =Float.parseFloat(sourceList.get(j));
						float ta_float = ((Double) obj_COSTPRICE).floatValue();
						Assert.assertEquals(so_float, ta_float, 0.0001);
					}
					break;
					
				case "Lines_DB_CUST_TAX_CHG":
					System.out.println("******Lines_DB_CUST_TAX_CHG******");
					String source_CUST_TAX_CHG = sourceList.get(j).trim();
					if (source_CUST_TAX_CHG.isEmpty()) {
						source_CUST_TAX_CHG = null;
					}
					String target_CUST_TAX_CHG = targetList.get(j);
					Assert.assertEquals(source_CUST_TAX_CHG,target_CUST_TAX_CHG);
					break;
					
//				case "Lines_DB_DELIVERY_DT":
//					System.out.println("******Lines_DB_DELIVERY_DT******");
//					break;
					
				case "Lines_DB_DISC_AMT":
					Object obj_DISC_AMT = targetList.get(j);
					if (obj_DISC_AMT instanceof Double) {
						System.out.println("*****Lines_DB_DISC_AMT********");
						float so_float =Float.parseFloat(sourceList.get(j));
						float ta_float = ((Double) obj_DISC_AMT).floatValue();
						Assert.assertEquals(so_float, ta_float, 0.0001);
					}
					break;
					
				case "Lines_DB_ES_MODIFIEDDATETIME":
					String SO_ES_MODIFIEDDATETIME = targetList.get(j).replace("\"", "").replace("/", "-");
					System.out.println("&&&&&"+SO_ES_MODIFIEDDATETIME);
					String ss1= SO_ES_MODIFIEDDATETIME.substring(0,10);
					String ss2= SO_ES_MODIFIEDDATETIME.substring(SO_ES_MODIFIEDDATETIME.length()-8);
					String ss3= ss1+"T"+ss2;
					System.out.println("^^^^^^"+ss3);
					Assert.assertEquals(sourceList.get(j).trim(), ss3);
					System.out.println("*****Lines_DB_ES_MODIFIEDDATETIME******");
					break;
					
				case "Lines_DB_FILL_DT":
					System.out.println("******Lines_DB_FILL_DT******");
					String source_FILL_DT = sourceList.get(j).trim();
					if (source_FILL_DT.isEmpty()) {
						source_FILL_DT = null;
					}
					String target_FILL_DT = targetList.get(j);
					Assert.assertEquals(source_FILL_DT,target_FILL_DT);
					break;
					
				case "Lines_DB_MFIEXTENDEDPRICE":
					Object obj_MFIEXTENDEDPRICE = targetList.get(j);
					if (obj_MFIEXTENDEDPRICE instanceof Double) {
						System.out.println("*****Lines_DB_MFIEXTENDEDPRICE********");
						float so_float =Float.parseFloat(sourceList.get(j));
						float ta_float = ((Double) obj_MFIEXTENDEDPRICE).floatValue();
						Assert.assertEquals(so_float, ta_float, 0.0001);
					}
					break;
					
				case "Lines_DB_OOC_QTY":
					System.out.println("******Lines_DB_OOC_QTY******");
					String source_OOC_QTY = sourceList.get(j).trim();
					if (source_OOC_QTY.isEmpty()) {
						source_OOC_QTY = null;
					}
					String target_OOC_QTY = targetList.get(j);
					Assert.assertEquals(source_OOC_QTY,target_OOC_QTY);
					break;
					
				case "Lines_DB_PCT_OF_SALE1":
					System.out.println("******Lines_DB_PCT_OF_SALE1******");
					String source_PCT_OF_SALE1 = sourceList.get(j).trim();
					if (source_PCT_OF_SALE1.isEmpty()) {
						source_PCT_OF_SALE1 = null;
					}
					String target_PCT_OF_SALE1 = targetList.get(j);
					Assert.assertEquals(source_PCT_OF_SALE1,target_PCT_OF_SALE1);
					break;
					
				case "Lines_DB_PCT_OF_SALE2":
					System.out.println("******Lines_DB_PCT_OF_SALE2******");
					String source_PCT_OF_SALE2 = sourceList.get(j).trim();
					if (source_PCT_OF_SALE2.isEmpty()) {
						source_PCT_OF_SALE2 = null;
					}
					String target_PCT_OF_SALE2 = targetList.get(j);
					Assert.assertEquals(source_PCT_OF_SALE2,target_PCT_OF_SALE2);
					break;
					
				case "Lines_DB_QTY":
					Object obj_DB_QTY = targetList.get(j);
					if (obj_DB_QTY instanceof Double) {
						System.out.println("*****Lines_DB_QTY********");
						float so_float =Float.parseFloat(sourceList.get(j));
						float ta_float = ((Double) obj_DB_QTY).floatValue();
						Assert.assertEquals(so_float, ta_float, 0.0001);
					}
					break;
					
				case "Lines_DB_SALESPRICE":
					Object obj_SALESPRICE = targetList.get(j);
					if (obj_SALESPRICE instanceof Double) {
						System.out.println("*****Lines_DB_SALESPRICE********");
						float so_float =Float.parseFloat(sourceList.get(j));
						float ta_float = ((Double) obj_SALESPRICE).floatValue();
						Assert.assertEquals(so_float, ta_float, 0.0001);
					}
					break;
					
				case "Lines_DB_SHIPCARRIERDLVTYPE":
					Object obj_SHIPCARRIERDLVTYPE = targetList.get(j);
					if (obj_SHIPCARRIERDLVTYPE instanceof Integer) {
						System.out.println("*****Lines_DB_SHIPCARRIERDLVTYPE********");
						float so_float =Float.parseFloat(sourceList.get(j));
						float ta_float = ((Integer) obj_SHIPCARRIERDLVTYPE).floatValue();
						Assert.assertEquals(so_float, ta_float, 0.0001);
					}
					break;
					
				case "Lines_DB_SPIFF":
					System.out.println("******Lines_DB_SPIFF******");
					String source_SPIFF = sourceList.get(j).trim();
					if (source_SPIFF.isEmpty()) {
						source_SPIFF = null;
					}
					String target_SPIFF = targetList.get(j);
					Assert.assertEquals(source_SPIFF,target_SPIFF);
					break;
					
				case "Lines_DB_STORE_TAX_CHG":
					System.out.println("******Lines_DB_STORE_TAX_CHG******");
					String source_STORE_TAX_CHG = sourceList.get(j).trim();
					if (source_STORE_TAX_CHG.isEmpty()) {
						source_STORE_TAX_CHG = null;
					}
					String target_STORE_TAX_CHG = targetList.get(j);
					Assert.assertEquals(source_STORE_TAX_CHG,target_STORE_TAX_CHG);
					break;
					
				case "Lines_DB_TAX_BASIS":
					System.out.println("******Lines_DB_TAX_BASIS******");
					String source_TAX_BASIS = sourceList.get(j).trim();
					if (source_TAX_BASIS.isEmpty()) {
						source_TAX_BASIS = null;
					}
					String target_TAX_BASIS = targetList.get(j);
					Assert.assertEquals(source_TAX_BASIS,target_TAX_BASIS);
					break;
					
				case "Lines_DB_UNIT_PRC":
					Object obj_UNIT_PRC = targetList.get(j);
					if (obj_UNIT_PRC instanceof Double) {
						System.out.println("*****Lines_DB_UNIT_PRC********");
						float so_float =Float.parseFloat(sourceList.get(j));
						float ta_float = ((Double) obj_UNIT_PRC).floatValue();
						Assert.assertEquals(so_float, ta_float, 0.0001);
					}
					break;
				
					default:
						System.out.println("insisde default");
						Assert.assertTrue(sourceList.get(j).equals(targetList.get(j)));
				}
				}
			}
		}
	}
	
	@When("^Generate random '(.*)'$")
    public void Generate_random_key(String key) {
       
        switch (key) {
        case "postbackRefID":
            String randomPostBackrefernceID =RandomStringUtils.randomAlphanumeric(25);
            HashMapContainer.add("$$RandomPostBackrefernceID", randomPostBackrefernceID);
            break;
        case "UniqueID":
            String UniqueIDno =RandomStringUtils.randomNumeric(3);
            String UniqueID = "63bd23a5-1ea5-4549-97db-09e8ecd47"+UniqueIDno;
            HashMapContainer.add("$$UniqueID", UniqueID);
            break;
        case "Email":
            String Emailid =RandomStringUtils.randomAlphabetic(5).toLowerCase();
            String Email = Emailid+"@rc.com";
            HashMapContainer.add("$$Email", Email);
            break;
        case "LeadDate":
            MF_MuleSoft_CustomStep steps =new MF_MuleSoft_CustomStep();
        	steps.get__FormattedTime_byTimeZone_And_TimeOffsetbyMinutes("UTC", "yyyy-MM-dd HH:mm:ss", 0);
        	String String_date = HashMapContainer.get("$$Time");
        	String LeadDate = String_date+".000Z";
        	HashMapContainer.add("$$LeadDate", LeadDate);
        	System.out.println(LeadDate);
            break;
        case "Guestemail":
            String Guestemail =RandomStringUtils.randomAlphanumeric(5).toLowerCase()+"@rc.com";
            HashMapContainer.add("$$Guestemail", Guestemail);
            break;
        case "AssociateEmail":
            String AssociateEmail ="associate"+RandomStringUtils.randomNumeric(4).toLowerCase()+"@rc.com";
            HashMapContainer.add("$$AssociateEmail", AssociateEmail);
            break;
        case "PostalCode":
            String PostalCode =RandomStringUtils.randomNumeric(20);
            HashMapContainer.add("$$PostalCode", PostalCode);
            break;
        case "ApplicationKey":
            String ApplicationKey =RandomStringUtils.randomNumeric(10);
            HashMapContainer.add("$$Applicationkey", ApplicationKey);
            break;
        case "CreditLimit":
            String randomCreditLimit =RandomStringUtils.randomNumeric(4);
            HashMapContainer.add("$$RandomCreditLimit", randomCreditLimit);
            break;
        case "phoneNumber":
            String randomphoneNumber =RandomStringUtils.randomNumeric(10);
            HashMapContainer.add("$$RandomphoneNumber", randomphoneNumber);
            break;
        case "AssociateADID":
            String AssociateADID =RandomStringUtils.randomNumeric(18);
            HashMapContainer.add("$$AssociateADID", AssociateADID);
            break;
        case "Guestfirstname":
            String Guestfirstname ="Guest"+RandomStringUtils.randomAlphabetic(3);
            HashMapContainer.add("$$Guestfirstname", Guestfirstname);
            break;
        case "FirstName":
            String randomFirstName ="test"+RandomStringUtils.randomAlphabetic(3);
            HashMapContainer.add("$$RandomFirstName", randomFirstName);
            break;
        
        case "GLName":
            String GLName ="test"+RandomStringUtils.randomAlphabetic(3);
            HashMapContainer.add("$$GLName", GLName);
            break;
        case "Firstname":
            String Firstname ="test"+RandomStringUtils.randomAlphabetic(5);
            HashMapContainer.add("$$Firstname", Firstname);
            break;
        case "Lastname":
            String Lastname ="test"+RandomStringUtils.randomAlphabetic(4);
            HashMapContainer.add("$$Lastname", Lastname);
            break;
        case "AssociateName":
            String AssociateName ="test"+RandomStringUtils.randomAlphabetic(4);
            HashMapContainer.add("$$AssociateName", AssociateName);
            break;
        case "StoreID":
            String randomStoreID =RandomStringUtils.randomNumeric(6);
            HashMapContainer.add("$$RandomStoreID", randomStoreID);
            break;
        case "LeadCreative":
            String leadCreative ="Test lead creative "+RandomStringUtils.randomAlphabetic(9);
            HashMapContainer.add("$$LeadCreative", leadCreative);
            break;
        case "LeadPage":
            String leadPage ="Test lead page "+RandomStringUtils.randomAlphabetic(9);
            HashMapContainer.add("$$LeadPage", leadPage);
            break;
        case "DetailsOffer":
            String detailsOffer =RandomStringUtils.randomAlphabetic(4)+" "+RandomStringUtils.randomAlphabetic(9)+" "+RandomStringUtils.randomAlphabetic(4)+" "+RandomStringUtils.randomAlphabetic(9);
            HashMapContainer.add("$$DetailsOffer", detailsOffer);
            break;
        }
    }
	
	@Given("Set the Timestamp to {string} {string} {int}")
	public void get__FormattedTime_byTimeZone_And_TimeOffsetbyMinutes(String timeZone, String timeFormat, int timeOffsetfByMinutes) {
			
			DateFormat tFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String a = null;
			Date date = new Date();
			String currentTime =  tFormat.format(date);
			System.out.println("Current Local Time: "+currentTime);
			
			
			DateFormat tFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//			String a1 = null;
			Date date1 = new Date();
			String currentTime1 =  tFormat1.format(date1);
			System.out.println("Current Local Time: "+currentTime1);
			HashMapContainer.add("$$Time1", currentTime1);
			
			DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime datetime1 = LocalDateTime.parse(currentTime1,formatter1);
			if(timeOffsetfByMinutes<0) {
				System.out.println("Subtracting Time");
				datetime1=datetime1.minusMinutes(-timeOffsetfByMinutes);
				a= datetime1.toString();
				System.out.println("------------"+a);
			}else {
				System.out.println("Adding Time");
				datetime1=datetime1.plusMinutes(timeOffsetfByMinutes);
				 a= datetime1.toString();
			}
			
			HashMapContainer.add("$$Time", a);
			tFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
			String Time = tFormat.format(date);
			System.out.println(timeZone+" Time: "+Time);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime datetime = LocalDateTime.parse(Time,formatter);
			
			if(timeOffsetfByMinutes<0) {
				System.out.println("Subtracting Time");
				datetime=datetime.minusMinutes(-timeOffsetfByMinutes);
			}else {
				System.out.println("Adding Time");
				datetime=datetime.plusMinutes(timeOffsetfByMinutes);
			}
			
			DateTimeFormatter finalformat = DateTimeFormatter.ofPattern(timeFormat);
//			return datetime.format(finalformat);
			String timestamp = datetime.format(finalformat);
			HashMapContainer.add("$$Timestamp", timestamp);
			System.out.println("******************************************"+timestamp);
		}
	
	@When("^Verify the data is present in SF using Dynamic Query - Request '(.*)' for key '(.*)' with data '(.*)'$")
	public static void update_String_data_request_SFCC(String file, String key, String store) throws Exception {
		File ft = new File(System.getProperty("user.dir") + "\\src/test/java/com/TEAF/json/" + file + ".json");
		RestAssuredUtility.updateJSONforPOSTRequestSFCC(ft, key, store);
	}
	
	@Then("^assert the value present in '(.*)' is equal to '(.*)'$")
	public void assertion_between_two_HashMap_values(String value1, String value2) {
		
		if(value1.startsWith("$$")) {
			value1 = HashMapContainer.get(value1);
		}
		if(value2.startsWith("$$")) {
			value2 = HashMapContainer.get(value2);
		}
		
		Assert.assertEquals(value1, value2);
		System.out.println(value1+" matches with "+value2);
	}
	
	@Then("^assert the value that '(.*)' contains '(.*)'$")
	public void contains_assertion_between_two_HashMap_values(String value1, String value2) {
		
		if(value1.startsWith("$$")) {
			value1 = HashMapContainer.get(value1);
		}
		if(value2.startsWith("$$")) {
			value2 = HashMapContainer.get(value2);
		}
		
		Assert.assertTrue(value1.contains(value2));
		System.out.println(value1+" contains "+value2);
	}
	
	@And("^assert the fields that contains date and time by doing transformation$")
	public void assert_the_fields_that_contains_date_and_time_by_doing_transformation(DataTable fields) throws ParseException, Throwable {
		List<String> dateFields = fields.asList();
		for (int i=0; i<dateFields.size(); i++) {
			String fieldName = dateFields.get(i);
			System.out.println("fieldName "+fieldName);
			if (fieldName.equals("createdDateTime") || fieldName.equals("ES_MODIFIEDDATETIME")
					|| fieldName.equals("modifiedDateTime")) {
				String api_createdDateTime = HashMapContainer.get("$$API_"+fieldName);
				String es_createdDateTime = HashMapContainer.get("$$ES_"+fieldName);
				System.out.println(api_createdDateTime+" before "+es_createdDateTime);
				SimpleDateFormat source_format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		        Date date = source_format.parse(es_createdDateTime);
		        SimpleDateFormat target_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		        es_createdDateTime = target_format.format(date);
		        System.out.println(api_createdDateTime+" after "+es_createdDateTime);
		        Assert.assertTrue(es_createdDateTime.contains(api_createdDateTime));
		        System.out.println(api_createdDateTime+" matches with "+es_createdDateTime);
			}else if (fieldName.equals("eta") || fieldName.equals("object_created_date")
					|| fieldName.equals("object_updated_date") || fieldName.equals("original_eta")
					|| fieldName.equals("status_date")){
				String api_createdDateTime = HashMapContainer.get("$$API_"+fieldName);
				String es_createdDateTime = HashMapContainer.get("$$ES_"+fieldName);
				Assert.assertTrue(es_createdDateTime.contains(api_createdDateTime));
		        System.out.println(es_createdDateTime+" contains "+api_createdDateTime);
			}else if (fieldName.equals("service_level_name")) {
				String api_createdDateTime = HashMapContainer.get("$$API_"+fieldName);
				String es_createdDateTime = HashMapContainer.get("$$ES_"+fieldName);
				String Decryptedvalue = new String(api_createdDateTime.getBytes("ISO-8859-1"), "UTF-8");
				Assert.assertTrue(es_createdDateTime.contains(Decryptedvalue));
		        System.out.println(Decryptedvalue+" matches with "+es_createdDateTime);
			}
		}
	}
	
	@When("^get the Key value of '(.*)' & '(.*)' and verify with the total '(.*)'$")
    public void validate_the_total_value(String key, String key1, String count) {
        String Actual = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), key);
        System.out.println(Actual);
        String Actual1 = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), key1);
        System.out.println(Actual1);
        double sum = Double.parseDouble(Actual) + Double.parseDouble(Actual1);
        System.out.println(sum);
        String data = Double.toString(sum);
        String Actual3 = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), count);
        System.out.println(Actual3);
        double sum1 = Double.parseDouble(Actual3);
        String data1 = Double.toString(sum1);
        assertEquals(data, data1);
    }
	
	@When("^Verify key '(.*)' count present in the response orders- '(.*)'$")
    public void key_count_present_in_the_response_orders(String key, String count) {

        String data = HashMapContainer.get(count);
        List<String> read = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), key);
        Assert.assertEquals(Integer.parseInt(data), read.size());
    }
	
	@When("^Key '(.*)' List should not contain more than '(.*)' set of records$")
    public static void List_should_contain_5_set_of_records(String key, int key1) throws Exception {
        List<String> read = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), key);
        int data = read.size();
//        String str1 = Integer.toString(data);
        System.out.println(data);
        try {
            int data1 = data;
//        boolean data11 =true;
            if (data1 <= key1) {
                boolean data11 = true;
                System.out.println("contain 5 set of records");
                assertTrue(data11);
            } else {
                assertTrue(false);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new CucumberException(e);
//        System.out.println("It contains more than 5 records");
        }
    }
	
	@Then("^Retrieve all item details from so_ln direct delivery Query Result and store in respective variables$")
	public void Retrieve_all_item_details_from_direct_del_Query_Result_and_store_in_respective_variables() throws Throwable {
		
		int totalCountOfRecords = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.total");
		
		for (int i=0; i<totalCountOfRecords; i++) {
			String ITM_CD_STRIPPED = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.ITM_CD_STRIPPED");
			HashMapContainer.add("$$item["+i+"].DirectDel.ITM_CD_STRIPPED", ITM_CD_STRIPPED);
			int UNIT_PRC = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.UNIT_PRC");
			HashMapContainer.add("$$item["+i+"].DirectDel.UNIT_PRC", String.valueOf(UNIT_PRC));
			int QTY = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.QTY");
			HashMapContainer.add("$$item["+i+"].DirectDel.QTY", String.valueOf(QTY));
			String PRODUCT_SIZE = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.PRODUCT_SIZE");
			HashMapContainer.add("$$item["+i+"].DirectDel.PRODUCT_SIZE", PRODUCT_SIZE);
		}
		
		for (int m=0; m<totalCountOfRecords; m++) {
			
			ElasticSearchJestUtility.getIndicesExists("itm");
			ElasticSearchJestUtility.searchQuerywithIndex("{\"query\":{\"match\":{\"ITM_CD.keyword\":\"AX-"+HashMapContainer.get("$$item["+m+"].DirectDel.ITM_CD_STRIPPED")+"\"}}}");
			
			String ModelName = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.ModelName");
			HashMapContainer.add("$$item["+m+"].DirectDel.ModelName", ModelName);
			String MASTER_ITEM_CD = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.MASTER_ITEM_CD");
			HashMapContainer.add("$$item["+m+"].DirectDel.MASTER_ITEM_CD", MASTER_ITEM_CD);
		}

		Double totalUnitPrice = 0.0;
		for (int j=0; j<totalCountOfRecords; j++) {
			Object price = HashMapContainer.get("$$item["+j+"].DirectDel.UNIT_PRC");
			if (price instanceof Double)
				totalUnitPrice = totalUnitPrice+(Double) price;
		}
		HashMapContainer.add("$$Direct_Del_shipmentTotal", totalUnitPrice.toString());
	}
	
	@Then("^Retrieve Tracking number from order delivery index by using order number$")
	public void Retrieve_Tracking_number_from_order_delivery_index_by_using_order_number() throws Throwable {
		
		ElasticSearchJestUtility.searchQuerywithIndex("{\"query\":{\"match\":{\"DEL_DOC_NUM.keyword\":\"AX-"+HashMapContainer.get("$$ES_DEL_DOC_NUM_STRIPPED")+"\"}}}");
		String trackingNumber = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.TRACKING_NUM");
		HashMapContainer.add("$$ES_TRACKING_NUM", trackingNumber);
	}
	
	@Then("^Run shippo tracking status query using '(.*)'$")
	public void Run_shippo_tracking_status_query_using_tracking_number(String trackingNo) throws Throwable {
		ElasticSearchJestUtility.searchQuerywithIndex("{\"query\":{\"match\":{\"tracking_number.keyword\":\""+HashMapContainer.get(trackingNo)+"\"}}}");
	}
	
	@Then("^Run shippo tracking history query using '(.*)'$")
	public void Run_shippo_tracking_history_query_using_tracking_number(String trackingNo) throws Throwable {
		
		ElasticSearchJestUtility.searchQuerywithIndex("{\r\n" + 
				"  \"query\": {\r\n" + 
				"    \"match\": {\r\n" + 
				"      \"tracking_number.keyword\": \""+HashMapContainer.get(trackingNo)+"\"\r\n" + 
//				"      \"tracking_number.keyword\": \"1Z09W5R50326842467\"\r\n" + 
				"    }\r\n" + 
				"  }, \"_source\": {\r\n" + 
				"    \"include\": [\"status\",\"location_country\",\"status_date\",\"location_state\",\"location_city\",\"location_zip\"]\r\n" + 
				"  }, \"size\": 1000\r\n" + 
				"}");
	}
	
	@Then("^Retrieve data '(.*)' from '(.*)' index query result with array and store in respective key variables for '(.*)'$")
	public void Retrieve_data_from_Query_Result_with_array_and_store_in_respective_key_variables(String key, String index, String flowName) {
		
		String subKey = null;
		switch(flowName) {
		case "Red Carpet":
			subKey = "$$ES_"+index+"_RedCarpet[";
			break;
		case "Direct Del":
			subKey = "$$ES_"+index+"_DirectDel[";
			break;
		}
		
		JSONArray array = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), key);
		for (int i = 0; i < array.size(); i++) {
			HashMap<String, String> array_record = (HashMap<String, String>) array.get(i);
			List<String> keys = new ArrayList<String>();
			keys.addAll(array_record.keySet());
			for (String k : keys) {
				Object value = array_record.get(k);
				if (value == null) {
					HashMapContainer.add(subKey + i + "]." + k, "");
					System.out.println(subKey + i + "]." + k + " : " + "");
				} else {
					HashMapContainer.add(subKey + i + "]." + k, value.toString());
					System.out.println(subKey + i + "]." + k + " : " + value.toString());
				}
			}
		}
	}
	
	@Then("^Retrieve all item details from so_ln red carpet Query Result and store in respective variables$")
	public void Retrieve_all_item_details_from_red_carpet_Query_Result_and_store_in_respective_variables() throws Throwable {
		
		int totalCountOfRecords = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.total");
		
		for (int i=0; i<totalCountOfRecords; i++) {
			String ITM_CD_STRIPPED = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.ITM_CD_STRIPPED");
			HashMapContainer.add("$$item["+i+"].RedCarpet.ITM_CD_STRIPPED", ITM_CD_STRIPPED);
			Object UNIT_PRC = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.UNIT_PRC");
			if (UNIT_PRC instanceof Integer || UNIT_PRC instanceof Double) {
				HashMapContainer.add("$$item["+i+"].RedCarpet.UNIT_PRC", String.valueOf(UNIT_PRC));
				System.out.println("$$item["+i+"].RedCarpet.UNIT_PRC: "+HashMapContainer.get("$$item["+i+"].RedCarpet.UNIT_PRC"));
			}
			int QTY = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.QTY");
			HashMapContainer.add("$$item["+i+"].RedCarpet.QTY", String.valueOf(QTY));
			String PRODUCT_SIZE = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.PRODUCT_SIZE");
			HashMapContainer.add("$$item["+i+"].RedCarpet.PRODUCT_SIZE", PRODUCT_SIZE);
			String SALESSTATUS = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+i+"]._source.SALESSTATUS");
			HashMapContainer.add("$$item["+i+"].RedCarpet.SALESSTATUS", SALESSTATUS);
			
		}
		
		Double totalUnitPrice = 0.0;
		for (int j=0; j<totalCountOfRecords; j++) {
			Object price = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+j+"]._source.UNIT_PRC");
			int qty = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+j+"]._source.QTY");
			System.out.println("pricee: "+price);
			System.out.println("qty: "+qty);
//			double roundOff;
			if (price instanceof Double) {
//				roundOff = Math.round((Double) price * 100.0) / 100.0;
//				System.out.println("roundOff: "+roundOff);
				totalUnitPrice = totalUnitPrice+((Double) price*qty);
				
//				totalUnitPrice = totalUnitPrice+(roundOff*qty);
				System.out.println("totalUnitPrice: "+totalUnitPrice);
			}else if (price instanceof Integer)
				totalUnitPrice = totalUnitPrice+((Integer) price*qty);
			System.out.println("totalUnitPriceInt: "+totalUnitPrice);
		}
		double totalUnitPrice_roundOff = Math.round(totalUnitPrice * 100.0) / 100.0;
		HashMapContainer.add("$$Red_Carpet_shipmentTotal", String.valueOf(totalUnitPrice_roundOff));
		System.out.println("$$Red_Carpet_shipmentTotal: "+HashMapContainer.get("$$Red_Carpet_shipmentTotal"));
		
		Double totalTax = 0.0;
		for (int k=0; k<totalCountOfRecords; k++) {
			Object tax = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits["+k+"]._source.MFIRETAILTAXAMOUNT");
			if (tax instanceof Double)
				totalTax = totalTax+(Double) tax;
			else if (tax instanceof Integer)
				totalTax = totalTax+(Integer) tax;
		}
		HashMapContainer.add("$$Red_Carpet_tax", totalTax.toString());
		System.out.println("$$Red_Carpet_tax: "+HashMapContainer.get("$$Red_Carpet_tax"));
		
		for (int m=0; m<totalCountOfRecords; m++) {
			
			ElasticSearchJestUtility.getIndicesExists("itm");
			ElasticSearchJestUtility.searchQuerywithIndex("{\"query\":{\"match\":{\"ITM_CD.keyword\":\"AX-"+HashMapContainer.get("$$item["+m+"].RedCarpet.ITM_CD_STRIPPED")+"\"}}}");
			
			String ModelName = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.ModelName");
			HashMapContainer.add("$$item["+m+"].RedCarpet.ModelName", ModelName);
			String MASTER_ITEM_CD = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.MASTER_ITEM_CD");
			HashMapContainer.add("$$item["+m+"].RedCarpet.MASTER_ITEM_CD", MASTER_ITEM_CD);
			String ProductType = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.ProductType");
			HashMapContainer.add("$$item["+m+"].RedCarpet.ProductType", ProductType);
		}
	}
	
	@Then("^Run ES query to fetch order number by filtering with Dlv date '(.*)' and Dlv mode as '(.*)' and SalesStatus as '(.*)'$")
	public void Run_ES_query_fetch_order_num_by_using_filters(String dlv_date, String dlv_mode, String salesStatus) throws Throwable {
		
		String opp_mode = null;
		switch (dlv_mode) {
			case "Red Carpet":
				opp_mode = "Direct Del";
				break;
			case "Direct Del":
				opp_mode = "Red Carpet";
				break;
		}
		ElasticSearchJestUtility.searchQuerywithIndex("{\r\n" + 
				"  \"query\": {\r\n" + 
				"    \"bool\": {\r\n" + 
				"      \"must\": [\r\n" + 
				"        {\r\n" + 
				"          \"match\": {\r\n" + 
				"            \"DLVMODE.keyword\": \""+dlv_mode+"\"\r\n" + 
				"          }\r\n" + 
				"        },{\r\n" + 
				"          \"match\": {\r\n" + 
				"            \"DELIVERY_DT\": \""+HashMapContainer.get(dlv_date)+"\"\r\n" + 
				"          }\r\n" + 
				"        },{\r\n" + 
				"          \"match\": {\r\n" + 
				"            \"SALESSTATUS.keyword\": \""+salesStatus+"\"\r\n" + 
				"          }\r\n" + 
				"        }\r\n" + 
				"      ]\r\n" + 
				"    }\r\n" + 
				"  }, \"size\": 100\r\n" + 
				"}");
		
		List<String> dlv_mode_dEL_DOC_NUMs = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[*]._source.DEL_DOC_NUM");
		HashSet<String> dlv_mode_dEL_DOC_NUM_hs = new HashSet<String>();
		dlv_mode_dEL_DOC_NUM_hs.addAll(dlv_mode_dEL_DOC_NUMs);
		List<String> only_dlv_mode_dEL_DOC_NUMs = new ArrayList<String>();
		for (String del_doc_num: dlv_mode_dEL_DOC_NUM_hs) {
			
			ElasticSearchJestUtility.searchQuerywithIndex("{\r\n" + 
					"  \"query\": {\r\n" + 
					"    \"match\": {\r\n" + 
					"      \"DEL_DOC_NUM.keyword\": \""+del_doc_num+"\"\r\n" + 
					"    }\r\n" + 
					"  } \r\n" + 
					"}");
			List<String> dlv_modes = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[*]._source.DLVMODE");
			if (!dlv_modes.contains(opp_mode)) {
				only_dlv_mode_dEL_DOC_NUMs.add(del_doc_num);
			}
		}
		System.out.println("dlv_mode_dEL_DOC_NUM_hs: "+dlv_mode_dEL_DOC_NUM_hs);
		System.out.println("only_dlv_mode_dEL_DOC_NUMs: "+only_dlv_mode_dEL_DOC_NUMs);
		HashMapContainer.addHashSetList("$$so_ln_orderNumbers", only_dlv_mode_dEL_DOC_NUMs);
	}
	
	@Then("^Run ES query to fetch order number by filtering with Dlv date '(.*)' and with both Dlv modes and SalesStatus as '(.*)'$")
	public void Run_ES_query_fetch_order_num_by_using_filters(String dlv_date, String salesStatus) throws Throwable {
		
		ElasticSearchJestUtility.searchQuerywithIndex("{\r\n" + 
				"  \"query\": {\r\n" + 
				"    \"bool\": {\r\n" + 
				"      \"must\": [\r\n" + 
				"        {\r\n" + 
				"          \"match\": {\r\n" + 
				"            \"DLVMODE.keyword\": \"Red Carpet\"\r\n" + 
				"          }\r\n" + 
				"        },{\r\n" + 
				"          \"match\": {\r\n" + 
				"            \"DELIVERY_DT\": \""+HashMapContainer.get(dlv_date)+"\"\r\n" + 
				"          }\r\n" + 
				"        },{\r\n" + 
				"          \"match\": {\r\n" + 
				"            \"SALESSTATUS.keyword\": \""+salesStatus+"\"\r\n" + 
				"          }\r\n" + 
				"        }\r\n" + 
				"      ]\r\n" + 
				"    }\r\n" + 
				"  }, \"size\": 500\r\n" + 
				"}");
		
		List<String> redCarpet_dEL_DOC_NUMs = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[*]._source.DEL_DOC_NUM");
		ElasticSearchJestUtility.searchQuerywithIndex("{\r\n" + 
				"  \"query\": {\r\n" + 
				"    \"bool\": {\r\n" + 
				"      \"must\": [\r\n" + 
				"        {\r\n" + 
				"          \"match\": {\r\n" + 
				"            \"DLVMODE.keyword\": \"Direct Del\"\r\n" + 
				"          }\r\n" + 
				"        },{\r\n" + 
				"          \"match\": {\r\n" + 
				"            \"DELIVERY_DT\": \""+HashMapContainer.get(dlv_date)+"\"\r\n" + 
				"          }\r\n" + 
				"        },{\r\n" + 
				"          \"match\": {\r\n" + 
				"            \"SALESSTATUS.keyword\": \""+salesStatus+"\"\r\n" + 
				"          }\r\n" + 
				"        }\r\n" + 
				"      ]\r\n" + 
				"    }\r\n" + 
				"  }, \"size\": 500\r\n" + 
				"}");
		
		List<String> directDel_dEL_DOC_NUMs = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[*]._source.DEL_DOC_NUM");
//		System.out.println("redCarpet_dEL_DOC_NUMs: "+redCarpet_dEL_DOC_NUMs);
//		System.out.println("directDel_dEL_DOC_NUMs: "+directDel_dEL_DOC_NUMs);
		redCarpet_dEL_DOC_NUMs.retainAll(directDel_dEL_DOC_NUMs);
//		System.out.println("redCarpet_dEL_DOC_NUMs: "+redCarpet_dEL_DOC_NUMs);
		HashMapContainer.addHashSetList("$$so_ln_orderNumbers", redCarpet_dEL_DOC_NUMs);
	}
	
	@Then("^Trigger Dispatch track API with order number to check response is not null to procced for further testing$")
	public void Trigger_Dispatch_track_API_to_check_response_is_not_null_to_procced_for_further_testing() throws Exception {
		
		List<String> orderNumbers = HashMapContainer.getHashSetList("$$so_ln_orderNumbers");
		for (String orderNumber: orderNumbers) {
			
			RestAssuredSteps.endPoint("https://mfrm.dispatchtrack.com/track_order/59cd99d/"+orderNumber.substring(3));
			RestAssuredSteps.content_type("application/json");
			RestAssuredSteps.updatequeryParams("format", "json");
			RestAssuredSteps.Method("GET");
			int resBodyLength = RestAssuredSteps.retuen_response_body_length();
			if (resBodyLength>2) {
				HashMapContainer.add("DispatchTrackOrderNumber", orderNumber);
				System.out.println("Success response from dispatch track API for orderNumber: "+orderNumber);
				break;
			}else {
				System.out.println("Null response from dispatch track API for orderNumber: "+orderNumber);
			}
		}
	}
	
	@Then("^Check order delivery index with order number to check response is not null to procced for further testing$")
	public void Check_order_delivery_index_with_order_number_to_check_response_is_not_null_to_procced_for_further_testing() throws Throwable {
		List<String> orderNumbers = HashMapContainer.getHashSetList("$$so_ln_orderNumbers");
		ElasticSearchJestUtility.getIndicesExists("orderdelivery");
		for (String orderNumber: orderNumbers) {
			ElasticSearchJestUtility.searchQuerywithIndex("{\r\n" + 
					"  \"query\": {\r\n" + 
					"    \"match\": {\r\n" + 
					"      \"DEL_DOC_NUM.keyword\": \""+orderNumber+"\"\r\n" + 
					"    }\r\n" + 
					"  } \r\n" + 
					"}");
		}
	}
	
	@Then("^fetch zipcode, HPhone, and lastname for the filtered order number$")
	public void fetch_zipcode_HPhone_and_lastname_for_the_filtered_order_number() throws Throwable {
		
		ElasticSearchJestUtility.searchQuerywithIndex("{\r\n" + 
				"  \"query\": {\r\n" + 
				"    \"match\": {\r\n" + 
				"      \"DEL_DOC_NUM.keyword\": \""+HashMapContainer.get("DispatchTrackOrderNumber")+"\"\r\n" + 
				"    }\r\n" + 
				"  } \r\n" + 
				"}");
		HashMapContainer.add("OD_ZipCode", com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.SHIP_TO_ZIP_CD"));
		HashMapContainer.add("OD_LNAME", com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.SHIP_TO_L_NAME"));
		HashMapContainer.add("OD_PhoneNumber", com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.hits[0]._source.SHIP_TO_H_PHONE1"));
	}
	
	@Then("^prepare Order delivery API request body, SO index query, so_ln_Direct_Del_query, so_ln_Red_Carpet_query, and DispatchTrackAPI$")
	public void prepare_Order_delivery_API_request_body_SO_so_ln_index_query_DispatchTrackAPI() {
		
		String dispatchTrackOrderNumber = HashMapContainer.get("DispatchTrackOrderNumber");
		String Order_delivery_API_request_body = "{\"order_number\":\""+dispatchTrackOrderNumber.substring(3)+"\",\"phonenumber\":\""+HashMapContainer.get("OD_PhoneNumber")+"\"}";
//		String Order_delivery_API_request_body = "{\"order_number\":\"S035248504\",\"phonenumber\":\"9048442429\"}";
		HashMapContainer.add("$$Order_delivery_API_request_body", Order_delivery_API_request_body);
		
		String SO_index_query = "{\"query\":{\"match\":{\"DEL_DOC_NUM.keyword\":\""+HashMapContainer.get("DispatchTrackOrderNumber")+"\"}}}";
//		String SO_index_query = "{\"query\":{\"match\":{\"DEL_DOC_NUM.keyword\":\"AX-S035248504\"}}}";
		HashMapContainer.add("$$SO_index_query", SO_index_query);
		
		String so_ln_Direct_Del_query = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"DEL_DOC_NUM.keyword\":\""+HashMapContainer.get("DispatchTrackOrderNumber")+"\"}}],\"must_not\":[{\"match\":{\"DLVMODE.keyword\":\"Red Carpet\"}}]}}}";
//		String so_ln_Direct_Del_query = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"DEL_DOC_NUM.keyword\":\"AX-S035248504\"}},{\"match\":{\"DLVMODE.keyword\":\"Direct Del\"}}]}}}";
//		String so_ln_Direct_Del_query = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"DEL_DOC_NUM.keyword\":\"AX-S035248504\"}}],\"must_not\":[{\"match\":{\"DLVMODE.keyword\":\"Red Carpet\"}}]}}}";
		HashMapContainer.add("$$so_ln_Direct_Del_query", so_ln_Direct_Del_query);
		
		String so_ln_query_Red_Carpet = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"DEL_DOC_NUM.keyword\":\""+HashMapContainer.get("DispatchTrackOrderNumber")+"\"}}],\"must_not\":[{\"match\":{\"DLVMODE.keyword\":\"Direct Del\"}}]}}}";
//		String so_ln_query_Red_Carpet = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"DEL_DOC_NUM.keyword\":\"AX-S035248504\"}},{\"match\":{\"DLVMODE.keyword\":\"Red Carpet\"}}]}}}";
//		String so_ln_query_Red_Carpet = "{\"query\":{\"bool\":{\"must\":[{\"match\":{\"DEL_DOC_NUM.keyword\":\"AX-S035248504\"}}],\"must_not\":[{\"match\":{\"DLVMODE.keyword\":\"Direct Del\"}}]}}}";
		HashMapContainer.add("$$so_ln_query_Red_Carpet", so_ln_query_Red_Carpet);
		
		String DispatchTrackAPI = "https://mfrm.dispatchtrack.com/track_order/59cd99d/"+dispatchTrackOrderNumber.substring(3);
//		String DispatchTrackAPI = "https://mfrm.dispatchtrack.com/track_order/59cd99d/S035248504";
		HashMapContainer.add("$$DispatchTrackAPI", DispatchTrackAPI);
	}
	
	@Then("^assert all the Red Carpet items along with params based on dynamic items count$")
	public void assert_all_the_Red_Carpet_items_along_with_params_based_on_dynamic_items_count() {
		
		String api_ItemsArraySize = HashMapContainer.get("$$shipments[0].totalItems");
		String es_so_ln_itemsArraySize = HashMapContainer.get("$$ES_RedCarpet_total");
		
		for (int i=0; i<Integer.valueOf(api_ItemsArraySize); i++) {
			for (int j=0; j<Integer.valueOf(es_so_ln_itemsArraySize); j++) {
				
				String api_itemCode = HashMapContainer.get("$$shipments[0].items["+i+"].itemCode");
				String es_itemCode = HashMapContainer.get("$$ES_so_ln_RedCarpet["+j+"].ITM_CD_STRIPPED");
				String api_price = HashMapContainer.get("$$shipments[0].items["+i+"].price");
				String es_price = HashMapContainer.get("$$ES_so_ln_RedCarpet["+j+"].UNIT_PRC");
				System.out.println("api_itemCode: "+api_itemCode);
				System.out.println("es_itemCode: "+es_itemCode);
				if (api_itemCode.equals(es_itemCode) && api_price.equals(es_price)) {
					System.out.println("inside if for api_itemCode: "+api_itemCode);
					if (HashMapContainer.get("$$shipments[0].items["+i+"].name").equals("DELIVERY")) {
						Assert.assertEquals("Assertion for Red Carpet Item code for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].itemCode"), HashMapContainer.get("$$ES_so_ln_RedCarpet["+j+"].ITM_CD_STRIPPED"));
						Assert.assertEquals("Assertion for Red Carpet Item name for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].name"), HashMapContainer.get("$$item["+j+"].RedCarpet.ModelName"));
						Assert.assertEquals("Assertion for Red Carpet Item masterItemCode for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].masterItemCode"), HashMapContainer.get("$$item["+j+"].RedCarpet.MASTER_ITEM_CD"));
						Assert.assertTrue("Assertion for Red Carpet Item price for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].price").contains(HashMapContainer.get("$$ES_so_ln_RedCarpet["+j+"].UNIT_PRC")));
						Assert.assertTrue("Assertion for Red Carpet Item quantity for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].quantity").contains(HashMapContainer.get("$$ES_so_ln_RedCarpet["+j+"].QTY")));
						Assert.assertEquals("Assertion for Red Carpet Item deliveryStatus for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].deliveryStatus"), "");
						String salesStatus = HashMapContainer.get("$$ES_so_ln_RedCarpet["+j+"].SALESSTATUS");
						if (salesStatus.equals("1"))
							salesStatus = "Open";
						else if (salesStatus.equals("2"))
							salesStatus = "Delivered";
						else if (salesStatus.equals("3"))
							salesStatus = "Invoiced";
						else if (salesStatus.equals("4"))
							salesStatus = "Cancelled";
						Assert.assertEquals("Assertion for Red Carpet Item orderLineStatus for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].orderLineStatus"), salesStatus);
						Assert.assertEquals("Assertion for Red Carpet Item productType for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].productType"), HashMapContainer.get("$$item["+j+"].RedCarpet.ProductType"));
						Assert.assertEquals("Assertion for Red Carpet Item size for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].size"), HashMapContainer.get("$$ES_so_ln_RedCarpet["+j+"].PRODUCT_SIZE"));
						break;
					}else {
						Assert.assertEquals("Assertion for Red Carpet Item code for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].itemCode"), HashMapContainer.get("$$ES_so_ln_RedCarpet["+j+"].ITM_CD_STRIPPED"));
						Assert.assertEquals("Assertion for Red Carpet Item name for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].name"), HashMapContainer.get("$$item["+j+"].RedCarpet.ModelName"));
						Assert.assertEquals("Assertion for Red Carpet Item masterItemCode for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].masterItemCode"), HashMapContainer.get("$$item["+j+"].RedCarpet.MASTER_ITEM_CD"));
						Assert.assertTrue("Assertion for Red Carpet Item price for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].price").contains(HashMapContainer.get("$$ES_so_ln_RedCarpet["+j+"].UNIT_PRC")));
						Assert.assertTrue("Assertion for Red Carpet Item quantity for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].quantity").contains(HashMapContainer.get("$$ES_so_ln_RedCarpet["+j+"].QTY")));
						Assert.assertEquals("Assertion for Red Carpet Item deliveryStatus for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].deliveryStatus"), HashMapContainer.get("$$shipments[0].trackingInfo.status"));
						String salesStatus = HashMapContainer.get("$$ES_so_ln_RedCarpet["+j+"].SALESSTATUS");
						if (salesStatus.equals("1"))
							salesStatus = "Open";
						else if (salesStatus.equals("2"))
							salesStatus = "Delivered";
						else if (salesStatus.equals("3"))
							salesStatus = "Invoiced";
						else if (salesStatus.equals("4"))
							salesStatus = "Cancelled";
						Assert.assertEquals("Assertion for Red Carpet Item orderLineStatus for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].orderLineStatus"), salesStatus);
						Assert.assertEquals("Assertion for Red Carpet Item productType for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].productType"), HashMapContainer.get("$$item["+j+"].RedCarpet.ProductType"));
						Assert.assertEquals("Assertion for Red Carpet Item size for api_itemCode: "+api_itemCode, HashMapContainer.get("$$shipments[0].items["+i+"].size"), HashMapContainer.get("$$ES_so_ln_RedCarpet["+j+"].PRODUCT_SIZE"));
						
	//					String api_itemCodee = HashMapContainer.get("$$shipments[0].items["+i+"].itemCode");
	//					String api_name = HashMapContainer.get("$$shipments[0].items["+i+"].name");
	//					String api_masterItemCode = HashMapContainer.get("$$shipments[0].items["+i+"].masterItemCode");
	//					String api_price = HashMapContainer.get("$$shipments[0].items["+i+"].price");
	//					String api_quantity = HashMapContainer.get("$$shipments[0].items["+i+"].quantity");
	//					String api_deliveryStatus = HashMapContainer.get("$$shipments[0].items["+i+"].deliveryStatus");
	//					String api_orderLineStatus = HashMapContainer.get("$$shipments[0].items["+i+"].orderLineStatus");
	//					String api_productType = HashMapContainer.get("$$shipments[0].items["+i+"].productType");
	//					String api_size = HashMapContainer.get("$$shipments[0].items["+i+"].size");
						
						break;
					}
				}
			}
		}
		
	}
	
	
	
	
	
}
