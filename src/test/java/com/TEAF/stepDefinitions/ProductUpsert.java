package com.TEAF.stepDefinitions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.minidev.json.JSONValue;

public class ProductUpsert {
	
	private String Name;
	private String Description;
	private boolean IsActive;
	private String Brand__c;
	private String Product_Type__c;
	private String Size__c;
	private String VSN__c;
	private String Vendor__c;
	private String Warranty__c;
	private String Core_Price__c;
	private String Merch_Sales_Price__c;
	private String Domain__c;
	private String Class__c;
	private String Segment__c;
	private String Box__c;
	private String Disposition__c;

	public String getName() {
	return Name;
	}

	public void setName(String Name) {
	this.Name = Name;
	}

	public String getDescription() {
	return Description;
	}

	public void setDescription(String Description) {
	this.Description = Description;
	}

	public boolean getIsActive() {
		return IsActive;
		}

	public void setIsActive(boolean IsActive) {
	this.IsActive = IsActive;
	}

	public String getBrand__c() {
	return Brand__c;
	}

	public void setBrand__c(String Brand__c) {
	this.Brand__c = Brand__c;
	}

	public String getProduct_Type__c() {
	return Product_Type__c;
	}

	public void setProduct_Type__c(String Product_Type__c) {
	this.Product_Type__c = Product_Type__c;
	}

	public String getSize__c() {
	return Size__c;
	}

	public void setSize__c(String Size__c) {
	this.Size__c = Size__c;
	}

	public String getVSN__c() {
	return VSN__c;
	}

	public void setVSN__c(String VSN__c) {
	this.VSN__c = VSN__c;
	}

	public String getVendor__c() {
	return Vendor__c;
	}

	public void setVendor__c(String Vendor__c) {
	this.Vendor__c = Vendor__c;
	}

	public String getWarranty__c() {
	return Warranty__c;
	}

	public void setWarranty__c(String Warranty__c) {
	this.Warranty__c = Warranty__c;
	}

	public String getCore_Price__c() {
	return Core_Price__c;
	}

	public void setCore_Price__c(String Core_Price__c) {
	this.Core_Price__c = Core_Price__c;
	}

	public String getMerch_Sales_Price__c() {
	return Merch_Sales_Price__c;
	}

	public void setMerch_Sales_Price__c(String Merch_Sales_Price__c) {
	this.Merch_Sales_Price__c = Merch_Sales_Price__c;
	}

	public String getDomain__c() {
	return Domain__c;
	}

	public void setDomain__c(String Domain__c) {
	this.Domain__c = Domain__c;
	}

	public String getClass__c() {
	return Class__c;
	}

	public void setClass__c(String Class__c) {
	this.Class__c = Class__c;
	}

	public String getSegment__c() {
	return Segment__c;
	}

	public void setSegment__c(String Segment__c) {
	this.Segment__c = Segment__c;
	}

	public String getBox__c() {
	return Box__c;
	}

	public void setBox__c(String Box__c) {
	this.Box__c = Box__c;
	}

	public String getDisposition__c() {
	return Disposition__c;
	}

	public void setDisposition__c(String Disposition__c) {
	this.Disposition__c = Disposition__c;
	}
}
