package com.TEAF.stepDefinitions;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.TEAF.Hooks.Hooks;
import com.TEAF.customUtilities.ECom_Exception;
import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;
import com.steadystate.css.parser.ParseException;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.CucumberException;

public class CustomMethods {

	public static WebDriver driver = StepBase.getDriver();
	static Utilities util = new Utilities();
	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CustomMethods.class.getName());
	
	@When("^I navigate to '(.*)' FILE$")
	public static void i_navigate_to_file(String url) throws ECom_Exception, Exception {
		
		String methodName= "i_navigate_to_file '"+url+"'";

		try {
			String UpdateUrl = url;
			driver.navigate().to("file:///"+System.getProperty("user.dir")+UpdateUrl);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
			
		}
	}
	
	@When("^I press key: enter$")
	public static void i_press_enter_key_() throws Exception {
		
		String methodName= "i_press_enter_key_";

		try {
			Actions ac = new Actions(driver);
			ac.sendKeys(Keys.ENTER).build().perform();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);

		}
	}
	
	
	@Then("^I close the marketing popup$")
	public static void i_close_marketing_popup() throws ECom_Exception, Exception {
		
		String methodName= "i_close_marketing_popup";

		try {
			WebDriverWait wb = new WebDriverWait(driver, 10);
			try {
				WebElement attentive = driver.findElement(By.xpath("//iframe[@id='attentive_creative']"));
				wb.until(ExpectedConditions.visibilityOf(attentive));
				driver.switchTo().frame(attentive);
				log.info("Switched to Iframe");
				WebElement marketingpopup = driver.findElement(By.xpath("//button[@id=\"closeIconContainer\"]"));
				wb.until(ExpectedConditions.visibilityOf(marketingpopup));
				marketingpopup.click();
//			Thread.sleep(1000);
				
			} catch (Exception e) {
				CommonSteps.I_wait_for_visibility_of_element("DavidsBridalLogo");
				
			}
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
		
		
	}

	@When("^I navigate back$")
	public static void i_navigate_back() throws ECom_Exception, Exception {
		
		String methodName= "i_navigate_back";

		try {
			driver.navigate().back();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}


	@Given("^I scroll using actions$")
	public static void i_scroll_usingactions() throws ECom_Exception, Exception {
		
		String methodName= "i_scroll_usingactions";

		try {
			Actions ac = new Actions(driver);
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			Thread.sleep(2000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Given("^I scroll to header of the page$")
	public static void i_scroll_to_top_of_the_page() throws ECom_Exception, Exception {
		
		String methodName= "i_scroll_to_top_of_the_page";

		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollTo(0, -document.body.scrollHeight)");
			Thread.sleep(3000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Given("^I switch to alert and accept$")
	public static void i_switchto_alert() throws ECom_Exception, Exception {
		
		String methodName= "i_switchto_alert";

		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}
	
	@Then("^I click continue shopping popup if it is present on the page$")
	public static void I_click_continue_shopping_popup() throws Exception {
		
		String methodName= "I_click_continue_shopping_popup";

		try {
			try {
			WebDriverWait wb = new WebDriverWait(driver, 40);
			WebElement shoppingpopup = driver.findElement(GetPageObjectRead.OR_GetElement("CntnueShoppingPopup"));
			shoppingpopup.click();
			Thread.sleep(1000);
		} catch (Exception e) {
				log.info("Continue Popup is not displayed");
				CommonSteps.I_should_see_on_page("DeleteList_FavPage");
				
			}
		}catch(Exception e) {
				CommonSteps.I_should_see_on_page("MyFavorites_MyAccPage");
				CommonSteps.I_click("MyFavorites_MyAccPage");
				e.printStackTrace();
			    new ECom_Exception(methodName, e);
		}
	}

	@Given("^I scroll to click Apply button$")
	public static void i_scroll_to_click_apply() throws ECom_Exception, Exception {
		
		String methodName= "i_scroll_to_click_apply";

		try {
			Actions ac = new Actions(driver);
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			Thread.sleep(2000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Given("^I scroll to click logout$")
	public static void i_scroll_down_using_actions() throws ECom_Exception, Exception {
		
		String methodName= "i_scroll_down_using_actions";

		try {
			Actions ac = new Actions(driver);
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			ac.sendKeys(Keys.TAB).build().perform();
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Given("^I select the suggestion$")
	public static void i_select_suggestion() throws ECom_Exception, Exception {
		
		String methodName= "i_select_suggestion";

		try {
			Actions ac = new Actions(driver);
			ac.sendKeys(Keys.DOWN).build().perform();
			ac.sendKeys(Keys.ENTER).build().perform();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@When("^I change the screen dimension to '(.*)' - '(.*)'$")
	public static void i_change_the_screen_dimension(int width, int height) throws Exception {
		
		String methodName= "i_change_the_screen_dimension '"+width+"' '"+height+"'";

		try {
			driver.manage().window().setSize(new Dimension(width, height));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}
	
	@Then("^I verify Header Main Nav Topic Heading$")
	public static void i_verify_Main_Nav_Heading() throws ECom_Exception, Exception {
		
		String methodName= "i_verify_Main_Nav_Heading";


		try {
			List<WebElement> headingElement = driver
					.findElements(By.xpath("//div[@class=\"header__main-nav-topic-heading\"]"));

			for (WebElement menu : headingElement) {

//			Actions ac = new Actions(driver);
//			JavascriptExecutor executor = (JavascriptExecutor)driver;
//			executor.executeScript("arguments[0].click();", menu);
//				ac.moveToElement(menu).build().perform();
//				ac.click(menu).build().perform();
//				WebDriverWait wb = new WebDriverWait(driver, 20);
				CommonSteps.I_should_see_on_page("menu");
//				CommonSteps.I_click("DavidsBridalLogo");
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I select recently created Favorite list from the dropdown$")
	public static void i_select_recently_created_favorite_liste_from_the_dropdown() throws ECom_Exception, Exception {
		
		String methodName= "i_select_recently_created_favorite_liste_from_the_dropdown";
		
		try {
			String FavList = HashMapContainer.get("$$DBIFavList");
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			WebElement ListofShopping = driver
					.findElement(By.xpath("//a[@class=\"tlignore\" and contains(text(),'" + FavList + "')]"));
			Thread.sleep(2000);
			 executor.executeScript("arguments[0].click();", ListofShopping);
//		ListofShopping.click();
			Thread.sleep(2000);
//		CommonSteps.I_clickJS("$$NewListFieldPopup");
//		Thread.sleep(1000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	

	@Then("^I generate random email id and get text from email field and enter in the verify email id field '(.*)'$")
	public static void i_get_text_from_email_field_and_enter_in_the_verify_email_id_field(String Locator)
			throws ECom_Exception, Exception {
		
		String methodName= "i_get_text_from_email_field_and_enter_in_the_verify_email_id_field '"+Locator+"'";

		try {
			Actions ac = new Actions(driver);
			WebElement emailfield = driver.findElement(By.xpath("//input[@id=\"email1\"]"));
			emailfield.clear();
			emailfield.click();
			Random randomGenerator = new Random();
			int randomInt = randomGenerator.nextInt(2000);
			String email = "QA_Automation" + randomInt + "@gmail.com";
					emailfield.sendKeys(email);
			WebDriverWait wb = new WebDriverWait(driver, 20);
			WebElement verifyemailfield = driver.findElement(GetPageObjectRead.OR_GetElement(Locator));
			verifyemailfield.click();
			verifyemailfield.sendKeys(email);
			Thread.sleep(1000);
			if (!Locator.equals("FianceemailSignUp")) {
				HashMapContainer.add("$$RegisteredEmailId", "QA_Automation" + randomInt + "@gmail.com");
				java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));

				FileInputStream fin = new FileInputStream(f);
				Workbook wb1 = new XSSFWorkbook(fin);
				Sheet sheetAt = wb1.getSheet("Registration");
				int prows = sheetAt.getPhysicalNumberOfRows();
				sheetAt.createRow(prows).createCell(0).setCellValue(email);
				FileOutputStream fout = new FileOutputStream(f);
				wb1.write(fout);
				wb1.close();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}


	}


	@Then("^I close the alert popup if it is present$")
	public static void i_close_the_aleret_popup() throws ECom_Exception, Exception {
		
		String methodName= "i_close_the_aleret_popup";

		try {
			WebElement popup = driver.findElement(By.xpath("//button[@class=\"mt-lightbox__close\"]"));
			popup.click();
			Thread.sleep(2000);
		} catch (Exception e) {
			
			CommonSteps.I_should_see_on_page("DavidsBridalLogo");
			Thread.sleep(2000);
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
		
	}
	
	@Then("^I verify Header Main Nav Topic Heading for UK or CA$")
	public static void i_verify_header_main_nav_topic_heading_for_uk_and_ca() throws ECom_Exception, Exception {
		
		String methodName= "i_verify_header_main_nav_topic_heading_for_uk_and_ca";


		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			List<WebElement> headingElement = driver
					.findElements(By.xpath("(//div[@class='header__main-nav-topic-heading']/a)"));
			for(int j=0;j<headingElement.size();j++)
			{
				WebElement HeadingMenu = driver.findElement(By.xpath("(//div[@class='header__main-nav-topic-heading']/a)["+(j+1)+"]"));
				Boolean element = HeadingMenu.isDisplayed();
				assertTrue(element);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}

	@Then("^I scroll to bottom of the page$")
	public static void i_scroll_to_bottom() throws ECom_Exception, Exception {
		
		String methodName= "i_scroll_to_bottom";

		try {
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}


		
	}
	
	@Then("^I verify Footer Links Heading$")
    public static void i_verify_footer_links_heading() throws ECom_Exception, Exception {
		
		String methodName= "i_verify_footer_links_heading";

        
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			Actions ac = new Actions(driver);
//        int errorCount=0;
			List<WebElement> footerLink = driver.findElements(By.xpath("//div[@id='footer']//a"));
			for (int j = 0; j <footerLink.size(); j++) {
				
				WebElement FooterMenu = driver.findElement(By.xpath("(//div[@id='footer']//a)["+(j+1)+"]"));
				Boolean submenu = FooterMenu.isDisplayed();
				assertTrue(submenu);
				
//            ac.click(driver.findElement(By.xpath("(//div[@id='footer']//a)["+(j+1)+"]"))).build().perform();;
//            String title = driver.getTitle();
//            if (title.length() > 1 && !title.toLowerCase().contains("error")) {
//                assertTrue(true);
//                log.info("Page Loaded Successfully "+title);
//            } else {
//                log.error("Page Load error "+ title);
//                errorCount++;
//            }
//            driver.navigate().back();
//        }
//       
//        if (errorCount>1) {
//            fail("Any of Footer link failure");
//        }
   }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);		
			}
	}
	

//	@When("^I get reset password link from the provided email and Navigate to Reset Password link$")
//	public static void i_get_reset_password_link_from_email_provided() throws ECom_Exception, Exception {
//		
//		String methodName= "i_get_reset_password_link_from_email_provided";
//
//		try {
//			String resetPassWordLinkUrl = FetchingEmail.getResetPassWordLink();
//			driver.navigate().to(resetPassWordLinkUrl);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			new ECom_Exception(methodName, e);		
//		}
//	}

	@Then("^I Verify Business Navigation Menu '(.*)' and sub menus pages are loaded$")
    public static void i_verify_business_navigation_menus(String businesNav) throws ECom_Exception, Exception {
		
		String methodName= "i_verify_business_navigation_menus '"+businesNav+"'";

        try {
			Actions ac = new Actions(driver);
			String fid = driver.getWindowHandle();
			WebDriverWait wb = new WebDriverWait(driver, 10);
			WebElement busNav = driver
			        .findElement(By.xpath("//nav[@class='header__business-nav']//li//a[text()='" + businesNav + "']"));
			ac.moveToElement(busNav).build().perform();
			List<WebElement> navlinkDD = driver
			        .findElements(By.xpath("(//div[@class='header__business-nav-link-dropdown'])[4]//li"));
			wb.until(ExpectedConditions.visibilityOfAllElements(navlinkDD));
			int errorCount = 0;
			for (int i = 0; i < navlinkDD.size(); i++) {
			    driver.navigate().refresh();

 

			    String mainTitle = driver.getCurrentUrl();
			    busNav = driver
			            .findElement(By.xpath("//nav[@class='header__business-nav']//li//a[text()='" + businesNav + "']"));
			    wb.until(ExpectedConditions.visibilityOf(busNav));
			    boolean displayed = busNav.isDisplayed();
			    ac.moveToElement(busNav).build().perform();
			    Thread.sleep(2000);
			    WebElement menuItem = driver.findElement(
			            By.xpath("(//div[@class='header__business-nav-link-dropdown'])[4]//li[" + (i + 1) + "]"));
			    String menuitemText = menuItem.getText();
			    log.info(menuitemText);
			    wb.until(ExpectedConditions.visibilityOf(menuItem));
			    wb.until(ExpectedConditions.elementToBeClickable(menuItem));
			    // JavascriptExecutor executor = (JavascriptExecutor) driver;
			    // executor.executeScript("arguments[0].click();", menuItem);
			    WebElement menus = driver.findElement(By.xpath(
			            "(//div[@class='header__business-nav-link-dropdown'])[4]//li//a[text()='"+menuitemText+"']"));
			    JavascriptExecutor executor = (JavascriptExecutor) driver;
			    executor.executeScript("arguments[0].click();", menus);
			    Thread.sleep(1500);
			    // CommonSteps.I_should_see_on_page("DavidsBridalLogo");
			    Set<String> wid = driver.getWindowHandles();
			    // System.out.println(wid.size());
			    boolean flag = false;

 

			    if (wid.size() >= 2) {
			        // waitForNewWindow(driver,10);
			        for (String pid : wid) {
			            if (!pid.equals(fid)) {
			                driver.switchTo().window(pid);
			                String title = driver.getTitle();
			                if (title.length() > 2 && !title.toLowerCase().contains("denied")
			                        && !title.toLowerCase().contains("error")
			                        && !driver.getCurrentUrl().toLowerCase().equals(mainTitle)) {
			                    assertTrue(true);
			                    log.info("Page Loaded Successfully " + driver.getTitle());
			                } else {
			                    log.info("Page Load error " + menuitemText);
			                }
			                driver.close();
			                driver.switchTo().window(fid);
			                flag = true;
			            }
			        }
			    }
			    if (flag) {
			        continue;
			    }

 

			    List<WebElement> pageContent = driver
			            .findElements(By.xpath("//div[@id='page' or @id='content' or @role='main']"));
			    String title = driver.getTitle();
			    if (pageContent.size() >= 1 && title.length() > 2 && !title.toLowerCase().contains("denied")
			            && !title.toLowerCase().contains("error")
			            && !driver.getCurrentUrl().toLowerCase().equals(mainTitle)) {
			        assertTrue(true);
			        log.info("Page Loaded Successfully " + driver.getTitle());
			    } else {
			        log.error("Page Load error " + menuitemText);
			        errorCount++;
			    }

 

			}
			if (errorCount > 1) {
			    fail("Business Nav Validation Failed");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);		
		}

 

    }
 
	    
	
	@Then("^I Verify Business Navigation Menu '(.*)' and sub menus pages are loaded in PROD$")
	public static void i_verify_business_navigation_menus_in_prod(String businesNav) throws ECom_Exception, Exception {
		
		String methodName= "i_verify_business_navigation_menus_in_prod '"+businesNav+"'";

		try {
			Actions ac = new Actions(driver);
			String fid = driver.getWindowHandle();
			WebDriverWait wb = new WebDriverWait(driver, 10);
			WebElement busNav = driver.findElement(By.xpath("//a[@onclick=\"businessNavClickEvent('Inspiration');\"]"));
			ac.moveToElement(busNav).build().perform();
			List<WebElement> navlinkDD = driver
			        .findElements(By.xpath("//div[@data-dropdown-menu='" + businesNav + "']//li//a"));
			wb.until(ExpectedConditions.visibilityOfAllElements(navlinkDD));
			int errorCount = 0;
			for (int i = 0; i < navlinkDD.size(); i++) {
			    driver.navigate().refresh();

 

			    String mainTitle = driver.getCurrentUrl();
			    busNav = driver
			            .findElement(By.xpath("//li[@data-menu-topic='Inspiration']/a"));
			    wb.until(ExpectedConditions.visibilityOf(busNav));
			    boolean displayed = busNav.isDisplayed();
			    ac.moveToElement(busNav).build().perform();
			    Thread.sleep(2000);
			    WebElement menuItem = driver.findElement(
			            By.xpath("(//div[@data-dropdown-menu='" + businesNav + "']//li//a)["+(i+1)+"]"));
			    String menuitemText = menuItem.getText();
			    log.info(menuitemText);
			    wb.until(ExpectedConditions.visibilityOf(menuItem));
			    wb.until(ExpectedConditions.elementToBeClickable(menuItem));
			    // JavascriptExecutor executor = (JavascriptExecutor) driver;
			    // executor.executeScript("arguments[0].click();", menuItem);
			    WebElement menus = driver.findElement(By.xpath(
			            "//div[@data-dropdown-menu='Inspiration']//li//a[text()='" + menuitemText + "']"));
			    JavascriptExecutor executor = (JavascriptExecutor) driver;
			    executor.executeScript("arguments[0].click();", menus);
			    Thread.sleep(1500);
			    // CommonSteps.I_should_see_on_page("DavidsBridalLogo");
			    Set<String> wid = driver.getWindowHandles();
			    // System.out.println(wid.size());
			    boolean flag = false;

 

			    if (wid.size() >= 2) {
			        // waitForNewWindow(driver,10);
			        for (String pid : wid) {
			            if (!pid.equals(fid)) {
			                driver.switchTo().window(pid);
			                String title = driver.getTitle();
			                if (title.length() > 2 && !title.toLowerCase().contains("denied")
			                        && !title.toLowerCase().contains("error")
			                        && !driver.getCurrentUrl().toLowerCase().equals(mainTitle)) {
			                    assertTrue(true);
			                    log.info("Page Loaded Successfully " + driver.getTitle());
			                } else {
			                    log.info("Page Load error " + menuitemText);
			                }
			                driver.close();
			                driver.switchTo().window(fid);
			                flag = true;
			            }
			        }
			    }
			    if (flag) {
			        continue;
			    }

 

			    List<WebElement> pageContent = driver
			            .findElements(By.xpath("//div[@id='page' or @id='content' or @role='main']"));
			    String title = driver.getTitle();
			    if (pageContent.size() >= 1 && title.length() > 2 && !title.toLowerCase().contains("denied")
			            && !title.toLowerCase().contains("error")
			            && !driver.getCurrentUrl().toLowerCase().equals(mainTitle)) {
			        assertTrue(true);
			        log.info("Page Loaded Successfully " + driver.getTitle());
			    } else {
			        log.error("Page Load error " + menuitemText);
			        errorCount++;
			    }

 

			}
			if (errorCount > 1) {
			    fail("Business Nav Validation Failed");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);		
		}

 

    }
	
	@When("^I get the value in the feild '(.*)' and store in spreadsheet column '(.*)'$")
	public static void i_get_value_in_feild_and_store(String element, String column) throws ECom_Exception, Exception {
		
		String methodName= "i_get_value_in_feild_and_store '"+element+"' '"+column+"'";
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element),
					Integer.parseInt(System.getProperty("test.implicitlyWait")));
			String value = wElement.getAttribute("value");
			//HashMapContainer.add("$$" + element, value);
			System.out.println("Value in the feild "+value);
			java.io.File f = new java.io.File(System.getProperty("test.testdataFile"));

			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheetAt = wb.getSheet("Registration");
			int physicalNumberOfCells = sheetAt.getRow(0).getPhysicalNumberOfCells();
			for (int i = 0; i < physicalNumberOfCells; i++) {
				String appValue = sheetAt.getRow(0).getCell(i).getStringCellValue();
				if (appValue.equals(column)) {
					for (int j = 0; j < sheetAt.getPhysicalNumberOfRows(); j++) {
						if (sheetAt.getRow(j) == null) {
							continue;
						}

						String email = sheetAt.getRow(j).getCell(0).getStringCellValue();
						// System.out.println(HashMapContainer.get("$$EmailIdAppointment"));
						if (email.toLowerCase().equals(HashMapContainer.get("$$RegisteredEmailId"))) {
							sheetAt.getRow(j).createCell(i).setCellValue(value);
							Hooks.scenario.write(value);

						}
					}
				}
			}
			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();
		} catch (Exception e) {
			e.printStackTrace();
			new ECom_Exception(methodName, e);		
		}
	}

	@When("^I login the application with new registered User$")
	public static void i_login_the_application_new_registered_user() throws ECom_Exception, Exception {
		
		String methodName= "i_login_the_application_new_registered_user";

		try {
			try {
				WebDriverWait wb = new WebDriverWait(driver, 10);
				wb.until(ExpectedConditions
						.visibilityOf(driver.findElement(GetPageObjectRead.OR_GetElement("LoginLink_Header"))));
				driver.findElement(GetPageObjectRead.OR_GetElement("LoginLink_Header")).click();
				CommonSteps.I_clickJS("LoginLink_Header");
				CommonSteps.I_should_see_on_page("EmailFieldLogin");
				CommonSteps.I_enter_in_field(HashMapContainer.get("$$RegisteredEmailId"), "EmailFieldLogin");
				CommonSteps.I_enter_in_field("Cyber@123", "PwdFieldLogin");
				CommonSteps.I_doubleClick("LoginSubmitBtn");
				CommonSteps.I_wait_for_visibility_of_element("HeaderPersonalNavAccount");
				CommonSteps.I_should_see_on_page("HeaderPersonalNavAccount");

			} catch (org.openqa.selenium.NoSuchElementException e) {
				CommonSteps.I_should_see_on_page("HeaderPersonalNavAccount");

				log.info("User Already Logged In");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);		
		}

	}
	
	@When("^I Press Key down to select option '(.*)' for PLP$")
	public static void i_press_key_down_to_select_PLP(String option) throws ECom_Exception, Exception {
		
		String methodName= "i_press_key_down_to_select_PLP '"+option+"'";

		try {
			Actions ac = new Actions(driver);
			Thread.sleep(1000);
			if (option.equalsIgnoreCase("Recommended")) {

			} else if (option.equalsIgnoreCase("Newest")) {
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();

			} else if (option.contains("Price High to Low")) {
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();

			} else if (option.contains("Price Low to High")) {
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();
				ac.sendKeys(Keys.ARROW_DOWN).build().perform();

			} 
			ac.sendKeys(Keys.ENTER).build().perform();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);		
		}

	}
	
	@When("^I select value '(.*)' in dropdown '(.*)' by '(.*)'$")
	public static void i_select_value_in_dropdown(String option, String Locator, String type) throws ECom_Exception, Exception {
		
		String methodName= "i_select_value_in_dropdown '"+option+"' '"+Locator+"' '"+type+"'";

		try {
			WebElement ddvalue = driver.findElement(GetPageObjectRead.OR_GetElement(Locator));
			ddvalue.click();
			Select Sc = new Select(ddvalue);
			if(type.equals("text"))
			{
				Sc.selectByVisibleText(option);
				Thread.sleep(2000);
				
			}else if (type.equals("index")) {
			int indexoption = Integer.parseInt(option);
				Sc.selectByIndex(indexoption);
				Thread.sleep(1000);

			}else {
				Sc.selectByValue(option);
				Thread.sleep(1000);
					
}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);		
		}
	
	}
	
	@Then("^I click Accept if it is present on the page$")
	public static void i_click_accept_if_it_is_present_on_the_page() throws ECom_Exception, Exception {
		
		String methodName= "i_click_accept_if_it_is_present_on_the_page";

		try {
			try {
				WebElement accept = driver.findElement(By.xpath("//a[text()='ACCEPT']"));
				accept.click();
				Thread.sleep(1000);
			} catch (Exception e) {
				WebElement Logo = driver.findElement(By.xpath("//a[@class=\"header__logo-link\"]//img"));
				Logo.click();
				Thread.sleep(1000);
				e.printStackTrace();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);		
		}
	}
	
	

	@When("^I Clear the cart if product is present in it$")
	public void i_clear_the_cart_if_product_present_in_it() throws Exception {
		
		String methodName= "i_clear_the_cart_if_product_present_in_it";

		try {
			CommonSteps.I_doubleClick("CartSymbol");
			WebElement YourShoppingCartHeader = WrapperFunctions.getElementByLocator("YourShoppingCartHeader",
					GetPageObjectRead.OR_GetElement("YourShoppingCartHeader"), 60);
			String actual = YourShoppingCartHeader.getText();
			Assert.assertTrue(actual.contains("Your shopping cart"));
			Actions ac = new Actions(driver);
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			WebDriverWait wb = new WebDriverWait(driver, 40);

			try {
				List<WebElement> removeItems = driver.findElements(By.xpath("//a[@class='remove_item']"));
				System.out.println("Size " + removeItems.size());
				for (int i = 0; i < removeItems.size(); i++) {
					System.out.println("Remove Item " + i);

					WebElement removeItem = driver.findElement(By.xpath("(//a[@class='remove_item'])[1]"));
					wb.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOf(removeItem));
					wb.until(ExpectedConditions.elementToBeClickable(removeItem));
					ac.doubleClick(removeItem).build().perform();
					Thread.sleep(1000);
					WebElement removeMesage = driver.findElement(By.id("ErrorMessageText"));
					wb.until(ExpectedConditions.invisibilityOf(removeMesage));
					if ((i + 1) == removeItems.size()) {
						try {
							WebElement EmptyCart = driver.findElement(By.id("WC_EmptyShopCartDisplayf_div_1"));
							Assert.assertTrue(EmptyCart.isDisplayed());
						} catch (Exception e) {
							log.error("Remove Link is not working");
						}
					}
				}

			} catch (org.openqa.selenium.NoSuchElementException e) {
				log.error("Remove Link is not working");
				driver.findElement(GetPageObjectRead.OR_GetElement("DavidsBridalLogo")).click();

			}
			driver.manage().timeouts().implicitlyWait(Integer.parseInt(System.getProperty("test.implicitlyWait")),
					TimeUnit.SECONDS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
			}
	}

	

	@Then("^I generate random value and enter in the field '(.*)'$")
	public static void i_generate_random_value_and_enter_in_the_field(String Locator) throws ECom_Exception, Exception {
		
		String methodName= "i_generate_random_value_and_enter_in_the_field '"+Locator+"'";


		try {
			WebElement TextField = driver.findElement(GetPageObjectRead.OR_GetElement(Locator));
			TextField.clear();
			TextField.click();
			final int length = 7;
			String randomstring = RandomStringUtils.randomAlphabetic(length);
			TextField.sendKeys("FAV" + randomstring);
			HashMapContainer.add("$$DBIFavList", "FAV" + randomstring);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}

	

	@When("^I select Event role '(.*)' in the registration page$")
	public static void i_select_event_role_in_registration_page(String role)
			throws ECom_Exception, Exception {
		
		String methodName= "i_select_event_role_in_registration_page '"+role+"'";

		try {
			String option = null;
			if (role.contains("-")) {
				String[] split = role.split("-");
				role = split[0];
				option = split[1];
			}
			CommonSteps.I_select_option_in_dd_by(role, "EventroleselectSignUp", "text");
			if (option != null) {
				CommonSteps.I_click("EngagementdateSignUp");
				i_select_the_event_date_month_registration("7");
				if (option.equalsIgnoreCase("yes")) {
					driver.findElement(By.id("dressRadioButY")).click();
				} else if (option.equalsIgnoreCase("no")) {
					driver.findElement(By.id("dressRadioButN")).click();
				}
			}
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}

	}
	

	@When("^I Select the Event Date '(.*)' in the registration page$")
	public static void i_select_the_event_date_month_registration(String date)
			throws ECom_Exception, Exception {
		
		String methodName= "i_select_the_event_date_month_registration '"+date+"'";

		try {
			WebDriverWait wb = new WebDriverWait(driver, 40);

			WebElement eventDate = driver.findElement(By.className("ui-datepicker-month"));
			Select monthDD = new Select(eventDate);

			LocalDate today = LocalDate.now();
			String[] dateSplit = today.toString().split("-");
			String currentDate = dateSplit[2];
			String currentMonth = dateSplit[1];

			if (currentDate.startsWith("0")) {
				currentDate = currentDate.substring(1);
			}
			if (currentMonth.startsWith("0")) {
				currentMonth = currentMonth.substring(1);
			}

			int currentDateinNum = Integer.parseInt(currentDate);
			int currentMonthinNum = Integer.parseInt(currentMonth);

			String[] monthNames = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
					"October", "November", "December" };

			currentMonth = monthNames[currentMonthinNum - 1];
			eventDate = driver.findElement(By.className("ui-datepicker-month"));
			monthDD = new Select(eventDate);

			String dt = currentMonthinNum + "-" + currentDateinNum;
			log.info(dt);
			// String[] startTime = time.split("-");
			log.info("before");
			SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(dt));
			c.add(Calendar.DATE, Integer.parseInt(date));
			dt = sdf.format(c.getTime());
			log.info(dt);
			log.info("after");
			String[] split = dt.split("-");
			int month = Integer.parseInt(split[0]);

			String mo = monthNames[month - 1];
			monthDD.selectByVisibleText(mo.substring(0, 3));
			int eventDateUp = Integer.parseInt(split[1]);

			WebElement datePick = driver.findElement(By.xpath("//a[text()='" + eventDateUp + "']"));
			wb.until(ExpectedConditions.visibilityOf(datePick));
			datePick.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		} 

	}
	
	

	@When("^I navigate to l2 '(.*)' and l3 '(.*)' menu options$")
	public static void i_navigate_to_l2_and_l3_products(String heading, String subheading) throws ECom_Exception, Exception {

		String methodName= "i_navigate_to_l2_and_l3_products '"+heading+"' '"+subheading+"'";

		try {
			WebElement headingL2 = driver
					.findElement(By.xpath("//div[@class='header__main-nav-topic-heading']//a[text()='" + heading + "']"));
			Actions ac = new Actions(driver);
			ac.moveToElement(headingL2).build().perform();
			Thread.sleep(10000);
			WebElement subHeadingL3 = driver.findElement(By
					.xpath("//li[@class='header__main-nav-topic-section-item']//a[contains(text(),'" + subheading + "')]"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", subHeadingL3);
//		ac.click(subHeadingL3).build().perform();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new ECom_Exception(methodName, e);
		}
	}
	

	
    
	    @When("^I Clear driver cookies$")
	    public static void clear_driver_cookies() throws ECom_Exception, Exception {
	    	
			String methodName= "clear_driver_cookies";

	        try {
				driver.manage().deleteAllCookies();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				new ECom_Exception(methodName, e);
			}
	    }
	    
}