package com.TEAF.stepDefinitions;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;

import cucumber.api.java.an.Y;
import cucumber.api.java.en.When;
import cucumber.runtime.CucumberException;

public class AdminConsoleDepositeJobExecution {

	public static final String USERNAME = "ravichandravelu1";
	public static final String AUTOMATE_KEY = "pdZsqs73Ai1mvfLCsA6c";
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

	public static WebDriver driver = StepBase.getDriver();

	@When("^I override certificate issue in IE$")
	public static void overRideCertificateIssue() {
		try {
			String fid = driver.getWindowHandle();
			HashMapContainer.add("$$Fid", fid);
			driver.findElement(By.xpath("//a[text()='More information']")).click();
			driver.findElement(By.id("overridelink")).click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}

	}

	@When("^I switch to parent window$")
	public static void i_switch_to_parent_window() {
		Set<String> windowHandles = driver.getWindowHandles();
		for (String x : windowHandles) {
			if (!x.equals(HashMapContainer.get("$$Fid"))) {
				driver.switchTo().window(x);
				System.out.println("Window switched");
			}
		}
	}

	@When("^I enter start year date month and time '(.*)' mins after local time$")
	public static void i_enter_start_year_month_date_time(String startTime) throws ParseException {
		Actions ac = new Actions(driver);
		LocalDate now = LocalDate.now();
		String date = now.toString();
		String[] split = date.split("-");

		LocalTime localtime = LocalTime.now();
		String time = localtime.toString();
		String[] split2 = time.split(":");
		String myTime = split2[0] + ":" + split2[1];
		SimpleDateFormat df = new SimpleDateFormat("HH:mm");
		Date d = df.parse(myTime);
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.add(Calendar.MINUTE, Integer.parseInt(startTime));
		String newTime = df.format(cal.getTime());

		System.out.println(newTime);

		DateFormat outputformat = new SimpleDateFormat("hh:mm aa");
		Date opdate = null;
		String output = null;
		try {
			opdate = df.parse(newTime);
			output = outputformat.format(opdate);
			System.out.println(output);
		} catch (ParseException pe) {
			pe.printStackTrace();
		}

		String[] split3 = output.split(" ");

		WebElement startYear = driver.findElement(By.name("startYear"));
		ac.sendKeys(startYear, "2020").build().perform();

		WebElement startMonth = driver.findElement(By.name("startMonth"));
		ac.sendKeys(startMonth, split[1]).build().perform();

		WebElement startDay = driver.findElement(By.name("startDay"));
		ac.sendKeys(startDay, split[2]).build().perform();

		WebElement startTimeele = driver.findElement(By.name("startTime"));
		ac.sendKeys(startTimeele, split3[0]).build().perform();

	}

	@When("^I click on new button for deposit scheduler job$")
	public static void i_click_on_new_button_deposit_schedule_job() {
		WebDriverWait wb = new WebDriverWait(driver, 50);
		WebElement NewButton = driver.findElement(By.name("schedulerAddJobButButton"));
		wb.until(ExpectedConditions.visibilityOf(NewButton));
		NewButton.click();
	}

	@When("^I click on ok button for deposit scheduler job$")
	public static void i_click_on_ok_button_deposit_schedule_job() {
		try {
			driver.findElement(By.id("OKButton")).click();
		} catch (CucumberException e) {
			// TODO Auto-generated catch block
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@When("^I click on log out button$")
	public static void i_click_logoutbutton() {
		try {
			driver.findElement(GetPageObjectRead.OR_GetElement("Logout")).click();
		} catch (CucumberException e) {
			// TODO Auto-generated catch block
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static String string;

	@When("^I schedule the Deposit job in Websphere e-commerce appliation$")
	public static void i_schedule_deposit_job_in_websphere() throws InterruptedException, Exception {

		if (StepBase.testBrowser.contains("browserstack")) {
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("browser", "IE");
			caps.setCapability("browser_version", "11.0");
			caps.setCapability("os", "Windows");
			caps.setCapability("os_version", "10");
			caps.setCapability("resolution", "1024x768");
			caps.setCapability("name", "Bstack-[Java] Sample Test");
			caps.setCapability("browserstack.local", "true");

			caps.setCapability("browserstack.localIdentifier", "TEAF123");

			caps.setCapability("browserstack.debug", true);

			WebDriver driver = new RemoteWebDriver(new java.net.URL(URL), caps);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		} else {

			System.setProperty("webdriver.ie.driver",
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\IEDriverServer.exe");
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			driver = new InternetExplorerDriver(capabilities);
		}

		String fid = driver.getWindowHandle();

		WebDriverWait wb = new WebDriverWait(driver, 50);
		driver.get("https://dbeqweb01:8002/adminconsole");

		driver.findElement(By.xpath("//a[text()='More information']")).click();
		driver.findElement(By.id("overridelink")).click();
		WebElement adminConsole = driver.findElement(By.xpath("//td[contains(text(),'Administration Console Logon')]"));
		Actions ac = new Actions(driver);

		wb.until(ExpectedConditions.visibilityOf(adminConsole));
		WebElement userName = driver.findElement(By.id("username"));
		ac.sendKeys(userName, "prem.prem").build().perform();
		Thread.sleep(3000);
		WebElement password = driver.findElement(By.id("password"));
		password.clear();
		ac.sendKeys(password, "prem4321").build().perform();
		String attribute = password.getAttribute("value");
		driver.findElement(By.id("logonButton")).click();

		WebElement siteStoreSelection = driver
				.findElement(By.xpath("//td[contains(text(),'Administration Console Site/Store Selection')]"));
		siteStoreSelection.isDisplayed();

		driver.findElement(By.xpath("//button[@type='submit' and text()='OK']")).click();

		Thread.sleep(3000);

		Set<String> windowHandles = driver.getWindowHandles();
		for (String x : windowHandles) {
			if (!x.equals(fid)) {
				driver.switchTo().window(x);
				System.out.println("Switch to Deposit Job window");
			}
		}

		try {
//			driver.findElement(By.xpath("//a[text()='More information']")).click();
//			driver.findElement(By.id("overridelink")).click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
		Thread.sleep(3000);

		WebElement mccFrame = driver.findElement(By.name("mccmenu"));
		driver.switchTo().frame(mccFrame);
		WebElement configMenu = driver.findElement(By.xpath("//div[@id='menu2']//a"));
		ac.click(configMenu).build().perform();

		driver.switchTo().defaultContent();
		Thread.sleep(3000);

		WebElement mainFrame = driver.findElement(By.name("mccmain"));
		driver.switchTo().frame(mainFrame);
		WebElement subMenuFrame = driver.findElement(By.name("mcsubmenu"));
		driver.switchTo().frame(subMenuFrame);
		WebElement menuId = driver.findElement(By.xpath("//div[@id='menu']"));
		menuId.isDisplayed();

		WebElement tr6 = driver.findElement(By.xpath("//table//tr[6]//td//div//a"));
		String text = tr6.getText();
		System.out.println(text);
		tr6.click();

		driver.switchTo().defaultContent();

		Thread.sleep(5000);

		List<String> depositJobList = new ArrayList<String>();
		depositJobList.add("DBIDepositCyberSourceCmd");
		depositJobList.add("DBIDepositAllianceCmd");
		depositJobList.add("DBIDepositAmazonCmd");
		depositJobList.add("DBIPayPalDepositJobCmd");
		depositJobList.add("DBIDepositEBayCmd");
		depositJobList.add("DBIDepositSOCyberSourceCmd");
		depositJobList.add("DBIPayPalDepositSOJobCmd");
		depositJobList.add("DBIDepositSOEmailCmdImpl");
		depositJobList.add("DBIEmailDeposit");
		depositJobList.add("DBIDepositAllianceFileCmd");

		for (int i = 0; i < depositJobList.size(); i++) {

			driver.switchTo().frame(driver.findElement(By.name("mccmain")));
			driver.switchTo().frame(driver.findElement(By.name("mcccontent")));

			driver.switchTo().frame(driver.findElement(By.name("buttons")));

			WebElement NewButton = driver.findElement(By.name("schedulerAddJobButButton"));
			wb.until(ExpectedConditions.visibilityOf(NewButton));
			NewButton.click();
			Thread.sleep(3000);
			driver.switchTo().defaultContent();

			driver.switchTo().frame(driver.findElement(By.name("mccmain")));

			driver.switchTo().frame(driver.findElement(By.name("mcccontent")));

			driver.switchTo().frame(driver.findElement(By.name("CONTENTS")));

			WebElement pathDD = driver.findElement(By.name("pathInfo"));
			wb.until(ExpectedConditions.visibilityOf(pathDD));

			pathDD.isDisplayed();
			Select sc = new Select(pathDD);
			sc.selectByVisibleText(depositJobList.get(i));
			LocalDate now = LocalDate.now();
			String date = now.toString();
			String[] split = date.split("-");

			LocalTime localtime = LocalTime.now();
			String time = localtime.toString();
			String[] split2 = time.split(":");
			String myTime = split2[0] + ":" + split2[1];
			SimpleDateFormat df = new SimpleDateFormat("HH:mm");
			Date d = df.parse(myTime);
			Calendar cal = Calendar.getInstance();
			cal.setTime(d);
			cal.add(Calendar.MINUTE, 3);
			String newTime = df.format(cal.getTime());

			DateFormat outputformat = new SimpleDateFormat("hh:mm aa");
			Date opdate = null;
			String output = null;
			try {
				opdate = df.parse(newTime);
				output = outputformat.format(opdate);
			} catch (ParseException pe) {
				pe.printStackTrace();
			}

			String[] split3 = output.split(" ");

			driver.findElement(By.name("startYear")).sendKeys("2020");
			driver.findElement(By.name("startMonth")).sendKeys(split[1]);
			driver.findElement(By.name("startDay")).sendKeys(split[2]);

			driver.findElement(By.name("startTime")).sendKeys(split3[0]);

			driver.switchTo().defaultContent();

			driver.switchTo().frame(driver.findElement(By.name("mccmain")));

			driver.switchTo().frame(driver.findElement(By.name("mcccontent")));

			driver.switchTo().frame(driver.findElement(By.name("NAVIGATION")));

			System.out.println(depositJobList.get(i) + " is scheduled at " + output);

			driver.findElement(By.id("OKButton")).click();
			Thread.sleep(3000);
			driver.switchTo().defaultContent();

		}

		driver.switchTo().frame(driver.findElement(By.name("mccmain")));

		driver.switchTo().frame(driver.findElement(By.name("mcbct")));

		WebElement logout = driver.findElement(By.xpath("//a[contains(text(), 'Logout')]"));
		logout.click();
		driver.quit();

	}

}
