package com.TEAF.customUtilities;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.simpleflatmapper.csv.CsvParser;
import org.simpleflatmapper.csv.CsvReader;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.dataformat.csv.CsvMapper;
//import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.flipkart.zjsonpatch.JsonDiff;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobId;
import com.google.cloud.bigquery.JobInfo;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableResult;
import com.mongodb.util.JSON;

public class JSONCOMPARE {
	public static com.google.cloud.bigquery.BigQuery bigquery;
	public static TableResult BibQuery_result;

	@SuppressWarnings("deprecation")
	public void connect() throws FileNotFoundException, IOException, InterruptedException {
		bigquery = BigQueryOptions.newBuilder().setProjectId("qa-mfrm-data")
				.setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream(System.getProperty("user.dir")
						+ "/src/test/java/com/Resources/svc-devops@qa-mfrm-data.iam.gserviceaccount.com.json")))
				.build().getService();
//		String BQ_query = "SELECT TO_JSON_STRING(t)FROM dev-mfrm-data.mfrm_customer_and_social.pod_podium_reviews AS t LIMIT 1000";
//		String BQ_query = "select TO_JSON_STRING(t) from `qa-mfrm-data.mfrm_customer_and_social.incontact_comp_with_hold_call` WHERE StartDate = '2020-09-15'";
		String BQ_query = "select TO_JSON_STRING(t) from qa-mfrm-data.mfrm_customer_and_social.incontact_comp_with_hold_call AS t  WHERE StartDate = '2020-09-15' limit 1";
		System.out.println("BigQuery executed: " + BQ_query);

		QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(BQ_query).setUseLegacySql(false).build();

		JobId jobId = JobId.of(UUID.randomUUID().toString());
		Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
		queryJob = queryJob.waitFor();

		if (queryJob == null) {
			throw new RuntimeException("Job no longer exists");
		} else if (queryJob.getStatus().getError() != null) {
			throw new RuntimeException(queryJob.getStatus().getError().toString());
		}

		BibQuery_result = queryJob.getQueryResults();
		FileWriter file = new FileWriter("F:\\Manoj Thota\\InContact\\Test folder\\BQResultRecords.json");
		String updated = BibQuery_result.toString()
				.replace("TableResult{rows=[[FieldValue{attribute=PRIMITIVE, value=", "")
				.replace(", schema=Schema{fields=[Field{name=f0_, type=STRING, mode=NULLABLE, description=null, policyTags=null}]}, totalRows=1, cursor=null}",
						"")
				.replace("[FieldValue{attribute=PRIMITIVE, value=", "").replace("}]]", "").replace("}],", ",")
				.replace("ContactId", "contact_id").replace("Mastercontact_id", "master_contact_id").replace("ContactCode", "Contact_Code")
				.replace("MediaName", "media_name").replace("ContactName", "contact_name").replace("ANIDialnum", "ANI_DIALNUM")
				.replace("SkillNo", "skill_no").replace("SkillName", "skill_name").replace("CampaignNo", "campaign_no")
				.replace("CampaignName", "campaign_name").replace("AgentNo", "agent_no").replace("AgentName", "agent_name")
				.replace("TeamNo", "team_no").replace("TeamName", "team_name").replace("DispositionCode", "disposition_code")
				.replace("StartDate", "start_date").replace("StartTime", "start_time").replace("AgentTime", "Agent_Time")
				.replace("TotalTime", "Total_Time").replace("AbandonTime", "Abandon_Time").replace("RoutingTime", "Routing_Time")
				.replace("Abandon", "abandon").replace("CallbackTime", "callback_time").replace("HoldTime", "Hold_Time");
		String updated1 = updated.replaceAll(":\\s([\\d]+)", ": \"$1\"");
		file.write("[" + updated + "]");
		JSON.parse(updated);
		file.flush();
		System.out.println("--------------" + updated1);

	}

	public static void main(String[] args) throws JsonProcessingException, IOException, InterruptedException {

		// TODO Auto-generated method stub
		 JSONCOMPARE test=new JSONCOMPARE();
		 test.connect();
//		
//		csvToJsonConvert();
		
//		ObjectMapper mapper = new ObjectMapper();
//		File f1 = new File("F:\\Manoj Thota\\InContact\\Test folder\\apison.json");
//		File f2 = new File("F:\\Manoj Thota\\InContact\\Test folder\\BQresults.json");
//		BufferedReader reader1=  new BufferedReader(new InputStreamReader(new FileInputStream(f1), "UTF-8"));
//        BufferedReader reader2=  new BufferedReader(new InputStreamReader(new FileInputStream(f2), "UTF-8"));
////		File f2 = new File("F:\\Manoj Thota\\InContact\\AgentList\\Agentlist_API_CSV_files\\csvjson1.json");
//		JsonNode node1 = mapper.readTree(reader1);
//		JsonNode node2 = mapper.readTree(reader2);
//		boolean status = node1.equals(node2);
//		JsonNode patch = JsonDiff.asJson(node1, node2);
//		String diffs = patch.toString();
//		System.out.println("status" + status);
//		System.out.println("diffs" + diffs);

		
	}
	
	public static void csvToJsonConvert() throws IOException {
        File input = new File("F:\\Manoj Thota\\InContact\\Test folder\\api.csv");
          
          File f3 = new File("F:\\Manoj Thota\\InContact\\Test folder\\apison.json");
          FileWriter file = new FileWriter(f3);
           
          BufferedReader objReader = new BufferedReader(new FileReader(input));
          CsvReader reader = CsvParser.reader(objReader);

          JsonFactory jsonFactory = new JsonFactory();
          StringWriter jsonObjectWriter = new StringWriter();
          Iterator<String[]> iterator = reader.iterator();
          String[] headers = iterator.next();
          JsonGenerator jsonGenerator = jsonFactory.createGenerator(jsonObjectWriter);
      
              jsonGenerator.writeStartArray();
      
              while (iterator.hasNext()) {
                  jsonGenerator.writeStartObject();
                  String[] values = iterator.next();
                  int nbCells = Math.min(values.length, headers.length);
                  for(int i = 0; i < nbCells; i++) {
                      jsonGenerator.writeFieldName(headers[i]);
                      jsonGenerator.writeString(values[i]);
                  }
                  jsonGenerator.writeEndObject();
              }
              jsonGenerator.writeEndArray();
              jsonGenerator.close();
              System.out.println("...."+jsonObjectWriter.toString());
              file.write(jsonObjectWriter.toString());
              file.flush();
              
          
	}

}
