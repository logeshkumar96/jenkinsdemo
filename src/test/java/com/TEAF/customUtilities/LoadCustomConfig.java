package com.TEAF.customUtilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.support.ui.WebDriverWait;

import com.TEAF.stepDefinitions.CommonSteps;

import cucumber.api.java.en.When;

public class LoadCustomConfig {
	
	public static Properties prop = new Properties();

	public LoadCustomConfig() throws Exception {
		File cf = new File(System.getProperty("user.dir")+"\\src\\test\\java\\com\\TEAF\\TestFiles\\Config\\Config.properties");
		FileInputStream fin = new FileInputStream(cf);
		prop.load(fin);
	}
	
	public String getProperty(String propKey) {
		String property = prop.getProperty(propKey);
		return property;
	}
}
