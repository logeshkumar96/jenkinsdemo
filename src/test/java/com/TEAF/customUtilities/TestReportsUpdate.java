package com.TEAF.customUtilities;

import java.io.File;
import java.io.IOException;

public class TestReportsUpdate {

	public static void updateReportFilenameandFolderName() throws IOException {
		File directory = new File(
				System.getProperty("user.dir") + "\\src\\test\\java\\com\\TestResults\\custom-report\\");
		File[] files = directory.listFiles();
		long lastModifiedTime = Long.MIN_VALUE;
		File chosenFolder = null;

		if (files != null) {
			for (File file : files) {
				if (file.lastModified() > lastModifiedTime) {
					chosenFolder = file;

					lastModifiedTime = file.lastModified();
				}

			}
		}

		String replace = chosenFolder.getName();
		System.out.println(replace);
		File reportFile = new File(
				System.getProperty("user.dir") + "\\src\\test\\java\\com\\TestResults\\custom-report\\" + replace
						+ "\\cucumber-html-reports\\overview-features.html");

		File renameReportfile = new File(
				System.getProperty("user.dir") + "\\src\\test\\java\\com\\TestResults\\custom-report\\" + replace
						+ "\\cucumber-html-reports\\reports.html");
		if (reportFile.exists()) {
			boolean renameTo = reportFile.renameTo(renameReportfile);
			System.out.println(renameTo);

		}

	}

	public static void main(String[] args) throws IOException {
		updateReportFilenameandFolderName();
	}

}
