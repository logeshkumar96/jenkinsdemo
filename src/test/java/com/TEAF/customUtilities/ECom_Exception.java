package com.TEAF.customUtilities;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.TEAF.framework.StepBase;

public class ECom_Exception extends Exception {
	static Logger log = Logger.getLogger(ECom_Exception.class.getName());

	public ECom_Exception(String methodName, Exception e) throws Exception {
		log.info("Checking for Marketing Popup");
		try {
			WebElement iframe = StepBase.getDriver().findElement(By.id("attentive_creative"));
			StepBase.getDriver().switchTo().frame(iframe);
			WebElement marketingPopup = StepBase.getDriver().findElement(By.id("dismissbutton2"));
			marketingPopup.click();
			log.info("Verified for Marketing popup and clicked");
			TEAF_Reflection.reflectionMethodName(methodName);
		}catch(Exception x) {
			try {
			WebElement marketingPopup1 = StepBase.getDriver().findElement(By.id("oo_close_prompt"));
			marketingPopup1.click();
			log.info("Verified for DBI Marketing popup and clicked");
			TEAF_Reflection.reflectionMethodName(methodName);
		}
		catch (Exception e1) {
			log.error("No Marketting popup appeared");
			e.printStackTrace();
			throw new Exception(e);
		}
		}
	}
}
