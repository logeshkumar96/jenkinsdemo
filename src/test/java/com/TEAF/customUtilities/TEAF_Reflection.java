package com.TEAF.customUtilities;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TEAF_Reflection {


	public static void reflectionMethodName(String methodNameLine) throws Exception {

		File fd = new File(System.getProperty("user.dir") + "\\src\\test\\java\\com\\TEAF\\stepDefinitions");
		File[] listFiles = fd.listFiles();

		for (File x : listFiles) {
			String name = x.getName();
			String[] split = name.split(".java");

			// load the AppTest at runtime
			Class cls = Class.forName("com.TEAF.stepDefinitions." + split[0]);
			Object obj = cls.newInstance();

			Class<?> noparams[] = {};

			Class<?>[] paramString1 = new Class[] { String.class };
			Class<?>[] paramString2 = new Class[] { String.class, String.class };
			Class<?>[] paramString3 = new Class[] { String.class, String.class, String.class };
			Class<?>[] paramString4 = new Class[] { String.class, String.class, String.class, String.class };

			String methodName = methodNameLine;
			String methodParams = null;
			int paramCount = -1;

			if (methodNameLine.contains(" ")) {
				String[] methodSplit = methodNameLine.split(" ", 2);

				methodName = methodSplit[0];
				methodParams = methodSplit[1];
				paramCount = 0;
			}

			String[] methodParamArray = null;
			if (methodParams != null) {
				for (int i = 0; i < methodParams.length(); i++) {
					char charAt = methodParams.charAt(i);
					if (charAt == ' ') {
						if (methodParams.charAt(i + 1) == '\'') {
							paramCount++;
						}
					}
				}
				methodParamArray = methodParams.split(" '");
			}

			try {
				Method method;
				if (paramCount == -1) {
					method = cls.getDeclaredMethod(methodName, noparams);
					method.invoke(obj, null);

				} else if (paramCount == 0) {
					method = cls.getDeclaredMethod(methodName, paramString1);
					method.invoke(obj, methodParamArray[0].replace("'", ""));

				} else if (paramCount == 1) {
					method = cls.getDeclaredMethod(methodName, paramString2);
					method.invoke(obj, methodParamArray[0].replace("'", ""), methodParamArray[1].replace("'", ""));

				} else if (paramCount == 2) {
					method = cls.getDeclaredMethod(methodName, paramString3);
					method.invoke(obj, methodParamArray[0].replace("'", ""), methodParamArray[1].replace("'", ""),
							methodParamArray[2].replace("'", ""));

				} else if (paramCount == 3) {
					method = cls.getDeclaredMethod(methodName, paramString4);
					method.invoke(obj, methodParamArray[0].replace("'", ""), methodParamArray[1].replace("'", ""),
							methodParamArray[2].replace("'", ""), methodParamArray[3].replace("'", ""));

				}
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
			//	System.out.println("Method is not in the class");
			}
		}
	}

}