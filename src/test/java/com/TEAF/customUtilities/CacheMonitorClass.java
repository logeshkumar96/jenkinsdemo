package com.TEAF.customUtilities;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.TEAF.stepDefinitions.CustomMethods;

public class CacheMonitorClass {

	static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CacheMonitorClass.class.getName());

	public static void clearCacheBeforeExecution() throws InterruptedException, IOException {
		log.info("Initializing Cache Clear");
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
		options.addArguments("--disable-gpu");
		WebDriver driver = new ChromeDriver(options);
		WebDriverWait wb = new WebDriverWait(driver, 30);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.get("http://dbeqapp01:9060/cachemonitor/");
		driver.switchTo().frame(driver.findElement(By.xpath("//frame[@name='detail']")));
		driver.findElement(By.xpath("//input[@value='Clear Cache']")).click();
		WebElement usedEntries = driver.findElement(By.xpath("//td[text()='Used Entries']//..//td[2]"));
		wb.until(ExpectedConditions.textToBePresentInElement(usedEntries, "0"));
		driver.findElement(By.xpath("//input[@value='Reset Statistics']")).click();
		WebElement cacheHits = driver.findElement(By.xpath("//td[text()='Cache Hits']//..//td[2]"));
		wb.until(ExpectedConditions.textToBePresentInElement(cacheHits, "0"));

		TakesScreenshot tschot = (TakesScreenshot) driver;
		File file = tschot.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file,
				new File(System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\cache.png"));

		Thread.sleep(2000);
		driver.quit();
		log.info("Cache Clear Completed");


	}

}
