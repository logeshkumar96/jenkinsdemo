package com.TEAF.customUtilities;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.UUID;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.Assert;

import com.TEAF.framework.HashMapContainer;
import com.TEAF.stepDefinitions.MF_MuleSoft_CustomStep;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobId;
import com.google.cloud.bigquery.JobInfo;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableResult;

public class RunnableDemo extends Thread {

//	private Thread t;
	private String threadName;
	private String filePath;
	private int startRecord;
	private int endRecord;
	
	public static com.google.cloud.bigquery.BigQuery bigquery;
	public static TableResult BibQuery_result;

	public RunnableDemo(String name, String path, int fromRecord, int toRecord) {
		threadName = name;
		filePath = path;
		startRecord = fromRecord;
		endRecord = toRecord;
		
//		System.out.println("Creating " + threadName);
	}

	public void run() {
		System.out.println("threadName: "+threadName);
		
//		String APIResultCSVFilePath = "F:\\Manoj Thota\\POC\\Multi threading\\Data_WithHold_2020_09_29_14_09_44.csv";
		MF_MuleSoft_CustomStep customobj = new MF_MuleSoft_CustomStep();
		try {
			customobj.CSV_and_BQ_records_assertion_MultiThread(filePath, startRecord, endRecord);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		
		/*try {
		BufferedReader sourceFile = new BufferedReader(new FileReader(filePath));
		for (int i=0; i<startRecord-1; i++)
		{
			sourceFile.readLine();
		}
		int numberOfProcessRecords = endRecord-startRecord;
		String fileContent;
		for (int j=0; j<=numberOfProcessRecords; j++) {
			while ((fileContent = sourceFile.readLine()) != null) {
				System.out.println("ThreadName "+threadName+" fileContent "+fileContent);
				Thread.sleep(1000);
				break;
			}
		}
		sourceFile.close();
		}catch (Exception e){
			e.getStackTrace();
		}*/
	
	}
//		try {
//			for (int i = 4; i > 0; i--) {
//				System.out.println("Thread: " + threadName + ", " + i);
//				// Let the thread sleep for a while.
//				Thread.sleep(50);
//			}
//		} catch (InterruptedException e) {
//			System.out.println("Thread " + threadName + " interrupted.");
//		}
//		System.out.println("Thread " + threadName + " exiting.");
	
//	public void start() {
//		System.out.println("Starting " + num);
//		if (t == null) {
//			t = new Thread(this, threadName);
//			t.start();
//		}
//		Thread t1 = new Thread();
//		t1.start();
//		run();
//		Thread t1 = new Thread();
//		t1.start();
//	}
	
//	public static void main(String[] args) throws Throwable {
//		
//		RunnableDemo R1 = new RunnableDemo();
//		R1.start();
//	}
}
