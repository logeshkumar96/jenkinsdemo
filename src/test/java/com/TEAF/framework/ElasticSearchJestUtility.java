package com.TEAF.framework;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.TEAF.Hooks.Hooks;
import com.TEAF.stepDefinitions.RestAssuredSteps;
import com.google.common.io.Files;
import com.google.gson.JsonObject;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.indices.IndicesExists;

public class ElasticSearchJestUtility {

	static Logger log = Logger.getLogger(ElasticSearchJestUtility.class.getName());
	public static ElasticSearchJest_UserDefined jud = new ElasticSearchJest_UserDefined();

	public static JestClient jestClient(String httpClient) throws Throwable {
System.out.println("httpClient"+httpClient);
		JestClientFactory factory = new JestClientFactory();
		factory.setHttpClientConfig(new HttpClientConfig.Builder(httpClient).multiThreaded(true).build());
		JestClient client = factory.getObject();
		jud.setClient(client);
		
		return client;

	}

	public static void getIndicesExists(String index) throws IOException {
		jud.setIndex(index);
		boolean indexExists = jud.getClient().execute(new IndicesExists.Builder(index).build()).isSucceeded();
		if (indexExists) {
			log.info(index + " exists : " + indexExists);
		} else {
			log.error(index + " exists : " + indexExists);

		}
	}

	public static SearchResult searchQuerywithIndex(String query) throws Throwable {
		if (query.startsWith("@")) {
			byte[] readAllBytes = java.nio.file.Files.readAllBytes(Paths
					.get(System.getProperty("user.dir") + "\\src\\test\\java\\com\\TEAF\\json\\" + query.substring(1) + ".json"));
			query = new String(readAllBytes);
		}
		System.out.println("jud.getIndex(): "+jud.getIndex());
		Search.Builder searchBuilder = new Search.Builder(query).addIndex(jud.getIndex());
		SearchResult result = jud.getClient().execute(searchBuilder.build());
		JsonObject js = result.getJsonObject();
//		System.out.println("Query result: "+js.toString());
		jud.setJestResultJson(js);
		jud.setJestResponse(result.getJsonString());
//		Hooks.scenario.write(getJestResponseAsString());
//		jud.setClient(null);
		return result;
	}
	
	public static JsonObject getJestResponseAsJSON() {
		return jud.getJestResultJson();
	}

	public static String getJestResponseAsString() {
		String jestResponse = jud.getJestResponse();
		return jestResponse;
	}
	

}
