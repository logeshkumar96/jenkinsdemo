package com.TEAF.framework;

import java.util.Set;

import org.json.simple.JSONObject;

import net.minidev.json.JSONArray;

public class D365 {

	private String unique_id;
	private String Email;
	private String Guestlastname;
	private String LeadSource;
	private String AssociateName;
	private String StoreID;
	private String PreferredContact;
	private String Lastname;
	private String Guestemail;
	private String Firstname;
	private String AssociateEmail;
	private String DetailsOffer;
	private String ShoppingFor;
	private String Guestphone;
	private String AssociateADID;
	private String Guestfirstname;
	private String PostalCode;
	private String LeadDate;
	public String getUnique_id() {
		return unique_id;
	}
	public void setUnique_id(String unique_id) {
		this.unique_id = unique_id;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getGuestlastname() {
		return Guestlastname;
	}
	public void setGuestlastname(String guestlastname) {
		Guestlastname = guestlastname;
	}
	public String getLeadSource() {
		return LeadSource;
	}
	public void setLeadSource(String leadSource) {
		LeadSource = leadSource;
	}
	public String getAssociateName() {
		return AssociateName;
	}
	public void setAssociateName(String associateName) {
		AssociateName = associateName;
	}
	public String getStoreID() {
		return StoreID;
	}
	public void setStoreID(String storeID) {
		StoreID = storeID;
	}
	public String getPreferredContact() {
		return PreferredContact;
	}
	public void setPreferredContact(String preferredContact) {
		PreferredContact = preferredContact;
	}
	public String getLastname() {
		return Lastname;
	}
	public void setLastname(String lastname) {
		Lastname = lastname;
	}
	public String getGuestemail() {
		return Guestemail;
	}
	public void setGuestemail(String guestemail) {
		Guestemail = guestemail;
	}
	public String getFirstname() {
		return Firstname;
	}
	public void setFirstname(String firstname) {
		Firstname = firstname;
	}
	public String getAssociateEmail() {
		return AssociateEmail;
	}
	public void setAssociateEmail(String associateEmail) {
		AssociateEmail = associateEmail;
	}
	public String getDetailsOffer() {
		return DetailsOffer;
	}
	public void setDetailsOffer(String detailsOffer) {
		DetailsOffer = detailsOffer;
	}
	public String getShoppingFor() {
		return ShoppingFor;
	}
	public void setShoppingFor(String shoppingFor) {
		ShoppingFor = shoppingFor;
	}
	public String getGuestphone() {
		return Guestphone;
	}
	public void setGuestphone(String guestphone) {
		Guestphone = guestphone;
	}
	public String getAssociateADID() {
		return AssociateADID;
	}
	public void setAssociateADID(String associateADID) {
		AssociateADID = associateADID;
	}
	public String getGuestfirstname() {
		return Guestfirstname;
	}
	public void setGuestfirstname(String guestfirstname) {
		Guestfirstname = guestfirstname;
	}
	public String getPostalCode() {
		return PostalCode;
	}
	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}
	public String getLeadDate() {
		return LeadDate;
	}
	public void setLeadDate(String leadDate) {
		LeadDate = leadDate;
	}
	
	
	
	
}