package com.TEAF.framework;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hpsf.Decimal;
//import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.minidev.json.JSONArray;
import net.sf.json.JSONString;

public class RestAssuredUtility {

	static Logger log = Logger.getLogger(RestAssuredUtility.class.getName());
	Response response;

	public Response Get(String path, RequestSpecification spec) {
		response = spec.get(path);
		System.out.println(response.getHeader("feed"));
		return response;
	}
	public Response GetWithURLEncodingFalse(String path, RequestSpecification spec) {
		response = spec.urlEncodingEnabled(false).get(path);
//		System.out.println(response.getHeader("feed"));
		return response;
	}

	public Response Post(String url, RequestSpecification spec) {
		System.out.println();
		response = spec.post(url);
		return response;
	}

	public Response Put(String url, RequestSpecification spec) {

		response = spec.patch(url);
		return response;
	}

	public Response Delete(String url, RequestSpecification spec) {
		response = spec.delete(url);
		return response;
	}

	public Response SOAP(String url, String file) throws Throwable {
		FileInputStream fileinputstream = new FileInputStream(file);
		RestAssured.baseURI = url;
		Response response = given().header("Content-Type", "text/xml").and()
				.body(IOUtils.toString(fileinputstream, "UTF-8")).when().post("");
		return response;
	}

	public static File updateJSONforPOSTRequest(File f, String key, String store) throws Exception {
		DocumentContext parse = JsonPath.parse(f);
		if (store.startsWith("$$")) {
			store = HashMapContainer.get(store);
		}
		DocumentContext set = parse.set(key, store);
		String jsonString = set.jsonString();
		Files.write(Paths.get(f.getAbsolutePath()), jsonString.getBytes());

		return f;
	}
	
	public static File updateJSONforPOSTRequestSFCC(File f, String key, String store) throws Exception {
		DocumentContext parse = JsonPath.parse(f);
		String store1 = null;
		if (store.startsWith("$$")) {
			store = HashMapContainer.get(store);
			 store1="email = '"+store+"' order by LastModifiedDate desc ";
		}
		DocumentContext set = parse.set(key, store1);
		String jsonString = set.jsonString();
		Files.write(Paths.get(f.getAbsolutePath()), jsonString.getBytes());

		return f;
	}
	public static File updateJSONforPOSTRequest_arrayData(File f, String key, String store) throws Exception {
//		File ft = new File(System.getProperty("user.dir") + "\\src/test/java/com/TEAF/json/" + f + ".json");
		DocumentContext parse = JsonPath.parse(f);
		if (store.startsWith("$$")) {
			store = HashMapContainer.get(store);
		}
		List<String> lidata = new ArrayList<String>();
		String[] split = store.replace("[", "").replace("]", "").replace("\"", "").split(",");
		for (int i = 0; i < split.length; i++) {
				lidata.add(split[i]);
		}
		System.out.println(lidata);
		DocumentContext set = parse.set(key, lidata);
		String jsonString = set.jsonString();
		Files.write(Paths.get(f.getAbsolutePath()), jsonString.getBytes());
//		Files.write(Paths.get(ft.getAbsolutePath()), jsonString.getBytes());

		return f;
	}

	public static void retriveJSONforPOSTRequest(Response res, String key, String store) throws Exception {

		Object value = JsonPath.read(res.getBody().asString(), key);
		log.info("Stored value " + String.valueOf(value));
		HashMapContainer.add("$$" + store, String.valueOf(value));
	}
	
	public static void retriveArrayDataFromJSONResponse(Response res, String key) {
		String subKey = "$$"+key.substring(0, key.length()-2);
//		System.out.println(subKey);
		JSONArray array = JsonPath.read(res.getBody().asString(), key);
//		System.out.println("Array size: "+array.size());
		List<String> hashMapKeys = new ArrayList<String>();
		for (int i = 0; i < array.size(); i++) {
			HashMap<String, String> array_record = (HashMap<String, String>) array.get(i);
			List<String> keys = new ArrayList<String>();
			keys.addAll(array_record.keySet());
			for (String k: keys) {
				Object value = array_record.get(k);
				if (value instanceof Double) {
					if (value.equals(null)) {
						HashMapContainer.add(subKey+i+"]."+k, "");
						hashMapKeys.add(subKey+i+"]."+k);
						System.out.println(subKey+i+"]."+k+" : "+"");
					}else {
						HashMapContainer.add(subKey+i+"]."+k, value.toString());
						hashMapKeys.add(subKey+i+"]."+k);
						System.out.println(subKey+i+"]."+k+" : "+value.toString());
					}
				}else {
					if (value.equals(null)) {
						HashMapContainer.add(subKey+i+"]."+k, "");
						hashMapKeys.add(subKey+i+"]."+k);
						System.out.println(subKey+i+"]."+k+" : "+"");
					}else {
						HashMapContainer.add(subKey+i+"]."+k, value.toString());
						hashMapKeys.add(subKey+i+"]."+k);
						System.out.println(subKey+i+"]."+k+" : "+value.toString());
					}
				}
			}
		}
//		System.out.println("hashMapKeys.size "+hashMapKeys.size());
		HashMapContainer.addList("$$childKeys", hashMapKeys);
	}
	
	public static void createHashMapForChildArrays(Response res) {
		List<String> hashMapKeys = HashMapContainer.getList("$$childKeys");
		for (String key : hashMapKeys) {
			String key_value = HashMapContainer.get(key);
			if (key_value.startsWith("[")) {
				String subArrayPath = key.substring(2)+"[*]";
//				System.out.println("subArrayPath "+subArrayPath);
				String subKey = "$$"+subArrayPath.substring(0, subArrayPath.length()-2);
//				System.out.println("subKey "+subKey);
				JSONArray subArray = JsonPath.read(res.getBody().asString(), subArrayPath);
//				int aSize = subArray.size();
//				HashMapContainer.addInt("$$childItemsArraySize", aSize);
//				System.out.println("@@@@"+subArray.size());
//				System.out.println("~~~~"+subArray.get(0));
//				System.out.println("~~~~"+subArray.get(1));
//				List<String> hashMapKeys = new ArrayList<String>();
				for (int i = 0; i < subArray.size(); i++) {
					HashMap<String, String> SubArray_record = (HashMap<String, String>) subArray.get(i);
					List<String> keys = new ArrayList<String>();
					keys.addAll(SubArray_record.keySet());
					for (String k: keys) {
						Object value = SubArray_record.get(k);
						if (value instanceof Double) {
							if (value.equals(null)) {
								HashMapContainer.add(subKey+i+"]."+k, "");
//								hashMapKeys.add(subKey+i+"]."+k);
								System.out.println(subKey+i+"]."+k+" : "+"");
							}else {
								HashMapContainer.add(subKey+i+"]."+k, value.toString());
//								hashMapKeys.add(subKey+i+"]."+k);
								System.out.println(subKey+i+"]."+k+" : "+value.toString());
							}
						}else {
							if (value == null) {
								HashMapContainer.add(subKey+i+"]."+k, "");
//								hashMapKeys.add(subKey+i+"]."+k);
								System.out.println(subKey+i+"]."+k+" : "+"");
							}else {
								HashMapContainer.add(subKey+i+"]."+k, value.toString());
//								hashMapKeys.add(subKey+i+"]."+k);
								System.out.println(subKey+i+"]."+k+" : "+value.toString());
							}
						}
					}
				}
				
			}
		}
	}
	
	public static void main (String[] args) {
		String key = "shipments[0].items[*]";
		String subKey = key.substring(0, key.length()-2);
		System.out.println(subKey);
	}

	public static void retriveJSONforPOSTRequestwithObjectIndex(Response res, String key, String store, String index)
			throws Exception {

		List<String> value = JsonPath.read(res.getBody().asString(), key);
		System.out.println("Stored value " + value);
		HashMapContainer.add("$$" + store, String.valueOf(value.get(Integer.parseInt(index))));
		System.out.println(value);
	}
	
	public static void retriveValuesAndStoreListValues(Response res, String key, String store) throws Exception {

		List<String> values = JsonPath.read(res.getBody().asString(), key);
		System.out.println("values size " + values.size());
		HashMapContainer.addList("$$" + store, values);
	}

	public static void retriveDataFromXMLResponse(Response res, String key, String store) {
		XmlPath xml=new XmlPath(res.asString());
		
		List<String> nodeList =xml.getList(key);
		HashMapContainer.addList("$$" + store, nodeList);
		//System.out.println("test........"+nodeList);		
	}
	
	public static void retriveDataFromXMLResponseInString(Response res, String key, String store) {
		XmlPath xml=new XmlPath(res.asString());
		
		String nodeList =xml.get(key);
		HashMapContainer.add("$$" + store, nodeList);
		//System.out.println("test........"+nodeList);		
	}
	
	public static void retriveDataFromJSONResponse( String key, String store) {
		
		List<String> values =com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), key);
		HashMapContainer.addList("$$" + store, values);
		
	}
	
	public static void retriveUniqueDataFromJSONResponse( String key, String store) {
		
		List<String> values =com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), key);
		HashMapContainer.addHashSetList("$$" + store, values);
		
	}
	
//	public static void retriveDataFromJSONResponseWithMapping(String key, String store) {
//		
//		if (key.contains("BALANCE_DUE")) {
//			
//			List<Decimal> values = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), key);
//			HashMapContainer.addDeciamlList("$$" + store, values);
//		}
//		
//		
//		
//	}
	
	public static void retriveCountOfXMLObject(Response res, String key, String store) {
		XmlPath xml=new XmlPath(res.asString());
		
		List<Object> nodeList =xml.getList(key);
		int size=nodeList.size();
		
		HashMapContainer.add("$$" + store, Integer.toString(size));
		System.out.println("test........"+nodeList);		
		
	}
}
