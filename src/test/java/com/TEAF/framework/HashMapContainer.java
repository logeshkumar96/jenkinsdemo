package com.TEAF.framework;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.hpsf.Decimal;

public class  HashMapContainer {

	static Map<String, String> hm = new java.util.HashMap<String, String>();
	static Map<String, String> hmPO = new java.util.HashMap<String, String>();
	static Map<String, List<String>> hm1 = new java.util.HashMap<String, List<String>>();
	static Map<String, List<Integer>> hm2 = new java.util.HashMap<String, List<Integer>>();
//	static Map<String, List<Decimal>> hm3 = new java.util.HashMap<String, List<Decimal>>();
	static Map<String, Integer> hm4 = new java.util.HashMap<String, Integer>();

	static Logger log = Logger.getLogger(HashMapContainer.class);

	public static void add(String key, String value){
		hm.put(key, value);
//		System.out.println("hashmap"+hm);

		
	}	
	
	public static void addPO(String key, String value){
		if(hmPO.get(key)!=null){
			log.debug("KeyValue: "+key);
		}else {
		hmPO.put(key, value);
		}
	}	

	public static String get(String key){
//		System.out.println("hashmap"+hm);
		return hm.get(key);
		
	}
	

	
	public static String getPO(String key){
		return hmPO.get(key.toLowerCase());
	}

	public static void remove(String key)throws NullPointerException{
		if(hm.get(key)!=null){
			//hm.remove(key);
		}
	}
	
	public static void removPO(String key)throws NullPointerException{
		if(hmPO.get(key)!=null){
		//	hmPO.remove(key);
		}
	}
	
	public static void ClearHM(){
		hm.clear();
		hmPO.clear();
	}
	public static void addList(String key, List<String> value){
		hm1.put(key, value);
		//System.out.println("hashmap"+hm1);

		
	}	
	public static List<String> getList(String key){
//		System.out.println("hashmap"+hm1);
		return hm1.get(key);
		
	}
	
	public static void addHashSetList(String key, List<String> value){
		HashSet<String> unique_Values = new HashSet<String>();
		unique_Values.addAll(value);
		List<String> unique_ArrayList = new ArrayList<String>();
		unique_ArrayList.addAll(unique_Values);
		
		hm1.put(key, unique_ArrayList);
		//System.out.println("hashmap"+hm1);

		
	}	
	public static List<String> getHashSetList(String key){
		System.out.println("Unique hashmap"+hm1);
		return hm1.get(key);
		
	}
	
	public static void addIntegerList(String key, List<Integer> i){
		hm2.put(key, i);
		//System.out.println("hashmap"+hm2);

		
	}	
	public static List<Integer> getIntegerList(String key){
		//System.out.println("hashmap"+hm2);
		return hm2.get(key);
		
	}

	public static void addInt(String key, int value) {
		hm4.put(key, value);
	}
	
	public static Integer getInt(String key) {
		return hm4.get(key);
	}
	
//	public static void addDeciamlList(String key, List<Decimal> i){
//		hm3.put(key, i);
//		//System.out.println("hashmap"+hm2);
//
//		
//	}	
//	public static List<Decimal> getDecimalList(String key){
//		//System.out.println("hashmap"+hm2);
//		return hm3.get(key);
//		
//	}
}
