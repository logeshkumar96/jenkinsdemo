package com.TEAF.framework;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

public class UploadReportsToFTPServer {

	static Logger log = Logger.getLogger(UploadReportsToFTPServer.class.getName());

	static FTPClient ftp = null;

	public static void startFTPConnect(String host, String user, String pwd) throws Exception {
		ftp = new FTPClient();
		int reply;
		ftp.connect(host);
		reply = ftp.getReplyCode();
		if (!FTPReply.isPositiveCompletion(reply)) {
			ftp.disconnect();
			throw new Exception("Exception in connecting to FTP Server");
		}
		ftp.login(user, pwd);
		ftp.setFileType(FTP.BINARY_FILE_TYPE);
		ftp.enterLocalPassiveMode();
	}

	public static void uploadFile(String localFileFullName, String fileName, String hostDir) throws Exception {
		try  {
			InputStream input = new FileInputStream(new File(localFileFullName));
			ftp.storeFile(hostDir + "/" + fileName, input);
		}catch(Exception e) {
			//
		}
	}

	public static void disconnect() {
		if (ftp.isConnected()) {
			try {
				ftp.logout();
				ftp.disconnect();
			} catch (IOException f) {
				// do nothing as file is already saved to server
			}
		}
	}

	public static void connectToFTPandTransferFiles(String host, String userName, String passWord, String localFileLoc,
			String remoteFileName, String remoteFolderName) throws Exception {
		try {
			log.info("Started File Transfer to FTP Server " + host + " from local location " + localFileLoc
					+ " to remote location " + remoteFolderName);
			startFTPConnect(host, userName, passWord);
			uploadFile(localFileLoc, remoteFileName, remoteFolderName);
			disconnect();
			log.info("File Transferred Successfully to remote file location "+ remoteFolderName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
