package com.TEAF.framework;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.TEAF.Hooks.Hooks;
import com.TEAF.stepDefinitions.RestAssuredSteps;
import com.jayway.jsonpath.JsonPath;
import com.opencsv.CSVWriter;

import io.restassured.response.ResponseBody;

public class Utilities {
	static Logger log = Logger.getLogger(Utilities.class.getName());
	public static WebDriver driver = StepBase.getDriver();

	public static byte[] takeScreenshotByte(WebDriver driver) {
		try {
			byte[] scrFile = null;
			if (System.getProperty("test.disableScreenshotCapture").equalsIgnoreCase("false")) {
				Thread.sleep(1000);
				scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			}
			return scrFile;
		} catch (Exception e) {

			throw new RuntimeException(e);

		}
	}
	
	//Generate Date (only) by input
    public String generateDate(String dateInput, String expectedDateFormat) {
        String generatedDate="";
        try {
            String timeZone= "CST";
            dateInput = dateInput.substring(2);
            
            if(dateInput!=null && dateInput.length()>0) {
                if (dateInput.equalsIgnoreCase("c")){
                    generatedDate= getRequiredDate(0, expectedDateFormat,timeZone );
                }else {
                    generatedDate= getRequiredDate(Integer.parseInt(dateInput), expectedDateFormat, timeZone);
                }
            }else{
                log.error("Invalid DateInput from Feature file! - DateInput format should be XD<postive or negative integer>" );
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
       
        return generatedDate;
    }
    
	public static String getRequiredDate(int incrementDays, String expectedDateFormat, String timeZoneId) {
		try {
			DateFormat dateFormat;
			Calendar calendar = Calendar.getInstance();
			dateFormat = new SimpleDateFormat(expectedDateFormat);
			if (timeZoneId != null && !timeZoneId.equals(""))
				dateFormat.setTimeZone(TimeZone.getTimeZone(timeZoneId));
			calendar.add(Calendar.DAY_OF_MONTH, incrementDays);
			Date tomorrow = calendar.getTime();
			String formattedDate = dateFormat.format(tomorrow);
			return formattedDate;
		} catch (Exception e) {

			return null;
		}
	}
	
	//Generate Date & Time by input
    public String generateDateAndTime(String dateInput, String timeInput, String expectedDateFormat) {
        String generatedDate="";
        try {
            String timeZone= "CST";
            dateInput = dateInput.substring(2);
            timeInput = timeInput.substring(2);
            
            if(dateInput!=null && dateInput.length()>0 && timeInput!=null && timeInput.length()>0) {
                if (dateInput.equalsIgnoreCase("c") && timeInput.equalsIgnoreCase("c")){
                    generatedDate= getRequiredDateAndTime(0, 0, expectedDateFormat,timeZone );
                }else if (dateInput.equalsIgnoreCase("c") && !timeInput.equalsIgnoreCase("c")){
                    generatedDate= getRequiredDateAndTime(0, Integer.parseInt(timeInput), expectedDateFormat,timeZone );
                }else if (!dateInput.equalsIgnoreCase("c") && timeInput.equalsIgnoreCase("c")){
                    generatedDate= getRequiredDateAndTime(Integer.parseInt(dateInput), 0, expectedDateFormat,timeZone );
                }else {
                    generatedDate= getRequiredDateAndTime(Integer.parseInt(dateInput), Integer.parseInt(timeInput), expectedDateFormat, timeZone);
                }
            }else{
                log.error("Invalid Date & Time Input from Feature file! - Date & Time Input format should be XD<postive or negative integer> & XT<postive or negative integer>" );
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
       
        return generatedDate;
    }
    
	public static String getRequiredDateAndTime(int incrementDays, int incrementHours,String expectedDateFormat, String timeZoneId) {
		try {
			DateFormat dateFormat;
			Calendar calendar = Calendar.getInstance();
			dateFormat = new SimpleDateFormat(expectedDateFormat);
			if (timeZoneId != null && !timeZoneId.equals(""))
				dateFormat.setTimeZone(TimeZone.getTimeZone(timeZoneId));
			calendar.add(Calendar.DAY_OF_MONTH, incrementDays);
			calendar.add(Calendar.HOUR_OF_DAY, incrementHours);
			Date tomorrow = calendar.getTime();
			String formattedDate = dateFormat.format(tomorrow);
			return formattedDate;
		} catch (Exception e) {

			return null;
		}
	}
	
	public static void main(String[] args) {

		Utilities ut = new Utilities();
		String res = ut.generateDateAndTime("XDC", "XTC", "yyyy/MM/dd HH:mm:ss");
		System.out.println(res);
		String res1 = ut.generateDateAndTime("XDC", "XT-1", "yyyy/MM/dd HH:mm:ss");
		System.out.println(res1);
		String res2 = ut.generateDateAndTime("XD+1", "XTC", "yyyy/MM/dd HH:mm:ss");
		System.out.println(res2);
		String res4 = ut.generateDateAndTime("XD-1", "XT+1", "yyyy/MM/dd HH:mm:ss");
		System.out.println(res4);
	}

	public static String takeScreenshot(WebDriver driver) {
		try {
			String SSPath = "";
			if (System.getProperty("test.disableScreenshotCapture").equalsIgnoreCase("false")) {
				Thread.sleep(1000);
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				File directory = new File(String.valueOf("Screenshots"));
				if (!directory.exists()) {
					directory.mkdir();
				}
				SSPath = "Screenshots/" + getRequiredDate(0, "yyyy_MM_dd_hh", null) + "/screenshot_"
						+ getRequiredDate(0, "yyyy_MM_dd_hh_mm_ss", null) + ".png";
				FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "/output/" + SSPath));
			}
			return SSPath;
		} catch (Exception e) {

			throw new RuntimeException(e);
		}
	}

	public void waitFor(final Long timeInMilliseconds) {
		try {
			Thread.sleep(timeInMilliseconds);
		} catch (Exception e) {

			throw new RuntimeException(e);
		}
	}

	public static void waittextToBePresentInElement(String location, String expectedText) {
		try {
			WebDriverWait wb = new WebDriverWait(driver, 50);

			wb.until(ExpectedConditions.textToBePresentInElement(
					driver.findElement(GetPageObjectRead.OR_GetElement(location)), expectedText));
		} catch (Exception e) {

			throw new RuntimeException("expected text");

		}
	}

	public static void scrollToTopOfPage() {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0,-document.body.scrollHeight);");
		} catch (Exception e) {

			throw new RuntimeException(e);

		}
	}

	public void waitForPageLoad() {
		try {
			WebDriverWait wait = new WebDriverWait(StepBase.getDriver(), 180);
			final JavascriptExecutor javascript = (JavascriptExecutor) (StepBase
					.getDriver() instanceof JavascriptExecutor ? StepBase.getDriver() : null);
			wait.until(new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver d) {
					return javascript.executeScript("return document.readyState").equals("complete");
				}
			});

			StepBase.getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (Exception e) {

			throw new RuntimeException(e);
		}
	}

	public static void deleteZipFiles(String desFN) {
		try {
			String zipName = System.getProperty("user.dir") + "/" + desFN + ".zip";
			String folderName = System.getProperty("user.dir") + "/" + desFN + ".zip";

			FileUtils.forceDelete(new File(zipName));
			FileUtils.forceDelete(new File(folderName));

		} catch (Exception e) {

		}
	}

	public static void deleteFiles(String desFN) {
		try {
			String zipName = System.getProperty("user.dir") + "/" + desFN;
			FileUtils.deleteDirectory(new File(zipName));

		} catch (Exception e) {

		}
	}

	public static void reportstoZipFile(String srcfolder, String desFN) throws Exception {

		try {

			// Use the following paths for windows
			String folderToZip = System.getProperty("user.dir") + "/" + srcfolder;
			String zipName = System.getProperty("user.dir") + "/" + desFN + ".zip";

			File f = new File(folderToZip);
			if (f.isDirectory()) {

				final Path sourceFolderPath = Paths.get(folderToZip);
				Path zipPath = Paths.get(zipName);
				final ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipPath.toFile()));
				Files.walkFileTree(sourceFolderPath, new SimpleFileVisitor<Path>() {
					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
						zos.putNextEntry(new ZipEntry(sourceFolderPath.relativize(file).toString()));
						Files.copy(file, zos);
						zos.closeEntry();
						return FileVisitResult.CONTINUE;
					}
				});
				zos.close();
			}
		} catch (Exception e) {
			//
		}

	}

	public static void auto_generation_Email(String mailId, String ccMail) throws Exception {

		// Create object of Property file
		Properties props = new Properties();
		props.put("mail.smtp.host", System.getProperty("email.smtp.server"));
		props.put("mail.smtp.socketFactory.port", System.getProperty("email.smtp.socketPortNumber"));
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", System.getProperty("email.smtp.portNumber"));
		props.put("mail.smtp.starttls.enable", "true");
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(System.getProperty("email.user.emailId"),
						System.getProperty("email.user.password"));
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(System.getProperty("email.user.emailAddress")));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailId));
			message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(ccMail));
			String lDate = LocalDate.now().toString();
			String lTime = LocalTime.now().toString();

			Collection<String> values = Hooks.scenarioStatus.values();
			String flag;
			int failCount = 0;
			int passCount = 0;
			int skipCount = 0;
			for (String x : values) {
				if (x.equalsIgnoreCase("Failed")) {
					failCount++;
				} else if (x.equalsIgnoreCase("Passed")) {
					passCount++;
				} else {
					skipCount++;
				}
			}
			if (values.contains("FAILED") || values.contains("SKIPPED")) {
				flag = "Build Failed";
			} else {
				flag = "Build Passed";
			}

			String subject = null;
			if (System.getProperty("email.subject").length() > 2) {
				subject = System.getProperty("email.subject") + ":" + " on " + lDate + ": " + lTime + " :" + flag;
			} else {
				subject = "RC TEAF Execution Report " + ":" + " on " + lDate + ": " + lTime + " :" + flag;

			}

			message.setSubject(subject);
			MimeMultipart multipart = new MimeMultipart("related");

			BodyPart messageBodyPart1 = new MimeBodyPart();
			StringBuffer ScenarioTable = new StringBuffer();
			LinkedHashMap<String, String> mp = new LinkedHashMap<String, String>();
			mp.putAll(Hooks.scenarioStatus);
			Set<Entry<String, String>> entrySet = mp.entrySet();

			for (Entry<String, String> entry : entrySet) {

				if (entry.getValue().equalsIgnoreCase("passed")) {
					ScenarioTable.append("<TR ALIGN='CENTER' bgcolor ='#f2f2f2'><TD>" + entry.getKey()
							+ "</TD><TD bgcolor= '#419c4d' >" + entry.getValue() + "</TD> " + "</TR>");
				} else {
					ScenarioTable.append("<TR ALIGN='CENTER' bgcolor ='#f2f2f2'><TD>" + entry.getKey()
							+ "</TD><TD bgcolor= '#d63a29' >" + entry.getValue() + "</TD> " + "</TR>");
				}

			}

			String messageBody = null;
			if (System.getProperty("email.messageBody").length() > 2) {
				messageBody = System.getProperty("email.messageBody");
			} else {
				messageBody = "";

			}

			String signature = null;
			if (System.getProperty("email.signature").length() > 2) {
				signature = System.getProperty("email.signature");
			} else {
				signature = "RC QA";

			}
//			File f = new File(System.getProperty("user.dir") + "/src/test/java/com/TestResults/cucumber-report/"
//                    + System.getProperty("test.jsonFileName") + ".json");
			File f = new File(System.getProperty("user.dir") + "/src/test/java/com/TestResults/cucumber-report/"
					+ "cucumber_1" + ".json");
			List<String> listSteps = JsonPath.read(f, "$..steps[*].result.status");
			int stepPassed = 0;
			int stepFailed = 0;
			int stepSkipped = 0;
			for (String x : listSteps) {
				if (x.toLowerCase().contains("pass")) {
					stepPassed++;
				} else if (x.toLowerCase().contains("fail")) {
					stepFailed++;
				} else if (x.toLowerCase().contains("skip")) {
					stepSkipped++;
				}
			}

			// Set the body of email
			String htmlText = "<H3><img src=\"cid:image\">   Script Execution Summary | Email Report </H>" + "<H4></H4>"
					+ "<TABLE WIDTH='75%' CELLPADDING='8' CELLSPACING='1'>"
					+ "<TR> <TH bgcolor = '#a8a7a7' COLSPAN='8'><H2>Test Execution Summary</H2></TH></TR><TR><TH>Total Test Steps: </TH><TH>Total Test Steps Passed: </TH><TH>Total Test Steps Failed: </TH><TH>Total Test Steps Skipped: </TH><TH>Total Test Cases: </TH><TH>Total Test cases Passed: </TH><TH>Total Test cases Failed: </TH><TH>Total Test cases Skipped: </TH>"
					+ "</TR>" + "<TR ALIGN='CENTER' bgcolor ='#f2f2f2'>" + "<TD>" + listSteps.size() + "</TD>"
					+ "<TD bgcolor= '#419c4d'>" + stepPassed + "</TD>" + "<TD  bgcolor= '#d63a29'>" + stepFailed
					+ "</TD>" + "<TD bgcolor= '#6bbbc7'>" + stepSkipped + "</TD>"

					+ "<TD>" + values.size() + "</TD><TD bgcolor= '#419c4d' >" + passCount
					+ "</TD><TD bgcolor= '#d63a29' >" + failCount + "</TD><TD bgcolor= '#6bbbc7' >" + skipCount
					+ "</TD>" + "</TR>" + "</TABLE><H4>" + messageBody + "<BR>" + "<BR>"
					+ "<p> Please find attached a detailed Test Automation execution report using RC TEAF with this email\r\n"
					+ " </p>" + "</H4> " + "\n" + "\n" + "<H4>Test Scenarios executed:</H4>"
					+ "<TABLE WIDTH='60%' CELLPADDING='4' CELLSPACING='1'>"
					+ "<TR ALIGN='CENTER' bgcolor ='#3767B6'><TD color: 'white'> Scenario Name </TD><TD bgcolor ='#3767B6' color: 'white'>Result</TD></TR>"
					+ ScenarioTable + "</TABLE>" + "\n" + "\n" +

					"Thanks, " + "\n" + "\n" + signature;
			messageBodyPart1.setContent(htmlText, "text/html");
			// add it
			multipart.addBodyPart(messageBodyPart1);

			// second part (the image)
			messageBodyPart1 = new MimeBodyPart();
			DataSource fds = new FileDataSource(
					System.getProperty("user.dir") + "/src/test/java/com/Resources/teaf.jpeg");

			messageBodyPart1.setDataHandler(new DataHandler(fds));
			messageBodyPart1.setHeader("Content-ID", "<image>");

			// add image to the multipart
			multipart.addBodyPart(messageBodyPart1);

			// Create another object to add another content
			MimeBodyPart messageBodyPart2 = new MimeBodyPart();
			MimeBodyPart messageBodyPart3 = new MimeBodyPart();

			// Mention the file which you want to send
			String fEN = System.getProperty("user.dir") + "/TestExecution_ExtentReports.zip";
			String fUI = System.getProperty("user.dir") + "/TestExecution_UIReports.zip";

			multipart.addBodyPart(messageBodyPart1);

			if (java.nio.file.Files.exists(Paths.get(fEN), LinkOption.NOFOLLOW_LINKS)) {
				DataSource src = new FileDataSource(fEN);
				// set the handler
				messageBodyPart2.setDataHandler(new DataHandler(src));
				// set the file
				messageBodyPart2.setFileName(fEN);
				multipart.addBodyPart(messageBodyPart2);
			}

			if (java.nio.file.Files.exists(Paths.get(fUI), LinkOption.NOFOLLOW_LINKS)) {
				DataSource source1 = new FileDataSource(fUI);

				// set the handler
				messageBodyPart3.setDataHandler(new DataHandler(source1));
				// set the file
				messageBodyPart3.setFileName(fUI);
				multipart.addBodyPart(messageBodyPart3);

			}
			message.setContent(multipart);
			// finally send the email
			Transport.send(message);

			log.info("\n =====Reports Sent through Email from " + System.getProperty("email.user.emailAddress")
					+ "=====");

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public static void testStatusToastMessage(String message) throws InterruptedException {

		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// Check for jQuery on the page, add it if need be
			js.executeScript("if (!window.jQuery) {"
					+ "var jquery = document.createElement('script'); jquery.type = 'text/javascript';"
					+ "jquery.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js';"
					+ "document.getElementsByTagName('head')[0].appendChild(jquery);" + "}");
			Thread.sleep(1000);
			// Use jQuery to add jquery-growl to the page
			js.executeScript("$.getScript('https://the-internet.herokuapp.com/js/vendor/jquery.growl.js')");

			// Use jQuery to add jquery-growl styles to the page
			js.executeScript("$('head').append('<link id=\"scenariotoast\" rel=\"stylesheet\" "
					+ "href=\"https://the-internet.herokuapp.com/css/jquery.growl.css\" " + "type=\"text/css\" />');");
			Thread.sleep(1000);
			// jquery-growl w/ no frills
			js.executeScript("$.growl({ title: 'Scenario', message: '" + message + "' });");

			Thread.sleep(3000);

			js.executeScript("var element = document.getElementById('scenariotoast');"
					+ "element.parentNode.removeChild(element);");
			Thread.sleep(1000);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());

		}

	}

	public static void testStatusFailToastMessage(String message) throws InterruptedException {

		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// Check for jQuery on the page, add it if need be
			js.executeScript("if (!window.jQuery) {"
					+ "var jquery = document.createElement('script'); jquery.type = 'text/javascript';"
					+ "jquery.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js';"
					+ "document.getElementsByTagName('head')[0].appendChild(jquery);" + "}");
			Thread.sleep(1000);
			// Use jQuery to add jquery-growl to the page
			js.executeScript("$.getScript('https://the-internet.herokuapp.com/js/vendor/jquery.growl.js')");
			js.executeScript("$('head').append('<link id=\"failtoast\" rel=\"stylesheet\" "
					+ "href=\"https://the-internet.herokuapp.com/css/jquery.growl.css\" " + "type=\"text/css\" />');");
			Thread.sleep(1000);
			// jquery-growl w/ no frills
			js.executeScript("$.growl.error({ title: 'Failed', message: '" + message + "' });");

			Thread.sleep(1000);

			js.executeScript(
					"var element = document.getElementById('failtoast');" + "element.parentNode.removeChild(element);");
			Thread.sleep(1000);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());

		}

	}

	public Map<String, String> executeSQLQuery(String DBURl, String DBUsername, String DBPassword, String Query) {
		Connection conn = null;
		Statement stmt = null;
		// JDBC driver name and database URL
		final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		final String DB_URL = DBURl;
		Map<String, String> columnData = new HashMap<String, String>();

		// Database credentials
		final String USER = DBUsername;
		final String PASS = DBPassword;
		ResultSet rs = null;
		String result = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// STEP 3: Open a connection
//			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
//			System.out.println("Connected database successfully...");
			// STEP 4: Execute a query
			stmt = conn.createStatement();

			String sql = Query;
			rs = stmt.executeQuery(sql);
			
			/*
			 * //STEP 5: Extract data from result set while(rs.next()){ //Retrieve by column
			 * name int id = rs.getInt("id"); int age = rs.getInt("age"); String first =
			 * rs.getString("first"); String last = rs.getString("last");
			 *
			 * //Display values System.out.print("ID: " + id); System.out.print(", Age: " +
			 * age); System.out.print(", First: " + first); System.out.println(", Last: " +
			 * last); }
			 */
			ResultSetMetaData metaData = rs.getMetaData();
			int columnCount = metaData.getColumnCount();
//			System.out.println("ColumnCount: "+columnCount);
			while (rs.next()) {
		
			for (int i = 1; i <= columnCount; i++) {
				String columnName = metaData.getColumnName(i);
				String value = rs.getString(columnName);
				columnData.put(columnName, value);
			}
			}
			rs.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return columnData;
	}
	
	public void executeSQLQuery_and_Download_result_CSV(String DBURl, String DBUsername, String DBPassword, String Query, String SQL_CSV_file_download_path) {
		Connection conn = null;
		Statement stmt = null;
		// JDBC driver name and database URL
//		final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		final String DB_URL = DBURl;
		Map<String, String> columnData = new HashMap<String, String>();

		// Database credentials
		final String USER = DBUsername;
		final String PASS = DBPassword;
		ResultSet rs = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// STEP 3: Open a connection
			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");
			// STEP 4: Execute a query
			stmt = conn.createStatement();

			String sql = Query;
			rs = stmt.executeQuery(sql);
			
			String timeStamp = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss").format(Calendar.getInstance().getTime());
			SQL_CSV_file_download_path = SQL_CSV_file_download_path+"_"+timeStamp+".csv";
			
			CSVWriter writer = new CSVWriter(new FileWriter(SQL_CSV_file_download_path));
			Boolean includeHeaders = true;
			writer.writeAll(rs, includeHeaders);
			HashMapContainer.add("$$SQLResultFilePath", SQL_CSV_file_download_path);
			System.out.println("SQL results got downloaded in path: "+SQL_CSV_file_download_path);
			writer.close();
			
			/*
			 * //STEP 5: Extract data from result set while(rs.next()){ //Retrieve by column
			 * name int id = rs.getInt("id"); int age = rs.getInt("age"); String first =
			 * rs.getString("first"); String last = rs.getString("last");
			 *
			 * //Display values System.out.print("ID: " + id); System.out.print(", Age: " +
			 * age); System.out.print(", First: " + first); System.out.println(", Last: " +
			 * last); }
			 */
			ResultSetMetaData metaData = rs.getMetaData();
			int columnCount = metaData.getColumnCount();
//			System.out.println("ColumnCount: "+columnCount);
			while (rs.next()) {
		
			for (int i = 1; i <= columnCount; i++) {
				String columnName = metaData.getColumnName(i);
				String value = rs.getString(columnName);

				columnData.put(columnName, value);
			}
			}
			
			rs.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
	}

	public List<String> executeSQLQueryAndReturnSingleColumn(String DBURl, String DBUsername, String DBPassword, String Query) {
		Connection conn = null;
		Statement stmt = null;
		// JDBC driver name and database URL
//		final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		final String DB_URL = DBURl;

		// Database credentials
		final String USER = DBUsername;
		final String PASS = DBPassword;
		ResultSet rs = null;
		List<String> api_keys = new ArrayList<String>();
		try {
			// STEP 2: Register JDBC driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// STEP 3: Open a connection
//			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
//			System.out.println("Connected database successfully...");
			// STEP 4: Execute a query
			stmt = conn.createStatement();

			String sql = Query;
			rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
			api_keys.add(rs.getString("API Key"));
			}
			
//			ResultSetMetaData metaData = rs.getMetaData();
//			int columnCount = metaData.getColumnCount();
////			System.out.println("ColumnCount: "+columnCount);
//			while (rs.next()) {
//		
//			for (int i = 1; i <= columnCount; i++) {
//				String columnName = metaData.getColumnName(i);
//				String value = rs.getString(columnName);
//
//				columnData.put(columnName, value);
//			}
//			}
			
			rs.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return api_keys;
	}
	
	public String unzip_a_file(String zipFilePath, String destDir) {
		String unZipped_File_path = null;
		File dir = new File(destDir);
        // create output directory if it doesn't exist
        if(!dir.exists()) dir.mkdirs();
        FileInputStream fis;
        //buffer for read and write data to file
        byte[] buffer = new byte[1024];
        try {
            fis = new FileInputStream(zipFilePath);
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry ze = zis.getNextEntry();
            while(ze != null){
                String fileName = ze.getName();
                File newFile = new File(destDir + File.separator + fileName);
                unZipped_File_path = newFile.getAbsolutePath();
                System.out.println("Unzipping to "+unZipped_File_path);
                //create directories for sub directories in zip
                new File(newFile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
                }
                fos.close();
                //close this ZipEntry
                zis.closeEntry();
                ze = zis.getNextEntry();
            }
            //close last ZipEntry
            zis.closeEntry();
            zis.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
		return unZipped_File_path;
    }

	public static void testStatusToastPass(String message) throws InterruptedException {

		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// Check for jQuery on the page, add it if need be
			js.executeScript("if (!window.jQuery) {"
					+ "var jquery = document.createElement('script'); jquery.type = 'text/javascript';"
					+ "jquery.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js';"
					+ "document.getElementsByTagName('head')[0].appendChild(jquery);" + "}");
			Thread.sleep(1000);
			js.executeScript("$.getScript('https://the-internet.herokuapp.com/js/vendor/jquery.growl.js')");

			js.executeScript("$('head').append('<link id=\"passtoast\" rel=\"stylesheet\" "
					+ "href=\"https://the-internet.herokuapp.com/css/jquery.growl.css\" " + "type=\"text/css\" />');");
			Thread.sleep(1000);
			js.executeScript("$.growl.notice({ title: 'Passed', message: '" + message + "' });");
			Thread.sleep(3000);

			js.executeScript(
					"var element = document.getElementById('passtoast');" + "element.parentNode.removeChild(element);");
			Thread.sleep(1000);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());

		}

	}
	
	
    public boolean isDuplicatesPresentInList(List<String> listContainingDuplicates ) {
    	boolean duplicates=false;
    	final Set<String> unique = new HashSet<String>(); 

    	for (String s:listContainingDuplicates){
            if(!unique.add(s)){   
                System.out.println("Duplicates found "+s);
                duplicates=true;
            }
    		
    }
    	return duplicates;
    }
    public List<String> getListOfValuesFromJson(String jsonPath) {
    	ResponseBody response = RestAssuredSteps.getResponse().getBody();
		List<String> values=new ArrayList<String>();

		List<String> data = com.jayway.jsonpath.JsonPath.read(response.asString(), jsonPath);
			for (int i = 0; i < data.size(); i++) {
				values.add(data.get(i));

			}
			System.out.println(values);
			return values;
		}
    public List<Long> getListOfLongValuesFromJson(String jsonPath) {
    	ResponseBody response = RestAssuredSteps.getResponse().getBody();
		List<Long> values=new ArrayList<Long>();

		List<Object> data = com.jayway.jsonpath.JsonPath.read(response.asString(), jsonPath);
			for (int i = 0; i < data.size(); i++) {
					Long l = Long.valueOf(data.get(i).toString());
					values.add(l);
				}

			return values;
		}
    public String convertMillisecondsToDateTime(long miliSec,String dateFormat) {
        	
        	  
            // Creating date format 
            DateFormat simple = new SimpleDateFormat(dateFormat); 
      
            // Creating date from milliseconds 
            // using Date() constructor 
            Date result = new Date(miliSec); 
      
            // Formatting Date according to the 
            // given format 
            System.out.println(simple.format(result)); 
            return simple.format(result);
    	    }
    public String convertMillisecondsToDateTime(int miliSec,String dateFormat) {
    	
  	  
        // Creating date format 
        DateFormat simple = new SimpleDateFormat(dateFormat); 
  
        // Creating date from milliseconds 
        // using Date() constructor 
        Date result = new Date(miliSec); 
  
        // Formatting Date according to the 
        // given format 
        System.out.println(simple.format(result)); 
        return simple.format(result);
	    }
    public String convertMillisecondsToDateTime(String miliSec,String dateFormat) {
    	
    	  
        // Creating date format 
        DateFormat simple = new SimpleDateFormat(dateFormat); 
  
        // Creating date from milliseconds 
        // using Date() constructor 
        @SuppressWarnings("deprecation")
		Date result = new Date(miliSec); 
  
        // Formatting Date according to the 
        // given format 
        System.out.println(simple.format(result)); 
        return simple.format(result);
	    }
    public List<Integer> getListOfIntegerValuesFromJson(String jsonPath) {
    	ResponseBody response = RestAssuredSteps.getResponse().getBody();
		List<Integer> values=new ArrayList<Integer>();

		List<Integer> data = com.jayway.jsonpath.JsonPath.read(response.asString(), jsonPath);
			for (int i = 0; i < data.size(); i++) {
				values.add(data.get(i));

			}
			return values;
		}
    public static String getURL(String key) throws IOException {
		String path=System.getProperty("user.dir")+"\\src\\test\\java\\com\\TEAF\\TestFiles\\Config\\apiURL.properties";
		BufferedReader reader = new BufferedReader(new FileReader(path));
		 Properties pro= new Properties();
		 pro.load(reader);
		return pro.getProperty(key);
	}
    
    public static String getProperties(String key) throws IOException {
		String path=System.getProperty("user.dir")+"\\src\\test\\java\\com\\TEAF\\TestFiles\\Config\\apiProperties_"+System.getProperty("env")+".properties";
		BufferedReader reader = new BufferedReader(new FileReader(path));
		 Properties pro= new Properties();
		 pro.load(reader);
		return pro.getProperty(key);
	}

}
