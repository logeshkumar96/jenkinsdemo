package com.TEAF.framework;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GenerateFailureReportJSON {

	List<Map<String, Object>> stepList = new ArrayList<Map<String, Object>>();
	List<Map<String, Object>> featureList = new ArrayList<Map<String, Object>>();
	List<Map<String, Object>> sceanrioList = new ArrayList<Map<String, Object>>();

	public List<Map<String, Object>> getStepList() {
		return stepList;
	}

	public void setStepList(List<Map<String, Object>> stepList) {
		this.stepList = stepList;
	}

	public List<Map<String, Object>> getFeatureList() {
		return featureList;
	}

	public void setFeatureList(List<Map<String, Object>> featureList) {
		this.featureList = featureList;
	}

	public List<Map<String, Object>> getSceanrioList() {
		return sceanrioList;
	}

	public void setSceanrioList(List<Map<String, Object>> sceanrioList) {
		this.sceanrioList = sceanrioList;
	}

}
