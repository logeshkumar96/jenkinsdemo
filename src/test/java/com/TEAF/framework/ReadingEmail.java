package com.TEAF.framework;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;

import org.apache.log4j.Logger;
import org.junit.Assert;

public class ReadingEmail {
	static Logger log = Logger.getLogger(ReadingEmail.class.getName());

	public static String fetch(String pop3Host, String storeType, String user, String password, String subject,
			String messageBody) {
		String sub = null;
		String writePart = "";
		try {
			// create properties field
			Properties properties = new Properties();
			properties.put("mail.store.protocol", "pop3");
			properties.put("mail.pop3.host", pop3Host);
			properties.put("mail.pop3.port", "995");
			properties.put("mail.pop3.starttls.enable", "true");
			Session emailSession = Session.getDefaultInstance(properties);
			// emailSession.setDebug(true);
			Store store = emailSession.getStore("pop3s");
			store.connect(pop3Host, user, password);

			Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);

			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			Message[] messages = emailFolder.getMessages();
			log.info("messages.length---" + messages.length);
			for (int i = messages.length; i >= messages.length - 15; i--) {
				Message message = messages[i - 1];
				System.out.println(message.getSubject());
				if (message.getSubject().contains(subject) && sub == null) {
					log.info("Subject Matched " + message.getSubject() + "with " + subject);
					sub = message.getSubject().toString();
					writePart = writePart(message, messageBody);
					break;

				}
				emailFolder.close(false);
				store.close();

			}
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return writePart;

	}

	public static String getemailRead(String host, String mailStoreType, String username, String password,
			String subject, String messageBody) {
		String fetch = fetch(host, mailStoreType, username, password, subject, messageBody);
		return fetch;

	}
	public static String writePart(Part p, String verifyBody) throws Exception {
		String messageContent = "";
		if (p instanceof Message)
			writeEnvelope((Message) p);
		String saveDirectory = System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\";

		System.out.println("----------------------------");
		System.out.println("CONTENT-TYPE: " + p.getContentType());

		if (p.isMimeType("text/plain")) {
			System.out.println("This is plain text");
			System.out.println("---------------------------");
			System.out.println((String) p.getContent());
			messageContent = (String) p.getContent();
			Assert.assertTrue(messageContent.contains(verifyBody));
		}
		else if (p.isMimeType("multipart/*")) {
			System.out.println("Inside the Multipart");
			String attachFiles = "";

			// content may contain attachments
			Multipart multiPart = (Multipart) p.getContent();
			int numberOfParts = multiPart.getCount();
			for (int partCount = 0; partCount < numberOfParts; partCount++) {
				MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
				if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
					String fileName = part.getFileName();
					part.saveFile(saveDirectory + File.separator + fileName);
					System.out.println("File Created at " + saveDirectory + File.separator + fileName);
				} else {
					messageContent = part.getContent().toString();
				}
			}

			if (attachFiles.length() > 1) {
				attachFiles = attachFiles.substring(0, attachFiles.length() - 2);
			}
		} else if (p.getContentType().contains("image/"))

		{
			System.out.println("content type" + p.getContentType());
			File f = new File("image" + new Date().getTime() + ".jpg");
			DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(f)));
			com.sun.mail.util.BASE64DecoderStream test = (com.sun.mail.util.BASE64DecoderStream) p.getContent();
			byte[] buffer = new byte[1024];
			int bytesRead;
			while ((bytesRead = test.read(buffer)) != -1) {
				output.write(buffer, 0, bytesRead);
			}
		} else {
			Object o = p.getContent();
			if (o instanceof String) {
				// System.out.println("This is a string");
				// System.out.println("---------------------------");
				String html = (String) o;

				File htmlFile = new File(
						System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\EmailBody.html");
				FileOutputStream fio = new FileOutputStream(htmlFile);
				fio.write(html.getBytes());
				fio.close();
				// System.out.println((String) o);
			} else if (o instanceof InputStream) {
				System.out.println("This is just an input stream");
				System.out.println("---------------------------");
				String f = System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources";
				if (p.getContent() instanceof Multipart) {
					Multipart multipart = (Multipart) p.getContent();

					for (int i = 0; i < multipart.getCount(); i++) {
						Part part = multipart.getBodyPart(i);
						String disposition = part.getDisposition();
						if ((disposition != null) && ((disposition.equalsIgnoreCase(Part.ATTACHMENT)
								|| (disposition.equalsIgnoreCase(Part.INLINE))))) {
							MimeBodyPart mimeBodyPart = (MimeBodyPart) part;
							String fileName = mimeBodyPart.getFileName();
							System.out.println(fileName);
							File fileToSave = new File(f + fileName);
							mimeBodyPart.saveFile(fileToSave);
						}
					}
				}

			} else {
				System.out.println("This is an unknown type");
				System.out.println("---------------------------");
				System.out.println(o.toString());
			}
		}
		return messageContent;

	}

	public static void writeEnvelope(Message m) throws Exception {
		Address[] a;

		// FROM
		if ((a = m.getFrom()) != null) {
			for (int j = 0; j < a.length; j++)
				System.out.println("FROM: " + a[j].toString());
		}

		// TO
		if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
			for (int j = 0; j < a.length; j++)
				System.out.println("TO: " + a[j].toString());
		}

		// SUBJECT
		if (m.getSubject() != null)
			System.out.println("SUBJECT: " + m.getSubject());

	}

}