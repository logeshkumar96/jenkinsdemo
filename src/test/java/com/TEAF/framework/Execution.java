package com.TEAF.framework;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import com.TEAF.Hooks.Hooks;
import com.TEAF.stepDefinitions.GalenStep;
import com.galenframework.config.GalenConfig;
import com.galenframework.config.GalenProperty;

public class Execution {

	static Logger log = Logger.getLogger(Execution.class.getName());

	public static void setup(String Platform, String Browser, String URL, String appType, String winAppLocation,
			String TestName, String jsonFileName) {
		try {

			PropertyConfigurator.configure(
					System.getProperty("user.dir") + "/src/test/java/com/TEAF/TestFiles/Config/log4j.properties");

			// LoadConfigurations
			TestConfig.LoadAllConfig();

			Browser = System.getProperty("BrowserName", Browser);
			appType = System.getProperty("AppType", appType);
			winAppLocation = System.getProperty("AppLocation", winAppLocation);
			URL = System.getProperty("URL", URL);

			System.setProperty("test.platformName", Platform);
			System.setProperty("test.browserName", Browser);
			System.setProperty("test.jsonFileName", jsonFileName);

			System.setProperty("test.appType", appType);

			if (Browser.equals("BS-Chrome-Win10")) {
				Browser = "browserstack-desktop-browser-local";
				System.setProperty("test.browserName", "browserstack-desktop-browser-local");

				System.setProperty("test.bstack.browser", "Chrome");
				System.setProperty("test.bstack.browserVersion", "80.0");
				System.setProperty("test.bstack.os", "Windows");
				System.setProperty("test.bstack.osVersion", "10");
				System.setProperty("test.bstack.resolution", "1280x1024");
			} else if (Browser.equals("BS-Firefox-Win8")) {
				Browser = "browserstack-desktop-browser-local";

				System.setProperty("test.browserName", "browserstack-desktop-browser-local");

				System.setProperty("test.bstack.browser", "Firefox");
				System.setProperty("test.bstack.browserVersion", "73.0");
				System.setProperty("test.bstack.os", "Windows");
				System.setProperty("test.bstack.osVersion", "8");
				System.setProperty("test.bstack.resolution", "1280x1024");
			} else if (Browser.equals("BS-Edge-Win10")) {
				Browser = "browserstack-desktop-browser-local";

				System.setProperty("test.browserName", "browserstack-desktop-browser-local");

				System.setProperty("test.bstack.browser", "Edge");
				System.setProperty("test.bstack.browserVersion", "80.0");
				System.setProperty("test.bstack.os", "Windows");
				System.setProperty("test.bstack.osVersion", "10");
				System.setProperty("test.bstack.resolution", "1280x1024");
			} else if (Browser.equals("BS-Safari-OS-X")) {
				Browser = "browserstack-desktop-browser-local";

				System.setProperty("test.browserName", "browserstack-desktop-browser-local");

				System.setProperty("test.bstack.browser", "Safari");
				System.setProperty("test.bstack.browserVersion", "11.1");
				System.setProperty("test.bstack.os", "OS X");
				System.setProperty("test.bstack.osVersion", "High Sierra");
				System.setProperty("test.bstack.resolution", "1280x1024");
			}

			if (winAppLocation != null) {
				System.setProperty("test.winDesktopApp", winAppLocation);
			}
			if (URL != null) {
				System.setProperty("test.appUrl", URL);

			}
			System.setProperty("test.TestName", TestName);

			// Galen Property
			GalenConfig.getConfig().setProperty(GalenProperty.SCREENSHOT_FULLPAGE, "true");

			log.info("Platform: " + Platform);
			log.info("Browser : " + Browser);
			if (Platform.equals("desktop") && !Browser.equals("REST Service") && !Browser.equals("DB Service")) {
				StepBase.setUp(Platform, Browser);
			} else if (Platform.equals("mobile") && !Browser.equals("REST Service") && !Browser.equals("DB Service")) {
				StepBase.setUp(Platform, Browser);
			} else {
				System.out.println("Enter valid platform choice: desktop / android / ios");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void tearDown(String successFulRecipients, String failureRecipients, String successFulCCRecipients,
			String failureCCRecipients) {

		try {

			if (System.getProperty("test.disableGalenReport").equalsIgnoreCase("false")) {
				GalenStep.generateUIReport();
			}

			try {
				StepBase.proxy.stop();
			} catch (Exception e1) {
			}

		//	StepBase.tearDown();
			Collection<String> values = Hooks.scenarioStatus.values();
			String toMailId = null;
			String ccMailId = null;
			boolean flag = false;
			if ((values.contains("FAILED") || values.contains("SKIPPED"))
					&& !(Hooks.code == 520 || Hooks.code == 521)) {
				flag = true;
			}

			log.info("Failure Present: " + flag);
			if (flag == true) {
				// Mailing only internal team to review the failures
				toMailId = successFulRecipients;
				ccMailId = successFulCCRecipients;
			} else {
				// Mailing everyone as results look good
				toMailId = failureRecipients;
				ccMailId = failureCCRecipients;
			}

			ExcludeHook.removeBeforeAfterHookForPassedStatus(System.getProperty("test.jsonFileName"));

			// Generation of Default Cucumber Reports
			if (!System.getProperty("test.disableCucumberReport").equalsIgnoreCase("true")) {
				
				GenerateCustomReport.generateCustomeReport(System.getProperty("test.browserName"),
						System.getProperty("test.platformName"), System.getProperty("test.jsonFileName"));
				HashMapContainer.ClearHM();
			}

			String zipReportFiles = ZipFiles.zipReportFiles(GenerateCustomReport.reportPath,
					System.getProperty("reports.FolderName"));

			if (System.getProperty("test.generateFTPTransfer").equalsIgnoreCase("true")) {
				try {

					UploadReportsToFTPServer.connectToFTPandTransferFiles("ip", "user", "password", zipReportFiles,
							"reports.zip", "/user25");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			// Prepare reports for Email
			if (System.getProperty("test.generateEmail").equalsIgnoreCase("true")) {
				String toMail = System.getProperty("ToMailID", toMailId);
				String ccMail = System.getProperty("CCMailID", ccMailId);
				log.info("To email id : " + toMail);
				log.info("Cc email id : " + ccMail);
				try {
					Utilities.auto_generation_Email(toMail, ccMail);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			try {
				Utilities.deleteZipFiles(System.getProperty("reports.FolderName"));
				Utilities.deleteZipFiles("TestExecution_UIReports");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
