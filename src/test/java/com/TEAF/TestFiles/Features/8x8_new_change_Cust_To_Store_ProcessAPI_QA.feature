@8x8_new_change
Feature: Customer_To_Store Call using ProcessAPI in QA Environment

Description:
1. This feature File covers Process API for 8x8 call between Customer to Store
2. New change is to send sales person preferred name instead of sales person name

Scenario Outline: Validation for sending sales person preferred name instead of sales person name

	Given EndPoint 'https://qaawsmule.mfrm.com:9085/api/svoc/phoneNumber'
	When Query params with Key 'caller' and value '<callerValue>'
	When Query params with Key 'receiver' and value '<receiverValue>'
	When Method 'Get'
	And Print response
	And Retrieve data '$.CUST.OPEN_ORDERS_COUNT' from response and store in variable 'OpenOrderCount'
	And Retrieve data '$.CUST.CLOSED_ORDERS_COUNT' from response and store in variable 'ClosedOrderCount'
	And Statuscode '<StatusCode>'
	And get the Key value of '<OrderTotal1>' & '<OrderTotal2>' and verify with the total '$.CUST.TOTAL_SALE' 
	And Verify key '$.CLOSED_ORDERS.ORDER_NUMBER' count present in the response orders- '$$ClosedOrderCount' 
	And Verify key '$.OPEN_ORDERS[*].ORDER_NUMBER' count present in the response orders- '$$OpenOrderCount'
	And Key '$.CLOSED_ORDERS.ORDER_NUMBER' List should not contain more than '5' set of records
	And Key '$.OPEN_ORDERS[*].ORDER_NUMBER' List should not contain more than '5' set of records
	And Verify Value '<CustomerName>' present in field 'CUST.CUST_NAME' 
	And Verify Value '<CustomerTotalSale>' present in field 'CUST.TOTAL_SALE' 
	And Verify Value '<CustomerPhoneNumber>' present in field 'CUST.PHONE_NUMBER' 
	And Verify Value '<OpenOrderTotal>' present in field 'CUST.OPEN_ORDERS_TOTAL' 
	And Verify Value '<OpenOrderCount>' present in field as String 'CUST.OPEN_ORDERS_COUNT' 
	And Verify Value '<ClosedOrderCount>' present in field as String 'CUST.CLOSED_ORDERS_COUNT'
	And Verify Value '<ClosedOrderTotal>' present in field 'CUST.CLOSED_ORDERS_TOTAL' 
	And Verify Value '<OpenOrderNumber>' present in field '<OpenOrderNumberField>' 
	And Verify Value '<OpenOrderSalesPersonName1>' present in field '<OpenOrderSalesPersonName1Field>' 
	And Verify Value '<OpenOrderSalesPersonName2>' present in field '<OpenOrderSalesPersonName2Field>' 
	And Verify Value '<OpenOrderStoreCD>' present in field '<OpenOrderStoreCDField>' 
	And Verify Value '<OpenOrderCreatedDate>' present in field '<OpenOrderCreatedDateField>' 
	And Verify Value '<OpenOrderDeliveryDate>' present in field '<OpenOrderDeliveryDateField>' 
	
	
	And Verify Value '<OpenOrderNumber2>' present in field '<OpenOrderNumberField2>' 
	And Verify Value '<OpenOrderSalesPersonName1.2>' present in field '<OpenOrderSalesPersonName1Field2>' 
	And Verify Value '<OpenOrderSalesPersonName2.2>' present in field '<OpenOrderSalesPersonName2Field2>' 
	And Verify Value '<OpenOrderStoreCD2>' present in field '<OpenOrderStoreCDField2>' 
	And Verify Value '<OpenOrderCreatedDate2>' present in field '<OpenOrderCreatedDateField2>' 
	And Verify Value '<OpenOrderDeliveryDate2>' present in field '<OpenOrderDeliveryDateField2>' 
	
	
	And Verify Value '<OpenOrderNumber3>' present in field '<OpenOrderNumberField3>' 
	And Verify Value '<OpenOrderSalesPersonName1.3>' present in field '<OpenOrderSalesPersonName1Field3>' 
	And Verify Value '<OpenOrderSalesPersonName2.3>' present in field '<OpenOrderSalesPersonName2Field3>' 
	And Verify Value '<OpenOrderStoreCD3>' present in field '<OpenOrderStoreCDField3>' 
	And Verify Value '<OpenOrderCreatedDate3>' present in field '<OpenOrderCreatedDateField3>' 
	And Verify Value '<OpenOrderDeliveryDate3>' present in field '<OpenOrderDeliveryDateField3>' 
	
	
	And Verify Value '<OpenOrderNumber4>' present in field '<OpenOrderNumberField4>' 
	And Verify Value '<OpenOrderSalesPersonName1.4>' present in field '<OpenOrderSalesPersonName1Field4>' 
	And Verify Value '<OpenOrderSalesPersonName2.4>' present in field '<OpenOrderSalesPersonName2Field4>' 
	And Verify Value '<OpenOrderStoreCD4>' present in field '<OpenOrderStoreCDField4>' 
	And Verify Value '<OpenOrderCreatedDate4>' present in field '<OpenOrderCreatedDateField4>' 
	And Verify Value '<OpenOrderDeliveryDate4>' present in field '<OpenOrderDeliveryDateField4>' 
	
	And Verify Value '<OpenOrderNumber5>' present in field '<OpenOrderNumberField5>' 
	And Verify Value '<OpenOrderSalesPersonName1.5>' present in field '<OpenOrderSalesPersonName1Field5>' 
	And Verify Value '<OpenOrderSalesPersonName2.5>' present in field '<OpenOrderSalesPersonName2Field5>' 
	And Verify Value '<OpenOrderStoreCD5>' present in field '<OpenOrderStoreCDField5>' 
	And Verify Value '<OpenOrderCreatedDate5>' present in field '<OpenOrderCreatedDateField5>' 
	And Verify Value '<OpenOrderDeliveryDate5>' present in field '<OpenOrderDeliveryDateField5>' 
	
	And Verify Value '<ClosedOrderNumber1>' present in field '<ClosedOrderNumberField1>' 
	And Verify Value '<ClosedOrderNumber2>' present in field '<ClosedOrderNumberField2>' 
	And Verify Value '<ClosedOrderNumber3>' present in field '<ClosedOrderNumberField3>' 

Examples:	
|callerValue|receiverValue|StatusCode|CustomerName            |CustomerTotalSale|CustomerPhoneNumber|OpenOrderTotal|OpenOrderCount|ClosedOrderCount|ClosedOrderTotal|OpenOrderNumber|OpenOrderNumberField       |OpenOrderSalesPersonName1	  	  |OpenOrderSalesPersonName1Field   |OpenOrderSalesPersonName2	  |OpenOrderSalesPersonName2Field   |OpenOrderStoreCD|OpenOrderStoreCDField     |OpenOrderCreatedDate|OpenOrderCreatedDateField  |OpenOrderDeliveryDate|OpenOrderDeliveryDateField  |OpenOrderNumber2|OpenOrderNumberField2       |OpenOrderSalesPersonName1.2|OpenOrderSalesPersonName1Field2   |OpenOrderSalesPersonName2.2  |OpenOrderSalesPersonName2Field2   |OpenOrderStoreCD2|OpenOrderStoreCDField2     |OpenOrderCreatedDate2|OpenOrderCreatedDateField2  |OpenOrderDeliveryDate2|OpenOrderDeliveryDateField2  |OpenOrderNumber3|OpenOrderNumberField3       |OpenOrderSalesPersonName1.3	|OpenOrderSalesPersonName1Field3  |OpenOrderSalesPersonName2.3|OpenOrderSalesPersonName2Field3   |OpenOrderStoreCD3|OpenOrderStoreCDField3     |OpenOrderCreatedDate3|OpenOrderCreatedDateField3  |OpenOrderDeliveryDate3|OpenOrderDeliveryDateField3  |OpenOrderNumber4  |OpenOrderNumberField4       |OpenOrderSalesPersonName1.4|OpenOrderSalesPersonName1Field4   |OpenOrderSalesPersonName2.4|OpenOrderSalesPersonName2Field4   |OpenOrderStoreCD4|OpenOrderStoreCDField4     |OpenOrderCreatedDate4|OpenOrderCreatedDateField4  |OpenOrderDeliveryDate4|OpenOrderDeliveryDateField4  |OpenOrderNumber5  |OpenOrderNumberField5       |OpenOrderSalesPersonName1.5 |OpenOrderSalesPersonName1Field5   |OpenOrderSalesPersonName2.5  |OpenOrderSalesPersonName2Field5   |OpenOrderStoreCD5|OpenOrderStoreCDField5     |OpenOrderCreatedDate5|OpenOrderCreatedDateField5  |OpenOrderDeliveryDate5|OpenOrderDeliveryDateField5  |ClosedOrderNumber1|ClosedOrderNumber2|ClosedOrderNumber3|ClosedOrderNumberField1      |ClosedOrderNumberField2      |ClosedOrderNumberField3   |OrderTotal1               |OrderTotal2               |
|15033672238|18478188782  |200       |WILLIAM GRAVES          |800              |+1 (503) 367-2238  |800           |1             |0               |0               |AX-S030962084  |OPEN_ORDERS[0].ORDER_NUMBER|Bill Hulse               	  	  |OPEN_ORDERS[0].SALES_PERSON_NAME1|Chase McCullough         	  |OPEN_ORDERS[0].SALES_PERSON_NAME2|526015          |OPEN_ORDERS[0].SO_STORE_CD|06/06/2020          |OPEN_ORDERS[0].CREATED_DATE|01/01/2001           |OPEN_ORDERS[0].DELIVERY_DATE|                |                            |                           |                                  |                             |                                  |                 |                           |                     |                            |                      |                             |                |                            |                           	|                                 |                           |                                  |                 |                           |                     |                            |                      |                             |                  |                            |                           |                                  |                           |                                  |                 |                           |                     |                            |                      |                             |                  |                            |                            |                                  |                             |                                  |                 |                           |                     |                            |                      |                             |                  |                  |                  |                             |                             |                          |$.CUST.OPEN_ORDERS_TOTAL  |$.CUST.CLOSED_ORDERS_TOTAL|
|14422449691|18478188782  |200       |Manoj Thota             |800 	         	|+1 (442) 244-9691  |800	       |1             |0               |0		        |AX-T015289001  |OPEN_ORDERS[0].ORDER_NUMBER|Test1PrefFName Test1PrefLName	  |OPEN_ORDERS[0].SALES_PERSON_NAME1|Test2PrefFName Test2PrefLName|OPEN_ORDERS[0].SALES_PERSON_NAME2|526015  		 |OPEN_ORDERS[0].SO_STORE_CD|02/09/2021          |OPEN_ORDERS[0].CREATED_DATE|02/10/2021           |OPEN_ORDERS[0].DELIVERY_DATE|			     |OPEN_ORDERS[1].ORDER_NUMBER |                           |OPEN_ORDERS[1].SALES_PERSON_NAME1 |				               |OPEN_ORDERS[1].SALES_PERSON_NAME2 |	                |OPEN_ORDERS[1].SO_STORE_CD |			          |OPEN_ORDERS[1].CREATED_DATE |		              |OPEN_ORDERS[1].DELIVERY_DATE |			     |OPEN_ORDERS[2].ORDER_NUMBER |                           	|OPEN_ORDERS[2].SALES_PERSON_NAME1|                           |OPEN_ORDERS[2].SALES_PERSON_NAME2 |		           |OPEN_ORDERS[2].SO_STORE_CD |		             |OPEN_ORDERS[2].CREATED_DATE |			             |OPEN_ORDERS[2].DELIVERY_DATE |			      |OPEN_ORDERS[3].ORDER_NUMBER |                           |OPEN_ORDERS[3].SALES_PERSON_NAME1 |                           |OPEN_ORDERS[3].SALES_PERSON_NAME2 |		           |OPEN_ORDERS[3].SO_STORE_CD |		             |OPEN_ORDERS[3].CREATED_DATE |			             |OPEN_ORDERS[3].DELIVERY_DATE |                  |                            |                            |                                  |                             |                                  |                 |                           |                     |                            |                      |                             |				     |                  |                  |CLOSED_ORDERS.ORDER_NUMBER[0]|                             |                          |$.CUST.OPEN_ORDERS_TOTAL  |$.CUST.CLOSED_ORDERS_TOTAL|                          
#|14422449698|18478188782  |200       |Manoj Thota             |4000             |+1 (442) 244-9698  |4000          |5             |0               |0               |AX-T015289008  |OPEN_ORDERS[0].ORDER_NUMBER|sP81 PrefFName81 sP81 PrefLName81|OPEN_ORDERS[0].SALES_PERSON_NAME1|'sP82PrefFName82 '        	  |OPEN_ORDERS[0].SALES_PERSON_NAME2|526015          |OPEN_ORDERS[0].SO_STORE_CD|02/09/2021          |OPEN_ORDERS[0].CREATED_DATE|02/10/2021           |OPEN_ORDERS[0].DELIVERY_DATE|AX-T015289008   |OPEN_ORDERS[1].ORDER_NUMBER |              			  |OPEN_ORDERS[1].SALES_PERSON_NAME1 |SP84 Name84                  |OPEN_ORDERS[1].SALES_PERSON_NAME2 |526015           |OPEN_ORDERS[1].SO_STORE_CD |02/09/2021           |OPEN_ORDERS[1].CREATED_DATE |02/10/2021            |OPEN_ORDERS[1].DELIVERY_DATE |AX-T015289008   |OPEN_ORDERS[2].ORDER_NUMBER |Sp8 PrefFName8 sP8 PrefLName8|OPEN_ORDERS[2].SALES_PERSON_NAME1|                           |OPEN_ORDERS[2].SALES_PERSON_NAME2 |526015           |OPEN_ORDERS[2].SO_STORE_CD |02/09/2021           |OPEN_ORDERS[2].CREATED_DATE |02/10/2021            |OPEN_ORDERS[2].DELIVERY_DATE |AX-T015289008     |OPEN_ORDERS[3].ORDER_NUMBER |		                   |OPEN_ORDERS[3].SALES_PERSON_NAME1 |                           |OPEN_ORDERS[3].SALES_PERSON_NAME2 |526015           |OPEN_ORDERS[3].SO_STORE_CD |02/09/2021           |OPEN_ORDERS[3].CREATED_DATE |02/10/2021            |OPEN_ORDERS[3].DELIVERY_DATE |AX-T015289008     |OPEN_ORDERS[4].ORDER_NUMBER |SP851 name851               |OPEN_ORDERS[4].SALES_PERSON_NAME1 |sp852 name852                |OPEN_ORDERS[4].SALES_PERSON_NAME2 |526015           |OPEN_ORDERS[4].SO_STORE_CD |02/09/2021           |OPEN_ORDERS[4].CREATED_DATE |02/10/2021            |OPEN_ORDERS[4].DELIVERY_DATE |                  |                  |                  |                             |                             |                          |$.CUST.OPEN_ORDERS_TOTAL  |$.CUST.CLOSED_ORDERS_TOTAL|

Scenario: Validation for Customer to Store Call without "caller" input parameter using Process API 
	Given EndPoint 'https://qaawsmule.mfrm.com:9085/api/svoc/phoneNumber' 
	#	When Query params with Key 'caller' and value '14422449698' 
	When Query params with Key 'receiver' and value '18478188782' 
	When Method 'Get' 
	And Print response 
	And Statuscode '400' 
	And Verify Value 'Bad request' present in field 'message'
	
Scenario: Validation for Customer to Store Call without "receiver" input parameter using Process API 
	Given EndPoint 'https://qaawsmule.mfrm.com:9085/api/svoc/phoneNumber' 
	When Query params with Key 'caller' and value '14422449698' 
	#	When Query params with Key 'receiver' and value '18478188782' 
	When Method 'Get' 
	And Print response 
	And Statuscode '400' 
	And Verify Value 'Bad request' present in field 'message'
