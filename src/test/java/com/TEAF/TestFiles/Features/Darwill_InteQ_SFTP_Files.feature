@MF_Darwill_InteQ
Feature: Fetching data from Darwill SFTP file's and pushing that data into new file's in InteQ SFTP

Description:
1.Connect to Darwill SFTP and fetch 2 different CSV files
2.Connect to InteQ SFTP fetch 2 newly created CSV files
3.Validate end to end data between these 4 files

Scenario: Files transfer from Darwill SFTP to InteQ SFTP
	#	Dynamically generate date as XD-1: Sysdate-1, XD+1: Sysdate+1, XDC: Current Sysdate
	Given I generate date 'XD-1' in format 'yyyyMMdd' and store as 'DarwillInteQMFResponseReportDate'
	Then Connect to SFTP with host 'twowayftp.darwill.com', port '22', user name 'mattressfirm' and password 'a#*kTwoWYF!@#' then find 'MattressFirm_Response Report' for the date '$$DarwillInteQMFResponseReportDate' from directory '/writable' and download to 'F:\\Manoj Thota\\Darwill to InteQ SFTP file transfer\\Darwill SFTP files\\'
#	Then assert SFTP CSV file '$$SFTPDownloadedfilePath' record count with SQL result CSV file '$$SabaSQLResultFilePath' record count
#	Then assert end to end data between source SFTP CSV file '$$SFTPDownloadedfilePath' and target SQL stage table result CSV file '$$SabaSQLResultFilePath' for New Hire Exam
#	And assert end to end data for '20' records between source as SQL stage table result CSV file '$$SabaSQLResultFilePath' and target as SQL master table for New Hire Exam
##	And assert end to end data for all the records between source as SQL stage table result CSV file '$$SabaSQLResultFilePath' and target as SQL master table for New Hire Exam
	
	Given I generate date 'XD-1' in format 'yyyy-MM-dd' and store as 'DarwillInteQSelectedFinalMFReportDate'
	Then Connect to SFTP with host 'twowayftp.darwill.com', port '22', user name 'mattressfirm' and password 'a#*kTwoWYF!@#' then find 'Selected Final MF Report' for the date '$$DarwillInteQSelectedFinalMFReportDate' from directory '/writable' and download to 'F:\\Manoj Thota\\Darwill to InteQ SFTP file transfer\\Darwill SFTP files\\'
	