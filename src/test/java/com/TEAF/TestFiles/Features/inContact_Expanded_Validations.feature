@MF_inContact_Expanded_Report
Feature: InContact Expanded - Source InContact Endpoint results and Target is BQ and GCP

1.Call inContact endpoint and download CSV files for Expanded report.
2.Perform below validations for WithHold and Expanded report by comparing downloaded source file with target MF BQ table and GCS CSV file.
2.1 Duplicates in source CSV file.
2.2 Duplicates in target BQ table and GCS Bucket CSV file.
2.3 Count validation between source and target.
2.4 End to End data validation between source and target.

Description: In this Feature file we will be covering inContact endpoint, GC Bucket and BigQuery validations
Scenario: (Expanded) Connecting to BigQuery and fetch query results and download CSV file using inContact endpoint and then downloading GCP Bucket CSV file and validate the record count and content between BigQuery result and GCS CSV file and source CSV file
	Given Connect to BigQuery with project ID 'qa-mfrm-data'
	#	Dynamically generate date as XD-1: Sysdate-1, XD+1: Sysdate+1, XDC: Current Sysdate
	Then I generate date 'XD-1' in format 'yyyy-MM-dd' and store as 'startDate'
#	And I generate date 'XD-2' in format 'yyyy-MM-dd' and store as 'DateUsedForFetchingBQresults'
	Then fetch BigQuery results for Start date '$$startDate' using query "select * from `qa-mfrm-data.mfrm_customer_and_social.incontact_expanded_call`" for InContact
	And fetch 'ContactId' records and count from BigQuery results
	Then I generate date 'XD-1' in format 'MM-dd-yyyy' and store as 'FromDate'
	Then I generate date 'XDC' in format 'MM-dd-yyyy' and store as 'ToDate'
	Then capture CSV file for 'Expanded' flow between '$$FromDate' and '$$ToDate' using inContact API 'https://home-c16.incontact.com/ReportService/DataDownloadHandler.ashx?CDST=ElurGcWPf8xdyZtPUsN3LrcadtHb7aNgp%2f%2bipeEVk4lCVgtKu4L9FqRGd4jlDu%2b8zFoJUl2zkSbOfdQcW1nJgsEmmK6htmQKpxTBrmgO5KhD6HgxSZOWSk0dvtvxNiuY7B58RbBiF%2b%2bu1JMPDvaicfkB%2fOGtnlJ3BPDIEqObuwxDIWkvnse6oAI2cFFEBW%2f3VO9zRbJp4HPQAZeG54rglbtxqPTBd5PmVYNUaeXcOQYMkQaAJRb2mVfo&Format=CSV&IncludeHeaders=True&AppendDate=False' and download the CSV file to the path 'F:\Manoj Thota\InContact\Expanded\Expanded_API_CSV_files'
	And get the count of records available in inContact csv file '$$inContactAPIResultCSVFilePath'
	Then assert if duplicate records exist in source inContact API result csv file '$$inContactAPIResultCSVFilePath'
	And assert if duplicate records exist in target BigQuery results
	And assert all column values for count of '4' rows between BigQuery and CSV file '$$inContactAPIResultCSVFilePath' for Expanded
	#	Used to validate all the rows--> And assert all column values for all rows between BigQuery and CSV file '$$inContactAPIResultCSVFilePath' for Expanded
#	And assert BigQuery record count with CSV file '$$inContactAPIResultCSVFilePath' record count
	#	Initiating GCP Bucket CSV file download and validations
	Then Connect to GCS Bucket with project ID 'qa-mfrm-data'
	Then I generate date 'XD-1' in format 'yyyy-MM-dd' and store as 'CSVDate'
	Then for 'Expanded', open bucket 'qa-mfrm-mulesoft-data-bucket' and Download CSV file for the date '$$CSVDate' to the local path 'F:\\Manoj Thota\\InContact\\Expanded\\Expanded_Bucket_CSV_files'
#	Then assert end to end data between source CSV file '$$inContactAPIResultCSVFilePath' and target CSV file '$$GCSDownloadedCSVfilePath' for Expanded
	And assert if duplicate records exist in target GCP Bucket result CSV file '$$GCSDownloadedCSVfilePath'
	And assert inContact CSV file '$$inContactAPIResultCSVFilePath' record count with GCP Bucket CSV file '$$GCSDownloadedCSVfilePath' record count
	
