@Shippo_Tracking_ES_to_BQ
Feature: Shippo tracking Staus & History Data push from Elastic Search to BigQuery

Description:
1. Data push from shippo_tracking_status ES index to shippo_tracking_status_stg BQ table
2. Data push from shippo_tracking_status_stg BQ table to shippo_tracking_status master BQ table
3. Data push from shippo_tracking_history ES index to shippo_tracking_history_stg BQ table
4. Data push from shippo_tracking_history_stg BQ table to shippo_tracking_history master BQ table

Scenario: Validation od data push from shippo_tracking_status ES index to shippo_tracking_status_stg BQ table

	#	Dynamically generate date and Time as XD-1: Sysdate-1, XD+1: Sysdate+1, XDC: Current Sysdate,
	#	XT-1: SysTime-1 hour, XT+1: SysTime+1 hour, XTC: Current SysTime
 	Given generate date 'XDC' and time 'XT-2' in format 'yyyy/MM/dd HH:mm:dd' and store as 'fromDateTime'
 	And generate date 'XDC' and time 'XT-1' in format 'yyyy/MM/dd HH:mm:dd' and store as 'toDateTime'
 	Then ES Search with 'Kibana_qa'
	And Select and Verify the index 'shippo_tracking_status' exists
	Then Get Result by passing ES query using '$$fromDateTime' , '$$toDateTime' and store results in a pojo
	Then Connect to BigQuery with project ID 'qa-mfrm-data_Shippo-Tracking-ES-to-BQ'
	And fetch BigQuery results using query 'SELECT * FROM `qa-mfrm-data.mfrm_staging_dataset.shippo_tracking_status_stg`'
	Then assert ES shippo tracking status data with BQ shippo tracking status stage table
	And assert BQ shippo tracking status stage table data with status master table
	And assert BQ shippo tracking status stage table tracking numbers count with history stage table tracking numbers count
	And assert BQ shippo tracking history stage table data with history master table
	And Select and Verify the index 'shippo_tracking_history' exists
	Then assert BQ shippo tracking history stage table data with ES using tracking numbers fetched from BQ status stage table

	