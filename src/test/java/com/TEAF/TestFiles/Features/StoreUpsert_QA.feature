@MF_StoreUpsert_QA1
Feature: StoreUpsert End to End Flow in QA Environment 

Description: In this Feature We will be covering Email system API , SQL Server system API , Elastic Search API , Timestamp , Fetch Service run


@emailQA
Scenario: Store Upsert email Configuration that allows to setup Application Name ,Flow Name ,jobRunEmailDistroFlag ,Email Subject , Email message body content
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/emailconfig' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'StoreUpsert/EmailConfig' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
#	And Retrieve data '$..FROM_ADDRESS' from object index '0' and store in variable 'FROM_ADDRESS'
	And Retrieve data '$..FROM_ADDRESS' from object index '0' in response and store in variable 'FROM_ADDRESS'
	And Verify Value 'CustomerPortal' present in field '[0].INTEGRATION_NAME'
	And Verify Value 'StoreUpsert' present in field '[0].APPLICATION_NAME'
	
@emailQA
Scenario: MF Store Upsert Flow email distribution list when the job runs successfully
	Given EndPoint 'http://mule-worker-internal-sys-email-qa.us-w2.cloudhub.io:8091/mail/send-email-notification' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'StoreUpsert/EmailNotification' for key 'FROM_ADDRESS' with data '$$FROM_ADDRESS' 
	And Request body 'StoreUpsert/EmailNotification' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Email sent successfully to logeshkumar.r@royalcyber.com' present in field 'message' 
	And I wait for '40' seconds
	And I verify the email is received and read the body content
#	And I verify the Failure email is received and read the body content

@MF_StoreUpsertQA
Scenario:  MF Store Upsert Reset Timestamp allows us to resert the Last Rundate and Last Processed key
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/timestamp' 
	And Content type 'application/json' 
#	And Header key 'Content-Type' value 'application/json' 
	And Request body 'StoreUpsert/StoreTimestamp' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Record successfully updated in Service_Run Table.' present in field 'message' 
	
@MF_StoreUpsertQA
Scenario:  MF Store Insert Log Details allows us to insert the logs into the database
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/insert-log-details' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'StoreUpsert/InsertLogDetails' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Record inserted in LOG_DETAILS table.' present in field 'message' 

@MF_StoreUpsertQA
Scenario:  MF Store Fetch Service Run is to fetch the LastProcessedkey , Timestamp, LastRunDate ,lastExecDateTime ,lastExecTimeStamp from database
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/fetch-service-run' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'StoreUpsert/FetchService' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
#	And Retrive data '$.serviceRunRecord[0].lastRunDateTime' and store 'LastRunDateTime' as string 
#    And Retrieve data '(.*)' from response and store in variable '(.*)'$
	And Retrieve data '$.serviceRunRecord[0].lastRunDateTime' from response and store in variable 'LastRunDateTime'
	And Print Data 'serviceRunRecord[0].lastRunDateTime'
	And Retrieve data '$.serviceRunRecord[0].timestamp' from response and store in variable 'TimeStamp'
	And Print Data 'serviceRunRecord[0].timestamp'
	And Retrieve data '$.serviceRunRecord[0].lastProcessedKey' from response and store in variable 'LastProcessedKey'
    And Print Data 'serviceRunRecord[0].lastProcessedKey'

@MF_StoreUpsertQA
Scenario:  MF Store Elastic Search Index is to Fetch the datas that are need to be upserted in the Salesfore 
	Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/fetch-all-elasticsearch-index' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'StoreUpsert/ElasticSearchIndex' for key 'lastRunDate' with data '$$LastRunDateTime' 
	And Update data in the Request 'StoreUpsert/ElasticSearchIndex' for key 'lastProcessedKey' with data '$$LastProcessedKey' 
	And Update data in the Request 'StoreUpsert/ElasticSearchIndex' for key 'timest' with data '$$TimeStamp' 
	And Update data in the Request 'StoreUpsert/ElasticSearchIndex' for key 'esDeltaQuerySize' with data '2' 
	And Request body 'StoreUpsert/ElasticSearchIndex' 
	When Method 'Post'
	And Print response 
	And Statuscode '200'
	And Retrieve data '$.[1]ES_MODIFIEDDATETIME' from response and store in variable 'ModifiedTime'
	And Print Data 'ES_MODIFIEDDATETIME[1]' 
	And Retrieve data '$.[1]Id' from response and store in variable 'ModifiedID'
	And Print Data 'Id[1]' 
	And Statuscode '200' 
	And Key '$.[*]Id' count present in the response - '2'
	And Get the response for Elastic Search Index and store the keys
	
@MF_StoreUpsertQA
Scenario:  MF Store WarehouseList is to be a Get method gives all the warehouse datas that are available
	Given EndPoint 'http://mule-worker-internal-sys-salesforce-qa-v1.us-w2.cloudhub.io:8091/salesforce/warehouseList' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	When Method 'Get' 
	And Print response 
	And Statuscode '200' 
#	And Get Servicing DC Number from Elastic Search or warehouse list  //This step has been implemented in the previous scenario
	
@MF_StoreUpsertQA
Scenario:  MF Store SFCUpsert is to insert the data in the Salesforce with the help of "Elastic Search Index" and "WareHouse List"
	Given EndPoint 'http://mule-worker-internal-sys-salesforce-qa-v1.us-w2.cloudhub.io:8091/salesforce/sdfcustomerupsert' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	
	And Update the Store upsert data to SFDC Upsert
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Match JSONPath "$..number_records_processed" contains "2" 
	And Match JSONPath "$..number_records_failed" contains "0" 
	
@MF_StoreUpsertQA
Scenario: MF  StoreUpsert Timestamp is to Update the timestamp and Last Processed key in the database 
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/timestamp' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'StoreUpsert/StoreTimestamp' for key 'info[0].timestamp' with data '$$ModifiedTime' 
	And Update data in the Request 'StoreUpsert/StoreTimestamp' for key 'info[0].last_processedkey' with data '$$ModifiedID'
	And Request body 'StoreUpsert/StoreTimestamp' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Record successfully updated in Service_Run Table.' present in field 'message' 

Scenario: Store Upsert email Configuration that allows to setup Application Name ,Flow Name ,jobRunEmailDistroFlag ,Email Subject , Email message body content
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/emailconfig' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'StoreUpsert/EmailConfig' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Retrieve data '$..FROM_ADDRESS' from object index '0' in response and store in variable 'FROM_ADDRESS'
	And Verify Value 'CustomerPortal' present in field '[0].INTEGRATION_NAME'
	And Verify Value 'StoreUpsert' present in field '[0].APPLICATION_NAME'
	

Scenario: MF Store Upsert Flow email distribution list when the job runs successfully
	Given EndPoint 'http://mule-worker-internal-sys-email-qa.us-w2.cloudhub.io:8091/mail/send-email-notification' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'StoreUpsert/CompletedEmailNotification' for key 'FROM_ADDRESS' with data '$$FROM_ADDRESS' 
	And Request body 'StoreUpsert/CompletedEmailNotification' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Email sent successfully to logeshkumar.r@royalcyber.com' present in field 'message'
	And I wait for '30' seconds
And I verify the Proccess completed email is received and read the body content
		