@MF_Saba_GradedSim
Feature: In this Feature file we will be covering Saba Graded Simulation SFTP file download and cross check validation with MySQL stage and master DB tables

1.Connect to SFTP and download GradedSimulation CSV/Zip file with system date
2.Connect to MySQL DB and fetch query result from Stage and Master tables
3.Validations between SFTP CSV file and MySQL DB stage and master table results

Description: In this Feature file we will be covering Saba GradedSimulation SFTP file download and cross check validation with MySQL stage and master DB tables

Scenario: (Saba GradedSimulation) Connect to SFTP and download GradedSimulation CSV/Zip file then connect to MySQL DB and download query results and validate records between SFTP, SQL_Stage and SQL_Master tables
	#	Dynamically generate date as XD-1: Sysdate-1, XD+1: Sysdate+1, XDC: Current Sysdate
	Given fetch service run for the flow 'SabaGradedSimulationsNAPReports'
	Given I generate date 'XDC' in format 'yyyyMMdd' and store as 'SabaSFTPReportDate'
	Then Connect to SFTP with host 'sftpwt.sabahosted.com', port '22222', user name 'mattress' and password 'cTKEc6oPdB' then find 'Graded Simulations NAP' for the date '$$SabaSFTPReportDate' from directory '/mattress/mattress/qa/analytics_reports/dataextract' and download to 'F:\\Muthu\\Saba\\TestData\\GradedSimulations\\'
	Then Connect to MySQL DB with host 'jdbc:sqlserver://qdbsqltab01.mfrm.com:1433', user name 'Mule-QA', password 'test@123' and run the query 'SELECT * FROM [InternalProjects].[dbo].[SAB_GradedSimulationsNAP_STG]' and download query results to 'F:\\Muthu\\Saba\\TestData\\GradedSimulations\\SQLGradedSim_STG'
	Then assert SFTP CSV file '$$SFTPDownloadedfilePath' record count with SQL result CSV file '$$SQLResultFilePath' record count
	Then assert end to end data between source SFTP CSV file '$$SFTPDownloadedfilePath' and target SQL stage table result CSV file '$$SQLResultFilePath' for GradedSimulation
	And assert end to end data for '20' records between source as SQL stage table result CSV file '$$SQLResultFilePath' and target as SQL master table for Graded Simulation
#	And assert end to end data for all the records between source as SQL stage table result CSV file '$$SQLResultFilePath' and target as SQL master table for Curricula
	
	