@MF_Saba_Curricula
Feature: In this Feature file we will be covering Saba Curricula SFTP file download and cross check validation with MySQL stage and master DB tables
Feature: Saba Curricula - Source SFTP and Target is MySQL stage and master tables

1.Connect to SFTP and download Curricula CSV/Zip file with system date
2.Connect to MySQL DB and fetch query result from Stage and Master tables
3.Validations between SFTP CSV file and MySQL DB stage and master table results
4.Validations to make sure scheduler start and completed email notifications

Description: In this Feature file we will be covering Saba Curricula SFTP file download and cross check validation with MySQL stage and master DB tables
Scenario: (Saba Curricula) Connect to SFTP and download Curricula CSV/Zip file then connect to MySQL DB and download query results and validate records between SFTP, SQL_Stage and SQL_Master tables
	#	Dynamically generate date as XD-1: Sysdate-1, XD+1: Sysdate+1, XDC: Current Sysdate
	Given I generate date 'XDC' in format 'yyyyMMdd' and store as 'SabaSFTPReportDate'
	Then Connect to SFTP with host 'sftpwt.sabahosted.com', port '22222', user name 'mattress' and password 'cTKEc6oPdB' then find 'Curricula Report' for the date '$$SabaSFTPReportDate' from directory '/mattress/mattress/qa/analytics_reports/dataextract' and download to 'F:\\Manoj Thota\\Saba\\Test data_Curriculla\\'
	Then Connect to MySQL DB with host 'jdbc:sqlserver://qdbsqltab01.mfrm.com:1433', user name 'Mule-QA', password 'test@123' and run the query 'SELECT * FROM [InternalProjects].[dbo].[SAB_CurriculaReports_STG]' and download query results to 'F:\\Manoj Thota\\Saba\\Test data_Curriculla\\SQLCurriculla_STG'
	Then assert SFTP CSV file '$$SFTPDownloadedfilePath' record count with SQL result CSV file '$$SQLResultFilePath' record count
	Then assert end to end data between source SFTP CSV file '$$SFTPDownloadedfilePath' and target SQL stage table result CSV file '$$SQLResultFilePath' for Curricula
	And assert end to end data for '20' records between source as SQL stage table result CSV file '$$SQLResultFilePath' and target as SQL master table for Curricula
#	And assert end to end data for all the records between source as SQL stage table result CSV file '$$SQLResultFilePath' and target as SQL master table for Curricula
#	Then I validate email notification received with body contains 'CurriculaReports batch scheduler started' in '2' position of email inbox
#	Then I validate email notification received with body contains 'CurriculaReports batch scheduler completed' in '1' position of email inbox