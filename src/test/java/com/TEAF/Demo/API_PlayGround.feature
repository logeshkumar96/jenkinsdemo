@API_PlayGround_End-to-End
Feature: Verification of Product creation, modification and deleting the product by using RC TeAF FrameWork

Background: Create a new Product before each scenario and print the response
	Given I invoke 'CreateProductAPI' with 'Product_Details'

@Post
Scenario: Verifying Create Product API with product details as input and validating the API success response
#	Given I invoke 'CreateProductAPI' with 'Product_Details'
	Then  I get the response
	And   I will validate the response json Schema with 'Create_Product_Response_Schema'
	And   I will validate 'id' value exist in the response
	And   I will validate 'updatedAt' value exist in the response
	And   I will validate 'createdAt' value exist in the response
	And   I will validate the response with 'Product_Details'

@Get
Scenario: Verifying Get Product details API with product ID as input and validating the API response
#	Given I invoke 'CreateProductAPI' with 'Product_Details'
	Then  I invoke 'GetProductAPI' with 'RecentProductID'
	When  I get the response
	Then  I will validate the response json Schema with 'Get_Product_Response_Schema'
	And   I will validate 'id' value exist in the response
	And   I will validate the response with 'Product_Details'

@Patch
Scenario: Verifying Update Product details API with product details as input and validating the API response
#	Given I invoke 'CreateProductAPI' with 'Product_Details'
	Then  I invoke 'UpdateProductAPI' with 'Modified_Product_Details'
	When  I get the response
	Then  I will validate the response json Schema with 'Update_Product_Response_Schema'
	And   I will validate the response with 'Modified_Product_Details'
	
@Delete
Scenario: Verifying Delete Product API with product ID as input and validating the API response
#	Given I invoke 'CreateProductAPI' with 'Product_Details'
	Then  I invoke 'DeleteProductAPI' with 'RecentProductID'
	When  I get the response
	Then  I will validate the response json Schema with 'Delete_Product_Response_Schema'
	And   I will validate the response with 'Product_Details'
	Then  I invoke 'GetProductAPI' with 'RecentProductID'
	When  I get the response
	Then  I will validate the 'name' as 'NotFound' message in the response
	And   I will validate the response json Schema with 'Get_Deleted_Product_Response_Schema'
	