package com.TEAF.Demo.Resources.stepDefinitions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Properties;
import java.util.Set;

import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONTokener;
import org.junit.Assert;

import com.TEAF.Hooks.Hooks;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;

public class NewProduct {
	
	REST_methods RESTmethods  = new REST_methods();
	Response response;
	Properties properties = new Properties();
	String propfilepath = System.getProperty("user.dir")+"\\src\\test\\java\\com\\TEAF\\Demo\\Resources\\config\\properties.properties";
	String recentProductID;
	
//	public static Scenario scenario;
	
	@SuppressWarnings("unused")
	@Before
	public Properties getPropValues() throws IOException {

		FileInputStream fileinputstream = new FileInputStream(propfilepath);
		if (fileinputstream != null) {
			properties.load(fileinputstream);
			return properties;
		}else {
			throw new FileNotFoundException ("Property file not found");
		}
	}
	
//	@Before
//	public void create_product_API() throws IOException {
//		System.out.println("===============Initiate create product API Before hook===============");
//		response = RESTmethods.POST_Request("Product_Details");
//		System.out.println(response.prettyPeek());
//		recentProductID = response.getBody().jsonPath().getJsonObject("id").toString();
//	}
	
	@Given("^I invoke '(.*)' with '(.*)'$")
	public void I_invoke_API_with_input(String api, String variable) throws IOException {
		
		switch (api) {
		case "CreateProductAPI":
			System.out.println("===============Initiate Create product API===============");
			response = RESTmethods.POST_Request(variable);
//			System.out.println(response.getBody().prettyPeek());
			Hooks.scenario.write("CreateProductAPI Response : \n" + response.getBody().prettyPrint().toString());
			recentProductID = response.getBody().jsonPath().getJsonObject("id").toString();
			break;
	
		case "GetProductAPI":
			System.out.println("===============Initiate Get product API===============");
			response = RESTmethods.Get(recentProductID);
//			System.out.println(response.prettyPeek());
			Hooks.scenario.write("GetProductAPI Response : \n" + response.getBody().prettyPrint().toString());
			break;
			
		case "UpdateProductAPI":
			System.out.println("===============Initiate Update product API===============");
//			response = RESTmethods.Patch(variable, recentProductID);
			response = RESTmethods.Patch_Non_BDD(variable, recentProductID);
//			System.out.println(response.prettyPeek());
			Hooks.scenario.write("UpdateProductAPI Response : \n" + response.getBody().prettyPrint().toString());
			break;
			
		case "DeleteProductAPI":
			System.out.println("===============Initiate Delete product API===============");
			response = RESTmethods.Delete(recentProductID);
//			System.out.println(response.prettyPeek());
			Hooks.scenario.write("DeleteProductAPI Response : \n" + response.getBody().prettyPrint().toString());
			break;
		}
	}
	
	@When("^I get the response$")
	public void I_get_the_response() throws IOException {
		
		Assert.assertNotNull(response);
		
		String APIResponsejsonFilePath = System.getProperty("user.dir")+"\\src\\test\\java\\com\\TEAF\\Demo\\Resources\\jsonFiles\\APIResponse.json";
//		String APIResponsejsonFilePath = properties.getProperty("Filepath")+"APIResponse.json";
		
		File file = new File(APIResponsejsonFilePath);
        boolean Exists = file.exists();
        String APIResponse = response.prettyPeek().asString();
        if (Exists) {
               System.out.println("Printing response to the File");
               this.write(file, APIResponse);
        } else {
               file.createNewFile();
               System.out.println("New file created and Printing response to the File");
               this.write(file, APIResponse);
        }
   }

   private void write(File file, String APIResponse) {
       try {
           FileOutputStream fileOutputStream = new FileOutputStream(file);
           OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
           outputStreamWriter.write(APIResponse);
           outputStreamWriter.close();
           fileOutputStream.close();
       } catch (FileNotFoundException e) {
           System.out.println("File not found");
       } catch (IOException e) {
           System.out.println("Error initializing stream");
       }
	}
	
	@Then("^I will validate '(.*)' value exist in the response$")
	public void I_will_validate_value_exist_in_the_response(String key) {
		
		String actualvalue = response.getBody().jsonPath().getJsonObject(key).toString();
		if (actualvalue.equals("")) {
			String expected = key+" value should not be blank";
			Assert.assertEquals(actualvalue, expected);
		}else if (actualvalue.equals(null)) {
			String expected = key+" value is null in the response";
			Assert.assertNotNull(actualvalue, expected);
		}
	}
	
	@And("^I will validate the response with '(.*)'$")
	public void I_will_validate_the_response_with_jsonFileName(String jsonFileName) {
		System.out.println("===============Initiate End-to-End Data Assertions===============");
		String jsonFilePath = System.getProperty("user.dir")+"\\src\\test\\java\\com\\TEAF\\Demo\\Resources\\jsonFiles\\"+jsonFileName+".json";
//		String jsonFilePath = properties.getProperty("Filepath")+jsonFileName+".json";
//		JSONParser jsonParser = new JSONParser();
//		JSONObject jsonobject = new JSONObject();
        
//        try (FileReader reader = new FileReader(jsonFilePath))
		try (FileInputStream inputStream1 = new FileInputStream(jsonFilePath))
        {
//        	jsonobject = (JSONObject) jsonParser.parse(reader);
        	org.json.JSONObject jsonobject = new org.json.JSONObject(new JSONTokener(inputStream1));
 
        
        
        Set<String> keys = jsonobject.keySet();
        String[] jsonFileKeys = keys.toString().replace("[",  "").replace("]",  "").replaceAll("\\s",  "").split(",");
        
//        for (String i: jsonFileKeys) {
//        	System.out.println("jsonFileKeys: "+i);
//        	System.out.println("============="+jsonobject.get(i));
//        }
        
//        System.out.println("&&&&&&&&&&"+jsonobject.values());
//		  System.out.println("^^^^^^^^^^"+jsonobject.keySet());
		
		for (String i: jsonFileKeys) {
			String jsonFilekey = i;
//			System.out.println("#######"+jsonFilekey);
			String apiResponseValue = response.getBody().jsonPath().getJsonObject(jsonFilekey).toString();
			String jsonFileValue = jsonobject.get(i).toString();
			System.out.println("JSON file data for "+jsonFilekey+" : "+jsonFileValue);
			System.out.println("API response data for "+jsonFilekey+" : "+apiResponseValue);
//			System.out.println("jsonValue------"+jsonFileValue);
//			System.out.println("responseValue---------"+apiResponseValue);
			Assert.assertEquals(jsonFileValue, apiResponseValue);
		}
		
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } 
//		catch (ParseException e) {
//            e.printStackTrace();
//        }
	}
	
	@Then("^I will validate the '(.*)' as '(.*)' message in the response$")
	public void I_will_validate_the_key_as_value_message_in_the_response(String key, String expectedValue) {
		
		String actualValue = response.getBody().jsonPath().getJsonObject(key).toString();
		Assert.assertEquals(actualValue, expectedValue);
	}
	
	@Then("^I will validate the response json Schema with '(.*)'$")
	public void I_will_validate_the_response_json_Schema_with_Response_Schema(String jsonFileName) {
		
		System.out.println("===============Initiate JSON schema validation===============");
		String jsonFilePath = System.getProperty("user.dir")+"\\src\\test\\java\\com\\TEAF\\Demo\\Resources\\jsonFiles\\"+jsonFileName+".json";
//		String jsonFilePath = properties.getProperty("Filepath")+jsonFileName+".json";

		try (FileInputStream inputStream = new FileInputStream(jsonFilePath)) {

			  org.json.JSONObject rawSchema = new org.json.JSONObject(new JSONTokener(inputStream));
//			  System.out.println("^^^^^^^^^"+rawSchema.toString());
			  Schema schema = SchemaLoader.load(rawSchema);
//			  System.out.println("$$$$$$$"+schema.toString());
			  
//			  JSONObject responseSchema = new JSONObject(new JSONTokener(response.asInputStream()));
//			  System.out.println("########"+responseSchema.toString());
//			  Schema schema1 = SchemaLoader.load(responseSchema);
//			  System.out.println("!!!!!!!!!!"+schema1.toString());
			  
//			  String apiResponse = response.getBody().asString();
			  
			  String APIjsonFilePath = System.getProperty("user.dir")+"\\src\\test\\java\\com\\TEAF\\Demo\\Resources\\jsonFiles\\APIResponse.json";
//			  String APIjsonFilePath = properties.getProperty("Filepath")+"APIResponse.json";
			  
			  FileInputStream inputStream1 = new FileInputStream(APIjsonFilePath);
			  
//			  JSONParser jsonParser = new JSONParser();
			  org.json.JSONObject jsonobject = new org.json.JSONObject(new JSONTokener(inputStream1));
//			  FileReader reader = new FileReader(APIjsonFilePath);
//			  jsonobject = (org.json.JSONObject) jsonParser.parse(reader);
//			  org.json.simple.JSONObject jsonResponse = (org.json.simple.JSONObject) jsonParser.parse(apiResponse);
//			  System.out.println("@@@@@@@@"+jsonobject.toString());
			  schema.validate(jsonobject); // throws a ValidationException if this object is invalid
			  
			} catch (FileNotFoundException e) {
	            e.printStackTrace();
		    } catch (IOException e) {
		        e.printStackTrace();
		    } 
//		catch (ParseException e) {
//		        e.printStackTrace();
//		    }
	
		
		
//		JSONParser jsonParser = new JSONParser();
//		org.json.JSONObject jsonobject = new org.json.JSONObject();
//        
//        try (FileReader reader = new FileReader(jsonFilePath))
//        {
//        	jsonobject = (org.json.JSONObject) jsonParser.parse(reader);
// 
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        
//        Schema schema = SchemaLoader.load(jsonobject);
//        schema.validate(jsonobject);
	}

}
