package com.TEAF.Demo.Resources.stepDefinitions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.json.JSONTokener;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class REST_methods {
	
	Properties prop = new Properties();
	String propfilepath = System.getProperty("user.dir")+"\\src\\test\\java\\com\\TEAF\\Demo\\Resources\\config\\properties.properties";
	Response response;
	@SuppressWarnings("unused")
	public Properties getPropValues() throws IOException {
	
	FileInputStream f = new FileInputStream(propfilepath);
	if (f != null) {
		prop.load(f);
		return prop;
		}else {
			throw new FileNotFoundException ("Property file not found");
		}
	}
	
	@Given("Create new Product using POST request")
	public Response POST_Request(String jsonFileName) throws IOException {
		
			getPropValues();
			String jsonFilePath = System.getProperty("user.dir")+"\\src\\test\\java\\com\\TEAF\\Demo\\Resources\\jsonFiles\\"+jsonFileName+".json";
//			String jsonFilePath = prop.getProperty("Filepath")+jsonFileName+".json";
			File inputfile=new File(jsonFilePath);
			response = (Response) given()
			.headers(prop.getProperty("Header-Type"), prop.getProperty("Header-Type-Value"))
			.body(inputfile)
			.when()
			.post(prop.getProperty("Products-URL"));
//			.getBody().prettyPeek()
//			.then()
//			.assertThat()
//			.body("upc", is("9898787848"));
			
//			String responseUpc = response.getBody().jsonPath().getJsonObject("upc").toString();
			
//			Assert.assertEquals(responseUpc, prop.getProperty("Expected-UPC"));
			
//			System.out.println(response.prettyPeek());
			
			return response;
		}
	
	@Then("Get newly created Product details")
	public Response Get(String recentProductID) throws IOException {
		
		getPropValues();
		String productIDPath = prop.getProperty("Products-URL")+"/"+recentProductID;
		response = (Response) given()
		.when()
		.get(productIDPath);
//		.then()
//		.assertThat()
//		.body("data[1].type", is("Hard 1 Good"));
		
		return response;
		}

	@And("Update the new product record using PUT method")
	public Response Patch(String jsonFileName, String recentProductID) throws IOException
	{ 
		getPropValues();
		String jsonFilePath = System.getProperty("user.dir")+"\\src\\test\\java\\com\\TEAF\\Demo\\Resources\\jsonFiles\\"+jsonFileName+".json";
//		String jsonFilePath = prop.getProperty("Filepath")+jsonFileName+".json";
//		File inputfile=new File(jsonFilePath);
		String productIDPath = prop.getProperty("Products-URL")+"/"+recentProductID;
		
		try (FileInputStream inputStream1 = new FileInputStream(jsonFilePath))
        {
//        	jsonobject = (JSONObject) jsonParser.parse(reader);
        	org.json.JSONObject jsonobject = new org.json.JSONObject(new JSONTokener(inputStream1));
        	System.out.println("json file data"+jsonobject.toString());
//        }
		
		response = (Response) given()
		.body(inputStream1)
		.when()
		.patch(productIDPath);
//		.put(productIDPath);
//		.then()
//		.assertThat()
//		.body("className", is("bad-request"));
		
		return response;
        }
	}
	
	public Response Patch_Non_BDD(String jsonFileName, String recentProductID) throws IOException {
		RequestSpecification request = RestAssured.given();
		getPropValues();
		
//		String jsonString = "{\r\n" + 
//				"    \"type\": \"Updated Hard MTT Goods\",\r\n" + 
//				"    \"description\": \"This is updated new MT products.\"\r\n" + 
//				"}";
		String jsonFilePath = System.getProperty("user.dir")+"\\src\\test\\java\\com\\TEAF\\Demo\\Resources\\jsonFiles\\"+jsonFileName+".json";
//		String jsonFilePath = prop.getProperty("Filepath")+jsonFileName+".json";
		File inputfile=new File(jsonFilePath);
		
		String productIDPath = prop.getProperty("Products-URL")+"/"+recentProductID;
		
		request.contentType(ContentType.JSON);
		request.baseUri(productIDPath);
		request.body(inputfile);
		
		response = request.patch();
		
		return response;
	}

	@And("Delete the newly created Product")
	public Response Delete(String recentProductID)
	{ 
		String productIDPath = prop.getProperty("Products-URL")+"/"+recentProductID;
		response = (Response) given()
			.contentType("application/json")
			.when()
			.delete(productIDPath);
//			.then()
//			.assertThat()
//			.body("name", is("manoj"))
//			.body("id", is(9999978))
//			.body("upc", is("1587035463755"))
//			.body("model", is("NP12244"))
//			.body("createdAt", is("2020-04-16T11:11:03.891Z"));
		
		return response;
	}

}
