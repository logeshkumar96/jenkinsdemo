@Aislelabs_EOD_Feed 
Feature: Validation for the data load from visit's API to BQ table for Aislelabs EOD feed

Description:
1. Connect to SQL DB table and get unique API key's
2. Use the API key and trigger aislelabs auth API to get TDID's and store details
3. Use the TDID and API key to trigger aislelabs visits API to fetch complete 1 day records
4. Assert visits API response with 'aislelabs_30_min_interval_visits' BQ table

Scenario: Validation for the data load from visit's API to BQ table for Aislelabs EOD feed

	Given Connect to MySQL DB with host 'jdbc:sqlserver://qdbsqltab01.mfrm.com:1433', user name 'Mule-QA', password 'test@123' and run the query 'SELECT DISTINCT([API Key]) FROM [InternalProjects].[dbo].[ALAB_StoreXREF]'
	Then EndPoint 'https://api.aislelabs.com/api/rest/webuser/auth.json' 
	And Content type 'application/json'
	And Query params with Key "user_key" and value 'SQL_API_Keys'
	When Method 'GET'
	And Print response
	Then Statuscode '200'
	Then Retrieve data '$.response.tdidList[*].tdid' from response and store in list variable 'TDIDs'
	Then Retrieve data '$.response.tdidList[*].tdidName' from response and store in list variable 'TDIDnames'
	#	Dynamically generate date as XD-1: Sysdate-1, XD+1: Sysdate+1, XDC: Current Sysdate
	Then I generate date 'XD-1' in format 'yyyy-MM-dd' and store as 'startDateAislelabsEOD'
	And I generate date 'XDC' in format 'yyyy-MM-dd' and store as 'endDateAislelabsEOD'
	Then EndPoint 'https://api.aislelabs.com/api/rest/site/visits.json' 
	And Content type 'application/json'
	And Query params with Key "user_key" and value 'SQL_API_Keys'
	And Query params with Key "granularity" and value 'MINUTELY30'
	And Query params with Key "usePrecomputed" and value 'true'
	And Query params with Key "tdid" and value 'TDIDs'
	And Query params with Key "startDateTimeUTC" and value 'startDateAislelabsEOD'
	And Query params with Key "endDateTimeUTC" and value 'endDateAislelabsEOD'
	When Method 'GET'
	And Print response
	Then Statuscode '200'
	Then capture visitTrend, trendTimeBucketsUserTimezone, siteEngagedDwellMins and siteDwellTrendMins from visits API response
	Then Connect to BigQuery with project ID 'qa-mfrm-data'
	Then assert visits API response with aislelabs_30_min_interval_visits BQ table content
