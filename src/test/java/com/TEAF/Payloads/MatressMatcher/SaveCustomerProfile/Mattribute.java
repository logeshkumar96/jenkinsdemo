package com.TEAF.Payloads.MatressMatcher.SaveCustomerProfile;

import java.util.HashMap;
import java.util.Map;

public class Mattribute {

private String comfort;
private String craftsmanship;
private String motionSeparation;
private String pressureRelief;
private String sleepsCool;
private String support;
private String pricingTier;

public String getComfort() {
return comfort;
}

public void setComfort(String comfort) {
this.comfort = comfort;
}

public String getCraftsmanship() {
return craftsmanship;
}

public void setCraftsmanship(String craftsmanship) {
this.craftsmanship = craftsmanship;
}

public String getMotionSeparation() {
return motionSeparation;
}

public void setMotionSeparation(String motionSeparation) {
this.motionSeparation = motionSeparation;
}

public String getPressureRelief() {
return pressureRelief;
}

public void setPressureRelief(String pressureRelief) {
this.pressureRelief = pressureRelief;
}

public String getSleepsCool() {
return sleepsCool;
}

public void setSleepsCool(String sleepsCool) {
this.sleepsCool = sleepsCool;
}

public String getSupport() {
return support;
}

public void setSupport(String support) {
this.support = support;
}

public String getPricingTier() {
return pricingTier;
}

public void setPricingTier(String pricingTier) {
this.pricingTier = pricingTier;
}


	@Override
	public String toString() {
		return "Mattribute [comfort=" + comfort + ", craftsmanship=" + craftsmanship + ", motionSeparation="
				+ motionSeparation + ", pressureRelief=" + pressureRelief + ", sleepsCool=" + sleepsCool + ", support="
				+ support + ", pricingTier=" + pricingTier +  "]";
	}

}
