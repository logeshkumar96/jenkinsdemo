package com.TEAF.Payloads.MatressMatcher.SaveCustomerProfile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SaveCustomerProfile {

	
	private String storeCode;
	private String emailAddress;
	private Boolean isEmailRequestGenerated;
	private String sleepProfile;
	private String customerResponses;
	private String partnerResponses;
	private String sleepId;
	private Long bmi;
	private String topNMatchedProducts;
	private Boolean emailOptionIncluded;
	private String emailResponseOnError;
	private String couponCode;
	private String tealiumVisitorID;
	private String googleClientID;
	private String tealiumSessionID;
	private String mattressSize;
	private String name2;
	private String emailAddress2;
	private List<RecommendedProduct> recommendedProducts = null;

	public String getStoreCode() {
	return storeCode;
	}

	public void setStoreCode(String storeCode) {
	this.storeCode = storeCode;
	}

	public String getEmailAddress() {
	return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
	this.emailAddress = emailAddress;
	}

	public Boolean getIsEmailRequestGenerated() {
	return isEmailRequestGenerated;
	}

	public void setIsEmailRequestGenerated(Boolean isEmailRequestGenerated) {
	this.isEmailRequestGenerated = isEmailRequestGenerated;
	}

	public String getSleepProfile() {
	return sleepProfile;
	}

	public void setSleepProfile(String sleepProfile) {
	this.sleepProfile = sleepProfile;
	}

	public String getCustomerResponses() {
	return customerResponses;
	}

	public void setCustomerResponses(String customerResponses) {
	this.customerResponses = customerResponses;
	}

	public String getPartnerResponses() {
	return partnerResponses;
	}

	public void setPartnerResponses(String partnerResponses) {
	this.partnerResponses = partnerResponses;
	}

	public String getSleepId() {
	return sleepId;
	}

	public void setSleepId(String sleepId) {
	this.sleepId = sleepId;
	}

	public Long getBmi() {
	return bmi;
	}

	public void setBmi(Long bmi) {
	this.bmi = bmi;
	}

	public String getTopNMatchedProducts() {
	return topNMatchedProducts;
	}

	public void setTopNMatchedProducts(String topNMatchedProducts) {
	this.topNMatchedProducts = topNMatchedProducts;
	}

	public Boolean getEmailOptionIncluded() {
	return emailOptionIncluded;
	}

	public void setEmailOptionIncluded(Boolean emailOptionIncluded) {
	this.emailOptionIncluded = emailOptionIncluded;
	}

	public String getEmailResponseOnError() {
	return emailResponseOnError;
	}

	public void setEmailResponseOnError(String emailResponseOnError) {
	this.emailResponseOnError = emailResponseOnError;
	}

	public String getCouponCode() {
	return couponCode;
	}

	public void setCouponCode(String couponCode) {
	this.couponCode = couponCode;
	}

	public String getTealiumVisitorID() {
	return tealiumVisitorID;
	}

	public void setTealiumVisitorID(String tealiumVisitorID) {
	this.tealiumVisitorID = tealiumVisitorID;
	}

	public String getGoogleClientID() {
	return googleClientID;
	}

	public void setGoogleClientID(String googleClientID) {
	this.googleClientID = googleClientID;
	}

	public String getTealiumSessionID() {
	return tealiumSessionID;
	}

	public void setTealiumSessionID(String tealiumSessionID) {
	this.tealiumSessionID = tealiumSessionID;
	}

	public String getMattressSize() {
	return mattressSize;
	}

	public void setMattressSize(String mattressSize) {
	this.mattressSize = mattressSize;
	}

	public String getName2() {
	return name2;
	}

	public void setName2(String name2) {
	this.name2 = name2;
	}

	public String getEmailAddress2() {
	return emailAddress2;
	}

	public void setEmailAddress2(String emailAddress2) {
	this.emailAddress2 = emailAddress2;
	}

	public List<RecommendedProduct> getRecommendedProducts() {
	return recommendedProducts;
	}

	public void setRecommendedProducts(List<RecommendedProduct> recommendedProducts) {
	this.recommendedProducts = recommendedProducts;
	}

	@Override
	public String toString() {
		return "SaveCustomerProfile [storeCode=" + storeCode + ", emailAddress=" + emailAddress
				+ ", isEmailRequestGenerated=" + isEmailRequestGenerated + ", sleepProfile=" + sleepProfile
				+ ", customerResponses=" + customerResponses + ", partnerResponses=" + partnerResponses + ", sleepId="
				+ sleepId + ", bmi=" + bmi + ", topNMatchedProducts=" + topNMatchedProducts + ", emailOptionIncluded="
				+ emailOptionIncluded + ", emailResponseOnError=" + emailResponseOnError + ", couponCode=" + couponCode
				+ ", tealiumVisitorID=" + tealiumVisitorID + ", googleClientID=" + googleClientID
				+ ", tealiumSessionID=" + tealiumSessionID + ", mattressSize=" + mattressSize + ", name2=" + name2
				+ ", emailAddress2=" + emailAddress2 + ", recommendedProducts=" + recommendedProducts + "]";
	}
		

		}