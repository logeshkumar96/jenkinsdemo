package com.TEAF.Payloads.MatressMatcher.SaveCustomerProfile;

import java.util.HashMap;
import java.util.Map;

public class RecommendedProduct {

	

	private Long percentageMatch;
	private Long productCatalogId;
	private String productDetailPage;
	private String productName;
	private Double bvAvgRating;
	private Long bvRatingRange;
	private Double bvReviewCount;
	private String isAvailableToTryInStore;
	private String tryInStoreName;
	private String isParcelable;
	private String shippingInformation;
	private String adjustableBaseSuitable;
	private String mattressConstruction;
	private String mattressHeight;
	private String manufacturerName;
	private String collectionName;
	private String sleepPosition;
	private Double queenOLPPrice;
	private Double twinOLPPrice;
	private Double twinXLOLPPrice;
	private Double fullOLPPrice;
	private Double kingOLPPrice;
	private Double calKingOLPPrice;
	private Double queenSalesPrice;
	private Double twinSalesPrice;
	private Double fullSalesPrice;
	private Double kingSalesPrice;
	private Double calKingSalesPrice;
	private Double twinXLSalesPrice;
	private Long rank;
	private Long sku;
	private Mattribute mattributes;

	public Long getPercentageMatch() {
	return percentageMatch;
	}

	public void setPercentageMatch(Long percentageMatch) {
	this.percentageMatch = percentageMatch;
	}

	public Long getProductCatalogId() {
	return productCatalogId;
	}

	public void setProductCatalogId(Long productCatalogId) {
	this.productCatalogId = productCatalogId;
	}

	public String getProductDetailPage() {
	return productDetailPage;
	}

	public void setProductDetailPage(String productDetailPage) {
	this.productDetailPage = productDetailPage;
	}

	public String getProductName() {
	return productName;
	}

	public void setProductName(String productName) {
	this.productName = productName;
	}

	public Double getBvAvgRating() {
	return bvAvgRating;
	}

	public void setBvAvgRating(Double bvAvgRating) {
	this.bvAvgRating = bvAvgRating;
	}

	public Long getBvRatingRange() {
	return bvRatingRange;
	}

	public void setBvRatingRange(Long bvRatingRange) {
	this.bvRatingRange = bvRatingRange;
	}

	public Double getBvReviewCount() {
	return bvReviewCount;
	}

	public void setBvReviewCount(Double bvReviewCount) {
	this.bvReviewCount = bvReviewCount;
	}

	public String getIsAvailableToTryInStore() {
	return isAvailableToTryInStore;
	}

	public void setIsAvailableToTryInStore(String isAvailableToTryInStore) {
	this.isAvailableToTryInStore = isAvailableToTryInStore;
	}

	public String getTryInStoreName() {
	return tryInStoreName;
	}

	public void setTryInStoreName(String tryInStoreName) {
	this.tryInStoreName = tryInStoreName;
	}

	public String getIsParcelable() {
	return isParcelable;
	}

	public void setIsParcelable(String isParcelable) {
	this.isParcelable = isParcelable;
	}

	public String getShippingInformation() {
	return shippingInformation;
	}

	public void setShippingInformation(String shippingInformation) {
	this.shippingInformation = shippingInformation;
	}

	public String getAdjustableBaseSuitable() {
	return adjustableBaseSuitable;
	}

	public void setAdjustableBaseSuitable(String adjustableBaseSuitable) {
	this.adjustableBaseSuitable = adjustableBaseSuitable;
	}

	public String getMattressConstruction() {
	return mattressConstruction;
	}

	public void setMattressConstruction(String mattressConstruction) {
	this.mattressConstruction = mattressConstruction;
	}

	public String getMattressHeight() {
	return mattressHeight;
	}

	public void setMattressHeight(String mattressHeight) {
	this.mattressHeight = mattressHeight;
	}

	public String getManufacturerName() {
	return manufacturerName;
	}

	public void setManufacturerName(String manufacturerName) {
	this.manufacturerName = manufacturerName;
	}

	public String getCollectionName() {
	return collectionName;
	}

	public void setCollectionName(String collectionName) {
	this.collectionName = collectionName;
	}

	public String getSleepPosition() {
	return sleepPosition;
	}

	public void setSleepPosition(String sleepPosition) {
	this.sleepPosition = sleepPosition;
	}

	public Double getQueenOLPPrice() {
	return queenOLPPrice;
	}

	public void setQueenOLPPrice(Double queenOLPPrice) {
	this.queenOLPPrice = queenOLPPrice;
	}

	public Double getTwinOLPPrice() {
	return twinOLPPrice;
	}

	public void setTwinOLPPrice(Double twinOLPPrice) {
	this.twinOLPPrice = twinOLPPrice;
	}

	public Double getTwinXLOLPPrice() {
	return twinXLOLPPrice;
	}

	public void setTwinXLOLPPrice(Double twinXLOLPPrice) {
	this.twinXLOLPPrice = twinXLOLPPrice;
	}

	public Double getFullOLPPrice() {
	return fullOLPPrice;
	}

	public void setFullOLPPrice(Double fullOLPPrice) {
	this.fullOLPPrice = fullOLPPrice;
	}

	public Double getKingOLPPrice() {
	return kingOLPPrice;
	}

	public void setKingOLPPrice(Double kingOLPPrice) {
	this.kingOLPPrice = kingOLPPrice;
	}

	public Double getCalKingOLPPrice() {
	return calKingOLPPrice;
	}

	public void setCalKingOLPPrice(Double calKingOLPPrice) {
	this.calKingOLPPrice = calKingOLPPrice;
	}

	public Double getQueenSalesPrice() {
	return queenSalesPrice;
	}

	public void setQueenSalesPrice(Double queenSalesPrice) {
	this.queenSalesPrice = queenSalesPrice;
	}

	public Double getTwinSalesPrice() {
	return twinSalesPrice;
	}

	public void setTwinSalesPrice(Double twinSalesPrice) {
	this.twinSalesPrice = twinSalesPrice;
	}

	public Double getFullSalesPrice() {
	return fullSalesPrice;
	}

	public void setFullSalesPrice(Double fullSalesPrice) {
	this.fullSalesPrice = fullSalesPrice;
	}

	public Double getKingSalesPrice() {
	return kingSalesPrice;
	}

	public void setKingSalesPrice(Double kingSalesPrice) {
	this.kingSalesPrice = kingSalesPrice;
	}

	public Double getCalKingSalesPrice() {
	return calKingSalesPrice;
	}

	public void setCalKingSalesPrice(Double calKingSalesPrice) {
	this.calKingSalesPrice = calKingSalesPrice;
	}

	public Double getTwinXLSalesPrice() {
	return twinXLSalesPrice;
	}

	public void setTwinXLSalesPrice(Double twinXLSalesPrice) {
	this.twinXLSalesPrice = twinXLSalesPrice;
	}

	public Long getRank() {
	return rank;
	}

	public void setRank(Long rank) {
	this.rank = rank;
	}

	public Long getSku() {
	return sku;
	}

	public void setSku(Long sku) {
	this.sku = sku;
	}

	public Mattribute getMattributes() {
	return mattributes;
	}

	public void setMattributes(Mattribute mattributes) {
	this.mattributes = mattributes;
	}

	@Override
	public String toString() {
		return "RecommendedProduct [percentageMatch=" + percentageMatch + ", productCatalogId=" + productCatalogId
				+ ", productDetailPage=" + productDetailPage + ", productName=" + productName + ", bvAvgRating="
				+ bvAvgRating + ", bvRatingRange=" + bvRatingRange + ", bvReviewCount=" + bvReviewCount
				+ ", isAvailableToTryInStore=" + isAvailableToTryInStore + ", tryInStoreName=" + tryInStoreName
				+ ", isParcelable=" + isParcelable + ", shippingInformation=" + shippingInformation
				+ ", adjustableBaseSuitable=" + adjustableBaseSuitable + ", mattressConstruction="
				+ mattressConstruction + ", mattressHeight=" + mattressHeight + ", manufacturerName=" + manufacturerName
				+ ", collectionName=" + collectionName + ", sleepPosition=" + sleepPosition + ", queenOLPPrice="
				+ queenOLPPrice + ", twinOLPPrice=" + twinOLPPrice + ", twinXLOLPPrice=" + twinXLOLPPrice
				+ ", fullOLPPrice=" + fullOLPPrice + ", kingOLPPrice=" + kingOLPPrice + ", calKingOLPPrice="
				+ calKingOLPPrice + ", queenSalesPrice=" + queenSalesPrice + ", twinSalesPrice=" + twinSalesPrice
				+ ", fullSalesPrice=" + fullSalesPrice + ", kingSalesPrice=" + kingSalesPrice + ", calKingSalesPrice="
				+ calKingSalesPrice + ", twinXLSalesPrice=" + twinXLSalesPrice + ", rank=" + rank + ", sku=" + sku
				+ ", mattributes=" + mattributes + "]";
	}

		

		}

