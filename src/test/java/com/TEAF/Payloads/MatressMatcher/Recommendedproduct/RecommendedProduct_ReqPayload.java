package com.TEAF.Payloads.MatressMatcher.Recommendedproduct;

public class RecommendedProduct_ReqPayload {

private String storecode;
private String storezipcode;
private String customerzipcode;
private String warehouseid;
private String tealiumVisitorID;
private String googleClientId;
private String tealiumSessionID;
private String budgetValue;
private String mattressSize;
private int support;
private int pressureRelief;
private int comfort;
private int motionSeparation;
private int sleepsCool;
private String customerResponses;
private String partnerResponses;

public RecommendedProduct_ReqPayload(){
	
}

public String getStorecode() {
return storecode;
}

public void setStorecode(String storecode) {
this.storecode = storecode;
}

public String getStorezipcode() {
return storezipcode;
}

public void setStorezipcode(String storezipcode) {
this.storezipcode = storezipcode;
}

public String getCustomerzipcode() {
return customerzipcode;
}

public void setCustomerzipcode(String customerzipcode) {
this.customerzipcode = customerzipcode;
}

public String getWarehouseid() {
return warehouseid;
}

public void setWarehouseid(String warehouseid) {
this.warehouseid = warehouseid;
}

public String getTealiumVisitorID() {
return tealiumVisitorID;
}

public void setTealiumVisitorID(String tealiumVisitorID) {
this.tealiumVisitorID = tealiumVisitorID;
}

public String getGoogleClientId() {
return googleClientId;
}

public void setGoogleClientId(String googleClientId) {
this.googleClientId = googleClientId;
}

public String getTealiumSessionID() {
return tealiumSessionID;
}

public void setTealiumSessionID(String tealiumSessionID) {
this.tealiumSessionID = tealiumSessionID;
}

public String getBudgetValue() {
return budgetValue;
}

public void setBudgetValue(String budgetValue) {
this.budgetValue = budgetValue;
}

public String getMattressSize() {
return mattressSize;
}

public void setMattressSize(String mattressSize) {
this.mattressSize = mattressSize;
}

public int getSupport() {
return support;
}

public void setSupport(int support) {
this.support = support;
}

public int getPressureRelief() {
return pressureRelief;
}

public void setPressureRelief(int pressureRelief) {
this.pressureRelief = pressureRelief;
}

public int getComfort() {
return comfort;
}

public void setComfort(int comfort) {
this.comfort = comfort;
}

public int getMotionSeparation() {
return motionSeparation;
}

public void setMotionSeparation(int motionSeparation) {
this.motionSeparation = motionSeparation;
}

public int getSleepsCool() {
return sleepsCool;
}

public void setSleepsCool(int sleepsCool) {
this.sleepsCool = sleepsCool;
}

public String getCustomerResponses() {
return customerResponses;
}

public void setCustomerResponses(String customerResponses) {
this.customerResponses = customerResponses;
}

public String getPartnerResponses() {
return partnerResponses;
}

public void setPartnerResponses(String partnerResponses) {
this.partnerResponses = partnerResponses;
}

}