package com.Resources;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import net.masterthought.cucumber.Reportable;
import net.masterthought.cucumber.json.support.Status;
import net.masterthought.cucumber.presentation.PresentationMode;

public class GenerateJVMReport {
public static void main(String[] args) {
	getHTMLReport();
}
	public static void getHTMLReport() {
		File reportOutputDirectory = new File("target");
		List<String> jsonFiles = new ArrayList();
		jsonFiles.add(
				"C:\\Users\\User\\Downloads\\PremRC-matressfirm_testautomation-668ce572de29\\src\\test\\java\\com\\TestResults\\cucumber-report\\cucumber_1.json");
		String buildNumber = "1";
		String projectName = "cucumberProject";
		Configuration configuration = new Configuration(reportOutputDirectory, projectName);
		configuration.setBuildNumber(buildNumber);
		configuration.addClassifications("Platform", "Windows");
		ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
		Reportable result = reportBuilder.generateReports();
	}
}
