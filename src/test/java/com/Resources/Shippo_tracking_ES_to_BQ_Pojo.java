package com.Resources;

public class Shippo_tracking_ES_to_BQ_Pojo {

	String tracking_number;
	String carrier;
	String service_level_token;
	String service_level_name;
	String address_from_city;
	String address_from_state;
	String address_from_zip;
	String address_from_country;
	String address_to_city;
	String address_to_state;
	String address_to_zip;
	String address_to_country;
	String eta;
	String original_eta;
	String status_date;
	String status_details;
	String location_city;
	String location_state;
	String location_zip;
	String location_country;
	String substatus_code;
	String substatus_text;
	Boolean substatus_action_required;
	String object_created_date;
	String object_updated_date;
	String object_id;
	String status;
	String createdBy;
	String modifiedBy;
	String ES_MODIFIEDDATETIME;
	String createdDateTime;
	String modifiedDateTime;
	
	public String getTracking_number() {
		return tracking_number;
	}
	public void setTracking_number(String tracking_number) {
		this.tracking_number = tracking_number;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getService_level_token() {
		return service_level_token;
	}
	public void setService_level_token(String service_level_token) {
		this.service_level_token = service_level_token;
	}
	public String getService_level_name() {
		return service_level_name;
	}
	public void setService_level_name(String service_level_name) {
		this.service_level_name = service_level_name;
	}
	public String getAddress_from_city() {
		return address_from_city;
	}
	public void setAddress_from_city(String address_from_city) {
		this.address_from_city = address_from_city;
	}
	public String getAddress_from_state() {
		return address_from_state;
	}
	public void setAddress_from_state(String address_from_state) {
		this.address_from_state = address_from_state;
	}
	public String getAddress_from_zip() {
		return address_from_zip;
	}
	public void setAddress_from_zip(String address_from_zip) {
		this.address_from_zip = address_from_zip;
	}
	public String getAddress_from_country() {
		return address_from_country;
	}
	public void setAddress_from_country(String address_from_country) {
		this.address_from_country = address_from_country;
	}
	public String getAddress_to_city() {
		return address_to_city;
	}
	public void setAddress_to_city(String address_to_city) {
		this.address_to_city = address_to_city;
	}
	public String getAddress_to_state() {
		return address_to_state;
	}
	public void setAddress_to_state(String address_to_state) {
		this.address_to_state = address_to_state;
	}
	public String getAddress_to_zip() {
		return address_to_zip;
	}
	public void setAddress_to_zip(String address_to_zip) {
		this.address_to_zip = address_to_zip;
	}
	public String getAddress_to_country() {
		return address_to_country;
	}
	public void setAddress_to_country(String address_to_country) {
		this.address_to_country = address_to_country;
	}
	public String getEta() {
		return eta;
	}
	public void setEta(String eta) {
		this.eta = eta;
	}
	public String getOriginal_eta() {
		return original_eta;
	}
	public void setOriginal_eta(String original_eta) {
		this.original_eta = original_eta;
	}
	public String getStatus_date() {
		return status_date;
	}
	public void setStatus_date(String status_date) {
		this.status_date = status_date;
	}
	public String getStatus_details() {
		return status_details;
	}
	public void setStatus_details(String status_details) {
		this.status_details = status_details;
	}
	public String getLocation_city() {
		return location_city;
	}
	public void setLocation_city(String location_city) {
		this.location_city = location_city;
	}
	public String getLocation_state() {
		return location_state;
	}
	public void setLocation_state(String location_state) {
		this.location_state = location_state;
	}
	public String getLocation_zip() {
		return location_zip;
	}
	public void setLocation_zip(String location_zip) {
		this.location_zip = location_zip;
	}
	public String getLocation_country() {
		return location_country;
	}
	public void setLocation_country(String location_country) {
		this.location_country = location_country;
	}
	public String getSubstatus_code() {
		return substatus_code;
	}
	public void setSubstatus_code(String substatus_code) {
		this.substatus_code = substatus_code;
	}
	public String getSubstatus_text() {
		return substatus_text;
	}
	public void setSubstatus_text(String substatus_text) {
		this.substatus_text = substatus_text;
	}
	public Boolean getSubstatus_action_required() {
		return substatus_action_required;
	}
	public void setSubstatus_action_required(Boolean substatus_action_required) {
		this.substatus_action_required = substatus_action_required;
	}
	public String getObject_created_date() {
		return object_created_date;
	}
	public void setObject_created_date(String object_created_date) {
		this.object_created_date = object_created_date;
	}
	public String getObject_updated_date() {
		return object_updated_date;
	}
	public void setObject_updated_date(String object_updated_date) {
		this.object_updated_date = object_updated_date;
	}
	public String getObject_id() {
		return object_id;
	}
	public void setObject_id(String object_id) {
		this.object_id = object_id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getES_MODIFIEDDATETIME() {
		return ES_MODIFIEDDATETIME;
	}
	public void setES_MODIFIEDDATETIME(String eS_MODIFIEDDATETIME) {
		ES_MODIFIEDDATETIME = eS_MODIFIEDDATETIME;
	}
	public String getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(String createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getModifiedDateTime() {
		return modifiedDateTime;
	}
	public void setModifiedDateTime(String modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}
}
