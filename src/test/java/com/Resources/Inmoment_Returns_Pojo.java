package com.Resources;

public class Inmoment_Returns_Pojo {

	String Date_of_Service;
	String Return_Order_Number;
	String Store_hash;
	String Location;
	String Reason_for_Return;
	String Area_of_Discomfort;
	String How_Damaged;
	String Damaged_Mentioned_at_Delivery;
	String Was_Product_Sealed;
	String Comfort_Match_In_Store_Experience;
	String Complete_Comfort_Test_Online_Store;
	String Try_Product_Pre_Purchase;
	String Not_Match_Expectations;
	String Recommend_Comment;
	String Address;
	String City;
	String State;
	String Zip_Code;
	String Store_Name;
	String Email_Address;
	String First_Name;
	String Phone_Number;
	String Last_Name;
	String ProcessDateTime;
	String Sales_Person_1;
	String Sales_Person_2;
	String Product_Name_1;
	String Product_Variant_1;
	String ItemNum_1;
	String Product_Description_1;
	String Size_1;
	String Qty_1;
	String Return_Price_1;
	String Product_Category_1;
	String Product_Name_2;
	String Product_Variant_2;
	String ItemNum_2;
	String Product_Description_2;
	String Size_2;
	String Qty_2;
	String Return_Price_2;
	String Product_Category_2;
	String Product_Name_3;
	String Product_Variant_3;
	String ItemNum_3;
	String Product_Description_3;
	String Size_3;
	String Qty_3;
	String Return_Price_3;
	String Product_Category_3;
	String Mode;
	String Response_ID;
	
	public String getDate_of_Service() {
		return Date_of_Service;
	}
	public void setDate_of_Service(String date_of_Service) {
		Date_of_Service = date_of_Service;
	}
	public String getReturn_Order_Number() {
		return Return_Order_Number;
	}
	public void setReturn_Order_Number(String return_Order_Number) {
		Return_Order_Number = return_Order_Number;
	}
	public String getStore_hash() {
		return Store_hash;
	}
	public void setStore_hash(String store_hash) {
		Store_hash = store_hash;
	}
	public String getLocation() {
		return Location;
	}
	public void setLocation(String location) {
		Location = location;
	}
	public String getReason_for_Return() {
		return Reason_for_Return;
	}
	public void setReason_for_Return(String reason_for_Return) {
		Reason_for_Return = reason_for_Return;
	}
	public String getArea_of_Discomfort() {
		return Area_of_Discomfort;
	}
	public void setArea_of_Discomfort(String area_of_Discomfort) {
		Area_of_Discomfort = area_of_Discomfort;
	}
	public String getHow_Damaged() {
		return How_Damaged;
	}
	public void setHow_Damaged(String how_Damaged) {
		How_Damaged = how_Damaged;
	}
	public String getDamaged_Mentioned_at_Delivery() {
		return Damaged_Mentioned_at_Delivery;
	}
	public void setDamaged_Mentioned_at_Delivery(String damaged_Mentioned_at_Delivery) {
		Damaged_Mentioned_at_Delivery = damaged_Mentioned_at_Delivery;
	}
	public String getWas_Product_Sealed() {
		return Was_Product_Sealed;
	}
	public void setWas_Product_Sealed(String was_Product_Sealed) {
		Was_Product_Sealed = was_Product_Sealed;
	}
	public String getComfort_Match_In_Store_Experience() {
		return Comfort_Match_In_Store_Experience;
	}
	public void setComfort_Match_In_Store_Experience(String comfort_Match_In_Store_Experience) {
		Comfort_Match_In_Store_Experience = comfort_Match_In_Store_Experience;
	}
	public String getComplete_Comfort_Test_Online_Store() {
		return Complete_Comfort_Test_Online_Store;
	}
	public void setComplete_Comfort_Test_Online_Store(String complete_Comfort_Test_Online_Store) {
		Complete_Comfort_Test_Online_Store = complete_Comfort_Test_Online_Store;
	}
	public String getTry_Product_Pre_Purchase() {
		return Try_Product_Pre_Purchase;
	}
	public void setTry_Product_Pre_Purchase(String try_Product_Pre_Purchase) {
		Try_Product_Pre_Purchase = try_Product_Pre_Purchase;
	}
	public String getNot_Match_Expectations() {
		return Not_Match_Expectations;
	}
	public void setNot_Match_Expectations(String not_Match_Expectations) {
		Not_Match_Expectations = not_Match_Expectations;
	}
	public String getRecommend_Comment() {
		return Recommend_Comment;
	}
	public void setRecommend_Comment(String recommend_Comment) {
		Recommend_Comment = recommend_Comment;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getZip_Code() {
		return Zip_Code;
	}
	public void setZip_Code(String zip_Code) {
		Zip_Code = zip_Code;
	}
	public String getStore_Name() {
		return Store_Name;
	}
	public void setStore_Name(String store_Name) {
		Store_Name = store_Name;
	}
	public String getEmail_Address() {
		return Email_Address;
	}
	public void setEmail_Address(String email_Address) {
		Email_Address = email_Address;
	}
	public String getFirst_Name() {
		return First_Name;
	}
	public void setFirst_Name(String first_Name) {
		First_Name = first_Name;
	}
	public String getPhone_Number() {
		return Phone_Number;
	}
	public void setPhone_Number(String phone_Number) {
		Phone_Number = phone_Number;
	}
	public String getLast_Name() {
		return Last_Name;
	}
	public void setLast_Name(String last_Name) {
		Last_Name = last_Name;
	}
	public String getProcessDateTime() {
		return ProcessDateTime;
	}
	public void setProcessDateTime(String processDateTime) {
		ProcessDateTime = processDateTime;
	}
	public String getSales_Person_1() {
		return Sales_Person_1;
	}
	public void setSales_Person_1(String sales_Person_1) {
		Sales_Person_1 = sales_Person_1;
	}
	public String getSales_Person_2() {
		return Sales_Person_2;
	}
	public void setSales_Person_2(String sales_Person_2) {
		Sales_Person_2 = sales_Person_2;
	}
	public String getProduct_Name_1() {
		return Product_Name_1;
	}
	public void setProduct_Name_1(String product_Name_1) {
		Product_Name_1 = product_Name_1;
	}
	public String getProduct_Variant_1() {
		return Product_Variant_1;
	}
	public void setProduct_Variant_1(String product_Variant_1) {
		Product_Variant_1 = product_Variant_1;
	}
	public String getItemNum_1() {
		return ItemNum_1;
	}
	public void setItemNum_1(String itemNum_1) {
		ItemNum_1 = itemNum_1;
	}
	public String getProduct_Description_1() {
		return Product_Description_1;
	}
	public void setProduct_Description_1(String product_Description_1) {
		Product_Description_1 = product_Description_1;
	}
	public String getSize_1() {
		return Size_1;
	}
	public void setSize_1(String size_1) {
		Size_1 = size_1;
	}
	public String getQty_1() {
		return Qty_1;
	}
	public void setQty_1(String qty_1) {
		Qty_1 = qty_1;
	}
	public String getReturn_Price_1() {
		return Return_Price_1;
	}
	public void setReturn_Price_1(String return_Price_1) {
		Return_Price_1 = return_Price_1;
	}
	public String getProduct_Category_1() {
		return Product_Category_1;
	}
	public void setProduct_Category_1(String product_Category_1) {
		Product_Category_1 = product_Category_1;
	}
	public String getProduct_Name_2() {
		return Product_Name_2;
	}
	public void setProduct_Name_2(String product_Name_2) {
		Product_Name_2 = product_Name_2;
	}
	public String getProduct_Variant_2() {
		return Product_Variant_2;
	}
	public void setProduct_Variant_2(String product_Variant_2) {
		Product_Variant_2 = product_Variant_2;
	}
	public String getItemNum_2() {
		return ItemNum_2;
	}
	public void setItemNum_2(String itemNum_2) {
		ItemNum_2 = itemNum_2;
	}
	public String getProduct_Description_2() {
		return Product_Description_2;
	}
	public void setProduct_Description_2(String product_Description_2) {
		Product_Description_2 = product_Description_2;
	}
	public String getSize_2() {
		return Size_2;
	}
	public void setSize_2(String size_2) {
		Size_2 = size_2;
	}
	public String getQty_2() {
		return Qty_2;
	}
	public void setQty_2(String qty_2) {
		Qty_2 = qty_2;
	}
	public String getReturn_Price_2() {
		return Return_Price_2;
	}
	public void setReturn_Price_2(String return_Price_2) {
		Return_Price_2 = return_Price_2;
	}
	public String getProduct_Category_2() {
		return Product_Category_2;
	}
	public void setProduct_Category_2(String product_Category_2) {
		Product_Category_2 = product_Category_2;
	}
	public String getProduct_Name_3() {
		return Product_Name_3;
	}
	public void setProduct_Name_3(String product_Name_3) {
		Product_Name_3 = product_Name_3;
	}
	public String getProduct_Variant_3() {
		return Product_Variant_3;
	}
	public void setProduct_Variant_3(String product_Variant_3) {
		Product_Variant_3 = product_Variant_3;
	}
	public String getItemNum_3() {
		return ItemNum_3;
	}
	public void setItemNum_3(String itemNum_3) {
		ItemNum_3 = itemNum_3;
	}
	public String getProduct_Description_3() {
		return Product_Description_3;
	}
	public void setProduct_Description_3(String product_Description_3) {
		Product_Description_3 = product_Description_3;
	}
	public String getSize_3() {
		return Size_3;
	}
	public void setSize_3(String size_3) {
		Size_3 = size_3;
	}
	public String getQty_3() {
		return Qty_3;
	}
	public void setQty_3(String qty_3) {
		Qty_3 = qty_3;
	}
	public String getReturn_Price_3() {
		return Return_Price_3;
	}
	public void setReturn_Price_3(String return_Price_3) {
		Return_Price_3 = return_Price_3;
	}
	public String getProduct_Category_3() {
		return Product_Category_3;
	}
	public void setProduct_Category_3(String product_Category_3) {
		Product_Category_3 = product_Category_3;
	}
	public String getMode() {
		return Mode;
	}
	public void setMode(String mode) {
		Mode = mode;
	}
	public String getResponse_ID() {
		return Response_ID;
	}
	public void setResponse_ID(String response_ID) {
		Response_ID = response_ID;
	}
	
}
