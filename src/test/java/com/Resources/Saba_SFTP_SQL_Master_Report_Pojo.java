package com.Resources;

public class Saba_SFTP_SQL_Master_Report_Pojo {
	
	
	
	String TranscriptID;
	String Completion_Status;
	String Registration_Status;
	String Course_Title;
	String Course_Course_ID;
	String Class_ID;
	String Person_Username;
	String Person_Full_Name;
	String Person_Company;
	String Manager_Full_Name;
	String Person_Parent_Organization_Name;
	
	String Person_Job_Type_Name;
	String Person_Location_Name;
	String Completed_Courses_Transcript_Date_Marked_Complete;
	String Registration_Date;
	String Course_Target_Date;
	String Person_Employee_Type;
	String Person_Status;
	String Completed_Courses_Transcript_Score;
	public String getTranscriptID() {
		return TranscriptID;
	}
	public void setTranscriptID(String transcriptID) {
		TranscriptID = transcriptID;
	}
	public String getCompletion_Status() {
		return Completion_Status;
	}
	public void setCompletion_Status(String completion_Status) {
		Completion_Status = completion_Status;
	}
	public String getRegistration_Status() {
		return Registration_Status;
	}
	public void setRegistration_Status(String registration_Status) {
		Registration_Status = registration_Status;
	}
	public String getCourse_Title() {
		return Course_Title;
	}
	public void setCourse_Title(String course_Title) {
		Course_Title = course_Title;
	}
	public String getCourse_Course_ID() {
		return Course_Course_ID;
	}
	public void setCourse_Course_ID(String course_Course_ID) {
		Course_Course_ID = course_Course_ID;
	}
	public String getClass_ID() {
		return Class_ID;
	}
	public void setClass_ID(String class_ID) {
		Class_ID = class_ID;
	}
	public String getPerson_Username() {
		return Person_Username;
	}
	public void setPerson_Username(String person_Username) {
		Person_Username = person_Username;
	}
	public String getPerson_Full_Name() {
		return Person_Full_Name;
	}
	public void setPerson_Full_Name(String person_Full_Name) {
		Person_Full_Name = person_Full_Name;
	}
	public String getPerson_Company() {
		return Person_Company;
	}
	public void setPerson_Company(String person_Company) {
		Person_Company = person_Company;
	}
	public String getManager_Full_Name() {
		return Manager_Full_Name;
	}
	public void setManager_Full_Name(String manager_Full_Name) {
		Manager_Full_Name = manager_Full_Name;
	}
	public String getPerson_Parent_Organization_Name() {
		return Person_Parent_Organization_Name;
	}
	public void setPerson_Parent_Organization_Name(String person_Parent_Organization_Name) {
		Person_Parent_Organization_Name = person_Parent_Organization_Name;
	}
	public String getPerson_Job_Type_Name() {
		return Person_Job_Type_Name;
	}
	public void setPerson_Job_Type_Name(String person_Job_Type_Name) {
		Person_Job_Type_Name = person_Job_Type_Name;
	}
	public String getPerson_Location_Name() {
		return Person_Location_Name;
	}
	public void setPerson_Location_Name(String person_Location_Name) {
		Person_Location_Name = person_Location_Name;
	}
	public String getCompleted_Courses_Transcript_Date_Marked_Complete() {
		return Completed_Courses_Transcript_Date_Marked_Complete;
	}
	public void setCompleted_Courses_Transcript_Date_Marked_Complete(
			String completed_Courses_Transcript_Date_Marked_Complete) {
		Completed_Courses_Transcript_Date_Marked_Complete = completed_Courses_Transcript_Date_Marked_Complete;
	}
	public String getRegistration_Date() {
		return Registration_Date;
	}
	public void setRegistration_Date(String registration_Date) {
		Registration_Date = registration_Date;
	}
	public String getCourse_Target_Date() {
		return Course_Target_Date;
	}
	public void setCourse_Target_Date(String course_Target_Date) {
		Course_Target_Date = course_Target_Date;
	}
	public String getPerson_Employee_Type() {
		return Person_Employee_Type;
	}
	public void setPerson_Employee_Type(String person_Employee_Type) {
		Person_Employee_Type = person_Employee_Type;
	}
	public String getPerson_Status() {
		return Person_Status;
	}
	public void setPerson_Status(String person_Status) {
		Person_Status = person_Status;
	}
	public String getCompleted_Courses_Transcript_Score() {
		return Completed_Courses_Transcript_Score;
	}
	public void setCompleted_Courses_Transcript_Score(String completed_Courses_Transcript_Score) {
		Completed_Courses_Transcript_Score = completed_Courses_Transcript_Score;
	}
	
	
	
}