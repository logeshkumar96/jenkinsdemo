package com.Resources;

public class Saba_SFTP_SQL_NewHireExam_Report_Pojo {
	
	String TranscriptID;
	String Module_name;
	String Module_score;
	String Class_ID;
	String Person_Full_Name;
	String Manager_Full_Name;
	String Person_Username;
	String Person_Status;
	String Completed_Courses_Transcript_Score;
	String Learner_Module_Attempts;
	String Module_Completed_On;
	
	public String getTranscriptID() {
		return TranscriptID;
	}
	public void setTranscriptID(String transcriptID) {
		TranscriptID = transcriptID;
	}
	public String getModule_name() {
		return Module_name;
	}
	public void setModule_name(String module_name) {
		Module_name = module_name;
	}
	public String getModule_score() {
		return Module_score;
	}
	public void setModule_score(String module_score) {
		Module_score = module_score;
	}
	public String getClass_ID() {
		return Class_ID;
	}
	public void setClass_ID(String class_ID) {
		Class_ID = class_ID;
	}
	public String getPerson_Full_Name() {
		return Person_Full_Name;
	}
	public void setPerson_Full_Name(String person_Full_Name) {
		Person_Full_Name = person_Full_Name;
	}
	public String getManager_Full_Name() {
		return Manager_Full_Name;
	}
	public void setManager_Full_Name(String manager_Full_Name) {
		Manager_Full_Name = manager_Full_Name;
	}
	public String getPerson_Username() {
		return Person_Username;
	}
	public void setPerson_Username(String person_Username) {
		Person_Username = person_Username;
	}
	public String getPerson_Status() {
		return Person_Status;
	}
	public void setPerson_Status(String person_Status) {
		Person_Status = person_Status;
	}
	public String getCompleted_Courses_Transcript_Score() {
		return Completed_Courses_Transcript_Score;
	}
	public void setCompleted_Courses_Transcript_Score(String completed_Courses_Transcript_Score) {
		Completed_Courses_Transcript_Score = completed_Courses_Transcript_Score;
	}
	public String getLearner_Module_Attempts() {
		return Learner_Module_Attempts;
	}
	public void setLearner_Module_Attempts(String learner_Module_Attempts) {
		Learner_Module_Attempts = learner_Module_Attempts;
	}
	public String getModule_Completed_On() {
		return Module_Completed_On;
	}
	public void setModule_Completed_On(String module_Completed_On) {
		Module_Completed_On = module_Completed_On;
	}
}
