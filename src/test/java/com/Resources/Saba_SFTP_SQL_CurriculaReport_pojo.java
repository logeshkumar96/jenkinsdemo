package com.Resources;

public class Saba_SFTP_SQL_CurriculaReport_pojo {
	
	String Curriculum_Name;
	String Person_Username;
	String Student_Curriculum_Status;
	String Person_Full_Name;
	String Person_Person_No;
	String Manager_Full_Name;
	String Person_Location_Name;
	String Person_Parent_Organization_Name;
	String Person_Organization_Name;
	String Person_Status;
	String Person_Employee_Type;
	String Person_Company;
	String Person_Job_Type_Name;
	String Student_Curriculum_Assigned_On;
	String Student_Curriculum_Started_On;
	String Student_Curriculum_Due_Date;
	String Student_Curriculum_Acquired_On;
	String Curricula_Completion_Percentage;
	String Person_Start_Date;
	
	public String getCurriculum_Name() {
		return Curriculum_Name;
	}
	public void setCurriculum_Name(String curriculum_Name) {
		Curriculum_Name = curriculum_Name;
	}
	public String getPerson_Username() {
		return Person_Username;
	}
	public void setPerson_Username(String person_Username) {
		Person_Username = person_Username;
	}
	public String getStudent_Curriculum_Status() {
		return Student_Curriculum_Status;
	}
	public void setStudent_Curriculum_Status(String student_Curriculum_Status) {
		Student_Curriculum_Status = student_Curriculum_Status;
	}
	public String getPerson_Full_Name() {
		return Person_Full_Name;
	}
	public void setPerson_Full_Name(String person_Full_Name) {
		Person_Full_Name = person_Full_Name;
	}
	public String getPerson_Person_No() {
		return Person_Person_No;
	}
	public void setPerson_Person_No(String person_Person_No) {
		Person_Person_No = person_Person_No;
	}
	public String getManager_Full_Name() {
		return Manager_Full_Name;
	}
	public void setManager_Full_Name(String manager_Full_Name) {
		Manager_Full_Name = manager_Full_Name;
	}
	public String getPerson_Location_Name() {
		return Person_Location_Name;
	}
	public void setPerson_Location_Name(String person_Location_Name) {
		Person_Location_Name = person_Location_Name;
	}
	public String getPerson_Parent_Organization_Name() {
		return Person_Parent_Organization_Name;
	}
	public void setPerson_Parent_Organization_Name(String person_Parent_Organization_Name) {
		Person_Parent_Organization_Name = person_Parent_Organization_Name;
	}
	public String getPerson_Organization_Name() {
		return Person_Organization_Name;
	}
	public void setPerson_Organization_Name(String person_Organization_Name) {
		Person_Organization_Name = person_Organization_Name;
	}
	public String getPerson_Status() {
		return Person_Status;
	}
	public void setPerson_Status(String person_Status) {
		Person_Status = person_Status;
	}
	public String getPerson_Employee_Type() {
		return Person_Employee_Type;
	}
	public void setPerson_Employee_Type(String person_Employee_Type) {
		Person_Employee_Type = person_Employee_Type;
	}
	public String getPerson_Company() {
		return Person_Company;
	}
	public void setPerson_Company(String person_Company) {
		Person_Company = person_Company;
	}
	public String getPerson_Job_Type_Name() {
		return Person_Job_Type_Name;
	}
	public void setPerson_Job_Type_Name(String person_Job_Type_Name) {
		Person_Job_Type_Name = person_Job_Type_Name;
	}
	public String getStudent_Curriculum_Assigned_On() {
		return Student_Curriculum_Assigned_On;
	}
	public void setStudent_Curriculum_Assigned_On(String student_Curriculum_Assigned_On) {
		Student_Curriculum_Assigned_On = student_Curriculum_Assigned_On;
	}
	public String getStudent_Curriculum_Started_On() {
		return Student_Curriculum_Started_On;
	}
	public void setStudent_Curriculum_Started_On(String student_Curriculum_Started_On) {
		Student_Curriculum_Started_On = student_Curriculum_Started_On;
	}
	public String getStudent_Curriculum_Due_Date() {
		return Student_Curriculum_Due_Date;
	}
	public void setStudent_Curriculum_Due_Date(String student_Curriculum_Due_Date) {
		Student_Curriculum_Due_Date = student_Curriculum_Due_Date;
	}
	public String getStudent_Curriculum_Acquired_On() {
		return Student_Curriculum_Acquired_On;
	}
	public void setStudent_Curriculum_Acquired_On(String student_Curriculum_Acquired_On) {
		Student_Curriculum_Acquired_On = student_Curriculum_Acquired_On;
	}
	public String getCurricula_Completion_Percentage() {
		return Curricula_Completion_Percentage;
	}
	public void setCurricula_Completion_Percentage(String curricula_Completion_Percentage) {
		Curricula_Completion_Percentage = curricula_Completion_Percentage;
	}
	public String getPerson_Start_Date() {
		return Person_Start_Date;
	}
	public void setPerson_Start_Date(String person_Start_Date) {
		Person_Start_Date = person_Start_Date;
	}
	
}
