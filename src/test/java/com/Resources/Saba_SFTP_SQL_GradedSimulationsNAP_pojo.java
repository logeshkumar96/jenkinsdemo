package com.Resources;

public class Saba_SFTP_SQL_GradedSimulationsNAP_pojo {
	
	String Transcript_Internal_ID;
	String Class_Name;
	String Assessment_Attempt_Number;
	String Assessment_Version;
	String Learner_Module_Attempts;
	String Assessment_Completion_Date;
	String Class_ID;
	String Person_Full_Name;
	String Manager_Full_Name;
	String Person_Username;
	String Person_Status;
	String Assessment_Question_Response;
	public String getTranscript_Internal_ID() {
		return Transcript_Internal_ID;
	}
	public void setTranscript_Internal_ID(String transcript_Internal_ID) {
		Transcript_Internal_ID = transcript_Internal_ID;
	}
	public String getClass_Name() {
		return Class_Name;
	}
	public void setClass_Name(String class_Name) {
		Class_Name = class_Name;
	}
	public String getAssessment_Attempt_Number() {
		return Assessment_Attempt_Number;
	}
	public void setAssessment_Attempt_Number(String assessment_Attempt_Number) {
		Assessment_Attempt_Number = assessment_Attempt_Number;
	}
	public String getAssessment_Version() {
		return Assessment_Version;
	}
	public void setAssessment_Version(String assessment_Version) {
		Assessment_Version = assessment_Version;
	}
	public String getLearner_Module_Attempts() {
		return Learner_Module_Attempts;
	}
	public void setLearner_Module_Attempts(String learner_Module_Attempts) {
		Learner_Module_Attempts = learner_Module_Attempts;
	}
	public String getAssessment_Completion_Date() {
		return Assessment_Completion_Date;
	}
	public void setAssessment_Completion_Date(String assessment_Completion_Date) {
		Assessment_Completion_Date = assessment_Completion_Date;
	}
	public String getClass_ID() {
		return Class_ID;
	}
	public void setClass_ID(String class_ID) {
		Class_ID = class_ID;
	}
	public String getPerson_Full_Name() {
		return Person_Full_Name;
	}
	public void setPerson_Full_Name(String person_Full_Name) {
		Person_Full_Name = person_Full_Name;
	}
	public String getManager_Full_Name() {
		return Manager_Full_Name;
	}
	public void setManager_Full_Name(String manager_Full_Name) {
		Manager_Full_Name = manager_Full_Name;
	}
	public String getPerson_Username() {
		return Person_Username;
	}
	public void setPerson_Username(String person_Username) {
		Person_Username = person_Username;
	}
	public String getPerson_Status() {
		return Person_Status;
	}
	public void setPerson_Status(String person_Status) {
		Person_Status = person_Status;
	}
	public String getAssessment_Question_Response() {
		return Assessment_Question_Response;
	}
	public void setAssessment_Question_Response(String assessment_Question_Response) {
		Assessment_Question_Response = assessment_Question_Response;
	}
	
	
	
}