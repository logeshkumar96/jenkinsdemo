package com.Resources;

public class inContactBQ_CSVPojo_WH_EXPD_AgentList {

	String contact_id;
	String master_contact_id;
	String Contact_Code;
	String media_name;
	String contact_name;
	String ANI_DIALNUM;
	String skill_no;
	String skill_name;
	String campaign_no;
	String campaign_name;
	String agent_no;
	String agent_name;
	String team_no;
	String team_name;
	String disposition_code;
	String SLA;
	String start_date;
	String start_time;
	String PreQueue;
	String InQueue;
	String Agent_Time;
	String PostQueue;
	String Total_Time;
	String Abandon_Time;
	String Routing_Time;
	String abandon;
	String callback_time;
	String Logged;
	String Hold_Time;
	String ANI;
	String DNIS;
	String ACW_Seconds;
	String ACW_Time;
	
//	Below variables are for AgentList and "_AL" refers to Agent List
	String Agent_No_AL;
	String Team_No_AL;
	String First_Name_AL;
	String Last_Name_AL;
	String Email_AL;
	String Mod_Datetime_AL;
	String Last_Login_AL;
	String Status_AL;
	String Reports_To_AL;
	String Middle_Name_AL;
	String Username_AL;
	String Time_Zone_AL;
	String Chat_Timeout_AL;
	String Phone_Timeout_AL;
	String Voice_Timeout_AL;
	String Location_AL;
	String Hire_Date_AL;
	String Termination_Date_AL;
	String Hourly_Cost_AL;
	String Rehire_Status_AL;
	String Employment_Type_AL;
	String Referral_AL;
	String At_Home_Worker_AL;
	String Hiring_Source_AL;
	String NtLoginName_AL;
	String Custom1_AL;
	String Custom2_AL;
	String Custom3_AL;
	String Custom4_AL;
	String Custom5_AL;
	
	public String getContact_id() {
		return contact_id;
	}
	public void setContact_id(String contact_id) {
		this.contact_id = contact_id;
	}
	public String getMaster_contact_id() {
		return master_contact_id;
	}
	public void setMaster_contact_id(String master_contact_id) {
		this.master_contact_id = master_contact_id;
	}
	public String getContact_Code() {
		return Contact_Code;
	}
	public void setContact_Code(String contact_Code) {
		Contact_Code = contact_Code;
	}
	public String getMedia_name() {
		return media_name;
	}
	public void setMedia_name(String media_name) {
		this.media_name = media_name;
	}
	public String getContact_name() {
		return contact_name;
	}
	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}
	public String getANI_DIALNUM() {
		return ANI_DIALNUM;
	}
	public void setANI_DIALNUM(String aNI_DIALNUM) {
		ANI_DIALNUM = aNI_DIALNUM;
	}
	public String getSkill_no() {
		return skill_no;
	}
	public void setSkill_no(String skill_no) {
		this.skill_no = skill_no;
	}
	public String getSkill_name() {
		return skill_name;
	}
	public void setSkill_name(String skill_name) {
		this.skill_name = skill_name;
	}
	public String getCampaign_no() {
		return campaign_no;
	}
	public void setCampaign_no(String campaign_no) {
		this.campaign_no = campaign_no;
	}
	public String getCampaign_name() {
		return campaign_name;
	}
	public void setCampaign_name(String campaign_name) {
		this.campaign_name = campaign_name;
	}
	public String getAgent_no() {
		return agent_no;
	}
	public void setAgent_no(String agent_no) {
		this.agent_no = agent_no;
	}
	public String getAgent_name() {
		return agent_name;
	}
	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}
	public String getTeam_no() {
		return team_no;
	}
	public void setTeam_no(String team_no) {
		this.team_no = team_no;
	}
	public String getTeam_name() {
		return team_name;
	}
	public void setTeam_name(String team_name) {
		this.team_name = team_name;
	}
	public String getDisposition_code() {
		return disposition_code;
	}
	public void setDisposition_code(String disposition_code) {
		this.disposition_code = disposition_code;
	}
	public String getSLA() {
		return SLA;
	}
	public void setSLA(String sLA) {
		SLA = sLA;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getStart_time() {
		return start_time;
	}
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
	public String getPreQueue() {
		return PreQueue;
	}
	public void setPreQueue(String preQueue) {
		PreQueue = preQueue;
	}
	public String getInQueue() {
		return InQueue;
	}
	public void setInQueue(String inQueue) {
		InQueue = inQueue;
	}
	public String getAgent_Time() {
		return Agent_Time;
	}
	public void setAgent_Time(String agent_Time) {
		Agent_Time = agent_Time;
	}
	public String getPostQueue() {
		return PostQueue;
	}
	public void setPostQueue(String postQueue) {
		PostQueue = postQueue;
	}
	public String getTotal_Time() {
		return Total_Time;
	}
	public void setTotal_Time(String total_Time) {
		Total_Time = total_Time;
	}
	public String getAbandon_Time() {
		return Abandon_Time;
	}
	public void setAbandon_Time(String abandon_Time) {
		Abandon_Time = abandon_Time;
	}
	public String getRouting_Time() {
		return Routing_Time;
	}
	public void setRouting_Time(String routing_Time) {
		Routing_Time = routing_Time;
	}
	public String getAbandon() {
		return abandon;
	}
	public void setAbandon(String abandon) {
		this.abandon = abandon;
	}
	public String getCallback_time() {
		return callback_time;
	}
	public void setCallback_time(String callback_time) {
		this.callback_time = callback_time;
	}
	public String getLogged() {
		return Logged;
	}
	public void setLogged(String logged) {
		Logged = logged;
	}
	public String getHold_Time() {
		return Hold_Time;
	}
	public void setHold_Time(String hold_Time) {
		Hold_Time = hold_Time;
	}
	public String getANI() {
		return ANI;
	}
	public void setANI(String aNI) {
		ANI = aNI;
	}
	public String getDNIS() {
		return DNIS;
	}
	public void setDNIS(String dNIS) {
		DNIS = dNIS;
	}
	public String getACW_Seconds() {
		return ACW_Seconds;
	}
	public void setACW_Seconds(String aCW_Seconds) {
		ACW_Seconds = aCW_Seconds;
	}
	public String getACW_Time() {
		return ACW_Time;
	}
	public void setACW_Time(String aCW_Time) {
		ACW_Time = aCW_Time;
	}
	public String getAgent_No_AL() {
		return Agent_No_AL;
	}
	public void setAgent_No_AL(String agent_No_AL) {
		Agent_No_AL = agent_No_AL;
	}
	public String getTeam_No_AL() {
		return Team_No_AL;
	}
	public void setTeam_No_AL(String team_No_AL) {
		Team_No_AL = team_No_AL;
	}
	public String getFirst_Name_AL() {
		return First_Name_AL;
	}
	public void setFirst_Name_AL(String first_Name_AL) {
		First_Name_AL = first_Name_AL;
	}
	public String getLast_Name_AL() {
		return Last_Name_AL;
	}
	public void setLast_Name_AL(String last_Name_AL) {
		Last_Name_AL = last_Name_AL;
	}
	public String getEmail_AL() {
		return Email_AL;
	}
	public void setEmail_AL(String email_AL) {
		Email_AL = email_AL;
	}
	public String getMod_Datetime_AL() {
		return Mod_Datetime_AL;
	}
	public void setMod_Datetime_AL(String mod_Datetime_AL) {
		Mod_Datetime_AL = mod_Datetime_AL;
	}
	public String getLast_Login_AL() {
		return Last_Login_AL;
	}
	public void setLast_Login_AL(String last_Login_AL) {
		Last_Login_AL = last_Login_AL;
	}
	public String getStatus_AL() {
		return Status_AL;
	}
	public void setStatus_AL(String status_AL) {
		Status_AL = status_AL;
	}
	public String getReports_To_AL() {
		return Reports_To_AL;
	}
	public void setReports_To_AL(String reports_To_AL) {
		Reports_To_AL = reports_To_AL;
	}
	public String getMiddle_Name_AL() {
		return Middle_Name_AL;
	}
	public void setMiddle_Name_AL(String middle_Name_AL) {
		Middle_Name_AL = middle_Name_AL;
	}
	public String getUsername_AL() {
		return Username_AL;
	}
	public void setUsername_AL(String username_AL) {
		Username_AL = username_AL;
	}
	public String getTime_Zone_AL() {
		return Time_Zone_AL;
	}
	public void setTime_Zone_AL(String time_Zone_AL) {
		Time_Zone_AL = time_Zone_AL;
	}
	public String getChat_Timeout_AL() {
		return Chat_Timeout_AL;
	}
	public void setChat_Timeout_AL(String chat_Timeout_AL) {
		Chat_Timeout_AL = chat_Timeout_AL;
	}
	public String getPhone_Timeout_AL() {
		return Phone_Timeout_AL;
	}
	public void setPhone_Timeout_AL(String phone_Timeout_AL) {
		Phone_Timeout_AL = phone_Timeout_AL;
	}
	public String getVoice_Timeout_AL() {
		return Voice_Timeout_AL;
	}
	public void setVoice_Timeout_AL(String voice_Timeout_AL) {
		Voice_Timeout_AL = voice_Timeout_AL;
	}
	public String getLocation_AL() {
		return Location_AL;
	}
	public void setLocation_AL(String location_AL) {
		Location_AL = location_AL;
	}
	public String getHire_Date_AL() {
		return Hire_Date_AL;
	}
	public void setHire_Date_AL(String hire_Date_AL) {
		Hire_Date_AL = hire_Date_AL;
	}
	public String getTermination_Date_AL() {
		return Termination_Date_AL;
	}
	public void setTermination_Date_AL(String termination_Date_AL) {
		Termination_Date_AL = termination_Date_AL;
	}
	public String getHourly_Cost_AL() {
		return Hourly_Cost_AL;
	}
	public void setHourly_Cost_AL(String hourly_Cost_AL) {
		Hourly_Cost_AL = hourly_Cost_AL;
	}
	public String getRehire_Status_AL() {
		return Rehire_Status_AL;
	}
	public void setRehire_Status_AL(String rehire_Status_AL) {
		Rehire_Status_AL = rehire_Status_AL;
	}
	public String getEmployment_Type_AL() {
		return Employment_Type_AL;
	}
	public void setEmployment_Type_AL(String employment_Type_AL) {
		Employment_Type_AL = employment_Type_AL;
	}
	public String getReferral_AL() {
		return Referral_AL;
	}
	public void setReferral_AL(String referral_AL) {
		Referral_AL = referral_AL;
	}
	public String getAt_Home_Worker_AL() {
		return At_Home_Worker_AL;
	}
	public void setAt_Home_Worker_AL(String at_Home_Worker_AL) {
		At_Home_Worker_AL = at_Home_Worker_AL;
	}
	public String getHiring_Source_AL() {
		return Hiring_Source_AL;
	}
	public void setHiring_Source_AL(String hiring_Source_AL) {
		Hiring_Source_AL = hiring_Source_AL;
	}
	public String getNtLoginName_AL() {
		return NtLoginName_AL;
	}
	public void setNtLoginName_AL(String ntLoginName_AL) {
		NtLoginName_AL = ntLoginName_AL;
	}
	public String getCustom1_AL() {
		return Custom1_AL;
	}
	public void setCustom1_AL(String custom1_AL) {
		Custom1_AL = custom1_AL;
	}
	public String getCustom2_AL() {
		return Custom2_AL;
	}
	public void setCustom2_AL(String custom2_AL) {
		Custom2_AL = custom2_AL;
	}
	public String getCustom3_AL() {
		return Custom3_AL;
	}
	public void setCustom3_AL(String custom3_AL) {
		Custom3_AL = custom3_AL;
	}
	public String getCustom4_AL() {
		return Custom4_AL;
	}
	public void setCustom4_AL(String custom4_AL) {
		Custom4_AL = custom4_AL;
	}
	public String getCustom5_AL() {
		return Custom5_AL;
	}
	public void setCustom5_AL(String custom5_AL) {
		Custom5_AL = custom5_AL;
	}
}
