package com.Resources;

public class InMomentAttributesUpdatePojo {
	
	String Location;
	String Date_of_Survey;
	String Response_ID;
	String Ticket_No;
	String Last_Name;
	String First_Name;
	String Home_Phone;
	String Address;
	String City;
	String State;
	String Zip_Code;
	String Location_No;
	String Store_Name;
	String Date_of_Service;
	String Final_Date;
	String PU_DEL;
	String Email_Address;
	String Sales_Person_1;
	String Sales_Person_2;
	String Delivering_DC;
	String Delivery_Truck_No;
	String Delivery_Market;
	String Order_Value;
	String Delivery_Mode;
	String Truck_Contractor;
	String Time_of_Sale;
	String Delivery_Time;
	String Purchase_Channel;
	String Recommend;
	String Recommend_Comment;
	String Video_Feedback_Comment;
	String Delivery_Satisfaction;
	String Poor_Experience_Location;
	String Poor_Experience_Location_Other;
	String Contact_Request;
	String Contact_Method;
	String Contact_Email_Address;
	String Contact_Phone_Number;
	String NPS_Score;
	String Mode;
	String District;
	String Brand;
	String Survey_Attempt;
	String IP_Address;
	String Device_Type;
	String Vendor_ID;
	String Vendor_Name;
	String Scheduled_Delivery_Date;
	String Scheduled_Delivery_Time;
	String Actual_Delivery_Date;
	String Date_of_Sale;
	String Truck_Driver_ID;
	String Truck_Driver_Name;
	
	public String getLocation() {
		return Location;
	}
	public void setLocation(String location) {
		Location = location;
	}
	public String getDate_of_Survey() {
		return Date_of_Survey;
	}
	public void setDate_of_Survey(String date_of_Survey) {
		Date_of_Survey = date_of_Survey;
	}
	public String getResponse_ID() {
		return Response_ID;
	}
	public void setResponse_ID(String response_ID) {
		Response_ID = response_ID;
	}
	public String getTicket_No() {
		return Ticket_No;
	}
	public void setTicket_No(String ticket_No) {
		Ticket_No = ticket_No;
	}
	public String getLast_Name() {
		return Last_Name;
	}
	public void setLast_Name(String last_Name) {
		Last_Name = last_Name;
	}
	public String getFirst_Name() {
		return First_Name;
	}
	public void setFirst_Name(String first_Name) {
		First_Name = first_Name;
	}
	public String getHome_Phone() {
		return Home_Phone;
	}
	public void setHome_Phone(String home_Phone) {
		Home_Phone = home_Phone;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getZip_Code() {
		return Zip_Code;
	}
	public void setZip_Code(String zip_Code) {
		Zip_Code = zip_Code;
	}
	public String getLocation_No() {
		return Location_No;
	}
	public void setLocation_No(String location_No) {
		Location_No = location_No;
	}
	public String getStore_Name() {
		return Store_Name;
	}
	public void setStore_Name(String store_Name) {
		Store_Name = store_Name;
	}
	public String getDate_of_Service() {
		return Date_of_Service;
	}
	public void setDate_of_Service(String date_of_Service) {
		Date_of_Service = date_of_Service;
	}
	public String getFinal_Date() {
		return Final_Date;
	}
	public void setFinal_Date(String final_Date) {
		Final_Date = final_Date;
	}
	public String getPU_DEL() {
		return PU_DEL;
	}
	public void setPU_DEL(String pU_DEL) {
		PU_DEL = pU_DEL;
	}
	public String getEmail_Address() {
		return Email_Address;
	}
	public void setEmail_Address(String email_Address) {
		Email_Address = email_Address;
	}
	public String getSales_Person_1() {
		return Sales_Person_1;
	}
	public void setSales_Person_1(String sales_Person_1) {
		Sales_Person_1 = sales_Person_1;
	}
	public String getSales_Person_2() {
		return Sales_Person_2;
	}
	public void setSales_Person_2(String sales_Person_2) {
		Sales_Person_2 = sales_Person_2;
	}
	public String getDelivering_DC() {
		return Delivering_DC;
	}
	public void setDelivering_DC(String delivering_DC) {
		Delivering_DC = delivering_DC;
	}
	public String getDelivery_Truck_No() {
		return Delivery_Truck_No;
	}
	public void setDelivery_Truck_No(String delivery_Truck_No) {
		Delivery_Truck_No = delivery_Truck_No;
	}
	public String getDelivery_Market() {
		return Delivery_Market;
	}
	public void setDelivery_Market(String delivery_Market) {
		Delivery_Market = delivery_Market;
	}
	public String getOrder_Value() {
		return Order_Value;
	}
	public void setOrder_Value(String order_Value) {
		Order_Value = order_Value;
	}
	public String getDelivery_Mode() {
		return Delivery_Mode;
	}
	public void setDelivery_Mode(String delivery_Mode) {
		Delivery_Mode = delivery_Mode;
	}
	public String getTruck_Contractor() {
		return Truck_Contractor;
	}
	public void setTruck_Contractor(String truck_Contractor) {
		Truck_Contractor = truck_Contractor;
	}
	public String getTime_of_Sale() {
		return Time_of_Sale;
	}
	public void setTime_of_Sale(String time_of_Sale) {
		Time_of_Sale = time_of_Sale;
	}
	public String getDelivery_Time() {
		return Delivery_Time;
	}
	public void setDelivery_Time(String delivery_Time) {
		Delivery_Time = delivery_Time;
	}
	public String getPurchase_Channel() {
		return Purchase_Channel;
	}
	public void setPurchase_Channel(String purchase_Channel) {
		Purchase_Channel = purchase_Channel;
	}
	public String getRecommend() {
		return Recommend;
	}
	public void setRecommend(String recommend) {
		Recommend = recommend;
	}
	public String getRecommend_Comment() {
		return Recommend_Comment;
	}
	public void setRecommend_Comment(String recommend_Comment) {
		Recommend_Comment = recommend_Comment;
	}
	public String getVideo_Feedback_Comment() {
		return Video_Feedback_Comment;
	}
	public void setVideo_Feedback_Comment(String video_Feedback_Comment) {
		Video_Feedback_Comment = video_Feedback_Comment;
	}
	public String getDelivery_Satisfaction() {
		return Delivery_Satisfaction;
	}
	public void setDelivery_Satisfaction(String delivery_Satisfaction) {
		Delivery_Satisfaction = delivery_Satisfaction;
	}
	public String getPoor_Experience_Location() {
		return Poor_Experience_Location;
	}
	public void setPoor_Experience_Location(String poor_Experience_Location) {
		Poor_Experience_Location = poor_Experience_Location;
	}
	public String getPoor_Experience_Location_Other() {
		return Poor_Experience_Location_Other;
	}
	public void setPoor_Experience_Location_Other(String poor_Experience_Location_Other) {
		Poor_Experience_Location_Other = poor_Experience_Location_Other;
	}
	public String getContact_Request() {
		return Contact_Request;
	}
	public void setContact_Request(String contact_Request) {
		Contact_Request = contact_Request;
	}
	public String getContact_Method() {
		return Contact_Method;
	}
	public void setContact_Method(String contact_Method) {
		Contact_Method = contact_Method;
	}
	public String getContact_Email_Address() {
		return Contact_Email_Address;
	}
	public void setContact_Email_Address(String contact_Email_Address) {
		Contact_Email_Address = contact_Email_Address;
	}
	public String getContact_Phone_Number() {
		return Contact_Phone_Number;
	}
	public void setContact_Phone_Number(String contact_Phone_Number) {
		Contact_Phone_Number = contact_Phone_Number;
	}
	public String getNPS_Score() {
		return NPS_Score;
	}
	public void setNPS_Score(String nPS_Score) {
		NPS_Score = nPS_Score;
	}
	public String getMode() {
		return Mode;
	}
	public void setMode(String mode) {
		Mode = mode;
	}
	public String getDistrict() {
		return District;
	}
	public void setDistrict(String district) {
		District = district;
	}
	public String getBrand() {
		return Brand;
	}
	public void setBrand(String brand) {
		Brand = brand;
	}
	public String getSurvey_Attempt() {
		return Survey_Attempt;
	}
	public void setSurvey_Attempt(String survey_Attempt) {
		Survey_Attempt = survey_Attempt;
	}
	public String getIP_Address() {
		return IP_Address;
	}
	public void setIP_Address(String iP_Address) {
		IP_Address = iP_Address;
	}
	public String getDevice_Type() {
		return Device_Type;
	}
	public void setDevice_Type(String device_Type) {
		Device_Type = device_Type;
	}
	public String getVendor_ID() {
		return Vendor_ID;
	}
	public void setVendor_ID(String vendor_ID) {
		Vendor_ID = vendor_ID;
	}
	public String getVendor_Name() {
		return Vendor_Name;
	}
	public void setVendor_Name(String vendor_Name) {
		Vendor_Name = vendor_Name;
	}
	public String getScheduled_Delivery_Date() {
		return Scheduled_Delivery_Date;
	}
	public void setScheduled_Delivery_Date(String scheduled_Delivery_Date) {
		Scheduled_Delivery_Date = scheduled_Delivery_Date;
	}
	public String getScheduled_Delivery_Time() {
		return Scheduled_Delivery_Time;
	}
	public void setScheduled_Delivery_Time(String scheduled_Delivery_Time) {
		Scheduled_Delivery_Time = scheduled_Delivery_Time;
	}
	public String getActual_Delivery_Date() {
		return Actual_Delivery_Date;
	}
	public void setActual_Delivery_Date(String actual_Delivery_Date) {
		Actual_Delivery_Date = actual_Delivery_Date;
	}
	public String getDate_of_Sale() {
		return Date_of_Sale;
	}
	public void setDate_of_Sale(String date_of_Sale) {
		Date_of_Sale = date_of_Sale;
	}
	public String getTruck_Driver_ID() {
		return Truck_Driver_ID;
	}
	public void setTruck_Driver_ID(String truck_Driver_ID) {
		Truck_Driver_ID = truck_Driver_ID;
	}
	public String getTruck_Driver_Name() {
		return Truck_Driver_Name;
	}
	public void setTruck_Driver_Name(String truck_Driver_Name) {
		Truck_Driver_Name = truck_Driver_Name;
	}
}
