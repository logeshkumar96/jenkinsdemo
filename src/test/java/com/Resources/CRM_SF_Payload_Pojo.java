package com.Resources;

import java.util.List;

public class CRM_SF_Payload_Pojo {

	String Origin;
	String Priority;
	String Description;
	String Subject;
	String recordtypeid;
	String Status;
	String Sales_Associate_Email_1__c;
	String Sales_Associate_Name_1__c;
	String Customer_First_Name__c;
	String Customer_Last_Name__c;
	String Customer_Street__c;
	String Customer_Street_Line_2__c;
	String Customer_City__c;
	String Customer_State__c;
	String Customer_Zip_Code__c;
	String Customer_Code_Ax__c;
	String Customer_Email_Ax__c;
	String Original_Order_NumberX__c;
	String total_COGS__C;
	String Order_Written_Date__c;
	String sales_Associate_Name_2__c;
	String Sales_Associate_Email_2__c;
	String Type;
	String Contactid;
	String Order_Number__c;
	String Source__c;
	String Item_Number__c;
	String Store_Email__c;
	String SO_STORE_CD;
	
	public String getOrigin() {
		return Origin;
	}
	public void setOrigin(String origin) {
		Origin = origin;
	}
	public String getPriority() {
		return Priority;
	}
	public void setPriority(String priority) {
		Priority = priority;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getSubject() {
		return Subject;
	}
	public void setSubject(String subject) {
		Subject = subject;
	}
	public String getRecordtypeid() {
		return recordtypeid;
	}
	public void setRecordtypeid(String recordtypeid) {
		this.recordtypeid = recordtypeid;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getSales_Associate_Email_1__c() {
		return Sales_Associate_Email_1__c;
	}
	public void setSales_Associate_Email_1__c(String sales_Associate_Email_1__c) {
		Sales_Associate_Email_1__c = sales_Associate_Email_1__c;
	}
	public String getSales_Associate_Name_1__c() {
		return Sales_Associate_Name_1__c;
	}
	public void setSales_Associate_Name_1__c(String sales_Associate_Name_1__c) {
		Sales_Associate_Name_1__c = sales_Associate_Name_1__c;
	}
	public String getCustomer_First_Name__c() {
		return Customer_First_Name__c;
	}
	public void setCustomer_First_Name__c(String customer_First_Name__c) {
		Customer_First_Name__c = customer_First_Name__c;
	}
	public String getCustomer_Last_Name__c() {
		return Customer_Last_Name__c;
	}
	public void setCustomer_Last_Name__c(String customer_Last_Name__c) {
		Customer_Last_Name__c = customer_Last_Name__c;
	}
	public String getCustomer_Street__c() {
		return Customer_Street__c;
	}
	public void setCustomer_Street__c(String customer_Street__c) {
		Customer_Street__c = customer_Street__c;
	}
	public String getCustomer_Street_Line_2__c() {
		return Customer_Street_Line_2__c;
	}
	public void setCustomer_Street_Line_2__c(String customer_Street_Line_2__c) {
		Customer_Street_Line_2__c = customer_Street_Line_2__c;
	}
	public String getCustomer_City__c() {
		return Customer_City__c;
	}
	public void setCustomer_City__c(String customer_City__c) {
		Customer_City__c = customer_City__c;
	}
	public String getCustomer_State__c() {
		return Customer_State__c;
	}
	public void setCustomer_State__c(String customer_State__c) {
		Customer_State__c = customer_State__c;
	}
	public String getCustomer_Zip_Code__c() {
		return Customer_Zip_Code__c;
	}
	public void setCustomer_Zip_Code__c(String customer_Zip_Code__c) {
		Customer_Zip_Code__c = customer_Zip_Code__c;
	}
	public String getCustomer_Code_Ax__c() {
		return Customer_Code_Ax__c;
	}
	public void setCustomer_Code_Ax__c(String customer_Code_Ax__c) {
		Customer_Code_Ax__c = customer_Code_Ax__c;
	}
	public String getCustomer_Email_Ax__c() {
		return Customer_Email_Ax__c;
	}
	public void setCustomer_Email_Ax__c(String customer_Email_Ax__c) {
		Customer_Email_Ax__c = customer_Email_Ax__c;
	}
	public String getOriginal_Order_NumberX__c() {
		return Original_Order_NumberX__c;
	}
	public void setOriginal_Order_NumberX__c(String original_Order_NumberX__c) {
		Original_Order_NumberX__c = original_Order_NumberX__c;
	}
	public String getTotal_COGS__C() {
		return total_COGS__C;
	}
	public void setTotal_COGS__C(String total_COGS__C) {
		this.total_COGS__C = total_COGS__C;
	}
	public String getOrder_Written_Date__c() {
		return Order_Written_Date__c;
	}
	public void setOrder_Written_Date__c(String order_Written_Date__c) {
		Order_Written_Date__c = order_Written_Date__c;
	}
	public String getSales_Associate_Name_2__c() {
		return sales_Associate_Name_2__c;
	}
	public void setSales_Associate_Name_2__c(String sales_Associate_Name_2__c) {
		this.sales_Associate_Name_2__c = sales_Associate_Name_2__c;
	}
	public String getSales_Associate_Email_2__c() {
		return Sales_Associate_Email_2__c;
	}
	public void setSales_Associate_Email_2__c(String sales_Associate_Email_2__c) {
		Sales_Associate_Email_2__c = sales_Associate_Email_2__c;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getContactid() {
		return Contactid;
	}
	public void setContactid(String contactid) {
		Contactid = contactid;
	}
	public String getOrder_Number__c() {
		return Order_Number__c;
	}
	public void setOrder_Number__c(String order_Number__c) {
		Order_Number__c = order_Number__c;
	}
	public String getSource__c() {
		return Source__c;
	}
	public void setSource__c(String source__c) {
		Source__c = source__c;
	}
	public String getItem_Number__c() {
		return Item_Number__c;
	}
	public void setItem_Number__c(String item_Number__c) {
		Item_Number__c = item_Number__c;
	}
	public String getStore_Email__c() {
		return Store_Email__c;
	}
	public void setStore_Email__c(String store_Email__c) {
		Store_Email__c = store_Email__c;
	}
	public String getSO_STORE_CD() {
		return SO_STORE_CD;
	}
	public void setSO_STORE_CD(String sO_STORE_CD) {
		SO_STORE_CD = sO_STORE_CD;
	}
}
